<?php
if ( file_exists( dirname( __FILE__ ) . '/gd-config.php' ) ) {
	require_once( dirname( __FILE__ ) . '/gd-config.php' );
	define( 'FS_METHOD', 'direct' );
	define( 'FS_CHMOD_DIR', ( 0705 & ~ umask() ) );
	define( 'FS_CHMOD_FILE', ( 0604 & ~ umask() ) );
}
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', "html" );

/** MySQL database username */
define( 'DB_USER', "root" );

/** MySQL database password */
define( 'DB_PASSWORD', "admin" );

/** MySQL hostname */
define( 'DB_HOST', "localhost" );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

define('FS_METHOD','direct');

define( 'WP_MEMORY_LIMIT', '256M' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '3 xVf>OO!4IKg`/&?l)L{cvArHai~>$Kq*$$CKf;fC4$[R)Kw~0e!$ F`cmOT)V1' );
define( 'SECURE_AUTH_KEY',  'A[<^%*(J,x9,U:|e_fD6/nI:pWeJP{[f6b;KRlI[gAAeXu Ud|U1r`&U:+iH&TKM' );
define( 'LOGGED_IN_KEY',    'zLSKeI/:GFQrvt?O}LI5]A~N[5<O)B?GuOz9,11$bTqF+g3oSG;m $pupYT&IClK' );
define( 'NONCE_KEY',        'Dr)Bg[405g!Ed*a-i:.0$CQ^X0z];}EU+9S`9W(w&|Sh<Xcj}3h_q%9edArir+rh' );
define( 'AUTH_SALT',        'T2`5M)B?Z.e[E]3TV#V[Lcp3sw[Ee>&m0}8xDty[_YM= V=:[ybGOej=Sqxy-1vk' );
define( 'SECURE_AUTH_SALT', 'YhjNtj$`,]=fj..e9e,mU0gf/PA1T~RYmYfsNi|NbrY|Dim!w[+M[=@KIK?jk;WQ' );
define( 'LOGGED_IN_SALT',   'ok`(;e00b**uX<))W[WEM+LH:p!q)XXp65},wRGe!CK7M}c^V:h0UM`,;2xgB}]o' );
define( 'NONCE_SALT',       'HVez>U$V>fYos9/O5mRXpgATz7//,#-@[,E1zrQ%l+g/j,5Ve2CO+&pQDg;`Q|;g' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';
//define( 'FORCE_SSL_LOGIN', 1 );
//define( 'FORCE_SSL_ADMIN', 1 );

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );

