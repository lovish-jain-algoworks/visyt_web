<?php

class Gastro_Food_Menu
{

	public function __construct()
	{
		// Register "gst_menu" post type and related taxonomy
		add_action( 'init', array( $this, 'register_new_post_type' ) );

		// Add extra fields to gst_menu post type
		add_action( 'add_meta_boxes', array( $this, 'add_meta_box' ) );
		add_action( 'save_post', array( $this, 'meta_box_save' ) );

		// Add columns to gst_menu post admin page
		add_filter( 'manage_edit-gst_menu_columns', array( $this, 'columns_head' ) );
		add_filter( 'manage_edit-gst_menu_sortable_columns', array( $this, 'sortable_column' ) );
		add_action( 'manage_gst_menu_posts_custom_column', array( $this, 'columns_content' ), 10, 2 );

		// Enque Script for wp.media in gst_menu_tag page
		add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_scripts' ) );

		// Add form in gst_menu_tag page
		add_action( 'gst_menu_tag_add_form_fields', array( $this, 'add_tag_fields' ) );
		add_action( 'gst_menu_tag_edit_form_fields', array( $this, 'edit_tag_fields' ), 10 );
		add_action( 'created_term', array( $this, 'save_tag_fields' ), 10, 3 );
		add_action( 'edit_term', array( $this, 'save_tag_fields' ), 10, 3 );

		// Add columns to gst_menu_tag
		add_filter( 'manage_edit-gst_menu_tag_columns', array( $this, 'add_tag_columns' ) );
		add_filter( 'manage_gst_menu_tag_custom_column', array( $this, 'add_tag_column' ), 10, 3 );
	}


	public function register_new_post_type()
	{
		$gst_menu_slug = apply_filters( 'gst_menu_slug', 'gst_menu' );
		$gst_menu_label = apply_filters( 'gst_menu_label', esc_attr( 'Menu', 'gastro-core' ) );
		$gst_menu_category_slug = $gst_menu_slug . '_category';
		$gst_menu_tag_slug = $gst_menu_slug . '_tag';

		// Register Post type "gst_menu"
		register_post_type( 'gst_menu', array(
			'labels' => array(
				'name' => $gst_menu_label,
				'add_new_item' => sprintf( __( 'Add New %s', 'gastro-core' ), $gst_menu_label ),
				'edit_item' => sprintf( __( 'Edit %s', 'gastro-core' ), $gst_menu_label ),
				'new_item' => sprintf( __( 'New %s', 'gastro-core' ), $gst_menu_label ),
				'view_item' => sprintf( __( 'View %s', 'gastro-core' ), $gst_menu_label ),
				'all_items' => sprintf( __( 'All %ss', 'gastro-core' ), $gst_menu_label ),
				'archives' => sprintf( __( '%s Archives', 'gastro-core' ), $gst_menu_label )
			),
			'public' => true,
			'exclude_from_search' => true,
			'show_in_admin_bar' => false,
			'show_in_nav_gst_menus' => false,
			'publicly_queryable' => false,
			'query_var' => false,
			'has_archive' => false,
			'rewrite' => array(
				'slug' => $gst_menu_slug
			),
			'supports' => array(
				'title',
				'editor',
				'thumbnail',
				'custom-fields',
				'page-attributes',
				'publicize',
				'wpcom-markdown',
				'excerpt'
			),
			'menu_icon' => 'dashicons-clipboard'
		) );

		// Register Post Taxonomy "gst_menu_category"
		register_taxonomy( 'gst_menu_category', 'gst_menu', array(
			'labels' => array(
				'name' => sprintf( __( '%s Categories', 'gastro-core' ), $gst_menu_label ),
				'singular_name' => sprintf( __( '%s Categories', 'gastro-core' ), $gst_menu_label ),
			),
			'rewrite' => array(
				'slug' => $gst_menu_category_slug
			),
			'show_admin_column' => true,
			'hierarchical' => true,
			'publicly_queryable' => false
		) );

		// Register Post Taxonomy "gst_menu_tag"
		register_taxonomy( 'gst_menu_tag', 'gst_menu', array(
			'labels' => array(
				'name' => sprintf( __( '%s Tags', 'gastro-core' ), $gst_menu_label ),
				'singular_name' => sprintf( __( '%s Tags', 'gastro-core' ), $gst_menu_label ),
			),
			'rewrite' => array(
				'slug' => 'gst_menu'
			),
			'show_admin_column' => true,
			'hierarchical' => false,
			'publicly_queryable' => false
		) );
	}


	public function add_meta_box()
	{
		add_meta_box( 'gst_menu-extra-field', esc_html__( 'Menu Extra Field', 'gastro-core' ), array( $this, 'extra_meta_box' ), 'gst_menu', 'side', 'high' );
	}


	public function extra_meta_box( $post )
	{
		$values = get_post_custom( $post->ID );
		$featured = isset( $values['gst_menu_featured'] ) ? esc_attr( $values['gst_menu_featured'][0] ) : false;
		$price = isset( $values['gst_menu_price'] ) ? esc_attr( $values['gst_menu_price'][0] ) : '';
		$link = isset( $values['gst_menu_link'] ) ? esc_attr( $values['gst_menu_link'][0] ) : '';

		wp_nonce_field( 'gst_menu_meta_box_nonce', 'gst_menu_nonce' );
		?>
		<p>
			<label for="gst_menu_featured"><?php esc_html_e( 'Featured Menu', 'gastro-core' ); ?></label>
			<input type="checkbox" id="gst_menu_featured" name="gst_menu_featured" <?php checked( $featured ); ?> />
		</p>
		<p>
			<label for="gst_menu_price"><?php esc_html_e( 'Price', 'gastro-core' ); ?></label>
			<input type="text" size="5" name="gst_menu_price" id="gst_menu_price"  value="<?php echo esc_attr( $price ); ?>" />
		</p>
		<p><label for="gst_menu_link"><?php esc_html_e( 'Link With URL', 'gastro-core' ); ?></label></p>
		<p><input type="text" name="gst_menu_link" id="gst_menu_link"  value="<?php echo esc_attr( $link ); ?>" /></p>
		<p class="howto">Input URL to link from clicking on this menu.</p>
		<?php
	}


	public function meta_box_save( $post_id )
	{
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;
		if ( !isset( $_POST['gst_menu_nonce'] ) || !wp_verify_nonce( $_POST['gst_menu_nonce'], 'gst_menu_meta_box_nonce' ) ) return;
		if ( !current_user_can( 'edit_post' ) ) return;

		if ( isset( $_POST['gst_menu_price'] ) ) {
			update_post_meta( $post_id, 'gst_menu_price', esc_attr( $_POST['gst_menu_price'] ) );
		}

		if ( isset( $_POST['gst_menu_link'] ) ) {
			update_post_meta( $post_id, 'gst_menu_link', esc_attr( $_POST['gst_menu_link'] ) );
		}

		$chk = isset( $_POST['gst_menu_featured'] ) && $_POST['gst_menu_featured'] ? true : false;
		update_post_meta( $post_id, 'gst_menu_featured', $chk );
	}


	public function enqueue_scripts()
	{
		$screen       = get_current_screen();
		$screen_id    = $screen ? $screen->id : '';

		if ( in_array( $screen_id, array( 'edit-gst_menu_tag' ) ) ) {
			do_action( 'gastro_core_taxonomy_scripts', array(
				'jquery',
				'underscore',
				'backbone'
			) );
		}
	}


	public function columns_head( $columns )
	{
		$columns['featured'] = esc_html__( 'Featured Menu', 'gastro-core' );
		$columns['price'] = esc_html__( 'Menu Price', 'gastro-core' );

		return $columns;
	}


	public function columns_content( $column_name, $post_id ) {
		if ( $column_name == 'featured' ) {
			echo ( get_post_meta( $post_id, 'gst_menu_featured', true ) ) ? 'Yes' : 'No';
		} elseif ( $column_name == 'price' ) {
			echo get_post_meta( $post_id, 'gst_menu_price', true );
		}
	}


	public function sortable_column( $columns ) {
		$columns['featured'] = 'meta_value';
		$columns['price'] = 'meta_value';

		return $columns;
	}


	public function add_tag_fields()
	{
		?>
		<div class="form-field term-thumbnail-wrap">
			<label><?php esc_html_e( 'Thumbnail', 'gastro-core' ); ?></label>
			<div class="js-media-picker">
				<img style="margin-right:10px;" src="<?php echo esc_url( gastro_placeholder() ); ?>" width="60px" height="60px" />
				<input type="hidden" id="gst_menu_tag_thumbnail_id" name="gst_menu_tag_thumbnail_id" />
				<button type="button" class="upload-button button"><?php esc_html_e( 'Upload', 'gastro-core' ); ?></button>
				<button type="button" class="remove-button button"><?php esc_html_e( 'Remove', 'gastro-core' ); ?></button>
			</div>
			<div class="clear"></div>
			<p>Recommended media size "square 32x32px"</p>
		</div>
		<?php
	}


	public function edit_tag_fields( $term )
	{
		$thumbnail_id = absint( get_term_meta( $term->term_id, 'thumbnail_id', true ) );
		$image = $thumbnail_id ? wp_get_attachment_thumb_url( $thumbnail_id ) : gastro_placeholder();
		?>
		<tr class="form-field">
			<th scope="row" valign="top"><label><?php esc_html_e( 'Thumbnail', 'gastro-core' ); ?></label></th>
			<td>
				<div class="js-media-picker" style="line-height: 60px;">
					<img style="margin-right:10px;" src="<?php echo esc_url( $image ); ?>" width="60px" height="60px" />
					<input type="hidden" id="gst_menu_tag_thumbnail_id" name="gst_menu_tag_thumbnail_id" value="<?php echo esc_attr( $thumbnail_id ); ?>" />
					<button type="button" class="upload-button button"><?php esc_html_e( 'Upload', 'gastro-core' ); ?></button>
					<button type="button" class="remove-button button"><?php esc_html_e( 'Remove', 'gastro-core' ); ?></button>
				</div>
				<div class="clear"></div>
			</td>
		</tr>
		<?php
	}


	public function save_tag_fields( $term_id, $tt_id = '', $taxonomy = '' )
	{
		if ( isset( $_POST['gst_menu_tag_thumbnail_id'] ) && 'gst_menu_tag' === $taxonomy ) {
			update_term_meta( $term_id, 'thumbnail_id', absint( $_POST['gst_menu_tag_thumbnail_id'] ), '' );
		}
	}


	public function add_tag_columns( $columns )
	{
		$new_columns = array();

		if ( isset( $columns['cb'] ) ) {
			$new_columns['cb'] = $columns['cb'];
			unset( $columns['cb'] );
		}

		$new_columns['thumb'] = __( 'Image', 'gastro-core' );

		return array_merge( $new_columns, $columns );
	}


	public function add_tag_column( $columns, $column, $id )
	{
		if ( 'thumb' == $column ) {

			$thumbnail_id = get_term_meta( $id, 'thumbnail_id', true );

			if ( $thumbnail_id ) {
				$image = wp_get_attachment_thumb_url( $thumbnail_id );
			} else {
				$image = gastro_placeholder();
			}

			$image = str_replace( ' ', '%20', $image );
			$columns .= '<img src="' . esc_url( $image ) . '" alt="' . esc_attr__( 'Thumbnail', 'gastro-core' ) . '" class="wp-post-image" height="48" width="48" />';

		}

		return $columns;
	}
}
