<?php

class Gastro_Recipe
{

	public function __construct()
	{
		// Register "gst_recipe" post type and related taxonomy
		add_action( 'init', array( $this, 'register_new_post_type' ) );

		add_action( 'pre_get_posts', array( $this, 'recipe_per_page' ), 9 );

		// Add Taxonomy of this post type to blueprint
		add_filter( 'blueprint_options', array( $this, 'add_taxonomy_to_blueprint' ), 9 );

		// Register blueprint for this post type
		add_filter( 'support_blueprint_post_types', array( $this, 'include_recipe_post_type' ), 9 );

		// Add rating to comment
		add_filter( 'support_rating_post_types', array( $this, 'include_recipe_post_type' ) );

		// Add comment like/dislike
		add_filter( 'support_comment_like_dislike_post_types', array( $this, 'include_recipe_post_type' ) );
	}

	public function register_new_post_type()
	{
		$input_slug = get_theme_mod( 'recipe_slug' );

		if ( !empty( $input_slug ) ) {
			$recipe_label = apply_filters( 'gastro_recipe_label', ucfirst( $input_slug ) );
			$recipe_slug = str_replace( ' ', '', $input_slug );
			$recipe_slug = strtolower( $recipe_slug );
			$recipe_category_slug = $recipe_slug . '_category';
			$recipe_tag_slug = $recipe_slug . '_tag';
		} else {
			$recipe_label = apply_filters( 'gastro_recipe_label', 'Recipe' );
			$recipe_slug = 'recipe';
			$recipe_category_slug = 'recipe_category';
			$recipe_tag_slug = 'recipe_tag';
		}

		register_post_type( 'gst_recipe', array(
			'labels' => array(
				'name' => $recipe_label,
				'add_new_item' => sprintf( __( 'Add New %s', 'gastro-core' ), $recipe_label ),
				'edit_item' => sprintf( __( 'Edit %s', 'gastro-core' ), $recipe_label ),
				'new_item' => sprintf( __( 'New %s', 'gastro-core' ), $recipe_label ),
				'view_item' => sprintf( __( 'View %s', 'gastro-core' ), $recipe_label ),
				'all_items' => sprintf( __( 'All %ss', 'gastro-core' ), $recipe_label ),
				'archives' => sprintf( __( '% Archives', 'gastro-core' ), $recipe_label )
			),
			'public' => true,
			'has_archive' => true,
			'rewrite' => array(
				'slug' => $recipe_slug
			),
			'supports' => array(
				'title',
				'editor',
				'excerpt',
				'thumbnail',
				'comments',
				'page-attributes',
				'custom-fields'
			),
			'menu_icon' => 'dashicons-carrot'
		) );

		register_taxonomy( 'gst_recipe_category', 'gst_recipe', array(
			'labels' => array(
				'name' => sprintf( __( '%s Categories', 'gastro-core' ), $recipe_label ),
				'singular_name' => sprintf( __( '%s Categories', 'gastro-core' ), $recipe_label ),
			),
			'rewrite' => array(
				'slug' => $recipe_category_slug
			),
			'show_admin_column' => true,
			'hierarchical' => true
		) );

		register_taxonomy( 'gst_recipe_tag', 'gst_recipe', array(
			'labels' => array(
				'name' => sprintf( __( '%s Tags', 'gastro-core' ), $recipe_label ),
				'singular_name' => sprintf( __( '%s Tags', 'gastro-core' ), $recipe_label ),
			),
			'rewrite' => array(
				'slug' => $recipe_tag_slug
			),
			'show_admin_column' => true,
			'hierarchical' => false
		) );
	}

	public function recipe_per_page( $query )
	{
		if ( !is_admin() && $query->is_main_query() && is_post_type_archive( 'gst_recipe' ) ) {
			$query->set( 'posts_per_page', get_theme_mod( 'recipe_items' ) );
		}
	}

	public function add_taxonomy_to_blueprint( $opts )
	{
		$opts['recipe_categories'] = get_terms( 'gst_recipe_category', array( 'hide_empty' => false) );
		return $opts;
	}

	public function include_recipe_post_type( $post_types )
	{
		$post_types[] = 'gst_recipe';

		return $post_types;
	}
}
