<?php

class Gastro_Reservation {

	public function __construct()
	{
		define( 'RESERVATION_PLUGIN_CLASSES', untrailingslashit( plugin_dir_path( __FILE__ ) ) . '/reservation/' );
		define( 'RESERVATION_BOOKING_POST_TYPE', 'gst_booking' );
		define( 'RESERVATION_BRANCH_POST_TYPE', 'gst_branch' );
		define( 'RESERVATION_BOOKING_PAGE_SLUG', 'gst-booking' );

		$this->request = new stdClass();
		$this->request->request_processed = false;
		$this->request->request_inserted = false;

		// Add menu
		add_action( 'admin_menu', array( $this, 'add_booking_menu' ) );

		// Flush the rewrite rules
		register_activation_hook( __FILE__, array( $this, 'booking_rewrite_flush' ) );

		// Add Sections and Settings
		add_action( 'init', array( $this, 'load_settings_panel' ) );

		// Add booking manager role
		add_action( 'init', array( $this, 'add_booking_manager_roles' ) );

		// Add booking post type and booking post type status
		add_action( 'init', array( $this, 'add_booking_post_type' ) );
		add_action( 'init', array( $this, 'add_booking_post_status' ) );

		// Add branch
		if ( gastro_booking_setting( 'enable-branches' ) ) {
			require_once( RESERVATION_PLUGIN_CLASSES . 'booking-branches.php' );
			new Gastro_Booking_Branch();
		}

		// Register and enqueue script
		add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_admin_assets' ) );

		// Add modals to admin booking page
		add_action( 'admin_footer-toplevel_page_' . RESERVATION_BOOKING_PAGE_SLUG, array( $this, 'add_booking_admin_modals' ) );

		// Add booking ajax
		add_action( 'wp_ajax_nopriv_admin-booking-submit-modal' , array( $this , 'booking_nopriv_ajax' ) );
		add_action( 'wp_ajax_admin-booking-submit-modal', array( $this, 'booking_submit_ajax' ) );

		// Add trash ajax
		add_action( 'wp_ajax_nopriv_admin-trash-booking' , array( $this , 'booking_nopriv_ajax' ) );
		add_action( 'wp_ajax_admin-trash-booking', array( $this, 'booking_trash_ajax' ) );

		// Add email ajax
		add_action( 'wp_ajax_nopriv_admin-booking-email-modal' , array( $this , 'booking_nopriv_ajax' ) );
		add_action( 'wp_ajax_admin-booking-email-modal', array( $this, 'booking_email_ajax' ) );

		// Sanitize setting
		add_filter( 'sanitize_option_booking-settings', array( $this, 'sanitize_booking_setting' ), 100 );

		// Add capabilities to Booking Manager
		add_filter( 'option_page_capability_booking-settings', array( $this, 'manage_booking_setting_page' ) );
	}

	public function add_booking_menu()
	{
		add_menu_page(
			__( 'Bookings', 'gastro-core' ),
			__( 'Bookings', 'gastro-core' ),
			'manage_booking',
			RESERVATION_BOOKING_PAGE_SLUG,
			array( $this, 'show_admin_bookings_page' ),
			'dashicons-calendar-alt',
			'30'
		);
	}


	public function booking_rewrite_flush()
	{
		$this->add_booking_post_type();
		do_action( 'booking_extra_rewrite_flush' );

		flush_rewrite_rules();
	}


	// Load setting panel
	public function load_settings_panel()
	{
		require_once( CORE_PLUGIN_DIR . '/lib/classes/gastro-menu-pages.php' );
		$admin_page = new Gastro_Core_Menu_Pages;
		$branches_args = apply_filters( 'booking_branches_args', array() );

		$admin_page->add_submenu(
			array(
				'id' => 'booking-settings',
				'title' => __( 'Settings', 'gastro-core' ),
				'parent_menu' => RESERVATION_BOOKING_PAGE_SLUG,
				'manage_options' => 'manage_booking',
				'description' => '',
				'default_tab' => 'booking-general',
			)
		);

		$admin_page->add_section(
			array(
				'id' => 'booking-general',
				'page' => 'booking-settings',
				'title' => __( 'General', 'gastro-core' ),
				'is_tab' => true,
			)
		);

		$admin_page->add_setting(
			array(
				'id' => 'enable-branches',
				'page' => 'booking-settings',
				'section' => 'booking-general',
				'type' => 'checkbox',
				'title' => __( 'Enable Branches', 'gastro-core' ),
				'label' => __( 'Check this option to enable multiple branches booking', 'gastro-core' )
			)
		);

		$admin_page->add_setting(
			array(
				'id' => 'party-size-min',
				'page' => 'booking-settings',
				'section' => 'booking-general',
				'type' => 'select',
				'title' => __( 'Min Party Size', 'gastro-core' ),
				'blank_option' => false,
				'options' => gastro_booking_party_size_setting( false ),
			)
		);

		$admin_page->add_setting(
			array(
				'id' => 'party-size',
				'page' => 'booking-settings',
				'section' => 'booking-general',
				'type' => 'select',
				'title' => __( 'Max Party Size', 'gastro-core' ),
				'blank_option' => false,
				'options' => gastro_booking_party_size_setting(),
			)
		);

		$admin_page->add_setting(
			array(
				'id' => 'date-format',
				'page' => 'booking-settings',
				'section' => 'booking-general',
				'type' => 'text',
				'title' => __( 'Date Format', 'gastro-core' ),
				'description' => sprintf(
					__( 'Define how the date is formatted on the booking form. %sFormatting rules%s.', 'gastro-core' ),
					'<a target="_blank" href="http://amsul.ca/pickadate.js/date/#formats">',
					'</a>'
				),
				'placeholder' => gastro_booking_default_setting( 'date-format' )
			)
		);

		$admin_page->add_setting(
			array(
				'id' => 'time-format',
				'page' => 'booking-settings',
				'section' => 'booking-general',
				'type' => 'select',
				'title' => __( 'Time Format', 'gastro-core' ),
				'blank_option' => false,
				'options' => apply_filters( 'booking_time_formats_options', array(
					'h:i A' => __( '12h', 'gastro-core' ),
					'HH:i' => __( '24h', 'gastro-core' )
				) )
			)
		);

		$admin_page->add_setting(
			array(
				'id' => 'early-booking',
				'page' => 'booking-settings',
				'section' => 'booking-general',
				'type' => 'select',
				'title' => __( 'Early Bookings', 'gastro-core' ),
				'blank_option' => false,
				'options' => apply_filters( 'gastro_early_booking_options', array(
					'365' => __( 'Any time', 'gastro-core' ),
					'1' => __( '1 day', 'gastro-core' ),
					'3' => __( '3 days', 'gastro-core' ),
					'5' => __( '5 days', 'gastro-core' ),
					'7' => __( '1 week', 'gastro-core' ),
					'10' => __( '10 days', 'gastro-core' ),
					'14' => __( '2 weeks', 'gastro-core' ),
					'21' => __( '3 weeks', 'gastro-core' ),
					'30' => __( '30 days', 'gastro-core' ),
					'60' => __( '60 days', 'gastro-core' ),
					'90' => __( '90 days', 'gastro-core' ),
				) )
			)
		);

		$admin_page->add_setting(
			array(
				'id' => 'late-booking',
				'page' => 'booking-settings',
				'section' => 'booking-general',
				'type' => 'select',
				'title' => __( 'Late Bookings', 'gastro-core' ),
				'description' => __( 'Select how late customers can create booking.', 'gastro-core' ),
				'blank_option' => false,
				'options' => array(
					'' => __( 'No limit', 'gastro-core' ),
					'30' => __( '30 minutes in advance', 'gastro-core' ),
					'60' => __( '1 hour in advance', 'gastro-core' ),
					'120' => __( '2 hours in advance', 'gastro-core' ),
					'240' => __( '4 hours in advance', 'gastro-core' ),
					'360' => __( '6 hours in advance', 'gastro-core' ),
					'720' => __( '12 hours in advance', 'gastro-core' ),
					'10080' => __( '1 week in advance', 'gastro-core' ),
					'sameday' => __( 'Block same-day bookings', 'gastro-core' ),
				)
			)
		);

		$admin_page->add_setting(
			array(
				'id' => 'time-interval',
				'page' => 'booking-settings',
				'section' => 'booking-general',
				'type' => 'select',
				'title' => __( 'Time Interval', 'gastro-core' ),
				'blank_option' => false,
				'options' => array(
					'60' => __( '60 minutes', 'gastro-core' ),
					'30' => __( '30 minutes', 'gastro-core' ),
				),
				'default' => gastro_booking_default_setting( 'time-interval' )
			)
		);

		$admin_page->add_setting(
			array(
				'id' => 'mailchimp-subscription',
				'page' => 'booking-settings',
				'section' => 'booking-general',
				'type' => 'checkbox',
				'title' => __( 'Mailchimp Subscription', 'gastro-core' ),
				'label' => __( 'Enable subscription option.', 'gastro-core' ),
				'description' => sprintf(
					__( 'When user check "subscription" option and make the booking, it will automatically subscribe to your mailchimp list. %sThis will work only when %smailchimp for wordpress%s plugin is activated.', 'gastro-core' ),
					'<br>',
					'<a target="_blank" href="https://wordpress.org/plugins/mailchimp-for-wp/">',
					'</a>'
				)
			)
		);

		$admin_page->add_setting(
			array(
				'id' => 'mailchimp-list-id',
				'page' => 'booking-settings',
				'section' => 'booking-general',
				'type' => 'text',
				'title' => __( 'Mailchimp List ID', 'gastro-core' ),
				'description' => sprintf(
					__( 'Input Mailchimp list ID to add more subscriber when user choose to subscribe while making booking. Find the ID %shere%s.', 'gastro-core' ),
					'<a target="_blank" href="' . esc_url( admin_url( '/admin.php?page=mailchimp-for-wp' ) ) . '">',
					'</a>'
				),
				'class' => 'regular-text'
			)
		);

		$admin_page->add_section(
			array(
				'id' => 'availability',
				'page' => 'booking-settings',
				'title' => __( 'Availability', 'gastro-core' ),
				'is_tab' => true,
			)
		);

		$admin_page->add_setting(
			array_merge( array(
				'id' => 'scheduleopen',
				'page' => 'booking-settings',
				'section' => 'availability',
				'type' => 'timetable',
				'title' => __( 'Business Hours', 'gastro-core' ),
				'description' => sprintf( __( 'Add schedule during which time you accept booking. %s**This require at least one rule.%s', 'gastro-core' ), '<strong style="color:red;">', '</strong>' ),
				'schedule_type' => 'weekday',
				'extra_class' => 'opening-hour',
				'all_day' => __( 'open all day', 'gastro-core' )
			), $branches_args )
		);

		$admin_page->add_setting(
			array_merge( array(
				'id' => 'scheduleclose',
				'page' => 'booking-settings',
				'section' => 'availability',
				'type' => 'timetable',
				'title' => __( 'Off Hours', 'gastro-core' ),
				'description' => __( "Define special opening hours for holidays or leave time empty for closing all day.", 'gastro-core' ),
				'schedule_type' => 'date',
				'extra_class' => 'off-hour',
				'all_day' => __( 'close all day', 'gastro-core' )
			), $branches_args )
		);

		$admin_page->add_section(
			array(
				'id' => 'notifications',
				'page' => 'booking-settings',
				'title' => __( 'Notifications', 'gastro-core' ),
				'is_tab' => true,
			)
		);

		$admin_page->add_setting(
			array(
				'id' => 'booking-quicklink-notification',
				'page' => 'booking-settings',
				'section' => 'notifications',
				'type' => 'checkbox',
				'title' => __( 'Shortcut Button Notification', 'gastro-core' ),
				'label' => __( 'Send notification email to user when click "Confirm" or "Close" from shortcut button.', 'gastro-core' )
			)
		);

		$admin_page->add_setting(
			array(
				'id' => 'admin-email-notification',
				'page' => 'booking-settings',
				'section' => 'notifications',
				'type' => 'checkbox',
				'title' => __( 'Admin Notification', 'gastro-core' ),
				'label' => __( 'Send notification email to admin when a new booking is created.', 'gastro-core' )
			)
		);

		$admin_page->add_setting(
			array(
				'id' => 'admin-email-address',
				'page' => 'booking-settings',
				'section' => 'notifications',
				'type' => 'text',
				'title' => __( 'Admin Email Address', 'gastro-core' ),
				'description' => __( 'The email address where admin notifications should be sent.', 'gastro-core' ),
				'placeholder' => gastro_booking_default_setting( 'admin-email-address' ),
				'class' => 'regular-text'
			)
		);

		$admin_page->add_setting(
			array(
				'id' => 'reply-to-name',
				'page' => 'booking-settings',
				'section' => 'notifications',
				'type' => 'text',
				'title' => __( 'Reply-To Name', 'gastro-core' ),
				'description' => __( 'The name which appears in the Reply-To field of a user notification email', 'gastro-core' ),
				'placeholder' => gastro_booking_default_setting( 'reply-to-name' ),
				'class' => 'regular-text'
			)
		);

		$admin_page->add_setting(
			array(
				'id' => 'reply-to-email',
				'page' => 'booking-settings',
				'section' => 'notifications',
				'type' => 'text',
				'title' => __( 'Reply-To Email Address', 'gastro-core' ),
				'description' => __( 'The email which appears in the Reply-To field of a user notification email.', 'gastro-core' ),
				'placeholder' => gastro_booking_default_setting( 'reply-to-email' ),
				'class' => 'regular-text'
			)
		);

		$admin_page->add_setting(
			array(
				'id' => 'template-tags-description',
				'page' => 'booking-settings',
				'section' => 'notifications',
				'type' => 'html',
				'title' => __( 'Template Tags', 'gastro-core' ),
				'description' => __( 'Use following tags add to email subject or content.', 'gastro-core' ),
				'html' => gastro_booking_email_template_tag_description()
			)
		);

		$admin_page->add_setting(
			array(
				'id' => 'subject-pending-admin',
				'page' => 'booking-settings',
				'section' => 'notifications',
				'type' => 'text',
				'title' => __( 'Admin Notification Subject', 'gastro-core' ),
				'description' => __( 'The email subject for admin notifications.', 'gastro-core' ),
				'placeholder' => gastro_booking_default_setting( 'subject-pending-admin' ),
				'class' => 'regular-text'
			)
		);

		$admin_page->add_setting(
			array(
				'id' => 'template-pending-admin',
				'page' => 'booking-settings',
				'section' => 'notifications',
				'type' => 'editor',
				'title' => __( 'Admin Notification Email', 'gastro-core' ),
				'description' => __( 'Content of email that admin will receive when booking is created.', 'gastro-core' ),
				'default' => gastro_booking_default_setting( 'template-pending-admin' )
			)
		);

		$admin_page->add_setting(
			array(
				'id' => 'subject-pending-user',
				'page' => 'booking-settings',
				'section' => 'notifications',
				'type' => 'text',
				'title' => __( 'New Request Email Subject', 'gastro-core' ),
				'description' => __( 'Email\'s subject which user will receive when they create booking.', 'gastro-core' ),
				'placeholder' => gastro_booking_default_setting( 'subject-pending-user' ),
				'class' => 'regular-text'
			)
		);

		$admin_page->add_setting(
			array(
				'id' => 'template-pending-user',
				'page' => 'booking-settings',
				'section' => 'notifications',
				'type' => 'editor',
				'title' => __( 'New Request Email', 'gastro-core' ),
				'description' => __( 'Content of email that user will receive when they create booking.', 'gastro-core' ),
				'default' => gastro_booking_default_setting( 'template-pending-user' )
			)
		);

		$admin_page->add_setting(
			array(
				'id' => 'subject-confirm-user',
				'page' => 'booking-settings',
				'section' => 'notifications',
				'type' => 'text',
				'title' => __( 'Confirmed Email Subject', 'gastro-core' ),
				'description' => __( 'Email\'s subject which user will receive when their booking has been confirmed.', 'gastro-core' ),
				'placeholder' => gastro_booking_default_setting( 'subject-confirm-user' ),
				'class' => 'regular-text'
			)
		);

		$admin_page->add_setting(
			array(
				'id' => 'template-confirm-user',
				'page' => 'booking-settings',
				'section' => 'notifications',
				'type' => 'editor',
				'title' => __( 'Confirmed Email', 'gastro-core' ),
				'description' => __( 'Content of email that user will receive after their booking has been confirmed.', 'gastro-core' ),
				'default' => gastro_booking_default_setting( 'template-confirm-user' )
			)
		);

		$admin_page->add_setting(
			array(
				'id' => 'subject-close-user',
				'page' => 'booking-settings',
				'section' => 'notifications',
				'type' => 'text',
				'title' => __( 'Closed Email Subject', 'gastro-core' ),
				'description' => __( 'Email\'s subject which user will receive when their booking has been closed.', 'gastro-core' ),
				'placeholder' => gastro_booking_default_setting( 'subject-close-user' ),
				'class' => 'regular-text'
			)
		);

		$admin_page->add_setting(
			array(
				'id' => 'template-close-user',
				'page' => 'booking-settings',
				'section' => 'notifications',
				'type' => 'editor',
				'title' => __( 'Closed Email', 'gastro-core' ),
				'description' => __( 'Content of email that user will receive when their booking has been closed.', 'gastro-core' ),
				'default' => gastro_booking_default_setting( 'template-close-user' )
			)
		);

		$admin_page->add_setting(
			array(
				'id' => 'subject-update-user',
				'page' => 'booking-settings',
				'section' => 'notifications',
				'type' => 'text',
				'title' => __( 'Update Email Subject', 'gastro-core' ),
				'description' => __( 'Default update email\'s subject which will send from booking admin page.', 'gastro-core' ),
				'placeholder' => gastro_booking_default_setting( 'subject-update-user' ),
				'class' => 'regular-text'
			)
		);

		$admin_page->add_admin_pages();
	}


	// Sort off hours schedule and remove old schedule
	public function sanitize_booking_setting( $val )
	{
		if ( empty( $val['scheduleclose'] ) ) {
			return $val;
		}

		// Sort by date
		$schedule_close = $val['scheduleclose'];
		usort( $schedule_close, array( $this, 'sort_by_date' ) );

		$oldest_time = time() - 86400;

		for ( $i = 0; $i < count( $schedule_close ); $i++ ) {
			if ( strtotime( $schedule_close[$i]['date'] ) > $oldest_time ) {
				break;
			}
		}

		if ( $i > 0 ) {
			$schedule_close = array_slice( $schedule_close, $i );
		}

		$val['scheduleclose'] = $schedule_close;

		return $val;
	}


	// call back function for sanitize_booking_setting
	public function sort_by_date( $x, $y )
	{
		$x_date = empty( $x['date'] ) ? 0 : strtotime( $x['date'] );
		$y_date = empty( $y['date'] ) ? 0 : strtotime( $y['date'] );

		return $x_date - $y_date;
	}


	// Booking post type page
	public function show_admin_bookings_page()
	{
		require_once( RESERVATION_PLUGIN_CLASSES . 'booking-table.php' );
		$this->bookings_table = new Gastro_Booking_Table();
		$search_key = isset( $_POST['s'] ) ? $_POST['s'] : null;
		$this->bookings_table->prepare_items( $search_key );
		?>
		<div class="wrap">
			<h1>
				<?php esc_html_e( 'Bookings', 'gastro-core' ); ?>
				<a href="#" class="add-new-h2 page-title-action add-booking"><?php esc_html_e( 'Add New', 'gastro-core' ); ?></a>
			</h1>
			<?php do_action( 'booking_table_bulk_notice' ); ?>
			<form class="booking-table" method="POST" action="">
				<input type="hidden" name="post_type" value="<?php echo RESERVATION_BOOKING_POST_TYPE; ?>" />
				<input type="hidden" name="page" value="<?php echo esc_attr( $_REQUEST['page'] ) ?>">
				<?php $this->bookings_table->date_range_filter_view(); ?>
				<div class="booking-filters wp-clearfix">
					<?php $this->bookings_table->views(); ?>
					<?php $this->bookings_table->search_box( esc_attr__( 'Search Email', 'gastro-core' ), 'booking' ); ?>
				</div>
				<?php $this->bookings_table->display(); ?>
			</form>
		</div>
		<?php
	}


	public function add_booking_post_type()
	{
		register_post_type( RESERVATION_BOOKING_POST_TYPE, apply_filters( 'booking_post_type_args', array(
			'labels' => array(
				'name' => __( 'Booking', 'gastro-core' ),
				'add_new_item' => __( 'Add New Booking', 'gastro-core' ),
				'edit_item' => __( 'Edit Booking', 'gastro-core' ),
				'new_item' => __( 'New Booking', 'gastro-core' ),
				'view_item' => __( 'View Booking', 'gastro-core' ),
				'search_items' => __( 'Search Bookings', 'gastro-core' ),
				'all_items' => __( 'All Bookings', 'gastro-core' ),
			),
			'menu_icon' => 'dashicons-calendar-alt',
			'public' => false,
			'supports' => array( 'title' )
		) ) );
	}


	public function add_booking_post_status()
	{
		global $booking_controller;
		$booking_controller->booking_statuses = apply_filters( 'booking_post_status_args', array(
			'pending' => array(
				'label'						=> __( 'Pending', 'gastro-core' ),
				'default'					=> true,
				'user_selectable'			=> true
			),
			'confirm' => array (
				'label'                     => __( 'Confirmed', 'gastro-core' ),
				'default'					=> false,
				'user_selectable'			=> true
			),
			'close' => array(
				'label'                     => _x( 'Closed', 'Booking status for a closed booking', 'gastro-core' ),
				'default'					=> false,
				'user_selectable'			=> true,
			)
		) );

		// Register the custom post statuses
		foreach ( $booking_controller->booking_statuses as $status => $args ) {
			if ( !$args['default'] ) {
				register_post_status( $status, $args );
			}
		}
	}


	public function add_booking_admin_modals()
	{
		?>
		<div id="booking-submit-modal" class="booking-admin-modal">
			<div class="booking-modal-container">
				<div class="booking-admin-modal-title">
					<?php esc_html_e( 'New Booking', 'gastro-core' ); ?>
				</div>
				<form id="booking-submit-form" method="POST">
					<input type="hidden" name="action" value="admin_booking_request">
					<input type="hidden" name="ID" value="">
					<input class="js-picker-object" type="hidden" value="<?php echo esc_attr( json_encode( gastro_booking_get_picker_object() ) ); ?>" />
					<div class="booking-form-fields">
						<?php echo gastro_booking_admin_print_form(); ?>
					</div>
					<button type="submit" class="button button-primary">
						<?php esc_html_e( 'Save', 'gastro-core' ); ?>
					</button>
					<a href="#" class="modal-cancel-button button">
						<?php esc_html_e( 'Cancel', 'gastro-core' ); ?>
					</a>
					<div class="action-status">
						<span class="spinner loading"></span>
						<span class="dashicons dashicons-no-alt error"></span>
						<span class="dashicons dashicons-yes success"></span>
					</div>
				</form>
			</div>
		</div>
		<div id="booking-email-modal" class="booking-admin-modal">
			<div class="booking-modal-container">
				<div class="booking-admin-modal-title">
					<?php esc_html_e( 'Send Email', 'gastro-core' ); ?>
				</div>
				<form method="POST">
					<input type="hidden" name="action" value="admin_send_email">
					<input type="hidden" name="ID" value="">
					<input type="hidden" name="name" value="">
					<input type="hidden" name="email" value="">
					<fieldset>
						<div class="to">
							<label for="booking-email-to" class="booking-email-to-label"><?php _ex( 'To : ', 'Label next to the email address to which an email will be sent', 'gastro-core' ); ?></label>
							<span class="booking-email-to"></span>
						</div>
						<div class="subject">
							<label for="booking-email-subject"><?php esc_html_e( 'Subject', 'gastro-core' ); ?></label>
							<input type="text" name="booking-email-subject" id="booking-email-subject" placeholder="<?php echo gastro_booking_setting( 'subject-update-user' ); ?>">
						</div>
						<div class="message">
							<label for="booking-email-message"><?php esc_html_e( 'Message', 'gastro-core' ); ?></label>
							<textarea name="booking-email-message" id="booking-email-message"></textarea>
						</div>
					</fieldset>
					<button type="submit" class="button button-primary">
						<?php esc_html_e( 'Send Email', 'gastro-core' ); ?>
					</button>
					<a href="#" class="modal-cancel-button button">
						<?php esc_html_e( 'Cancel', 'gastro-core' ); ?>
					</a>
					<div class="action-status">
						<span class="spinner loading"></span>
						<span class="dashicons dashicons-no-alt error"></span>
						<span class="dashicons dashicons-yes success"></span>
					</div>
				</form>
			</div>
		</div>
		<div id="booking-message-modal" class="booking-admin-modal">
			<div class="booking-modal-container">
				<div class="booking-admin-modal-title">
					<?php esc_html_e( 'Message Detail', 'gastro-core' ); ?>
				</div>
				<div class="booking-message-content"></div>
				<a href="#" class="modal-cancel-button button">
					<?php esc_html_e( 'Close', 'gastro-core' ); ?>
				</a>
			</div>
		</div>
		<div id="booking-error-modal" class="booking-admin-modal">
			<div class="booking-modal-container">
				<div class="booking-error-msg"></div>
				<a href="#" class="modal-cancel-button button">
					<?php esc_html_e( 'Close', 'gastro-core' ); ?>
				</a>
			</div>
		</div>
		<?php
	}


	public function add_booking_manager_roles()
	{
		global $wp_roles;
		$booking_manager = add_role(
			'booking_manager',
			__( 'Booking Manager', 'gastro-core' ),
			array(
				'read'				=> true,
				'manage_booking'	=> true,
			)
		);

		$manage_booking_roles = apply_filters( 'default_manage_booking_roles', array(
			'administrator',
			'editor'
		) );

		foreach ( $manage_booking_roles as $role ) {
			$wp_roles->add_cap( $role, 'manage_booking' );
		}
	}

	public function manage_booking_setting_page()
	{
		return 'manage_booking';
	}

	// Enqueue the admin-only CSS and Javascript
	public function enqueue_admin_assets()
	{
		global $admin_page_hooks;

		$screen = get_current_screen();
		if ( empty( $screen ) || empty( $admin_page_hooks[RESERVATION_BOOKING_PAGE_SLUG] ) ) {
			return;
		}

		if ( $screen->base === 'toplevel_page_' . RESERVATION_BOOKING_PAGE_SLUG ) {
			$options = apply_filters( 'booking_admin_options', array(
				'nonce'		=> wp_create_nonce( 'booking-admin' ),
				'strings'=> array(
					'add'		=> __( 'Add Booking', 'gastro-core' ),
					'edit'		=> __( 'Edit Booking', 'gastro-core' ),
					'error'		=> __( 'Booking Error', 'gastro-core' )
				)
			) );
			do_action( 'gastro_core_booking_admin_scripts', array(
				'jquery',
				'underscore',
				'backbone'
			), $options );
		}
	}


	// Handle Loggedout user from actions.
	public function booking_nopriv_ajax()
	{
		wp_send_json_error(
			array(
				'error' => 'loggedout',
				'msg' => __( 'You have been logged out.', 'gastro-core' ),
			)
		);
	}


	public function booking_trash_ajax()
	{
		$result = wp_trash_post( $_POST['booking'] );
		if ( !$result ) {
			wp_send_json_error(
				array(
					'error' => 'trash_failed',
					'msg' => __( 'Unable to trash this post. Please try again. If you continue to have trouble, please refresh the page.', 'gastro-core' ),
				)
			);
		} else {
			wp_send_json_success(
				array( 'booking'	=> $id )
			);
		}
	}


	public function booking_submit_ajax()
	{
		global $booking_controller;

		// Retrieve a booking with a GET request
		if ( !empty( $_GET['booking'] ) && !empty( $_GET['booking']['ID'] ) ) {
			$id = (int)$_GET['booking']['ID'];

			require_once( RESERVATION_PLUGIN_CLASSES . 'booking-data.php' );
			$booking_controller->request = new Gastro_Booking_Data();
			$result = $booking_controller->request->load_booking_data( $id );

			if ( $result ) {
				// Don't allow edit trashed bookings.
				if ( $booking_controller->request->post_status == 'trash' ) {
					wp_send_json_error(
						array(
							'error'		=> 'booking_trashed',
							'msg'		=> sprintf( __( 'Booking has been sent to %sTrash%s.', 'gastro-core' ), '<a href="' . admin_url( 'admin.php?page=' . RESERVATION_BOOKING_PAGE_SLUG . '&status=trash' ) . '">', '</a>' ),
						)
					);
				}

				if ( empty( $booking_controller->request->request_date ) || empty( $booking_controller->request->request_time ) ) {
					$date = new DateTime( $booking_controller->request->date );
					$booking_controller->request->request_date = $date->format( 'Y/m/d' );
					$booking_controller->request->request_time = $date->format( 'h:i A' );
				}

				wp_send_json_success(
					array(
						'booking'	=> $booking_controller->request,
						'fields'	=> gastro_booking_admin_print_form(),
					)
				);
			} else {
				wp_send_json_error(
					array(
						'error'		=> 'booking_not_found',
						'msg'		=> __( 'Booking could not be retrieved. Please reload page and try again.', 'gastro-core' ),
					)
				);
			}
		// Insert or update a booking with a POST request
		} elseif ( !empty( $_POST['booking'] ) ) {
			foreach( $_POST['booking'] as $field ) {
				if ( substr( $field['name'], -2 ) === '[]' ) {
					$name = substr( $field['name'], 0, -2 );
					if ( !isset( $_POST[$name] ) ) {
						$_POST[$name] = array();
					}
					$_POST[$name][] = $field['value'];
				} else {
					$_POST[$field['name']] = $field['value'];
				}
			}

			require_once( RESERVATION_PLUGIN_CLASSES . 'booking-data.php' );
			$booking_controller->request = new Gastro_Booking_Data();

			// Add ID if update the post
			if ( !empty( $_POST['ID'] ) ) {
				$booking_controller->request->ID = (int) $_POST['ID'];
			}

			$result = $booking_controller->request->insert_booking( true );

			if ( $result ) {
				wp_send_json_success(
					array(
						'booking'	=> $booking_controller->request,
					)
				);
			} else {
				wp_send_json_error(
					array(
						'error'		=> 'invalid_booking_data',
						'booking'	=> $booking_controller->request,
						'fields'	=> gastro_booking_admin_print_form(),
					)
				);
			}
		} else {
			wp_send_json_error();
		}
	}


	public function booking_email_ajax()
	{
		// Set up $_POST object for validation
		foreach( $_POST['email'] as $field ) {
			$_POST[$field['name']] = $field['value'];
		}

		$id = (int)$_POST['ID'];
		$name = sanitize_text_field( $_POST['name'] );
		$email = sanitize_text_field( $_POST['email'] );
		$subject = stripcslashes( sanitize_text_field( $_POST['booking-email-subject'] ) );
		$message = stripcslashes( wp_kses_post( $_POST['booking-email-message'] ) );

		if ( empty( $message ) ) {
			wp_send_json_error(
				array(
					'error'		=> 'email_missing_message',
					'msg'		=> __( 'You cannot leave the message blank.', 'gastro-core' ),
				)
			);
		} else if ( empty( $id ) || empty( $name ) || empty( $email ) ) {
			wp_send_json_error(
				array(
					'error'		=> 'email_missing_data',
					'msg'		=> __( 'Some information are missing.', 'gastro-core' ),
				)
			);
		} else {
			$mail_subject = !empty( $subject ) ? $subject : gastro_booking_setting( 'subject-update-user' );
			$mail_header = gastro_booking_set_email_header();
			wp_mail( $email, $mail_subject, $message, $mail_header );
			wp_send_json_success();
		}
	}
}
