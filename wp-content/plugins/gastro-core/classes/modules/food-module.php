<?php

class Gastro_Food_Module extends Gastro_Base_Module
{
	const API_GASTRO_RECIPE_CATEGORIES = 'food/recipe-categories';
	const API_GASTRO_FOOD_MENU_CATEGORIES = 'food/menu-categories';

	public function get_name()
	{
		return 'food';
	}

	public function start()
	{

	}

	public function handle_api_action( $endpoint, $params )
	{
		switch ( $endpoint ) {
			case self::API_GASTRO_RECIPE_CATEGORIES:
				return $this->get_recipe_categories();
			case self::API_GASTRO_FOOD_MENU_CATEGORIES:
				return $this->get_menu_categories();
			default:
				return false;
		}
	}

	public function get_recipe_categories()
	{
		return get_terms( 'gst_recipe_category', array(
			'hide_empty' => false
		) );
	}

	public function get_menu_categories()
	{
		return get_terms( 'gst_menu_category', array(
			'hide_empty' => false
		) );
	}

}
