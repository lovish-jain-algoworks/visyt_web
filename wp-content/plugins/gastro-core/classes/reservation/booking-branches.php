<?php

class Gastro_Booking_Branch
{
	public function __construct()
	{
		// Add branch post type
		add_action( 'init', array( $this, 'add_branch_post_type' ) );
		add_action( 'booking_extra_rewrite_flush', array( $this, 'add_branch_post_type' ) );

		// Add branch setting to timetable
		add_filter( 'booking_branches_args', array( $this, 'get_branch_setting_args' ) );

		// Add branch field to booking form
		add_filter( 'booking_form_branch_field', array( $this, 'get_booking_branch_field' ), 9, 2 );

		// Add branch column to booking table
		add_filter( 'booking_table_columns', array( $this, 'add_branch_column' ), 9, 3 );

		// Add sortable column to booking table
		add_filter( 'booking_sortable_columns', array( $this, 'add_branch_sortable_column' ) );

		// Filter schedul open/close value which will pass through localize script
		add_filter( 'booking_localize_schedule_open', array( $this, 'filter_setting' ) );
		add_filter( 'booking_localize_schedule_close', array( $this, 'filter_setting' ) );

		// Use branch schedule open/close in booking validation
		add_filter( 'booking_validate_close_rules', array( $this, 'get_branch_rules' ), 9, 2  );
		add_filter( 'booking_validate_opening_rules', array( $this, 'get_branch_rules' ), 9, 2  );
	}


	public function add_branch_post_type()
	{
		$branch_slug = RESERVATION_BRANCH_POST_TYPE;
		$branch_label = esc_attr( 'Branch', 'gastro-core' );

		register_post_type( RESERVATION_BRANCH_POST_TYPE, array(
			'labels' => array(
				'name' => $branch_label,
				'add_new_item' => sprintf( __( 'Add New %s', 'gastro-core' ), $branch_label ),
				'edit_item' => sprintf( __( 'Edit %s', 'gastro-core' ), $branch_label ),
				'new_item' => sprintf( __( 'New %s', 'gastro-core' ), $branch_label ),
				'view_item' => sprintf( __( 'View %s', 'gastro-core' ), $branch_label ),
				'all_items' => sprintf( __( 'All %ss', 'gastro-core' ), $branch_label ),
				'archives' => sprintf( __( '%s Archives', 'gastro-core' ), $branch_label )
			),
			'public' => true,
			'exclude_from_search' => true,
			'show_in_admin_bar' => false,
			'show_in_nav_gst_menus' => false,
			'publicly_queryable' => false,
			'query_var' => false,
			'has_archive' => false,
			'rewrite' => array(
				'slug' => $branch_slug
			),
			'supports' => array(
				'title',
				'editor',
				'thumbnail',
				'custom-fields',
				'page-attributes',
				'publicize',
				'wpcom-markdown',
				'excerpt'
			),
			'menu_icon' => 'dashicons-store'
		) );
	}


	public function get_branch_setting_args()
	{
		return array(
			'multiple_selection' => true,
			'selection_key' => 'branches',
			'selection_title' => __( 'branches', 'gastro-core' ),
			'selection_options' => $this->get_branch_options()
		);
	}


	public function get_booking_branch_field( $args, $request )
	{
		$branch_options = $this->get_branch_options();
		if ( !empty( $branch_options ) ) {
			return array(
				'fields' => array(
					'branch' => array(
						'title' => __( 'Branch', 'gastro-core' ),
						'request_input' => empty( $request->branch ) ? '' : $request->branch,
						'callback' => 'gastro_booking_form_select_field',
						'callback_args' => array(
							'options' => $branch_options,
							'control_class' => 'js-picker-branch'
						)
					)
				)
			);
		}

		return $args;
	}


	public function get_branch_options()
	{
		$options = array();
		$query = new WP_Query( array(
			'post_type' => RESERVATION_BRANCH_POST_TYPE,
			'posts_per_page' => -1,
			'orderby' => 'menu_order'
		) );

		if ( $query->have_posts() ) {
			while( $query->have_posts() ) {
				$query->the_post();
				$options[get_the_ID()] = get_the_title();
			}

			wp_reset_postdata();
		}

		return $options;
	}


	public function add_branch_column( $all, $first_half, $second_half )
	{
		$branch_column = array( 'branch' => __( 'Branch', 'gastro-core' ) );

		return $first_half + $branch_column + $second_half;
	}


	public function add_branch_sortable_column( $column )
	{
		$column['branch'] = array( 'branch', true );

		return $column;
	}


	public function add_branch_required_field( $fields )
	{
		$fields[] = 'branch';

		return $fields;
	}


	public function filter_setting( $value )
	{
		$result = array();

		if ( empty( $value ) ) {
			return $result;
		}

		foreach ( $value as $index => $rule ) {
			if ( isset( $rule['branches'] ) && !empty( $rule['branches'] ) ) {
				foreach ( $rule['branches'] as $branch ) {
					unset( $value[$index]['branches'] );
					if ( isset( $result[$branch] ) && !empty( $result[$branch] ) ) {
						$result[$branch][] = $value[$index];
					} else {
						$result[$branch] = array( $value[$index] );
					}
				}
			}
		}

		return $result;
	}


	public function get_branch_rules( $value, $branch )
	{
		$filtered_rules = $this->filter_setting( $value );

		if ( !empty( $filtered_rules ) ) {
			if ( !empty( $branch ) ) {
				return $filtered_rules[$branch];
			} else {
				return $filtered_rules[0];
			}
		} else {
			return array();
		}
	}
}
