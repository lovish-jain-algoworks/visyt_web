<?php

class Gastro_Booking_Data
{
	public $request_processed = false;
	public $request_inserted = false;

	// Load booking information
	public function load_booking_data( $post )
	{
		$post = ( is_int( $post ) || is_string( $post ) ) ? get_post( $post ) : $post;
		if ( isset( $post->post_type ) && $post->post_type === RESERVATION_BOOKING_POST_TYPE ) {
			$this->post = $post;
			$this->ID = $post->ID;
			$this->name = $post->post_title;
			$this->date = $post->post_date;
			$this->message = $post->post_content;
			$this->post_status = $post->post_status;

			$defaults_meta_data = array(
				'branch' => '',
				'party' => '',
				'email' => '',
				'phone' => '',
				'submission_date' => '',
				'created_by' => ''
			);
			$meta_data = get_post_meta( $this->ID, 'booking_meta_data', true );
			$meta_data = is_array( $meta_data ) ? array_merge( $defaults_meta_data, $meta_data ) : $defaults_meta_data;
			$this->branch = $meta_data['branch'];
			$this->party = $meta_data['party'];
			$this->email = $meta_data['email'];
			$this->phone = $meta_data['phone'];
			$this->submission_date = $meta_data['submission_date'];
			$this->created_by = $meta_data['created_by'];

			return true;
		} else {
			return false;
		}
	}


	// Insert/Update a new booking submission into the database
	public function insert_booking( $from_admin = false )
	{
		if ( $this->request_processed === true ) {
			return true;
		}

		$this->request_processed = true;
		$action = empty( $this->ID ) ? 'insert' : 'update';

		// Validate values
		$this->validate_booking( $from_admin );

		if ( count( $this->validation_errors ) || !$this->insert_post_data() ) {
			return false;
		} else {
			$this->request_inserted = true;
		}

		// If insert submission pass then make subscription to mailchimp list when user insert booking
		// admin won't have permission to add email for subscription in booking admin page.
		if ( 'insert' === $action && $this->subscription ) {
			$mailchimp_subscription = gastro_booking_setting( 'mailchimp-subscription' );
			$mailchimp_list_id = gastro_booking_setting( 'mailchimp-list-id' );

			if ( !empty( $mailchimp_subscription ) && 1 == $mailchimp_subscription && !empty( $mailchimp_list_id ) ) {
				if ( function_exists( 'mc4wp_get_api' ) ) {
					$mailchimp_api = mc4wp_get_api();
					$mailchimp_api->subscribe( $mailchimp_list_id, $this->email );
				}
			}
		}

		// Send Email to user
		if ( $this->send_notification ) {
			$user_email = $this->email;
			$user_email_subject = gastro_booking_set_email_subject( $this, $this->post_status );
			$user_email_content = gastro_booking_set_email_content( $this, $this->post_status );
			$user_email_header = gastro_booking_set_email_header();
			wp_mail( $user_email, $user_email_subject, $user_email_content, $user_email_header);
		}

		// Send Notification to Admin when there is new booking
		if ( gastro_booking_setting( 'admin-email-notification' ) && !$from_admin && 'insert' === $action ) {
			$admin_email = gastro_booking_setting( 'admin-email-address' );
			$admin_email_subject = gastro_booking_set_email_subject( $this, 'pending', 'admin' );
			$admin_email_content = gastro_booking_set_email_content( $this, 'pending', 'admin' );
			$admin_email_header = gastro_booking_set_email_header( $this->name, $this->email );
			wp_mail( $admin_email, $admin_email_subject, $admin_email_content, $admin_email_header );
		}

		return true;
	}


	// Insert or update post data
	public function insert_post_data()
	{
		$post_args = array(
			'post_type' => RESERVATION_BOOKING_POST_TYPE,
			'post_title' => $this->name,
			'post_content' => $this->message,
			'post_date' => $this->date,
			'post_status' => $this->post_status,
		);

		$current_user = wp_get_current_user();
		$meta_args = array(
			'branch' => $this->branch,
			'party' => $this->party,
			'email' => $this->email,
			'phone' => $this->phone,
			'submission_date' => isset( $this->submission_date ) ? $this->submission_date : current_time( 'timestamp' ),
			'created_by' => $current_user->user_login
		);

		// If has ID then update post data
		if ( !empty( $this->ID ) ) {
			$post_args['ID'] = $this->ID;
		}

		$id = wp_insert_post( $post_args );

		if ( is_wp_error( $id ) || !$id ) {
			$this->insert_post_error = $id;
			return false;
		} else {
			$this->ID = $id;
			return update_post_meta( $this->ID, 'booking_meta_data', $meta_args );
		}
	}


	// Validate submission data.
	public function validate_booking( $from_admin = false )
	{
		global $booking_controller;
		$this->validation_errors = array();

		// Branch
		$this->branch = empty( $_POST['gst-booking-branch'] ) ? '' : sanitize_text_field( stripslashes_deep( $_POST['gst-booking-branch'] ) );

		$date = empty( $_POST['gst-booking-date'] ) ? false : stripslashes_deep( $_POST['gst-booking-date'] );
		if ( !$date ) {
			$this->validation_errors[] = array(
				'field' => 'date',
				'error_msg' => 'Booking date is missing',
				'message' => __( 'Please select the date.', 'gastro-core' ),
			);
		} else {
			try {
				$date = new DateTime( stripslashes_deep( $_POST['gst-booking-date'] ) );
			} catch ( Exception $e ) {
				$this->validation_errors[] = array(
					'field' => 'date',
					'error_msg' => $e->getMessage(),
					'message' => __( 'Invalid date input', 'gastro-core' ),
				);
			}
		}

		$time = empty( $_POST['gst-booking-time'] ) ? false : stripslashes_deep( $_POST['gst-booking-time'] );
		if ( !$time ) {
			$this->validation_errors[] = array(
				'field' => 'time',
				'error_msg' => 'Booking time is missing',
				'message' => __( 'Please select the time', 'gastro-core' ),
			);
		} else {
			try {
				$time = new DateTime( stripslashes_deep( $_POST['gst-booking-time'] ) );
			} catch ( Exception $e ) {
				$this->validation_errors[] = array(
					'field' => 'time',
					'error_msg' => $e->getMessage(),
					'message' => __( "Invalid time input", 'gastro-core' ),
				);
			}
		}

		// Check rules (exceptional rules & opening rules)
		if ( is_object( $time ) && is_object( $date ) ) {
			$request = new DateTime( $date->format( 'Y-m-d' ) . ' ' . $time->format( 'H:i:s' ) );
			$schedule_close = gastro_booking_setting( 'scheduleclose' );
			$exceptions = apply_filters( 'booking_validate_close_rules', $schedule_close, $this->branch );
			$is_exception = false;

			if ( empty( $this->validation_errors ) && !empty( $exceptions ) ) {
				$requested_time_available = false;

				foreach ( $exceptions as $exception ) {
					$excp_date = new DateTime( $exception['date'] );
					if ( $excp_date->format( 'Y-m-d' ) == $request->format( 'Y-m-d' ) ) {
						$is_exception = true;

						if ( empty( $exception['time'] ) ) {
							continue; // if close all day then next
						}

						$exception_start = empty( $exception['time']['start'] ) ? $request : new DateTime( $exception['date'] . ' ' . $exception['time']['start'] );
						$exception_end = empty( $exception['time']['end'] ) ? $request : new DateTime( $exception['date'] . ' ' . $exception['time']['end'] );

						if ( $request->format( 'U' ) >= $exception_start->format( 'U' ) && $request->format( 'U' ) <= $exception_end->format( 'U' ) ) {
							$requested_time_available = true;
							break; // if time available then break the loop
						}
					}
				} // end foreach

				if ( $is_exception && !$requested_time_available ) {
					$this->validation_errors[] = array(
						'field' => 'date',
						'error_msg' => 'Booking is in invalid date or time',
						'message' => __( 'Sorry, we cannot accept your booking in the requested time', 'gastro-core' ),
					);
				}
			} // end check exceptional time

			// Check opening hour rules
			$schedule_open = gastro_booking_setting( 'scheduleopen' );
			$opening_rules = apply_filters( 'booking_validate_opening_rules', $schedule_open, $this->branch );
			if ( !$is_exception && empty( $this->validation_errors ) && !empty( $opening_rules ) ) {
				$request_weekday = strtolower( $request->format( 'l' ) );
				$requested_time_available = false;
				$requested_day_available = false;

				foreach ( $opening_rules as $rule ) {
					if ( !empty( $rule['weekdays'][ $request_weekday ] ) ) {
						$requested_day_available = true;

						if ( empty( $rule['time'] ) ) {
							$requested_time_available = true;
							break; // Open all day case then break the loop
						}

						// Check if requested time is too early
						$request_too_early = true;
						if ( !empty( $rule['time']['start'] ) ) {
							$rule_start_time = new DateTime( $request->format( 'Y-m-d' ) . ' ' . $rule['time']['start'] );
							if ( $rule_start_time->format( 'U' ) <= $request->format( 'U' ) ) {
								$request_too_early = false;
							}
						}

						// Check if requested time is too late
						$request_too_late = true;
						if ( !empty( $rule['time']['end'] ) ) {
							$rule_end_time = new DateTime( $request->format( 'Y-m-d' ) . ' ' . $rule['time']['end'] );
							if ( $rule_end_time->format( 'U' ) >= $request->format( 'U' ) ) {
								$request_too_late = false;
							}
						}

						// Time is valid
						if ( !$request_too_early && !$request_too_late ) {
							$requested_time_available = true;
							break;
						}
					}
				}

				if ( !$requested_day_available ) {
					$this->validation_errors[] = array(
						'field' => 'date',
						'error_msg' => 'Booking is in invalid date',
						'message' => __( 'Sorry, booking is unavailable in the requested date', 'gastro-core' ),
					);
				} elseif ( !$requested_time_available ) {
					$this->validation_errors[] = array(
						'field' => 'time',
						'error_msg' => 'Booking is in invalid time',
						'message' => __( 'Sorry, booking is unavailable in the requested time', 'gastro-core' ),
					);
				}
			} // end check opening hours

			// Accept the date if it has passed validation
			if ( empty( $this->validation_errors ) ) {
				$this->date = $request->format( 'Y-m-d H:i:s' );
			}
		} // end checking rules

		$this->request_date = empty( $_POST['gst-booking-date'] ) ? '' : stripslashes_deep( $_POST['gst-booking-date'] );
		$this->request_time = empty( $_POST['gst-booking-time'] ) ? '' : stripslashes_deep( $_POST['gst-booking-time'] );

		// Name
		$this->name = empty( $_POST['gst-booking-name'] ) ? '' : sanitize_text_field( stripslashes_deep( $_POST['gst-booking-name'] ) );
		if ( empty( $this->name ) ) {
			$this->validation_errors[] = array(
				'field' => 'name',
				'post_variable' => $this->name,
				'message' => __( 'Please tell us your name', 'gastro-core' ),
			);
		}

		// Party Size
		$this->party = empty( $_POST['gst-booking-party'] ) ? '' : $_POST['gst-booking-party'];
		if ( empty( $this->party ) ) {
			$this->validation_errors[] = array(
				'field' => 'party',
				'post_variable' => $this->party,
				'message' => __( 'Please tell us how many people', 'gastro-core' ),
			);
		} else {
			$party_size = gastro_booking_setting( 'party-size' );
			if ( !empty( $party_size ) && $party_size < $this->party ) {
				$this->validation_errors[] = array(
					'field' => 'party',
					'post_variable' => $this->party,
					'message' => sprintf( __( 'We accept booking only parties up to %d people', 'gastro-core' ), $party_size ),
				);
			}
			$party_size_min = gastro_booking_setting( 'party-size-min' );
			if ( !empty( $party_size_min ) && $party_size_min > $this->party ) {
				$this->validation_errors[] = array(
					'field' => 'party',
					'post_variable' => $this->party,
					'message' => sprintf( __( 'We accept booking only parties more than %d people', 'gastro-core' ), $party_size_min ),
				);
			}
		}

		// Email
		$this->email = empty( $_POST['gst-booking-email'] ) ? '' : sanitize_text_field( stripslashes_deep( $_POST['gst-booking-email'] ) );
		if ( empty( $this->email ) ) {
			$this->validation_errors[] = array(
				'field' => 'email',
				'post_variable' => $this->email,
				'message' => __( 'Please input email address', 'gastro-core' ),
			);
		} else {
			if ( !strpos( $this->email, '@' ) !== false ) {
				$this->validation_errors[] = array(
					'field' => 'email',
					'post_variable' => $this->email,
					'message' => __( 'Please input email address in a correct format', 'gastro-core' ),
				);
			}
		}

		// Phone/Message
		$this->phone = empty( $_POST['gst-booking-phone'] ) ? '' : sanitize_text_field( stripslashes_deep( $_POST['gst-booking-phone'] ) );
		$this->message = empty( $_POST['gst-booking-message'] ) ? '' : nl2br( wp_kses_post( stripslashes_deep( $_POST['gst-booking-message'] ) ) );

		// Post Status
		$this->post_status = ( !empty( $_POST['gst-booking-status'] ) ) ? $_POST['gst-booking-status'] : 'pending';

		// Subscription
		$this->subscription = ( !empty( $_POST['gst-booking-subscription'] ) && '1' === $_POST['gst-booking-subscription'] ) ? true : false;

		// Send Notification
		if ( $from_admin ) {
			$this->send_notification = ( !empty( $_POST['gst-booking-notification'] ) && '1' === $_POST['gst-booking-notification'] ) ? true : false;
		} else { // always send notification if request from user
			$this->send_notification = true;
		}

		// Check if required fields are empty (for more required fields)
		$default_required_fields = array(
			'date',
			'time',
			'party',
			'name',
			'email'
		);
		$required_fields = apply_filters( 'booking_required_fields', $default_required_fields );

		foreach ( $required_fields as $field ) {
			if ( !gastro_booking_field_error( $field, $this->validation_errors ) && gastro_booking_field_empty( $field ) ) {
				$this->validation_errors[] = array(
					'field' => $field,
					'post_variable' => '',
					'message' => __( 'Please complete the field', 'gastro-core' ),
				);
			}
		}
	}
}
