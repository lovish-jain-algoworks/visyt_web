<?php

class Gastro_Booking_Table extends WP_List_Table
{
	public $post_per_page = 20; // Number of results to show per page in admin booking table
	public $booking_counts;
	public $bulk_action_processed = false; // If processed bulk action, shortcut link won't take effect
	public $bookings; // Array of booking data
	public $filter_start_date = null; // Current date filters
	public $filter_end_date = null; // Current date filters
	public $query_string; // Current query string
	public $action_result = array(); // Results of a bulk or shortcut
	public $last_action = null; // Type of last bulk or shortcut action

	public function __construct()
	{
		global $status, $page;

		// Set parent defaults
		parent::__construct( array(
			'singular' => __( 'Booking', 'gastro-core' ),
			'plural' => __( 'Bookings', 'gastro-core' )
		) );

		require_once( RESERVATION_PLUGIN_CLASSES . 'booking-data.php' );
		$this->advanced_date_filter();
		$this->remove_query_string();
		$this->process_bulk_action();
		$this->process_shortcut();
		$this->count_booking();
		$this->query_booking();
	}


	// Sortable columns
	public function get_sortable_columns()
	{
		$booking_sortable_columns = array(
			'date' => array( 'date', true ),
			'name' => array( 'title', true ),
			'party' => array( 'party', true ),
			'status' => array( 'status', true )
		);

		return apply_filters( 'booking_sortable_columns', $booking_sortable_columns );
	}


	// Set bulk actions
	public function get_bulk_actions()
	{
		return apply_filters( 'booking_bulk_actions', array(
			'delete' => __( 'Delete', 'gastro-core' ),
			'confirm' => __( 'Send To Confirmed', 'gastro-core' ),
			'pending' => __( 'Send To Pending', 'gastro-core' ),
			'close' => __( 'Send To Closed', 'gastro-core' )
		) );
	}


	// Get columns
	public function get_columns()
	{
		$first_half = array(
			'cb' => '<input type="checkbox" />',
			'date' => __( 'Date, Time', 'gastro-core' )
		);

		$second_half = array(
			'name' => __( 'Name', 'gastro-core' ),
			'email' => __( 'Email', 'gastro-core' ),
			'phone' => __( 'Phone', 'gastro-core' ),
			'status' => __( 'Status', 'gastro-core' ),
			'party' => __( 'Party', 'gastro-core' ),
			'details' => __( 'Details', 'gastro-core' )
		);

		return apply_filters( 'booking_table_columns', $first_half + $second_half, $first_half, $second_half );
	}


	// Checkbox column
	public function column_cb( $booking )
	{
		return sprintf(
			'<input type="checkbox" name="%1$s[]" value="%2$s" />',
			'bookings',
			$booking->ID
		);
	}


	// Get status filter view
	public function get_views()
	{
		$current_status = isset( $_GET['status'] ) ? $_GET['status'] : 'all';
		$query_string = remove_query_arg( array( 'shortcut', 'booking' ), $this->query_string );

		$view_args = array(
			'all' => array(
				'label' => esc_html__( 'All', 'gastro-core' ),
				'query_string' => remove_query_arg( array( 'status', 'paged' ), $query_string )
			),
			'pending' => array(
				'label' => esc_html__( 'Pending', 'gastro-core' ),
				'query_string' => add_query_arg( array( 'status' => 'pending', 'paged' => FALSE ), $query_string )
			),
			'confirm' => array(
				'label' => esc_html__( 'Confirmed', 'gastro-core' ),
				'query_string' => add_query_arg( array( 'status' => 'confirm', 'paged' => FALSE ), $query_string )
			),
			'close' => array(
				'label' => esc_html__( 'Closed', 'gastro-core' ),
				'query_string' => add_query_arg( array( 'status' => 'close', 'paged' => FALSE ), $query_string )
			),
			'trash' => array(
				'label' => esc_html__( 'Trash', 'gastro-core' ),
				'query_string' => add_query_arg( array( 'status' => 'trash', 'paged' => FALSE ), $query_string )
			),
		);

		$output = array();
		foreach ( $view_args as $key => $val ) {
			$output[$key] = sprintf(
				'<a href="%s"%s>%s</a>',
				esc_url( $val['query_string'] ),
				$current_status === $key ? ' class="current"' : '',
				$val['label'] . ' <span class="count">(' . $this->booking_counts[$key] . ')</span>'
			);
		}

		return $output;
	}


	// Booking table single row content
	public function single_row( $item )
	{
		static $row_alternate_class = '';
		$row_class = apply_filters( 'bookings_table_row_class', '', $item );

		if ( !empty( $row_alternate_class ) ) {
			$row_class .= 'alternate';
		}

		echo '<tr class="' . esc_attr( $row_class ) . '">';
		$this->single_row_columns( $item );
		echo '</tr>';
	}


	// Render columns of booking table
	public function column_default( $booking, $column_name )
	{
		switch ( $column_name ) {
			case 'date' :
				$output = gastro_booking_date_format( $booking->date );
				$output .= '<div class="status"><span class="spinner"></span> ' . __( 'Loading', 'gastro-core' ) . '</div>';

				if ( $booking->post_status !== 'trash' ) {
					$output .= '<div class="row-actions">';
					$output .= '<span class="edit">';
					$output .= 	'<a href="#" data-id="' . esc_attr( $booking->ID ) . '" data-action="edit">' . __( 'Edit', 'gastro-core' ) . '</a>';
					$output .= ' | </span>';
					$output .= '<span class="trash">';
					$output .= 	'<a href="#" data-id="' . esc_attr( $booking->ID ) . '" data-action="trash">' . __( 'Trash', 'gastro-core' ) . '</a>';
					$output .= '</div>';
				}
				break;
			case 'branch' :
				$output = '';
				if ( !empty( $booking->branch ) ) {
					$output .= get_the_title( $booking->branch );
				}
				break;
			case 'name' :
				$output = esc_html( $booking->name );
				break;
			case 'email' :
				global $booking_controller;
				$output = '';

				if ( !empty( $booking->email ) ) {
					$output .= esc_html( $booking->email );
					if ( 'trash' !== $booking->post_status ) {
						$output .= '<div class="status"><span class="spinner"></span> ' . __( 'Loading', 'gastro-core' ) . '</div>';
						$output .= '<div class="row-actions">';
						$output .= 	'<span class="edit">';
						$output .= 		'<a href="#" data-id="' . esc_attr( $booking->ID ) . '" data-action="email" data-email="' . esc_attr( $booking->email ) . '" data-name="' . $booking->name . '">' . esc_html__( 'Send Email', 'gastro-core' ) . '</a>';
						$output .= 	'</span>';
						$output .= '</div>';
					}
				}

				break;
			case 'phone' :
				$output = '';
				if ( !empty( $booking->phone ) ) {
					$output .= '<a href="tel:' . $booking->phone . '">' . esc_html( $booking->phone ) . '</a>';
				}
				break;
			case 'status' :
				global $booking_controller;
				$status = $booking_controller->booking_statuses[$booking->post_status]['label'];
				$output = '<span class="booking-status ' . esc_attr( $booking->post_status ) . '">' . esc_html( $status ) . '</span>';
				if ( 'pending' === $booking->post_status || 'confirm' === $booking->post_status ) {
					$output .= '<div class="row-actions">';
					if ( 'pending' === $booking->post_status ) {
						$output .= '<span class="confirm">';
						$output .= 		'<a href="' . admin_url( 'admin.php?page=' . RESERVATION_BOOKING_PAGE_SLUG . '&shortcut=confirm&booking=' . esc_attr( $booking->ID ) ) . '">';
						$output .= 			__( 'Confirm', 'gastro-core' );
						$output .= 		'</a>';
						$output .= ' | </span>';
						$output .= '<span class="trash">';
						$output .= 		'<a href="' . admin_url( 'admin.php?page=' . RESERVATION_BOOKING_PAGE_SLUG . '&shortcut=close&booking=' . esc_attr( $booking->ID ) ) . '">';
						$output .= 			__( 'Close', 'gastro-core' );
						$output .= 		'</a>';
						$output .= '</span>';
					}
					$output .= '</div>';
				}
				break;
			case 'party' :
				$output = esc_html( $booking->party );
				break;
			case 'details' :
				$submission_date = isset( $booking->submission_date ) && !empty( $booking->submission_date ) ? gastro_booking_timestamp_format( $booking->submission_date ) : __( 'Unknown Date', 'gastro-core' );
				$output = sprintf( esc_html__( 'Request on %s', 'gastro-core' ), $submission_date );

				if ( isset( $booking->created_by ) && !empty( $booking->created_by ) ) {
					$output .= sprintf( esc_html__( ' by %s', 'gastro-core' ), $booking->created_by );
				}

				if ( !empty( $booking->message ) ) {
					$output .= '<div class="booking-column-background"></div>';
					$output .= '<a href="#" class="booking-show-message" data-message="' . esc_attr( $booking->message ) . '">' . esc_html__( 'Read Message', 'gastro-core' ) . '</a>';
				}
				break;
			default:
				$output = isset( $booking->$column_name ) ? $booking->$column_name : '';
				break;
		}

		return $output;
	}


	// Set the advanced date filter
	public function advanced_date_filter()
	{
		if ( !empty( $_GET['action'] ) && 'clear_filter' === $_GET['action'] ) {
			$this->filter_start_date = null;
			$this->filter_end_date = null;
		} else {
			if ( !empty( $_POST['start_date'] ) ) {
				$this->filter_start_date = $_POST['start_date'];
			} else {
				$this->filter_start_date = !empty( $_GET['start_date'] ) ? $_GET['start_date'] : null;
			}

			if ( !empty( $_POST['end_date'] ) ) {
				$this->filter_end_date = $_POST['end_date'];
			} else {
				$this->filter_end_date = !empty( $_GET['end_date'] ) ? $_GET['end_date'] : null;
			}
		}
	}


	// Remove unwanted query string
	public function remove_query_string()
	{
		$this->query_string = remove_query_arg( array( 'action', 'start_date', 'end_date', 'shortcut', 'booking' ) );

		if ( !empty( $this->filter_start_date ) ) {
			$this->query_string = add_query_arg( array( 'start_date' => $this->filter_start_date ), $this->query_string );
		}

		if ( $this->filter_end_date !== null ) {
			$this->query_string = add_query_arg( array( 'end_date' => $this->filter_end_date ), $this->query_string );
		}
	}


	// Date range filter
	public function date_range_filter_view()
	{
		if ( !empty( $this->filter_start_date ) || !empty( $this->filter_end_date ) ) {
			$date_range = 'custom';
		} else {
			$date_range = ( !empty( $_GET['date_range'] ) ) ? $_GET['date_range'] : '';
		}

		$query_string = remove_query_arg(
			array( 'action', 'date_range', 'start_date', 'end_date', 'booking', 'shortcut' ),
			$this->query_string
		);

		$filters = array(
			'all' => sprintf(
				'<a href="%s"%s>%s</a>',
				esc_url( add_query_arg( array( 'date_range' => 'all', 'paged' => FALSE ), $query_string ) ),
				( 'all' === $date_range ) ? ' class="current"' : '', __( 'All', 'gastro-core' )
			),
			'upcoming' => sprintf(
				'<a href="%s"%s>%s</a>',
				esc_url( add_query_arg( array( 'paged' => FALSE ), remove_query_arg( array( 'date_range' ), $query_string ) ) ),
				empty( $date_range ) ? ' class="current"' : '', __( 'Upcoming', 'gastro-core' )
			),
			'today' => sprintf(
				'<a href="%s"%s>%s</a>',
				esc_url( add_query_arg( array( 'date_range' => 'today', 'paged' => FALSE ), $query_string ) ),
				( 'today' === $date_range ) ? ' class="current"' : '', __( 'Today', 'gastro-core' )
			)
		);

		if ( 'custom' === $date_range ) {
			$filters['date'] = '<a id="advanced-date-filter" class="current" href="#">';
			$filters['date'] .= '<span class="dashicons dashicons-calendar-alt"></span>';
			$filters['date'] .= $this->filter_start_date . '&nbsp;&gt;&nbsp;' . $this->filter_end_date;
			$filters['date'] .= '</a>';
		} else {
			$filters['date'] = '<a id="advanced-date-filter" href="#">' . esc_html__( 'Select Range', 'gastro-core' ) . '</a>';
		}

		?>
		<div class="booking-advanced-filter wp-clearfix">
			<ul class="subsubsub">
				<li class="booking-advanced-filter-label"><?php esc_html_e( 'Date : ', 'gastro-core' ); ?></li>
				<li><?php echo join( ' | </li><li>', $filters ); ?></li>
			</ul>

			<div class="date-filter">
				<input type="text" id="start-date" name="start_date" class="datepicker" value="<?php echo esc_attr( $this->filter_start_date ); ?>" placeholder="<?php esc_attr_e( 'Start Date', 'gastro-core' ); ?>" />
				<input type="text" id="end-date" name="end_date" class="datepicker" value="<?php echo esc_attr( $this->filter_end_date ); ?>" placeholder="<?php esc_attr_e( 'End Date', 'gastro-core' ); ?>" />
				<input type="submit" class="button" value="<?php esc_attr_e( 'Apply', 'gastro-core' ); ?>"/>
				<?php if ( 'custom' === $date_range ) : ?>
					<a href="<?php echo esc_url( add_query_arg( array( 'action' => 'clear_filter' ) ) ); ?>" class="button button-secondary">
						<?php esc_html_e( 'Clear Filter', 'gastro-core' ); ?>
					</a>
				<?php endif; ?>
			</div>
		</div>
		<?php
	}


	// Process the bulk actions
	public function process_bulk_action()
	{
		$bookings = isset( $_POST['bookings'] ) ? $_POST['bookings'] : false;
		$action = isset( $_POST['action'] ) ? $_POST['action'] : false;
		$action = ( '-1' === $action && isset( $_POST['action2'] ) ) ? $_POST['action2'] : $action;

		if ( !empty( $action ) && '-1' !== $action ) {
			global $booking_controller;
			$result = array();
			$bookings = is_array( $bookings ) ? $bookings : array( $bookings );
			$this->bulk_action_processed = true;

			foreach ( $bookings as $id ) {
				if ( 'delete' === $action ) {
					$result[$id] = gastro_booking_delete_post( $id );
				} elseif ( 'confirm' === $action ) {
					$result[$id] = gastro_booking_update_status( $id, 'confirm' );
				} elseif ( 'pending' === $action ) {
					$result[$id] = gastro_booking_update_status( $id, 'pending' );
				} elseif ( 'close' === $action ) {
					$result[$id] = gastro_booking_update_status( $id, 'close' );
				}
			}

			if ( !empty( $result ) ) {
				$this->action_result = $result;
				$this->last_action = $action;
				add_action( 'booking_table_bulk_notice', array( $this, 'bulk_action_notice' ) );
			}
		} else {
			$this->bulk_action_processed = false;
		}
	}


	// Bulk action notice message box
	public function bulk_action_notice()
	{
		$success = 0;
		$fail = 0;
		foreach( $this->action_result as $id => $result ) {
			if ( $result === true || $result === null ) {
				$success++;
			} else {
				$fail++;
			}
		}

		if ( $success > 0 ) { ?>
			<div class="updated">
				<?php if ( 'delete' === $this->last_action ) : ?>
					<p><?php echo sprintf( _n( '%d booking was deleted.', '%d bookings were deleted.', $success, 'gastro-core' ), $success ); ?></p>
				<?php elseif ( 'confirm' === $this->last_action ) : ?>
					<p><?php echo sprintf( _n( '%d booking was confirmed.', '%d bookings were confirmed.', $success, 'gastro-core' ), $success ); ?></p>
				<?php elseif ( 'pending' === $this->last_action ) : ?>
					<p><?php echo sprintf( _n( '%d booking moved to pending.', '%d bookings moved to pending.', $success, 'gastro-core' ), $success ); ?></p>
				<?php elseif ( 'close' === $this->last_action ) : ?>
					<p><?php echo sprintf( _n( '%d booking was closed.', '%d bookings were closed.', $success, 'gastro-core' ), $success ); ?></p>
				<?php endif; ?>
			</div>
		<?php }

		if ( $fail > 0 ) { ?>
			<div class="error">
				<p><?php echo sprintf( _n( '%d booking error.', '%d booking errors.', $fail, 'gastro-core' ), $fail ); ?></p>
			</div>
		<?php }
	}


	// Check if request is a shortcut link
	public function process_shortcut()
	{
		if ( empty( $_REQUEST['shortcut'] ) || !current_user_can( 'manage_booking' ) ) {
			return;
		}

		global $booking_controller;
		$booking_id = !empty( $_REQUEST['booking'] ) ? $_REQUEST['booking'] : false;

		if ( $booking_id && !$this->bulk_action_processed ) {
			$post_status = get_post_status( $booking_id );
			$quciklink_noti = gastro_booking_setting( 'booking-quicklink-notification' );

			if ( 'confirm' === $_REQUEST['shortcut'] && 'confirm' !== $post_status ) {
				gastro_booking_update_status( $booking_id, 'confirm' );
				$this->last_action = 'confirm';
			} elseif ( 'close' === $_REQUEST['shortcut'] && 'close' !== $post_status ) {
				gastro_booking_update_status( $booking_id, 'close' );
				$this->last_action = 'close';
			} else {
				$quciklink_noti = false;
			}

			if ( $quciklink_noti ) {
				$booking = new Gastro_Booking_Data();
				$booking->load_booking_data( $booking_id );
				$user_email = $booking->email;
				$user_email_subject = gastro_booking_set_email_subject( $booking, $this->last_action );
				$user_email_content = gastro_booking_set_email_content( $booking, $this->last_action );
				$user_email_header = gastro_booking_set_email_header();
				wp_mail( $booking->email, $user_email_subject, $user_email_content, $user_email_header);
			}
		}
	}


	// Count Booking
	public function count_booking()
	{
		global $wpdb;
		$where = "WHERE p.post_type = '" . RESERVATION_BOOKING_POST_TYPE . "'";

		if ( !empty( $this->filter_start_date ) || !empty( $this->filter_end_date ) ) {
			if ( !empty( $this->filter_start_date ) ) {
				$start_date = new DateTime( $this->filter_start_date );
				$where .= " AND p.post_date >= '" . $start_date->format( 'Y-m-d H:i:s' ) . "'";
			}

			if ( !empty( $this->filter_end_date ) ) {
				$end_date = new DateTime( $this->filter_end_date );
				$where .= " AND p.post_date <= '" . $end_date->format( 'Y-m-d H:i:s' ) . "'";
			}
		} elseif ( !empty( $_GET['date_range'] ) ) {
			if ( 'today' === $_GET['date_range'] ) {
				$today_start = date( 'Y-m-d', current_time( 'timestamp' ) );
				$today_end = date( 'Y-m-d', current_time( 'timestamp' ) + 86400 );
				$where .= " AND p.post_date >= '" . $today_start . "' AND p.post_date < '" . $today_end . "'";
			}
		} else {
			$where .= " AND p.post_date >= '" . date( 'Y-m-d H:i:s', current_time( 'timestamp' ) - 3600 ) . "'";
		}

		$join = '';
		$query = "SELECT p.post_status,count( * ) AS num_posts
			FROM $wpdb->posts p
			$join
			$where
			GROUP BY p.post_status
		";

		$count = $wpdb->get_results( $query, ARRAY_A );

		$this->booking_counts = array();
		foreach ( get_post_stati() as $state ) {
			$this->booking_counts[$state] = 0;
		}

		$this->booking_counts['all'] = 0;
		foreach ( (array) $count as $row ) {
			$this->booking_counts[$row['post_status']] = $row['num_posts'];
			$this->booking_counts['all'] += $row['num_posts'];
		}
	}


	// Collect all bookings from Gastro_Booking_Data
	public function query_booking()
	{
		global $booking_controller;
		$args = array();

		if ( !empty( $this->filter_start_date ) ) {
			$args['start_date'] = $this->filter_start_date;
		}

		if ( !empty( $this->filter_end_date ) ) {
			$args['end_date'] = $this->filter_end_date;
		}

		$query_defaults = array(
			'post_type'			=> RESERVATION_BOOKING_POST_TYPE,
			'posts_per_page'	=> $this->post_per_page,
			'date_range'		=> 'upcoming',
			'post_status'		=> array_keys( $booking_controller->booking_statuses ),
			'order'				=> 'ASC',
			'paged'				=> 1,
		);
		$query_args = wp_parse_args( $args, $query_defaults );

		if ( !empty( $_REQUEST['paged'] ) ) {
			$query_args['paged'] = (int) $_REQUEST['paged'];
		}

		if ( !empty( $_REQUEST['posts_per_page'] ) ) {
			$query_args['posts_per_page'] = (int) $_REQUEST['posts_per_page'];
		}

		if ( !empty( $_REQUEST['status'] ) ) {
			if ( is_string( $_REQUEST['status'] ) ) {
				$query_args['post_status'] = sanitize_text_field( $_REQUEST['status'] );
			} elseif ( is_array( $_REQUEST['status'] ) ) {
				$query_args['post_status'] = array();
				foreach( $_REQUEST['status'] as $status ) {
					$query_args['post_status'][] = sanitize_key( $status );
				}
			}
		}

		if ( !empty( $_REQUEST['orderby'] ) ) {
			$query_args['orderby'] = sanitize_key( $_REQUEST['orderby'] );
		}

		if ( !empty( $_REQUEST['order'] ) && 'desc' === $_REQUEST['order'] ) {
			$query_args['order'] = $_REQUEST['orderby'];
		}

		if ( !empty( $_REQUEST['date_range'] ) ) {
			$query_args['date_range'] = sanitize_key( $_REQUEST['date_range'] );
		}

		if ( !empty( $_REQUEST['start_date'] ) ) {
			$query_args['start_date'] = sanitize_text_field( $_REQUEST['start_date'] );
		}

		if ( !empty( $_REQUEST['end_date'] ) ) {
			$query_args['end_date'] = sanitize_text_field( $_REQUEST['end_date'] );
		}

		if ( is_string( $query_args['date_range'] ) ) {
			if ( !empty( $query_args['start_date'] ) || !empty( $query_args['end_date'] ) ) {
				$date_query = array( 'inclusive' => true );
				if ( !empty( $query_args['start_date'] ) ) {
					$date_query['after'] = sanitize_text_field( $query_args['start_date'] );
				}
				if ( !empty( $query_args['end_date'] ) ) {
					$date_query['before'] = sanitize_text_field( $query_args['end_date'] ) . ' 23:59'; // end of day
				}
				if ( count( $date_query ) ) {
					$query_args['date_query'] = $date_query;
				}
			} elseif ( $query_args['date_range'] === 'today' ) {
				$query_args['year'] = date( 'Y', current_time( 'timestamp' ) );
				$query_args['monthnum'] = date( 'm', current_time( 'timestamp' ) );
				$query_args['day'] = date( 'd', current_time( 'timestamp' ) );
			} elseif ( $query_args['date_range'] === 'upcoming' ) {
				$query_args['date_query'] = array(
					array(
						'after' => '-1 hour', // show bookings that have just passed
					)
				);
			}
		}

		if ( !empty( $query_args['post_status'] ) ) {
			if ( is_string( $query_args['post_status'] ) ) {
				// Parse a comma-separated string of statuses
				if ( strpos( $query_args['post_status'], ',' ) !== false ) {
					$statuses = explode( ',', $query_args['post_status'] );
					$query_args['post_status'] = array();
					foreach( $statuses as $status ) {
						$query_args['post_status'][] = sanitize_key( $status );
					}
				} else {
					$query_args['post_status'] = sanitize_key( $_REQUEST['status'] );
				}
			}
		}

		if ( 'all' === $query_args['date_range'] && !isset( $_REQUEST['orderby'] ) ) {
			$query_args['order'] = 'DESC';
		}

		$bookings = array();
		$query = new WP_Query( $query_args );
		if ( $query->have_posts() ) {
			while( $query->have_posts() ) {
				$query->the_post();
				$booking = new Gastro_Booking_Data();
				if ( $booking->load_booking_data( $query->post ) ) {
					$bookings[] = $booking;
				}
			}
		}
		$this->bookings = $bookings;
		wp_reset_query();
	}


	// Set booking table data
	public function prepare_items( $search = '' )
	{
		$columns = $this->get_columns();
		$sortable = $this->get_sortable_columns();
		$this->_column_headers = array( $columns, array(), $sortable );

		$this->items = array();
		$search_not_found_item = 0;

		if ( !empty( $search ) ) {
			foreach ( $this->bookings as $index => $booking ) {
				if ( strpos( $booking->email, $search ) !== false ) {
					$this->items[] = $booking;
				} else {
					$search_not_found_item = $search_not_found_item + 1;
				}
			}
		} else {
			$this->items = $this->bookings;
		}

		$total_items = empty( $_GET['status'] ) ? $this->booking_counts['all'] : $this->booking_counts[$_GET['status']];
		$total_items -= $search_not_found_item;

		if ( !empty( $_GET['status'] ) && 'trash' !== $_GET['status'] ) {
			$total_items -= $this->booking_counts['trash'];
		}

		$this->set_pagination_args( array(
				'total_items' => $total_items,
				'per_page'    => $this->post_per_page,
				'total_pages' => ceil( $total_items / $this->post_per_page )
			)
		);
	}
}
