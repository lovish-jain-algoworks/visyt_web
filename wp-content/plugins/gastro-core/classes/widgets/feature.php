<?php

class Gastro_Widget_Feature extends Gastro_Widget_Base
{
	protected function get_widget_options()
	{
		return array(
			'id' => 'gastro_widget_feature',
			'title' => esc_html__( 'Gastro Feature', 'gastro-core' ),
			'widget_options' => array(
				'description' => esc_html__( 'Gastro Feature', 'gastro-core' ),
				'classname' => 'gst-widget gst-widget-feature'
			),
		);
	}

	public function widget( $args, $instance )
	{
		$default = array(
			'alignment' => 'left',
			'image_url' => '',
			'image_size' => 'medium_large',
			'feature_title' => '',
			'feature_description' => '',
			'link_url' => '/',
			'link_label' => ''
		);

		$instance = array_merge( $default, $instance );
		$widget = '';
		$classes = array( 'gst-widget-item' );
		$classes[] = 'gst-' . $instance['alignment'] . '-align';
		$image_url = ( isset( $instance['image_url'] ) && !empty( $instance['image_url'] ) ) ? $instance['image_url'] : '';

		if ( !empty( $image_url ) || !empty( $instance['feature_title'] ) || !empty( $instance['feature_description'] ) || !empty( $instance['link_label'] ) ) {
			$widget .= '<div class="' . esc_attr( implode( ' ', $classes ) ) . '">';

			if ( !empty( $image_url ) ) {
				$widget .=    '<div class="gst-widget-media">';
				$widget .=      gastro_template_media( array(
					'image_url' => $image_url,
					'image_size' => $instance['image_size']
				) );
				$widget .=    '</div>';
			}

			$widget .=    '<div class="gst-widget-body">';
			if ( !empty( $instance['feature_title'] ) ) {
				$widget .=      '<h4 class="gst-widget-title">' . do_shortcode( $instance['feature_title'] ) . '</h4>';
			}

			if ( !empty( $instance['feature_description'] ) ) {
				$widget .=      '<div class="gst-widget-description">' . do_shortcode( $instance['feature_description'] ) . '</div>';
			}

			if ( !empty( $instance['link_label'] ) ) {
				$widget .=      '<a href="' . gastro_escape_url( $instance['link_url'] ) . '">';
				$widget .=         do_shortcode( $instance['link_label'] );
				$widget .=      '</a>';
			}
			$widget .=    '</div>';
			$widget .= '</div>';
		}

		$this->display_widget( $widget, $args, $instance );
	}

	public function form( $instance )
	{
		$title = ( !empty( $instance['title'] ) ) ? $instance['title'] : null;
		$output  = $this->render_text( 'title', $title, array(
			'label' => esc_html__( 'Title', 'gastro-core' )
		));

		$field = 'image_url';
		$value = ( !empty( $instance[$field] ) ) ? $instance[$field] : '';

		$output .= $this->render_media( $field, $value, array(
			'label' => esc_html__( 'Image Url', 'gastro-core' )
		));

		$field = 'image_size';
		$value = ( !empty( $instance[$field] ) ) ? $instance[$field] : 'medium_large';

		$output .= $this->render_select( $field, $value, array(
			'label' => esc_html__( 'Image Size', 'gastro-core' ),
			'choices' => array(
				'thumbnail' => esc_html__( 'Thumbnail', 'gastro-core' ),
				'medium' => esc_html__( 'Medium', 'gastro-core' ),
				'twist_medium' => esc_html__( 'Twist Medium', 'gastro-core' ),
				'medium_large' => esc_html__( 'Medium Large', 'gastro-core' ),
				'large' => esc_html__( 'Large', 'gastro-core' ),
				'twist_large' => esc_html__( 'Twist Large', 'gastro-core' ),
				'full' => esc_html__( 'Full', 'gastro-core' )
			)
		));

		$field = 'feature_title';
		$value =  ( !empty( $instance[$field] ) ) ? $instance[$field] : '';

		$output .= $this->render_text( $field, $value, array(
			'label' => esc_html__( 'Feature Title', 'gastro-core' )
		));

		$field = 'feature_description';
		$value = ( !empty( $instance[$field] ) ) ? $instance[$field] : '';

		$output .= $this->render_textarea( $field, $value, array(
			'label' => esc_html__( 'Feature Description', 'gastro-core' )
		));

		$field = 'link_label';
		$value = ( !empty( $instance[$field] ) ) ? $instance[$field] : '';

		$output .= $this->render_text( $field, $value, array(
			'label' => esc_html__( 'Link Label', 'gastro-core' )
		));

		$field = 'link_url';
		$value = ( !empty( $instance[$field] ) ) ? $instance[$field] : '';

		$output .= $this->render_text( $field, $value, array(
			'label' => esc_html__( 'Link URL', 'gastro-core' )
		));

		$field = 'alignment';
		$value = ( !empty( $instance[$field] ) ) ? $instance[$field] : 'center';

		$output .= $this->render_select( $field, $value, array(
			'label' => esc_html__( 'Alignment', 'gastro-core' ),
			'choices' => array(
				'left'   => esc_html__( 'Left', 'gastro-core' ),
				'center' => esc_html__( 'Center', 'gastro-core' ),
				'right'  => esc_html__( 'Right', 'gastro-core' )
			)
		));

		echo gastro_core_escape_content( $output );
	}

}
