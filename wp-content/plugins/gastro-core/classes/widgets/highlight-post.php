<?php

class Gastro_Widget_Highlight_Post extends Gastro_Widget_Base
{
	protected function get_widget_options()
	{
		return array(
			'id' => 'gastro_widget_highlight_post',
			'title' => esc_html__( 'Gastro Highlight Post', 'gastro-core' ),
			'widget_options' => array(
				'description' => esc_html__( 'Gastro Highlight Post', 'gastro-core' ),
				'classname' => 'gst-widget gst-widget-post'
			),
		);
	}

	public function widget( $args, $instance )
	{
		$default = array(
			'thumbnail_style' => 'square',
			'image_size' => 'medium',
			'entry_link' => 'post',
			'alignment' => 'left',
			'post_id' => null,
			'show_meta' => 'off'
		);

		$instance = array_merge( $default, $instance );
		$post_id = explode( ' ', $instance['post_id'] );
		$widget = '';

		if ( $post_id ) {
			$widget .= '<div class="gst-widget-post-inner ' . esc_attr( $instance['thumbnail_style'] ) . '-thumbnail-style">';

			foreach ( $post_id as $index => $id ) {
				if ( isset( $id ) && is_numeric( $id ) ) {
					$post = get_post( $id );

					if ( $post && !empty( $post ) ) {
						if ( isset( $post->guid ) ) {
							$permalink = $post->guid;

							if ( $bp_data = gastro_bp_data( $id ) ) {
								$post_setting = $bp_data['builder'];
								if ( isset( $post_setting['entry_link'] ) && 'default' !== $post_setting['entry_link'] ) {
									$instance['entry_link'] = $post_setting['entry_link'];
								}
								if ( 'alternate' === $instance['entry_link'] && isset( $post_setting['alternate_link'] ) && !empty( $post_setting['alternate_link'] ) ) {
									$permalink = $post_setting['alternate_link'];
								}
							}

							$widget .= '<a href="'. esc_url( $permalink ) .'" class="gst-widget-item gst-p-border-border">';

							if ( 'number' === $instance['thumbnail_style'] ) {
								$widget .= '<div class="gst-widget-number gst-p-bg-bg gst-p-border-border gst-s-text-color">'. esc_html( $index + 1 ) .'</div>';
							} else if ( 'none' !== $instance['thumbnail_style'] ) {

								$widget .= '<div class="gst-widget-media">';
								$widget .=    gastro_template_media( array(
									'image_id' => get_post_thumbnail_id( $id ),
									'image_ratio' => ( 'wide' !== $instance['thumbnail_style'] ) ? '1x1' : 'auto',
									'image_size' => $instance['image_size']
								) );
								$widget .= '</div>';
							}

							$widget .=    '<div class="gst-widget-body gst-' . esc_attr( $instance['alignment'] ) . '-align">';
							$widget .=        '<div class="gst-widget-title gst-secondary-font">' . esc_html( $post->post_title ) . '</div>';

							if ( 'on' === $instance['show_meta'] ) {
								$categories = gastro_term_names( gastro_get_taxonomy( null, false, $id ) );
								if ( !empty( $categories['name'] ) ) {
									$widget .= '<div class="gst-widget-category">' . esc_html( implode( ', ', $categories['name'] ) ) . '</div>';
								}
							}

							$widget .=    '</div>';
							$widget .= '</a>';
						}
					}
				}
			}

			$widget .= '</div>';
		}

		$this->display_widget( $widget, $args, $instance );
	}

	public function form( $instance )
	{
		// Title
		$title =  ( !empty( $instance['title'] ) ) ? $instance['title'] : null;
		$output  = $this->render_text( 'title', $title, array(
			'label' => esc_html__( 'Title', 'gastro-core' )
		) );

		$post_id = ( !empty( $instance['post_id'] ) ) ? $instance['post_id'] : null;
		$output .= $this->render_text( 'post_id', $post_id, array(
			'label' => esc_html__( 'Post ID (Ex: ID1 ID2 ID3)', 'gastro-core' )
		));

		$show_meta = ( !empty( $instance['show_meta'] ) ) ? $instance['show_meta'] : false;
		$output .= $this->render_checkbox( 'show_meta', $show_meta, array(
			'label' => esc_html__( 'Display Category', 'gastro-core' )
		) );

		$alignment = ( !empty( $instance['alignment'] ) ) ? $instance['alignment'] : 'left';
		$output .= $this->render_select( 'alignment', $alignment, array(
			'label' => esc_html__( 'Alignment', 'gastro-core' ),
			'choices' => array(
				'left' => esc_html__( 'Left', 'gastro-core' ),
				'center' => esc_html__( 'Center', 'gastro-core' ),
				'right' => esc_html__( 'Right', 'gastro-core' )
			)
		) );

		$style = ( !empty( $instance['thumbnail_style'] ) ) ? $instance['thumbnail_style'] : 'square';
		$output .= $this->render_select( 'thumbnail_style', $style, array(
			'label' => esc_html__( 'Thumbnail Style', 'gastro-core' ),
			'choices' => array(
				'none' => esc_html__( 'None', 'gastro-core' ),
				'square' => esc_html__( 'Square', 'gastro-core' ),
				'circle' => esc_html__( 'Circle', 'gastro-core' ),
				'number' => esc_html__( 'Number', 'gastro-core' ),
				'wide' => esc_html__( 'Wide', 'gastro-core' ),
				'hover' => esc_html__( 'Hover', 'gastro-core' )
			)
		) );


		$style = ( !empty( $instance['image_size'] ) ) ? $instance['image_size'] : 'medium';
		$output .= $this->render_select( 'image_size', $style, array(
			'label' => esc_html__( 'Thumbnail Size', 'gastro-core' ),
			'choices' => array(
				'thumbnail' => esc_html__( 'Thumbnail', 'gastro-core' ),
				'medium' => esc_html__( 'Medium', 'gastro-core' ),
				'twist_medium' => esc_html__( 'Twist Medium', 'gastro-core' ),
				'medium_large' => esc_html__( 'Medium Large', 'gastro-core' ),
				'large' => esc_html__( 'Large', 'gastro-core' ),
				'twist_large' => esc_html__( 'Twist Large', 'gastro-core' ),
				'full' => esc_html__( 'Full', 'gastro-core' )
			)
		));

		$entry_link = ( !empty( $instance['entry_link'] ) ) ? $instance['entry_link'] : 'post';
		$output .= $this->render_select( 'entry_link', $entry_link, array(
			'label' => esc_html__( 'Entry Link To', 'gastro-core' ),
			'choices' => array(
				'post' => esc_html__( 'Post URL', 'gastro-core' ),
				'alternate' => esc_html__( 'Alternate URL', 'gastro-core' )
			)
		) );

		echo gastro_core_escape_content( $output );
	}
}
