<?php

class Gastro_Widget_Post extends Gastro_Widget_Base
{
	protected function get_widget_options()
	{
		return array(
			'id' => 'gastro_widget_post',
			'title' => esc_html__( 'Gastro Post', 'gastro-core' ),
			'widget_options' => array(
				'description' => esc_html__( 'Gastro Post', 'gastro-core' ),
				'classname' => 'gst-widget gst-widget-post'
			),
		);
	}

	public function widget( $args, $instance )
	{
		$default = array(
			'thumbnail_style' => 'square',
			'image_size' => 'medium',
			'entry_link' => 'post',
			'alignment' => 'left',
			'no_of_items' => 5,
			'order_by' => 'post_date',
			'show_meta' => 'off',
			'category_filter' => array()
		);

		$instance = array_merge( $default, $instance );
		$widget = '<div class="gst-widget-post-inner ' . esc_attr( $instance['thumbnail_style'] ) . '-thumbnail-style">';
		$query_args = array(
			'posts_per_page' => $instance['no_of_items'],
			'orderby' => $instance['order_by']
		);

		if ( !empty( $instance['category_filter'] ) ) {
			$query_args['category__in'] = $instance['category_filter'];
		}

		$query = new WP_Query( $query_args );

		$index = 1;
		if ( $query->have_posts() ) {
			while ( $query->have_posts() ) {
				$query->the_post();
				$post_id = get_the_ID();
				$permalink = get_permalink();

				if ( $bp_data = gastro_bp_data( $post_id ) ) {
					$post_setting = $bp_data['builder'];
					if ( isset( $post_setting['entry_link'] ) && 'default' !== $post_setting['entry_link'] ) {
						$instance['entry_link'] = $post_setting['entry_link'];
					}
					if ( 'alternate' === $instance['entry_link'] && isset( $post_setting['alternate_link'] ) && !empty( $post_setting['alternate_link'] ) ) {
						$permalink = $post_setting['alternate_link'];
					}
				}

				$widget .= '<a href="'. esc_url( $permalink ) .'" class="gst-widget-item gst-p-border-border">';

				if ( 'number' === $instance['thumbnail_style'] ) {
					$widget .= '<div class="gst-widget-number gst-p-bg-bg gst-p-border-border gst-s-text-color">'. esc_html( $index ) .'</div>';
				} else if ( 'none' !== $instance['thumbnail_style'] ) {
					$widget .= '<div class="gst-widget-media">';
					$widget .=    gastro_template_media( array(
						'image_id' => get_post_thumbnail_id(),
						'image_ratio' => ( 'wide' !== $instance['thumbnail_style'] ) ? '1x1' : 'auto',
						'image_size' => $instance['image_size']
					) );
					$widget .= '</div>';
				}

				$widget .=    '<div class="gst-widget-body gst-' . esc_attr( $instance['alignment'] ) . '-align">';
				$widget .=        '<div class="gst-widget-title">' . get_the_title() . '</div>';

				if ( 'on' === $instance['show_meta'] ) {
					$categories = gastro_term_names( gastro_get_taxonomy( 'category' ) );
					if ( !empty( $categories['name'] ) ) {
						$widget .= '<div class="gst-widget-category">' . esc_html( implode( ', ', $categories['name'] ) ) . '</div>';
					}
				}

				$widget .=    '</div>';
				$widget .= '</a>';

				$index++;
			}
			wp_reset_postdata();
		}

		$widget .= '</div>';

		$this->display_widget( $widget, $args, $instance );
	}

	public function form( $instance )
	{
		$title =  ( !empty( $instance['title'] ) ) ? $instance['title'] : null;
		$output  = $this->render_text( 'title', $title, array(
			'label' => esc_html__( 'Title', 'gastro-core' )
		) );

		$choices = array();
		$categories = get_terms( 'category', array( 'hide_empty' => false ) );
		foreach ( $categories as $category ) {
			$choices[$category->term_id] = $category->name;
		}

		$category_filter = ( !empty( $instance['category_filter'] ) ) ? $instance['category_filter'] : array();
		$output .= $this->render_select( 'category_filter', $category_filter, array(
			'label' => esc_html__( 'Categories Filter', 'gastro-core' ),
			'class' => 'widefat',
			'choices' => $choices,
			'multiple' => true
		) );

		$show_meta = ( !empty( $instance['show_meta'] ) ) ? $instance['show_meta'] : false;
		$output .= $this->render_checkbox( 'show_meta', $show_meta, array(
			'label' => esc_html__( 'Display Category', 'gastro-core' )
		) );

		$no_of_items = ( !empty( $instance['no_of_items'] ) ) ? $instance['no_of_items'] : 5;
		$output .= $this->render_select( 'no_of_items', $no_of_items, array(
			'label' => esc_html__( 'No. of Items', 'gastro-core' ),
			'choices' => array(
				'-1' => esc_html__( 'all', 'gastro-core' ),
				'1' => 1,
				'2' => 2,
				'3' => 3,
				'4' => 4,
				'5' => 5,
				'6' => 6,
				'7' => 7,
				'8' => 8,
				'9' => 9,
				'10' => 10
			)
		) );

		$orderby = ( !empty( $instance['order_by'] ) ) ? $instance['order_by'] : 'post_date';
		$output .= $this->render_select( 'order_by', $orderby, array(
			'label' => esc_html__( 'Order By', 'gastro-core' ),
			'choices' => array(
				'post_date' => esc_html__( 'Recent Publish', 'gastro-core' ),
				'modified' => esc_html__( 'Recent Update', 'gastro-core' ),
				'rand' => esc_html__( 'Random', 'gastro-core' ),
				'menu_order' => esc_html__( 'Menu Order', 'gastro-core' ),
				'author' => esc_html__( 'Author', 'gastro-core' ),
				'title' => esc_html__( 'Title', 'gastro-core' ),
				'name' => esc_html__( 'Name (Post Slug)', 'gastro-core' ),
				'comment_count' => esc_html__( 'Comment Count', 'gastro-core' )
			)
		) );

		$alignment = ( !empty( $instance['alignment'] ) ) ? $instance['alignment'] : 'left';
		$output .= $this->render_select( 'alignment', $alignment, array(
			'label' => esc_html__( 'Alignment', 'gastro-core' ),
			'choices' => array(
				'left' => esc_html__( 'Left', 'gastro-core' ),
				'center' => esc_html__( 'Center', 'gastro-core' ),
				'right' => esc_html__( 'Right', 'gastro-core' )
			)
		) );

		$style = ( !empty( $instance['thumbnail_style'] ) ) ? $instance['thumbnail_style'] : 'square';
		$output .= $this->render_select( 'thumbnail_style', $style, array(
			'label' => esc_html__( 'Thumbnail Style', 'gastro-core' ),
			'choices' => array(
				'none' => esc_html__( 'None', 'gastro-core' ),
				'square' => esc_html__( 'Square', 'gastro-core' ),
				'circle' => esc_html__( 'Circle', 'gastro-core' ),
				'number' => esc_html__( 'Number', 'gastro-core' ),
				'wide' => esc_html__( 'Wide', 'gastro-core' ),
				'hover' => esc_html__( 'Hover', 'gastro-core' )
			)
		) );

		$image_size = ( !empty( $instance['image_size'] ) ) ? $instance['image_size'] : 'medium';
		$output .= $this->render_select( 'image_size', $image_size, array(
			'label' => esc_html__( 'Thumbnail Size', 'gastro-core' ),
			'choices' => array(
				'thumbnail' => esc_html__( 'Thumbnail', 'gastro-core' ),
				'medium' => esc_html__( 'Medium', 'gastro-core' ),
				'twist_medium' => esc_html__( 'Twist Medium', 'gastro-core' ),
				'medium_large' => esc_html__( 'Medium Large', 'gastro-core' ),
				'large' => esc_html__( 'Large', 'gastro-core' ),
				'twist_large' => esc_html__( 'Twist Large', 'gastro-core' ),
				'full' => esc_html__( 'Full', 'gastro-core' )
			)
		));

		$entry_link = ( !empty( $instance['entry_link'] ) ) ? $instance['entry_link'] : 'post';
		$output .= $this->render_select( 'entry_link', $entry_link, array(
			'label' => esc_html__( 'Entry Link To', 'gastro-core' ),
			'choices' => array(
				'post' => esc_html__( 'Post URL', 'gastro-core' ),
				'alternate' => esc_html__( 'Alternate URL', 'gastro-core' )
			)
		) );

		echo gastro_core_escape_content( $output );
	}
}
