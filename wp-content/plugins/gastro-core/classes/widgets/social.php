<?php

class Gastro_Widget_Social extends Gastro_Widget_Base
{
	protected function get_widget_options()
	{
		return array(
			'id' => 'gastro_widget_social',
			'title' => esc_html__( 'Gastro Social', 'gastro-core' ),
			'widget_options' => array(
				'description' => esc_html__( 'Gastro Social', 'gastro-core' ),
				'classname' => 'gst-widget gst-widget-social'
			),
		);
	}

	public function widget( $args, $instance )
	{
		$widget = '';
		$social_class  = 'gst-social';
		$instance['default_color'] = true;

		$widget .= '<div class="gst-social">';
		$widget .=    '<div class="gst-social-inner">';
		$widget .=        gastro_get_social_icon( $instance );
		$widget .=    '</div>';
		$widget .= '</div>';

		$this->display_widget( $widget, $args, $instance );
	}

	public function form( $instance )
	{
		// Title
		$field = 'title';
		$value =  ( !empty( $instance[$field] ) ) ? $instance[$field] : null;

		$output  = $this->render_text( $field, $value, array(
			'label' => esc_html__( 'Title', 'gastro-core' )
		));

		$field = 'icon_size';
		$value = ( !empty( $instance[$field] ) ) ? $instance[$field] : 'small';

		$output .= $this->render_select( 'icon_size', $value, array(
			'label' => esc_html__( 'Icon Size', 'gastro-core' ),
			'choices' => array(
				'small' => esc_html__( 'Small', 'gastro-core' ),
				'medium' => esc_html__( 'Medium', 'gastro-core' ),
				'large' => esc_html__( 'Large', 'gastro-core' ),
				'x-large' => esc_html__( 'X-Large', 'gastro-core' )
			)
		));

		echo gastro_core_escape_content( $output );
	}
}
