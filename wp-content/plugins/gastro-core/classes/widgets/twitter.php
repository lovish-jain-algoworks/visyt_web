<?php

class Gastro_Widget_Twitter extends Gastro_Widget_Base
{
	protected function get_widget_options()
	{
		return array(
			'id' => 'gastro_widget_twitter',
			'title' => esc_html__( 'Gastro Twitter', 'gastro-core' ),
			'widget_options' => array(
				'description' => 'Gastro Twitter',
				'classname' => 'gst-widget gst-widget-twitter'
			),
		);
	}

	public function widget( $args, $instance )
	{
		$twitter_username = $instance['twitter_username'];
		$display_num = $instance['display_num'];
		$consumer_key = $instance['consumer_key'];
		$consumer_secret = $instance['consumer_secret'];
		$access_token = $instance['access_token'];
		$access_token_secret = $instance['access_token_secret'];
		$cache_time = $instance['cache_time'];
		$gst_twitter = get_option( 'gst_twitter', array() );

		if ( !is_array( $gst_twitter ) && !empty( $gst_twitter ) ) {
			$gst_twitter = unserialize( $gst_twitter );
		}

		if ( !is_array($gst_twitter) ) {
			$gst_twitter = array();
		}

		if ( empty( $gst_twitter[$twitter_username][$display_num]['data'] ) ||
		empty( $gst_twitter[$twitter_username][$display_num]['cache_time'] ) ||
		time() - intval( $gst_twitter[$twitter_username][$display_num]['cache_time'] ) >= ( $cache_time * 3600 ) ) {

			$tweets_data = gastro_get_tweets( $consumer_key, $consumer_secret, $access_token, $access_token_secret, $twitter_username, $display_num );

			if ( !empty( $tweets_data ) ) {
				$gst_twitter[$twitter_username][$display_num]['data'] = $tweets_data;
				$gst_twitter[$twitter_username][$display_num]['cache_time'] = time();
				update_option( 'gst_twitter', $gst_twitter );
			}
		} else {
			$tweets_data = $gst_twitter[$twitter_username][$display_num]['data'];
		}

		$widget = '<div class="gst-widget-item gst-twitter">';

		if ( is_string( $tweets_data ) ) {
			$widget .= $tweets_data;
		} else {
			foreach( $tweets_data as $tweet_data ){
				$widget .= '<div class="gst-twitter-item">';
				$widget .=   '<div class="gst-twitter-media"><i class="twf twf-twitter"></i></div>';
				$widget .=   '<div class="gst-twitter-body">' . utf8_decode( $tweet_data ) . '</div>';
				$widget .= '</div>';
			}
		}

		$widget .= '</div>';

		$this->display_widget ( $widget, $args, $instance );
	}

	public function form( $instance )
	{
		// Title
		$field = 'title';
		$value =  ( !empty( $instance[$field] ) ) ? $instance[$field] : null;

		$output  = $this->render_text( $field, esc_attr( $value ), array(
			'label' => esc_html__( 'Title', 'gastro-core' )
		));

		// Username
		$field = 'twitter_username';
		$value = ( !empty( $instance[$field] ) ) ? $instance[$field] : '';

		$output .= $this->render_text( $field, $value, array(
			'label' => esc_html__( 'Username', 'gastro-core' )
		));

		// No. Of Tweets
		$field = 'display_num';
		$value = ( !empty($instance[$field] ) ) ? $instance[$field] : '';

		$output .= $this->render_text( $field, $value, array(
			'label' => esc_html__( 'No. of Tweets to display', 'gastro-core' )
		));

		// Consumer Key
		$field = 'consumer_key';
		$value = ( !empty( $instance[$field] ) ) ? $instance[$field] : '';

		$output .= $this->render_text( $field, $value, array(
			'label' => esc_html__( 'Consumer Key', 'gastro-core' )
		));

		// Consumer Secret
		$field = 'consumer_secret';
		$value = ( !empty( $instance[$field] ) ) ? $instance[$field] : '';

		$output .= $this->render_text( $field, $value, array(
			'label' => esc_html__( 'Consumer Secret', 'gastro-core' )
		));

		// Access Token
		$field = 'access_token';
		$value = ( !empty( $instance[$field] ) ) ? $instance[$field] : '';

		$output .= $this->render_text( $field, $value, array(
			'label' => esc_html__( 'Access Token', 'gastro-core' )
		));

		// Access Token Secret
		$field = 'access_token_secret';
		$value = ( !empty( $instance[$field] ) ) ? $instance[$field] : '';

		$output .= $this->render_text( $field, $value, array(
			'label' => esc_html__( 'Access Token Secret', 'gastro-core' )
		));

		// Cache Time
		$field = 'cache_time';
		$value = ( !empty( $instance[$field] ) ) ? $instance[$field] : '';

		$output .= $this->render_text( $field, $value, array(
			'label' => esc_html__( 'Cache Time (hour)', 'gastro-core' )
		));

		echo gastro_core_escape_content( $output );
	}
}
