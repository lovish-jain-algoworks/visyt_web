<?php

function gastro_blueprint_preset_templates()
{
	$templates = array(
		'home-classic' => array(
			'name' => 'Classic',
			'category' => 'home',
			'preview' => 'http://www.gastrotheme.com/home/classic/',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/home-classic.jpg'
		),
		'home-creative' => array(
			'name' => 'Creative',
			'category' => 'home',
			'preview' => 'http://www.gastrotheme.com/home/creative/',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/home-creative.jpg'
		),
		'home-contemporary' => array(
			'name' => 'Comtemporary',
			'category' => 'home',
			'preview' => 'http://www.gastrotheme.com/home/contemporary/',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/home-contemporary.jpg'
		),
		'home-slide' => array(
			'name' => 'Slide',
			'category' => 'home',
			'preview' => 'http://www.gastrotheme.com/home/slide/',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/home-slide.jpg'
		),
		'home-modern' => array(
			'name' => 'Modern',
			'category' => 'home',
			'preview' => 'http://www.gastrotheme.com/home/modern/',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/home-modern.jpg'
		),
		'home-eccentric' => array(
			'name' => 'Eccentric',
			'category' => 'home',
			'preview' => 'http://www.gastrotheme.com/home/eccentric/',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/home-eccentric.jpg'
		),
		'menu-classic' => array(
			'name' => 'Classic',
			'category' => 'menu',
			'preview' => 'http://www.gastrotheme.com/pages/menu-classic/',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/menu-classic.jpg'
		),
		'menu-one-page' => array(
			'name' => 'One Page',
			'category' => 'menu',
			'preview' => 'http://www.gastrotheme.com/pages/menu-one-page/',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/menu-one-page.jpg'
		),
		'menu-half' => array(
			'name' => 'Half',
			'category' => 'menu',
			'preview' => 'http://www.gastrotheme.com/pages/menu-half/',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/menu-half.jpg'
		),
		'menu-seamless' => array(
			'name' => 'Seamless',
			'category' => 'menu',
			'preview' => 'http://www.gastrotheme.com/pages/menu-seamless/',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/menu-seamless.jpg'
		),
		'menu-accordion' => array(
			'name' => 'Accordion',
			'category' => 'menu',
			'preview' => 'http://www.gastrotheme.com/pages/menu-accordion/',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/menu-accordion.jpg'
		),
		'menu-showcase' => array(
			'name' => 'Showcase',
			'category' => 'menu',
			'preview' => 'http://www.gastrotheme.com/pages/menu-showcase/',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/menu-showcase.jpg'
		),
		'menu-grid' => array(
			'name' => 'Grid',
			'category' => 'menu',
			'preview' => 'http://www.gastrotheme.com/pages/menu-grid/',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/menu-grid.jpg'
		),
		'menu-modal' => array(
			'name' => 'Modal',
			'category' => 'menu',
			'preview' => 'http://www.gastrotheme.com/pages/menu-modal/',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/menu-modal.jpg'
		),
		'reservation-classic' => array(
			'name' => 'Classic',
			'category' => 'reservation',
			'preview' => 'http://www.gastrotheme.com/pages/reservation-classic/',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/reservation-classic.jpg'
		),
		'reservation-overlap' => array(
			'name' => 'Overlap',
			'category' => 'reservation',
			'preview' => 'http://www.gastrotheme.com/pages/reservation-overlap/',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/reservation-overlap.jpg'
		),
		'reservation-half' => array(
			'name' => 'Half',
			'category' => 'reservation',
			'preview' => 'http://www.gastrotheme.com/pages/reservation-half/',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/reservation-half.jpg'
		),
		'reservation-asymmetry' => array(
			'name' => 'Asymmetry',
			'category' => 'reservation',
			'preview' => 'http://www.gastrotheme.com/pages/reservation-asymmetry/',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/reservation-asymmetry.jpg'
		),
		'reservation-float' => array(
			'name' => 'Float',
			'category' => 'reservation',
			'preview' => 'http://www.gastrotheme.com/pages/reservation-float/',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/reservation-float.jpg'
		),
		'reservation-bar' => array(
			'name' => 'Bar',
			'category' => 'reservation',
			'preview' => 'http://www.gastrotheme.com/pages/reservation-bar/',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/reservation-bar.jpg'
		),
		'reservation-hero' => array(
			'name' => 'Hero',
			'category' => 'reservation',
			'preview' => 'http://www.gastrotheme.com/pages/reservation-hero/',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/reservation-hero.jpg'
		),
		'reservation-modal' => array(
			'name' => 'Modal',
			'category' => 'reservation',
			'preview' => 'http://www.gastrotheme.com/pages/reservation-modal/',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/reservation-modal.jpg'
		),
		'location-classic' => array(
			'name' => 'Classic',
			'category' => 'location',
			'preview' => 'http://www.gastrotheme.com/pages/location-classic/',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/location-classic.jpg'
		),
		'location-boxed' => array(
			'name' => 'Boxed',
			'category' => 'location',
			'preview' => 'http://www.gastrotheme.com/pages/location-boxed/',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/location-boxed.jpg'
		),
		'location-half' => array(
			'name' => 'Half',
			'category' => 'location',
			'preview' => 'http://www.gastrotheme.com/pages/location-half/',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/location-half.jpg'
		),
		'location-asymmetry' => array(
			'name' => 'Asymmetry',
			'category' => 'location',
			'preview' => 'http://www.gastrotheme.com/pages/location-asymmetry/',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/location-asymmetry.jpg'
		),
		'location-float' => array(
			'name' => 'Float',
			'category' => 'location',
			'preview' => 'http://www.gastrotheme.com/pages/location-float/',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/location-float.jpg'
		),
		'location-modern' => array(
			'name' => 'Modern',
			'category' => 'location',
			'preview' => 'http://www.gastrotheme.com/pages/location-modern/',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/location-modern.jpg'
		),
		'location-grid' => array(
			'name' => 'Grid',
			'category' => 'location',
			'preview' => 'http://www.gastrotheme.com/pages/location-grid/',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/location-grid.jpg'
		),
		'location-zigzag' => array(
			'name' => 'Zigzag',
			'category' => 'location',
			'preview' => 'http://www.gastrotheme.com/pages/location-zigzag/',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/location-zigzag.jpg'
		),
		'delivery-list' => array(
			'name' => 'Delivery List',
			'category' => 'delivery',
			'preview' => 'http://www.gastrotheme.com/delivery/delivery-list/',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/delivery-list.jpg'
		),
		'delivery-grid' => array(
			'name' => 'Delivery Grid',
			'category' => 'delivery',
			'preview' => 'http://www.gastrotheme.com/delivery/delivery-grid/',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/delivery-grid.jpg'
		),
		'other-404' => array(
			'name' => '404 Page',
			'category' => 'other',
			'preview' => 'http://www.gastrotheme.com/pages/404-2/',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/other-404.jpg'
		),
		'other-coming-soon' => array(
			'name' => 'Coming Soon',
			'category' => 'other',
			'preview' => 'http://www.gastrotheme.com/pages/coming-soon/',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/other-coming-soon.jpg'
		),
		'other-maintenance' => array(
			'name' => 'Maintenance',
			'category' => 'other',
			'preview' => 'http://www.gastrotheme.com/pages/maintenance/',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/other-maintenance.jpg'
		),
		'other-about' => array(
			'name' => 'About',
			'category' => 'other',
			'preview' => 'http://www.gastrotheme.com/pages/about/',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/other-about.jpg'
		),
		'other-chef' => array(
			'name' => 'Chef',
			'category' => 'other',
			'preview' => 'http://www.gastrotheme.com/pages/chef/',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/other-chef.jpg'
		),
		'other-gallery' => array(
			'name' => 'Gallery',
			'category' => 'other',
			'preview' => 'http://www.gastrotheme.com/pages/gallery/',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/other-gallery.jpg'
		),
		'other-contact' => array(
			'name' => 'Contact',
			'category' => 'other',
			'preview' => 'http://www.gastrotheme.com/pages/contact/',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/other-contact.jpg'
		),
		'other-special-offers' => array(
			'name' => 'Special Offers',
			'category' => 'other',
			'preview' => 'http://www.gastrotheme.com/pages/offers/',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/other-special-offers.jpg'
		),
		'blog-index-list' => array(
			'name' => 'Index List',
			'category' => 'blog',
			'preview' => 'http://www.gastrotheme.com/blog/blog-list/',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/blog-index-list.jpg'
		),
		'blog-index-grid' => array(
			'name' => 'Index Grid',
			'category' => 'blog',
			'preview' => 'http://www.gastrotheme.com/blog/blog-grid/',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/blog-index-grid.jpg'
		),
		'blog-index-masonry' => array(
			'name' => 'Index Masonry',
			'category' => 'blog',
			'preview' => 'http://www.gastrotheme.com/blog/blog-masonry/',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/blog-index-masonry.jpg'
		),
		'blog-index-zigzag' => array(
			'name' => 'Index Zigzag',
			'category' => 'blog',
			'preview' => 'http://www.gastrotheme.com/blog/blog-zigzag/',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/blog-index-zigzag.jpg'
		),
		'blog-index-sidebar' => array(
			'name' => 'Index Sidebar',
			'category' => 'blog',
			'preview' => 'http://www.gastrotheme.com/blog/blog-sidebar/',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/blog-index-sidebar.jpg'
		),
		'blog-index-carousel' => array(
			'name' => 'Index Carousel',
			'category' => 'blog',
			'preview' => 'http://www.gastrotheme.com/blog/blog-carousel/',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/blog-index-carousel.jpg'
		),
		'blog-index-half' => array(
			'name' => 'Index Half',
			'category' => 'blog',
			'preview' => 'http://www.gastrotheme.com/blog/blog-half/',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/blog-index-half.jpg'
		),
		'blog-index-boxed' => array(
			'name' => 'Index Boxed',
			'category' => 'blog',
			'preview' => 'http://www.gastrotheme.com/blog/blog-boxed/',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/blog-index-boxed.jpg'
		),
		'blog-index-custom' => array(
			'name' => 'Index Custom',
			'category' => 'blog',
			'preview' => 'http://www.gastrotheme.com/blog/blog-custom/',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/blog-index-custom.jpg'
		),
		'blog-custom-1' => array(
			'name' => 'Custom Layout I',
			'category' => 'blog',
			'preview' => 'http://www.gastrotheme.com/blog/custom-layout/',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/blog-custom-1.jpg'
		),
		'blog-custom-2' => array(
			'name' => 'Custom Layout II',
			'category' => 'blog',
			'preview' => 'http://www.gastrotheme.com/blog/custom-layout-2/',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/blog-custom-2.jpg'
		),
		'recipe-index-grid' => array(
			'name' => 'Index Grid',
			'category' => 'recipe',
			'preview' => 'http://www.gastrotheme.com/recipes/recipe-grid/',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/recipe-index-grid.jpg'
		),
		'recipe-index-masonry' => array(
			'name' => 'Index Masonry',
			'category' => 'recipe',
			'preview' => 'http://www.gastrotheme.com/recipes/recipe-masonry/',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/recipe-index-masonry.jpg'
		),
		'recipe-index-carousel' => array(
			'name' => 'Index Carousel',
			'category' => 'recipe',
			'preview' => 'http://www.gastrotheme.com/recipes/recipe-carousel/',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/recipe-index-carousel.jpg'
		),
		'recipe-index-zigzag' => array(
			'name' => 'Index Zigzag',
			'category' => 'recipe',
			'preview' => 'http://www.gastrotheme.com/recipes/recipe-portrait/',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/recipe-index-zigzag.jpg'
		),
		'recipe-index-creative' => array(
			'name' => 'Index Creative',
			'category' => 'recipe',
			'preview' => 'http://www.gastrotheme.com/recipes/recipe-creative/',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/recipe-index-creative.jpg'
		),
		'recipe-index-sidebar' => array(
			'name' => 'Index Sidebar',
			'category' => 'recipe',
			'preview' => 'http://www.gastrotheme.com/recipes/recipe-sidebar/',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/recipe-index-sidebar.jpg'
		),
		'recipe-index-half' => array(
			'name' => 'Index Half',
			'category' => 'recipe',
			'preview' => 'http://www.gastrotheme.com/recipes/half/',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/recipe-index-half.jpg'
		),
		'recipe-index-showcase' => array(
			'name' => 'Index Showcase',
			'category' => 'recipe',
			'preview' => 'http://www.gastrotheme.com/recipes/showcase/',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/recipe-index-showcase.jpg'
		),
		'recipe-index-boxed' => array(
			'name' => 'Index Boxed',
			'category' => 'recipe',
			'preview' => 'http://www.gastrotheme.com/recipes/recipe-boxed/',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/recipe-index-boxed.jpg'
		),
		'recipe-index-modern' => array(
			'name' => 'Index Modern',
			'category' => 'recipe',
			'preview' => 'http://www.gastrotheme.com/recipes/recipe-modern/',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/recipe-index-modern.jpg'
		),
		'recipe-index-asymmetry' => array(
			'name' => 'Index Asymmetry',
			'category' => 'recipe',
			'preview' => 'http://www.gastrotheme.com/recipes/asymmetry/',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/recipe-index-asymmetry.jpg'
		),
		'recipe-index-collection' => array(
			'name' => 'Index Collection',
			'category' => 'recipe',
			'preview' => 'http://www.gastrotheme.com/recipes/collection/',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/recipe-index-collection.jpg'
		),
		'recipe-half' => array(
			'name' => 'Half',
			'category' => 'recipe',
			'preview' => 'http://www.gastrotheme.com/recipe/recipe-half/',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/recipe-half.jpg'
		),
		'recipe-step-side-stacking' => array(
			'name' => 'Step Side Stacking',
			'category' => 'recipe',
			'preview' => 'http://www.gastrotheme.com/recipe/step-side-stacking/',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/recipe-step-side-stacking.jpg'
		),
		'recipe-multiple-menu-tab' => array(
			'name' => 'Multiple Menu Tab',
			'category' => 'recipe',
			'preview' => 'http://www.gastrotheme.com/recipe/multiple-menu-tab/',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/recipe-multiple-menu-tab.jpg'
		),
		'recipe-multiple-recipe-stack' => array(
			'name' => 'Multiple Recipe Stack',
			'category' => 'recipe',
			'preview' => 'http://www.gastrotheme.com/recipe/multiple-recipe-stack/',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/recipe-multiple-recipe-stack.jpg'
		),
		'recipe-step-by-step' => array(
			'name' => 'Step By Step',
			'category' => 'recipe',
			'preview' => 'http://www.gastrotheme.com/recipe/step-by-step/',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/recipe-step-by-step.jpg'
		),
		'recipe-full-slider' => array(
			'name' => 'Full Slider',
			'category' => 'recipe',
			'preview' => 'http://www.gastrotheme.com/recipe/full-slider/',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/recipe-full-slider.jpg'
		),
		'recipe-centered' => array(
			'name' => 'Centered',
			'category' => 'recipe',
			'preview' => 'http://www.gastrotheme.com/recipe/centered/',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/recipe-centered.jpg'
		),
		'recipe-overlap-header' => array(
			'name' => 'Overlap Header',
			'category' => 'recipe',
			'preview' => 'http://www.gastrotheme.com/recipe/overlap-header/',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/recipe-overlap-header.jpg'
		),
		'recipe-minimal' => array(
			'name' => 'Minimal',
			'category' => 'recipe',
			'preview' => 'http://www.gastrotheme.com/recipe/minimal/',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/recipe-minimal.jpg'
		),
		'recipe-ingredient-left' => array(
			'name' => 'Ingredient Left',
			'category' => 'recipe',
			'preview' => 'http://www.gastrotheme.com/recipe/ingredient-left/',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/recipe-ingredient-left.jpg'
		),
		'recipe-ingredient-right' => array(
			'name' => 'Ingredient Right',
			'category' => 'recipe',
			'preview' => 'http://www.gastrotheme.com/recipe/ingredient-right/',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/recipe-ingredient-right.jpg'
		),
		'recipe-magazine' => array(
			'name' => 'Magazine',
			'category' => 'recipe',
			'preview' => 'http://www.gastrotheme.com/recipe/magazine/',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/recipe-magazine.jpg'
		),
		'bp-a-la-carte-menu' => array(
			'name' => 'A La Carte Menu',
			'category' => 'blueprint',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/bp-a-la-carte-menu.jpg'
		),
		'bp-accordion-cocktail' => array(
			'name' => 'Accordion Cocktail',
			'category' => 'blueprint',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/bp-accordion-cocktail.jpg'
		),
		'bp-bottom-bar' => array(
			'name' => 'Bottom Bar',
			'category' => 'blueprint',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/bp-bottom-bar.jpg'
		),
		'bp-chef-header' => array(
			'name' => 'Chef Header',
			'category' => 'blueprint',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/bp-chef-header.jpg'
		),
		'bp-cocktail-gallery' => array(
			'name' => 'Cocktail Gallery',
			'category' => 'blueprint',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/bp-cocktail-gallery.jpg'
		),
		'bp-drink-menu' => array(
			'name' => 'Drink Menu',
			'category' => 'blueprint',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/bp-drink-menu.jpg'
		),
		'bp-footer-1' => array(
			'name' => 'Footer 1',
			'category' => 'blueprint',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/bp-footer-1.jpg'
		),
		'bp-footer-2' => array(
			'name' => 'Footer 2',
			'category' => 'blueprint',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/bp-footer-2.jpg'
		),
		'bp-footer-3' => array(
			'name' => 'Footer 3',
			'category' => 'blueprint',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/bp-footer-3.jpg'
		),
		'bp-footer-4' => array(
			'name' => 'Footer 4',
			'category' => 'blueprint',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/bp-footer-4.jpg'
		),
		'bp-footer-5' => array(
			'name' => 'Footer 5',
			'category' => 'blueprint',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/bp-footer-5.jpg'
		),
		'bp-footer-6' => array(
			'name' => 'Footer 6',
			'category' => 'blueprint',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/bp-footer-6.jpg'
		),
		'bp-footer-7' => array(
			'name' => 'Footer 7',
			'category' => 'blueprint',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/bp-footer-7.jpg'
		),
		'bp-footer-8' => array(
			'name' => 'Footer 8',
			'category' => 'blueprint',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/bp-footer-8.jpg'
		),
		'bp-footer-9' => array(
			'name' => 'Footer 9',
			'category' => 'blueprint',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/bp-footer-9.jpg'
		),
		'bp-footer-10' => array(
			'name' => 'Footer 10',
			'category' => 'blueprint',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/bp-footer-10.jpg'
		),
		'bp-instagram' => array(
			'name' => 'Instagram',
			'category' => 'blueprint',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/bp-instagram.jpg'
		),
		'bp-location-float' => array(
			'name' => 'Location Float',
			'category' => 'blueprint',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/bp-location-float.jpg'
		),
		'bp-location-tab' => array(
			'name' => 'Location Tab',
			'category' => 'blueprint',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/bp-location-tab.jpg'
		),
		'bp-maintenance' => array(
			'name' => 'Maintenance',
			'category' => 'blueprint',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/other-maintenance.jpg'
		),
		'bp-modal-menu' => array(
			'name' => 'Modal Menu',
			'category' => 'blueprint',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/bp-modal-menu.jpg'
		),
		'bp-modal-menu-breakfast' => array(
			'name' => 'Modal Menu Breakfast',
			'category' => 'blueprint',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/bp-modal-menu-breakfast.jpg'
		),
		'bp-modal-menu-dinner' => array(
			'name' => 'Modal Menu Dinner',
			'category' => 'blueprint',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/bp-modal-menu-dinner.jpg'
		),
		'bp-modal-menu-drink' => array(
			'name' => 'Modal Menu Drink',
			'category' => 'blueprint',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/bp-modal-menu-drink.jpg'
		),
		'bp-modal-menu-lunch' => array(
			'name' => 'Modal Menu Lunch',
			'category' => 'blueprint',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/bp-modal-menu-lunch.jpg'
		),
		'bp-newsletter-splash' => array(
			'name' => 'Newsletter Splash',
			'category' => 'blueprint',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/bp-newsletter-splash.jpg'
		),
		'bp-recipe-asymmetry' => array(
			'name' => 'Recipe Asymmetry',
			'category' => 'blueprint',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/bp-recipe-asymmetry.jpg'
		),
		'bp-recipe-overlap-header' => array(
			'name' => 'Recipe Overlap Header',
			'category' => 'blueprint',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/bp-recipe-overlap-header.jpg'
		),
		'bp-reservation-float' => array(
			'name' => 'Reservation Float',
			'category' => 'blueprint',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/bp-reservation-float.jpg'
		),
		'bp-reservation-hero-box' => array(
			'name' => 'Reservation Hero Box',
			'category' => 'blueprint',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/bp-reservation-hero-box.jpg'
		),
		'bp-reservation-overlap' => array(
			'name' => 'Reservation Overlap',
			'category' => 'blueprint',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/bp-reservation-overlap.jpg'
		),
		'bp-seamless-drink-menu' => array(
			'name' => 'Seamless Drink Menu',
			'category' => 'blueprint',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/bp-seamless-drink-menu.jpg'
		),
		'bp-social-dark' => array(
			'name' => 'Social Dark',
			'category' => 'blueprint',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/bp-social-dark.jpg'
		),
		'bp-subscription' => array(
			'name' => 'Subscription',
			'category' => 'blueprint',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/bp-subscription.jpg'
		),
		'bp-tab-recipe-dessert' => array(
			'name' => 'Tab Recipe Dessert',
			'category' => 'blueprint',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/bp-tab-recipe-dessert.jpg'
		),
		'bp-tab-recipe-main' => array(
			'name' => 'Tab Recipe Main',
			'category' => 'blueprint',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/bp-tab-recipe-main.jpg'
		),
		'bp-tab-recipe-pasta' => array(
			'name' => 'Tab Recipe Pasta',
			'category' => 'blueprint',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/bp-tab-recipe-pasta.jpg'
		),
		'bp-tab-recipe-soup' => array(
			'name' => 'Tab Recipe Soup',
			'category' => 'blueprint',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/bp-tab-recipe-soup.jpg'
		),
		'bp-tab-recipe-starter' => array(
			'name' => 'Tab Recipe Starter',
			'category' => 'blueprint',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/bp-tab-recipe-starter.jpg'
		),
		'bp-tasting-menu' => array(
			'name' => 'Tasting Menu',
			'category' => 'blueprint',
			'thumbnail' => 'http://static.gastrotheme.com/images/blueprint-templates/bp-tasting-menu.jpg'
		),
	);

	update_option( 'bp_preset_templates', $templates );
}
add_action( 'gastro_core_init', 'gastro_blueprint_preset_templates' );
