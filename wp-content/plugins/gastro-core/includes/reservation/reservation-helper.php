<?php

// Get setting
function gastro_booking_setting( $key )
{
	$settings = get_option( 'booking-settings' );
	$default = gastro_booking_default_setting( $key );

	if ( is_array( $settings ) && !empty( $settings[$key] ) ) {
		return !is_numeric( $settings[$key] ) ? $settings[$key] : (int)( $settings[$key] );
	} elseif ( !empty( $default ) ) {
		return !is_numeric( $default ) ? $default : (int)( $default );
	} else {
		return null;
	}
}


// Get party size options in setting
function gastro_booking_party_size_setting( $max = true )
{
	$options = array();
	$max_val = apply_filters( 'booking_max_party', 40 );

	if ( $max ) {
		$options[''] = __( 'Any size', 'gastro-core' );
	}

	for ( $i = 1; $i <= $max_val; $i++ ) {
		$options[$i] = $i;
	}

	return $options;
}


// Get booking form party options
function gastro_booking_party_size_options()
{
	$party_size = gastro_booking_setting( 'party-size' );
	$party_size_min = gastro_booking_setting( 'party-size-min' );

	$min = empty( $party_size_min ) ? 1 : gastro_booking_setting( 'party-size-min' );
	$max = empty( $party_size ) ? apply_filters( 'booking_max_party', 40 ) : gastro_booking_setting( 'party-size' );

	for ( $i = $min; $i <= $max; $i++ ) {
		$options[$i] = $i;
	}

	return $options;
}


// Get disable dates from rules
function gastro_booking_disable_rules( $enable_branches )
{
	$first_day = (int)apply_filters( 'booking_schedule_start_day', 0 ); // 0 to start from Sunday

	$disable_weekday_rules = array();
	$disable_date_rules = array();
	$branch_disable_rules = array();

	$default_weekdays = array(
		'sunday'	=> ( 1 - $first_day == 0 ) ? 7 : 1,
		'monday'	=> 2 - $first_day,
		'tuesday'	=> 3 - $first_day,
		'wednesday'	=> 4 - $first_day,
		'thursday'	=> 5 - $first_day,
		'friday'	=> 6 - $first_day,
		'saturday'	=> 7 - $first_day,
	);

	// Get disable weekday
	$schedule_open = gastro_booking_setting( 'scheduleopen' );

	if ( is_array( $schedule_open ) ) {
		foreach ( $schedule_open as $index => $rule ) {
			$disable_weekday_rules[$index] = array();
			$disable_weekdays = $default_weekdays;

			if ( !empty( $rule['weekdays'] ) ) {
				foreach ( $rule['weekdays'] as $weekday => $value ) {
					unset( $disable_weekdays[$weekday] );
				}
			}

			if ( count( $disable_weekdays ) < 7 ) {
				foreach ( $disable_weekdays as $weekday ) {
					$disable_weekday_rules[$index][] = $weekday;
				}

				if ( isset( $rule['branches'] ) && !empty( $rule['branches'] ) ) {
					foreach ( $rule['branches'] as $branch ) {
						if ( !isset( $branch_disable_rules[$branch] ) ) {
							$branch_disable_rules[$branch] = array();
						}

						if ( isset( $branch_disable_rules[$branch]['disable_weekday'] ) && !empty( $branch_disable_rules[$branch]['disable_weekday'] ) ) {
							$branch_disable_rules[$branch]['disable_weekday'][] = $disable_weekday_rules[$index];
						} else {
							$branch_disable_rules[$branch]['disable_weekday'] = array( $disable_weekday_rules[$index] );
						}
					}
				}
			}
		}
	}

	// Get exception
	$schedule_close = gastro_booking_setting( 'scheduleclose' );
	if ( is_array( $schedule_close ) ) {
		foreach ( $schedule_close as $close_index => $rule ) {
			// Close all day
			if ( !empty( $rule['date'] ) && empty( $rule['time'] ) ) {
				$date = new DateTime( $rule['date'] );
				$disable_date_rules[$close_index] = array( $date->format( 'Y' ), ( $date->format( 'n' ) - 1 ), $date->format( 'j' ) );
			// Open in specific time
			} elseif ( !empty( $rule['date'] ) ) {
				$date = new DateTime( $rule['date'] );
				$disable_date_rules[$close_index] = array( $date->format( 'Y' ), ( $date->format( 'n' ) - 1 ), $date->format( 'j' ), 'inverted' );
			}

			if ( isset( $rule['branches'] ) && !empty( $rule['branches'] ) ) {
				foreach ( $rule['branches'] as $branch ) {
					if ( !isset( $branch_disable_rules[$branch] ) ) {
						$branch_disable_rules[$branch] = array();
					}

					if ( isset( $branch_disable_rules[$branch]['disable_date'] ) && !empty( $branch_disable_rules[$branch]['disable_date'] ) ) {
						$branch_disable_rules[$branch]['disable_date'][] = $disable_date_rules[$close_index];
					} else {
						$branch_disable_rules[$branch]['disable_date'] = array( $disable_date_rules[$close_index] );
					}
				}
			}
		}
	}

	if ( $enable_branches && !empty( $branch_disable_rules ) ) { // has branches case
		$output = array();

		foreach ( $branch_disable_rules as $key => $value ) {
			$branch_disable_weekday = isset( $value['disable_weekday'] ) && is_array( $value['disable_weekday'] ) ? $value['disable_weekday'] : array();
			$branch_disable_date = isset( $value['disable_date'] ) && is_array( $value['disable_date'] ) ? $value['disable_date'] : array();

			if ( !empty( $branch_disable_weekday ) ) {
				if ( count( $branch_disable_weekday ) > 1 ) {
					$branch_disable_weekday = call_user_func_array( 'array_intersect', $branch_disable_weekday );
				} else {
					$branch_disable_weekday = $branch_disable_weekday[0];
				}

				$output[$key] = array_merge( $branch_disable_weekday, $branch_disable_date );
			}

			$branch_disable_weekday = array();
			$branch_disable_date = array();
		}
	} else {
		if ( count( $disable_weekday_rules ) > 1 ) {
			$disable_weekday_rules = call_user_func_array( 'array_intersect', $disable_weekday_rules );
		} elseif ( !empty( $disable_weekday_rules ) ) {
			$disable_weekday_rules = $disable_weekday_rules[0];
		}

		$output = array_merge( $disable_weekday_rules, $disable_date_rules );
	}

	return $output;
}


function gastro_booking_get_picker_object()
{
	$enable_branches = gastro_booking_setting( 'enable-branches' );
	$is_manager = ( is_admin() && current_user_can( 'manage_booking' ) ) ? true : false;

	return array(
		'branchSelector' => '.js-picker-branch',
		'dateSelector' => '.js-picker-date',
		'timeSelector' => '.js-picker-time',
		'enableBranches' => $enable_branches ? 'true' : 'false',
		'dateFormat' => gastro_booking_setting( 'date-format' ),
		'timeFormat' => gastro_booking_setting( 'time-format' ),
		'scheduleOpen' => apply_filters( 'booking_localize_schedule_open', gastro_booking_setting( 'scheduleopen' ) ), // this param need to support branch
		'scheduleClose' => apply_filters( 'booking_localize_schedule_close', gastro_booking_setting( 'scheduleclose' ) ), // this param need to support branch
		'disableDates' => gastro_booking_disable_rules( $enable_branches ), // this param need to support branch
		'earlyBooking' => $is_manager ? 'true' : gastro_booking_setting( 'early-booking' ),
		'lateBooking' => $is_manager ? '' : gastro_booking_setting( 'late-booking' ),
		'timeInterval' => gastro_booking_setting( 'time-interval' ),
		'firstDay' => (int)apply_filters( 'booking_schedule_start_day', 0 ), // default Sunday
		'allowPast' => $is_manager ? 'true' : 'false' // In admin page can add past booking
	);
}


// Format date
function gastro_booking_date_format( $date )
{
	$date = mysql2date( get_option( 'date_format' ) . ' ' . get_option( 'time_format' ), $date );
	return apply_filters( 'get_the_date', $date );
}


// Change timestamp to a readable date
function gastro_booking_timestamp_format( $timestamp )
{
	$time = date( get_option( 'date_format' ) . ' ' . get_option( 'time_format' ), $timestamp );
	return gastro_booking_date_format( $time );
}


// Delete Booking
function gastro_booking_delete_post( $id )
{
	$booking = get_post( $id );

	if ( !empty( $_GET['status'] ) && 'trash' === $_GET['status'] ) {
		$screen = get_current_screen();
		if ( $screen->base == 'toplevel_page_' . RESERVATION_BOOKING_PAGE_SLUG ) {
			$output = wp_delete_post( $id, true );
		}
	} else {
		$output = wp_trash_post( $id );
	}

	return !$output ? false : true;
}


// Update booking status.
function gastro_booking_update_status( $id, $status )
{
	$booking = get_post( $id );
	if ( $booking->post_status === $status ) {
		return null;
	}

	$output_status = wp_update_post(
		array(
			'ID' => $id,
			'post_status' => $status,
			'edit_date' => current_time( 'mysql' ),
		)
	);

	$current_user = wp_get_current_user();
	$created_by = isset( $current_user->user_login ) && !empty( $current_user->user_login ) ? $current_user->user_login : '';
	$booking_meta = get_post_meta( $id, 'booking_meta_data', true );

	$output_meta = update_post_meta(
		$id,
		'booking_meta_data',
		array(
			'branch' => $booking_meta['branch'],
			'party' => $booking_meta['party'],
			'email' => $booking_meta['email'],
			'phone' => $booking_meta['phone'],
			'submission_date' => $booking_meta['submission_date'],
			'created_by' => $created_by
		)
	);

	return !$output_status || !$output_meta ? false : true;
}


// Check if field has error
function gastro_booking_field_error( $field, $errors )
{
	foreach( $errors as $error ) {
		if ( $error['field'] === $field ) {
			return true;
		}
	}

	return false;
}


// Check if field is empty
function gastro_booking_field_empty( $field )
{
	$input = isset( $_POST['gst-booking-' . $field ] ) ? $_POST['gst-booking-' . $field] : '';
	if ( ( is_string( $input ) && trim( $input ) === '' ) || ( is_array( $input ) && empty( $input ) ) ) {
		return true;
	} else {
		return false;
	}
}


// Print form class
if ( !function_exists( 'gastro_booking_field_class' ) ) :
function gastro_booking_field_class( $slug, $additional_classes = array() )
{
	$classes = empty( $additional_classes ) ? array() : $additional_classes;

	if ( ! empty( $slug ) ) {
		array_push( $classes, $slug );
	}

	$class_attr = esc_attr( join( ' ', $classes ) );
	return empty( $class_attr ) ? '' : sprintf( 'class="%s"', $class_attr );
}
endif;


// Fulter options pass from blueprint
if ( !function_exists( 'gastro_booking_options_filter' ) ) :
function gastro_booking_options_filter( $opts = array() )
{
	$item_defaults = array(
		'style' => 'inline',
		'form_style' => 'bar',
		'components' => array( 'phone', 'message', 'subscription' ),
		'force_one_column' => false,
		'full_width_button' => false,
		'button_label' => esc_html__( 'Reserve A Table', 'gastro-core' ),
		'alignment' => 'left'
	);

	$opts = array_merge( $item_defaults, $opts );
	$opts['phone_on'] = isset( $opts['phone_on'] ) ? $opts['phone_on'] : in_array( 'phone', $opts['components'] );

	if ( 'inline' !== $opts['style'] ) {
		$opts['message_on'] = isset( $opts['message_on'] ) ? $opts['message_on'] : in_array( 'message', $opts['components'] );
	} else {
		$opts['message_on'] = false;
	}

	// allow subscription only when activate plugin mailchimp for wordpress
	$opts['subscription_on'] = isset( $opts['subscription_on'] ) ? $opts['subscription_on'] : in_array( 'subscription', $opts['components'] );

	$opts['button_class'] = 'gst-form-submit';
	$opts['branch_column_class'] = 'gst-col-12';
	$opts['date_column_class'] = 'gst-col-5';
	$opts['time_column_class'] = 'gst-col-4';
	$opts['party_column_class'] = 'gst-col-3';
	$opts['name_column_class'] = 'gst-col-12';
	$opts['email_column_class'] = 'gst-col-12';
	$opts['phone_column_class'] = 'gst-col-12';
	$opts['message_column_class'] = 'gst-col-12';
	$opts['subscription_column_class'] = 'gst-col-12';
	$opts['booking_extra_class'] = 'gst-reservation-form gst-' . $opts['alignment'] . '-align';

	if ( $opts['phone_on'] ) {
		$opts['booking_extra_class'] .= ' phone-on';
	}

	if ( 'stacked' === $opts['style'] ) {
		if ( !$opts['force_one_column'] && $opts['phone_on'] ) {
			$opts['email_column_class'] = 'gst-col-6';
			$opts['phone_column_class'] = 'gst-col-6';
		} elseif ( !$opts['force_one_column'] && !$opts['phone_on'] ) {
			$opts['name_column_class'] = 'gst-col-6';
			$opts['email_column_class'] = 'gst-col-6';
		}

		if ( $opts['full_width_button'] ) {
			$opts['button_class'] .= ' full';
		}
	}

	return $opts;
}
endif;
