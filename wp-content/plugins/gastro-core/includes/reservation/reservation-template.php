<?php

// Get default setting
function gastro_booking_default_setting( $key )
{
	$default_settings = apply_filters( 'booking_default_settings' ,array(
		'enable-branches' => false,
		'date-format' => 'mmmm d, yyyy',
		'time-format' => 'h:i A',
		'time-interval' => '30',
		'admin-email-address' => get_option( 'admin_email' ),
		'reply-to-name' => get_bloginfo( 'name' ),
		'reply-to-email' => get_option( 'admin_email' ),
		'subject-pending-admin' => sprintf( __( 'New Booking created from %s', 'gastro-core' ), get_bloginfo( 'name' ) ),
		'subject-pending-user' => sprintf( __( 'Your booking at %s is now pending', 'gastro-core' ), get_bloginfo( 'name' ) ),
		'subject-confirm-user' => sprintf( __( 'Your booking at %s has been confirmed', 'gastro-core' ), get_bloginfo( 'name' ) ),
		'subject-close-user' => sprintf( __( 'Your booking at %s was not accepted', 'gastro-core' ), get_bloginfo( 'name' ) ),
		'subject-update-user' => sprintf( __( 'Update your booking at %s', 'gastro-core' ), get_bloginfo( 'name' ) ),
		'template-pending-admin' => gastro_booking_set_default_email( array(
				'title' => __( 'New booking has been created', 'gastro-core' ),
				'footer' => '{view_booking}{confirm_booking}{close_booking}',
		) ),
		'template-pending-user' => gastro_booking_set_default_email( array(
				'title' => __( 'Your booking is now pending', 'gastro-core' ),
				'subtitle' => __( 'Please give us a few moments to confirm the seat. You will receive confirmation email shortly.', 'gastro-core' )
		) ),
		'template-confirm-user' => gastro_booking_set_default_email( array(
				'title' => __( 'Your booking has been confirmed', 'gastro-core' ),
				'subtitle' => __( 'We look forward to seeing you soon.', 'gastro-core' )
		) ),
		'template-close-user' => gastro_booking_set_default_email( array(
				'title' => __( 'We are so sorry', 'gastro-core' ),
				'subtitle' => __( 'We\'re full at the time you requested. Please try to book different time.', 'gastro-core' )
		) )
	) );

	return isset( $default_settings[$key] ) ? $default_settings[$key] : null;
}


function gastro_booking_form_field( $request = null )
{
	if ( empty( $request ) ) {
		global $booking_controller;
		$request = $booking_controller->request;
	}

	// Reservation fieldset
	$reservation_field = apply_filters( 'booking_form_reservation_fields', array(
		'fields' => array(
			'date' => array(
				'title' => __( 'Date', 'gastro-core' ),
				'request_input' => empty( $request->request_date ) ? '' : $request->request_date,
				'callback' => 'gastro_booking_form_text_field',
				'callback_args' => array(
					'control_class' => 'js-picker-date'
				)
			),
			'time' => array(
				'title' => __( 'Time', 'gastro-core' ),
				'request_input' => empty( $request->request_time ) ? '' : $request->request_time,
				'callback' => 'gastro_booking_form_text_field',
				'callback_args' => array(
					'control_class' => 'js-picker-time'
				)
			),
			'party' => array(
				'title' => __( 'Party', 'gastro-core' ),
				'request_input' => empty( $request->party ) ? '' : $request->party,
				'callback' => 'gastro_booking_form_select_field',
				'callback_args' => array(
					'options' => gastro_booking_party_size_options(),
				)
			),
		),
	) );

	// Contact fieldset
	$contact_field = apply_filters( 'booking_form_contact_fields', array(
		'fields' => array(
			'name' => array(
				'title' => __( 'Name', 'gastro-core' ),
				'request_input' => empty( $request->name ) ? '' : $request->name,
				'callback' => 'gastro_booking_form_text_field',
			),
			'email' => array(
				'title' => __( 'Email', 'gastro-core' ),
				'request_input' => empty( $request->email ) ? '' : $request->email,
				'callback' => 'gastro_booking_form_text_field',
				'callback_args' => array(
					'type' => 'email',
				)
			),
			'phone' => array(
				'title' => __( 'Phone', 'gastro-core' ),
				'request_input' => empty( $request->phone ) ? '' : $request->phone,
				'callback' => 'gastro_booking_form_text_field',
				'callback_args' => array(
					'type' => 'tel',
				),
			),
			'message' => array(
				'title' => apply_filters( 'booking_message_label', __( 'Message', 'gastro-core' ) ),
				'request_input' => empty( $request->message ) ? '' : $request->message,
				'callback' => 'gastro_booking_form_textarea_field',
			),
		)
	) );

	$branch_field = apply_filters( 'booking_form_branch_field', array(), $request );

	if ( !empty( $branch_field ) ) {
		$fields = array(
			'branch' => $branch_field,
			'reservation' => $reservation_field,
			'contact' => $contact_field
		);
	} else {
		$fields = array(
			'reservation' => $reservation_field,
			'contact' => $contact_field
		);
	}

	return apply_filters( 'booking_form_default_fields', $fields );
}


// Print booking form for frontend
if ( !function_exists( 'gastro_booking_print_form' ) ) :
function gastro_booking_print_form( $args = array(), $item_opts = array() )
{
	global $booking_controller;
	$opts = gastro_booking_options_filter( $item_opts ); // filter options pass from item first

	// // Only allow the form to be displayed once on a page
	// if ( $booking_controller->form_rendered ) {
	// 	return;
	// } else {
	// 	$booking_controller->form_rendered = true;
	// }

	// Process booking request
	if ( !empty( $_POST['action'] ) && 'booking_request' === $_POST['action'] ) {
		if ( 'stdClass' === get_class( $booking_controller->request ) ) {
			require_once( RESERVATION_PLUGIN_CLASSES . 'booking-data.php' );
			$booking_controller->request = new Gastro_Booking_Data();
		}

		$booking_controller->request->insert_booking();
	}

	// Retrieve form default fields
	$get_fields = gastro_booking_form_field( $booking_controller->request );

	// Duplicate message field to reservation field
	$get_fields['reservation']['fields']['message'] = $get_fields['contact']['fields']['message'];
	unset( $get_fields['contact']['fields']['message'] );

	if ( !$opts['phone_on'] ) {
		unset( $get_fields['contact']['fields']['phone'] );
	}

	if ( !$opts['message_on'] ) {
		unset( $get_fields['reservation']['fields']['message'] );
	}

	if ( $opts['subscription_on'] ) {
		$get_fields['reservation']['fields']['subscription'] = array(
			'title' => apply_filters( 'booking_subscription_label',__( 'Subscribe to our newsletter', 'gastro-core' ) ),
			'request_input' => empty( $booking_controller->request->subscription ) ? '' : $booking_controller->request->subscription,
			'callback' => 'gastro_booking_form_checkbox_field',
		);
	}
	$fields = array();

	if ( isset( $get_fields['branch'] ) ) {
		$fields['branch'] = $get_fields['branch'];
	}

	if ( 'stacked' === $opts['style'] ) {
		$fields['contact'] = $get_fields['contact'];
		$fields['reservation'] = $get_fields['reservation'];
	} else {
		$fields['reservation'] = array(
			'fields' => array_merge( $get_fields['contact']['fields'], $get_fields['reservation']['fields'] )
		);
	}

	ob_start();
	?>
	<div class="<?php echo esc_attr( $opts['booking_extra_class'] );?>">
		<?php if ( $booking_controller->request->request_inserted === true ) : ?>
		<div class="gst-reservation-message" id="booking-submit-form">
			<p><?php esc_html_e( 'Thank you. Your booking request is being processed. You will get booking confirmation email soon.', 'gastro-core' ); ?></p>
		</div>
		<?php else : ?>
		<form id="booking-submit-form" class="gst-p-border-border" method="POST" action="<?php echo get_permalink(); ?>#booking-submit-form">
			<?php foreach( $fields as $fieldset => $contents ) :
				$fieldset_classes = isset( $contents['callback_args']['classes'] ) ? $contents['callback_args']['classes'] : array();
				$fieldset_classes[] = 'gst-row';
			?>
			<fieldset <?php echo gastro_booking_field_class( $fieldset, $fieldset_classes ); ?>>
				<?php
					foreach( $contents['fields'] as $slug => $field ) {
						$args = isset( $field['callback_args'] )  && is_array( $field['callback_args'] ) ? $field['callback_args'] : array();

						// Styling from theme and added up some variable from theme argument
						$args['has_label'] = false;
						$args['classes'] = isset( $args['classes'] ) && is_array( $args['classes'] ) ? $args['classes'] : array();
						$args['classes'][] = 'gst-reservation-' . esc_attr( $slug );
						$args['placeholder'] = $field['title'];

						if ( 'stacked' === $opts['style'] ) {
							$args['classes'][] = $opts[$slug . '_column_class'];
						}

						if ( 'inline' === $opts['style'] && 'line' !== $opts['form_style'] ) {
							$args['has_container'] = false;
						} else {
							$args['has_container'] = true;
							$args['classes'][] = 'form-container';
						}

						if ( !empty( $field['required'] ) ) {
							$args = array_merge( $args, array( 'required' => $field['required'] ) );
						}

						if ( 'branch' === $slug ) {
							echo '<div class="gst-reservation-branch-label">' . esc_html__( 'Booking At:' ,'gastro-core' ) . '</div>';
						}

						call_user_func( $field['callback'], $slug, $field['title'], $field['request_input'], $args );
					}
				?>
			</fieldset>
			<?php endforeach; ?>
			<?php
				echo apply_filters( 'booking_form_submit_button', sprintf(
					'<button class="' . esc_attr( $opts['button_class'] ) . '" type="submit">%s</button>',
					$opts['button_label']
				) );
			?>
			<input type="hidden" name="action" value="booking_request">
			<input class="js-picker-object" type="hidden" value="<?php echo esc_attr( json_encode( gastro_booking_get_picker_object() ) ); ?>" />
		</form>
		<?php endif; ?>
	</div>

	<?php
	return ob_get_clean();
}
endif;


// Print admin booking form modal.
if ( !function_exists( 'gastro_booking_admin_print_form' ) ) :
function gastro_booking_admin_print_form()
{
	global $booking_controller;
	$fields = gastro_booking_form_field( $booking_controller->request );

	// Add Booking status and notification fields to booking form
	$booking_statuses = array();
	foreach ( $booking_controller->booking_statuses as $status => $args ) {
		$booking_statuses[$status] = $args['label'];
	}

	$fields['admin'] = array(
		'fields' => array(
			'status' => array(
				'title' => __( 'Booking Status', 'gastro-core' ),
				'request_input' => empty( $booking_controller->request->post_status ) ? '' : $booking_controller->request->post_status,
				'callback' => 'gastro_booking_form_select_field',
				'callback_args' => array(
					'options' => $booking_statuses,
				)
			),
			'notification' => array(
				'title' => __( 'Send notification email to user', 'gastro-core' ),
				'request_input' => empty( $booking_controller->request->send_notification ) ? false : $booking_controller->request->send_notification,
				'callback' => 'gastro_booking_form_checkbox_field',
			),
		),
	);

	ob_start();
	?>
		<?php foreach( $fields as $fieldset => $contents ) : ?>
		<fieldset class="<?php echo esc_attr( $fieldset ); ?>">
			<?php
				foreach( $contents['fields'] as $slug => $field ) {
					$args = empty( $field['callback_args'] ) ? null : $field['callback_args'];
					call_user_func( $field['callback'], $slug, $field['title'], $field['request_input'], $args );
				}
			?>
		</fieldset>
		<?php endforeach;
	return ob_get_clean();
}
endif;


// Text input form
if ( !function_exists( 'gastro_booking_form_text_field' ) ) :
function gastro_booking_form_text_field( $slug, $title = '', $value = '', $args = array() )
{
	$type = !empty( $args['type'] ) ? $args['type'] : 'text';
	$control_class = isset( $args['control_class'] ) ? $args['control_class'] : '';
	$classes = isset( $args['classes'] ) ? $args['classes'] : array();
	$required = isset( $args['required'] ) && $args['required'] ? ' required aria-required="true"' : '';
	$has_container = isset( $args['has_container'] ) ? $args['has_container'] : true;
	$has_label = isset( $args['has_label'] ) ? $args['has_label'] : true;
	$placeholder = isset( $args['placeholder'] ) && !empty( $args['placeholder'] ) ? ' placeholder="' . esc_attr( $args['placeholder'] ) .'"' : '';

	if ( $has_container ) : ?>
		<?php echo gastro_booking_form_error( $slug ); ?>
		<div <?php echo gastro_booking_field_class( $slug, $classes ); ?>>
			<?php if ( $has_label && !empty( $title ) ) : ?>
				<label for="gst-booking-<?php echo esc_attr( $slug ); ?>">
					<?php echo esc_html( $title ); ?>
				</label>
			<?php endif; ?>
			<input<?php echo !empty( $control_class ) ? ' class="' . esc_attr( $control_class ) . '"' : ''; ?> type="<?php echo esc_attr( $type ); ?>" name="gst-booking-<?php echo esc_attr( $slug ); ?>" id="gst-booking-<?php echo esc_attr( $slug ); ?>" value="<?php echo esc_attr( $value ); ?>"<?php echo gastro_core_escape_content( $required ); ?><?php echo gastro_core_escape_content( $placeholder ); ?>>
		</div>
	<?php else : ?>
		<?php echo gastro_booking_form_error( $slug ); ?>
		<input<?php echo !empty( $control_class ) ? ' class="' . esc_attr( $control_class ) . '"' : ''; ?> <?php echo gastro_booking_field_class( $slug, $classes ); ?> type="<?php echo esc_attr( $type ); ?>" name="gst-booking-<?php echo esc_attr( $slug ); ?>" id="gst-booking-<?php echo esc_attr( $slug ); ?>" value="<?php echo esc_attr( $value ); ?>"<?php echo gastro_core_escape_content( $required ); ?><?php echo gastro_core_escape_content( $placeholder ); ?>>
	<?php endif;
}
endif;


// Textarea form
if ( !function_exists( 'gastro_booking_form_textarea_field' ) ) :
function gastro_booking_form_textarea_field( $slug, $title = '', $value = '', $args = array() )
{
	$value = preg_replace( '/\<br(\s*)?\/?\>/i', '', $value );
	$control_class = isset( $args['control_class'] ) ? $args['control_class'] : '';
	$classes = isset( $args['classes'] ) ? $args['classes'] : array();
	$required = isset( $args['required'] ) && $args['required'] ? ' required aria-required="true"' : '';
	$has_label = isset( $args['has_label'] ) ? $args['has_label'] : true;
	$placeholder = isset( $args['placeholder'] ) && !empty( $args['placeholder'] ) ? ' placeholder="' . esc_attr( $args['placeholder'] ) .'"' : '';
	?>

	<?php echo gastro_booking_form_error( $slug ); ?>
	<div <?php echo gastro_booking_field_class( $slug, $classes ); ?>>
		<?php if ( $has_label && !empty( $title ) ) : ?>
			<label for="gst-booking-<?php echo esc_attr( $slug ); ?>">
				<?php echo esc_html( $title ); ?>
			</label>
		<?php endif; ?>
		<textarea<?php echo !empty( $control_class ) ? ' class="' . esc_attr( $control_class ) . '"' : ''; ?> name="gst-booking-<?php echo esc_attr( $slug ); ?>" id="gst-booking-<?php echo esc_attr( $slug ); ?>"<?php echo gastro_core_escape_content( $required ); ?> <?php echo gastro_core_escape_content( $placeholder ); ?>><?php echo gastro_core_escape_content( $value ); ?></textarea>
	</div>

	<?php
}
endif;


// Select form
if ( !function_exists( 'gastro_booking_form_select_field' ) ) :
function gastro_booking_form_select_field( $slug, $title = '', $value = '', $args = array() )
{
	$options = is_array( $args['options'] ) ? $args['options'] : array();
	$control_class = isset( $args['control_class'] ) ? $args['control_class'] : '';
	$classes = isset( $args['classes'] ) ? $args['classes'] : array();
	$required = isset( $args['required'] ) && $args['required'] ? ' required aria-required="true"' : '';
	$has_container = isset( $args['has_container'] ) ? $args['has_container'] : true;
	$has_label = isset( $args['has_label'] ) ? $args['has_label'] : true;

	if ( $has_container ) : ?>
		<?php echo gastro_booking_form_error( $slug ); ?>
		<div <?php echo gastro_booking_field_class( $slug, $classes ); ?>>
			<?php if ( $has_label && !empty( $title ) ) : ?>
				<label for="gst-booking-<?php echo esc_attr( $slug ); ?>">
					<?php echo esc_html( $title ); ?>
				</label>
			<?php endif; ?>
			<select<?php echo !empty( $control_class ) ? ' class="' . esc_attr( $control_class ) . '"' : ''; ?> name="gst-booking-<?php echo esc_attr( $slug ); ?>" id="gst-booking-<?php echo esc_attr( $slug ); ?>"<?php echo gastro_core_escape_content( $required ); ?>>
				<?php foreach ( $options as $opt_value => $opt_label ) : ?>
				<option value="<?php echo esc_attr( $opt_value ); ?>" <?php selected( $opt_value, $value ); ?>><?php echo esc_attr( $opt_label ); ?></option>
				<?php endforeach; ?>
			</select>
		</div>
	<?php else : ?>
		<?php echo gastro_booking_form_error( $slug ); ?>
		<select<?php echo !empty( $control_class ) ? ' class="' . esc_attr( $control_class ) . '"' : ''; ?> <?php echo gastro_booking_field_class( $slug, $classes ); ?> name="gst-booking-<?php echo esc_attr( $slug ); ?>" id="gst-booking-<?php echo esc_attr( $slug ); ?>"<?php echo gastro_core_escape_content( $required ); ?>>
			<?php foreach ( $options as $opt_value => $opt_label ) : ?>
			<option value="<?php echo esc_attr( $opt_value ); ?>" <?php selected( $opt_value, $value ); ?>><?php echo esc_attr( $opt_label ); ?></option>
			<?php endforeach; ?>
		</select>
	<?php endif;
}
endif;


// Checkbox form
if ( !function_exists( 'gastro_booking_form_checkbox_field' ) ) :
function gastro_booking_form_checkbox_field( $slug, $title = '', $value = false, $args = array() )
{
	$value = (bool)$value;
	$control_class = isset( $args['control_class'] ) ? $args['control_class'] : '';
	$classes = isset( $args['classes'] ) ? $args['classes'] : array();
	?>

	<?php echo gastro_booking_form_error( $slug ); ?>
	<div <?php echo gastro_booking_field_class( $slug, $classes ); ?>>
		<label>
			<input<?php echo !empty( $control_class ) ? ' class="' . esc_attr( $control_class ) . '"' : ''; ?> type="checkbox" name="gst-booking-<?php echo esc_attr( $slug ); ?>" value="1"<?php checked( $value ); ?>>
			<?php echo esc_html( $title ); ?>
		</label>
	</div>
	<?php
}
endif;


// Print form error
if ( !function_exists( 'gastro_booking_form_error' ) ) :
function gastro_booking_form_error( $field )
{
	global $booking_controller;
	if ( !empty( $booking_controller->request ) && !empty( $booking_controller->request->validation_errors ) ) {
		foreach ( $booking_controller->request->validation_errors as $error ) {
			if ( $field === $error['field'] ) {
				echo '<div class="booking-error-note">' . $error['message'] . '</div>';
			}
		}
	}
}
endif;


// Tag for email template in settings
if ( !function_exists( 'gastro_booking_email_template_tag_description' ) ) :
function gastro_booking_email_template_tag_description()
{
	$args = array(
		'{email}'			=> __( 'Email of the user', 'gastro-core' ),
		'{name}'			=> __( 'Name of the user', 'gastro-core' ),
		'{party}'			=> __( 'Number of booking party size', 'gastro-core' ),
		'{date}'			=> __( 'Booking date and time', 'gastro-core' ),
		'{phone}'			=> __( 'Phone number of the user', 'gastro-core' ),
		'{message}'			=> __( 'Message (special request) from user', 'gastro-core' ),
		'{branch}'			=> __( 'Branch', 'gastro-core' ),
		'{site_name}'		=> __( 'The name of this site', 'gastro-core' ),
		'{site_link}'		=> __( 'The link of this site', 'gastro-core' ),
		'{view_booking}'	=> sprintf( __( 'A button to booking admin page. %s*Beware:%s use with only admin notification', 'gastro-core' ), '<strong style="color:red;">', '</strong>' ),
		'{confirm_booking}'	=> sprintf( __( 'A button to confirm booking. %s*Beware:%s use with only admin notification', 'gastro-core' ), '<strong style="color:red;">', '</strong>' ),
		'{close_booking}'		=> sprintf( __( 'A button to close booking. %s*Beware:%s use with only admin notification', 'gastro-core' ), '<strong style="color:red;">', '</strong>' )
	);

	$output = '';

	foreach ( $args as $title => $description ) {
		$output .= '<div class="bp-admin-html-tag">';
		$output .= 	'<div class="bp-admin-html-tag-title">' . esc_html( $title ) . '</div>';
		$output .=	'<div class="bp-admin-html-tag-description">' . $description . '</div>';
		$output .= '</div>';
	}

	return $output;
}
endif;


function gastro_booking_process_template( $message, $booking )
{
	if ( is_object( $booking ) && !empty( $message ) ) {
		$template_tags = array(
			'{email}'			=> $booking->email,
			'{name}'			=> $booking->name,
			'{party}'			=> $booking->party,
			'{date}'			=> gastro_booking_date_format( $booking->date ),
			'{phone}'			=> $booking->phone,
			'{message}'			=> $booking->message,
			'{view_booking}'	=>
				'<a href="' . admin_url( 'admin.php?page=' . RESERVATION_BOOKING_PAGE_SLUG . '&status=pending' ) . '" style="display:inline-block; line-height:1; padding:0.8em 1.5em; margin:0 0.15em; text-decoration:none; color:#555; background-color:#dadada;">' . __( 'View', 'gastro-core' ) . '</a>',
			'{confirm_booking}'	=>
				'<a href="' . admin_url( 'admin.php?page=' . RESERVATION_BOOKING_PAGE_SLUG . '&shortcut=confirm&booking=' . esc_attr( $booking->ID ) ) . '" style="display:inline-block; line-height:1; padding:0.8em 1.5em; margin:0 0.15em; text-decoration:none; color:#fff; background-color:#7ad03a;">' . __( 'Confirm', 'gastro-core' ) . '</a>',
			'{close_booking}'	=>
				'<a href="' . admin_url( 'admin.php?page=' . RESERVATION_BOOKING_PAGE_SLUG . '&shortcut=close&booking=' . esc_attr( $booking->ID ) ) . '" style="display:inline-block; line-height:1; padding:0.8em 1.5em; margin:0 0.15em; text-decoration:none; color:#fff; background-color:#cc6863;">' . __( 'Close', 'gastro-core' ) . '</a>',
			'{site_name}'		=> get_bloginfo( 'name' ),
			'{site_link}'		=> '<a href="' . home_url( '/' ) . '">' . get_bloginfo( 'name' ) . '</a>',
		);

		if ( isset( $booking->branch ) && !empty( $booking->branch ) ) {
			$template_tags['{branch}'] = get_the_title( $booking->branch );
		}

		foreach ( $template_tags as $key => $value ) {
			$message = str_replace( $key, $value, $message );
		}

		return $message;
	} else {
		return '';
	}
}


function gastro_booking_set_email_subject( $booking, $status = 'pending', $role = 'user' )
{
	$subject = gastro_booking_setting( 'subject-' . $status . '-' . $role );
	return gastro_booking_process_template( $subject, $booking );
}


if ( !function_exists( 'gastro_booking_set_default_email' ) ) :
function gastro_booking_set_default_email( $args )
{
	$defaults = array(
		'title' => '',
		'subtitle' => '',
		'footer' => ''
	);
	$args = array_merge( $defaults, $args );

	$logo = get_theme_mod( 'logo' );
	$email_logo = '';
	if ( !empty( $logo ) ) {
		$email_logo .= '<table style="width:100%;">';
		$email_logo .= 	'<tbody>';
		$email_logo .= 		'<tr>';
		$email_logo .= 			'<td style="text-align:center;">';
		$email_logo .= 				'<a target="_blank" href="' . home_url( '/' ) . '">';
		$email_logo .= 					'<img style="max-width: 400px;" src="' . $logo . '" alt="logo" >';
		$email_logo .= 				'</a>';
		$email_logo .=			'</td>';
		$email_logo .= 		'</tr>';
		$email_logo .= 	'</tbody>';
		$email_logo .= '</table>';
	}

	$email_subtitle = '';
	if ( !empty( $args['subtitle'] ) ) {
		$email_subtitle .= '<tr>';
		$email_subtitle .=	'<td style="padding:1em 10% 2.5em; font-size:0.8em; text-align:center; color:#363636;">' . $args['subtitle'] . '</td>';
		$email_subtitle .= '</tr>';
	}

	$email_footer = '';
	if ( !empty( $args['footer'] ) ) {
		$email_footer .= '<tr>';
		$email_footer .= 	'<td class="button" style="font-size:0.8em; padding:0 10% 2.5em; text-align:center;">' . $args['footer'] . '</td>';
		$email_footer .= '</tr>';
	}

	ob_start();
	?>
	<body offset="0" style="width:100%; height:100%; line-height:1.5em; padding:0; margin:0; background-color:#eee; color:#6f6f6f; font-family:\'Roboto\', \'Arial\', \'sans-serif\'; font-weight:400; font-size:18px; -webkit-font-smoothing:antialiased; -webkit-text-size-adjust:none;">
		<table style="width:90%; max-width:700px; margin:0 auto;">
			<tbody>
				<tr>
					<td><?php echo gastro_core_escape_content( $email_logo ); ?>
						<table style="width:100%; background-color:#fbc140;">
							<tbody>
								<tr>
									<td style="line-height:1.2em; padding:1.5em 10% 0.5em; font-size:2.2em; text-align:center; color:#000;">
										<strong><?php echo gastro_core_escape_content( $args['title'] ); ?></strong>
									</td>
								</tr>
								<?php echo gastro_core_escape_content( $email_subtitle ); ?>
							</tbody>
						</table>
						<table style="width:100%; line-height:2em; background-color:#ffffff;">
							<tbody>
								<tr>
									<td style="padding-top:2em; font-size:1.2em; text-align:center;">
										<strong><?php esc_html_e( 'Booking Detail', 'gastro-core' ); ?></strong>
									</td>
								</tr>
								<tr>
									<td style="padding:1em 10% 2em; text-align:left;">
										<?php esc_html_e( 'Name : ', 'gastro-core' ); ?>{name}<br>
										<?php esc_html_e( 'Party : ', 'gastro-core' ); ?>{party}<?php esc_html_e( ' people', 'gastro-core' ); ?><br>
										<?php esc_html_e( 'Date : ', 'gastro-core' ); ?>{date}<br>
										<?php esc_html_e( 'Phone : ', 'gastro-core' ); ?>{phone}<br>
										<?php esc_html_e( 'Message : ', 'gastro-core' ); ?>{message}
									</td>
								</tr><?php echo gastro_core_escape_content( $email_footer ); ?>
								<tr>
									<td style="padding:0.5em 10%; font-size:0.8em; text-align:center; color:#fff; background-color:#363636;">
										© 2017 {site_link} <?php esc_html_e( 'All Rights Reserved', 'gastro-core' ); ?>
									</td>
								</tr>
							</tbody>
						</table>
					</td>
				</tr>
			</tbody>
		</table>
	</body>
	<?php
	return ob_get_clean();
}
endif;


function gastro_booking_set_email_content( $booking, $status = 'pending', $role = 'user' )
{
	$content = gastro_booking_setting( 'template-' . $status . '-' . $role );
	$template = '<head>
					<meta name="viewport" content="width=device-width, initial-scale=1" />
					<title>Booking Notification</title>
					<style type="text/css">
						img {
							max-width: 600px;
							outline: none;
							text-decoration: none;
							-ms-interpolation-mode: bicubic;
						}
						a img {
							border: none;
						}
						table {
							border-collapse: collapse !important;
						}
						#outlook a {
							padding: 0;
						}
						.ReadMsgBody {
							width: 100%;
						}
						.ExternalClass {
							width: 100%;
						}
						.backgroundTable {
							margin:0 auto;
							padding: 0;
							width:100% !important;
						}
						table td {
							border-collapse: collapse;
						}
						.ExternalClass * {
							line-height: 115%;
						}
						a {
							color: #fbc140;
							text-decoration: none;
						}
					</style>
					<style type="text/css" media="screen">
							@media screen {
								@import url(https://fonts.googleapis.com/css?family=Roboto:400,700);
							}
					</style>
					<style type="text/css" media="only screen and (max-width: 480px)">
						@media only screen and (max-width: 480px) {
							body {
								font-size: 14px !important;
							}
							.button > a {
								display: block !important;
								max-width: 100%;
								padding: 1em !important;
								margin: 1em auto !important;
							}
						}
					</style>
				</head>';

	$template .= 	wpautop( $content );
	$email_template = apply_filters( 'default_email_template', $template, $content );

	return gastro_booking_process_template( $template, $booking );
}


if ( !function_exists( 'gastro_booking_set_email_header' ) ) :
function gastro_booking_set_email_header( $name = null, $email = null )
{
	$name = empty( $name ) ? gastro_booking_setting( 'reply-to-name' ) : $name;
	$email = empty( $email ) ? gastro_booking_setting( 'reply-to-email' ) : $email;
	$output = "From: " . stripslashes_deep( html_entity_decode( $name ), ENT_COMPAT, 'UTF-8' ) . " <" . get_option( 'admin_email' ) . ">\r\n";
	$output .= "Reply-To: =?utf-8?Q?" . quoted_printable_encode( $name ) . "?= <" . $email . ">\r\n";
	$output .= "Content-Type: text/html; charset=utf-8\r\n";

	return $output;
}
endif;
