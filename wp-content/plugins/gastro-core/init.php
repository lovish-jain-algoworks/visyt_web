<?php

/**
 * Plugin Name: Gastro Core
 * Plugin URI: http://www.twisttheme.com
 * Description: GastroCore
 * Author: Twisttheme
 * Version: 1.1.0
 * Author URI: http://www.twisttheme.com
 */

define( 'CORE_PLUGIN_DIR', untrailingslashit( plugin_dir_path( __FILE__ ) ) );
define( 'CORE_PLUGIN_URL', untrailingslashit( plugins_url( basename( plugin_dir_path( __FILE__ ) ), basename( __FILE__ ) ) ) );

require_once CORE_PLUGIN_DIR . '/lib/gastro-core.php';

// Plugins
require_once CORE_PLUGIN_DIR . '/classes/gastro-delivery.php';
require_once CORE_PLUGIN_DIR . '/classes/gastro-food-menu.php';
require_once CORE_PLUGIN_DIR . '/classes/gastro-recipe.php';

// Reservation
require_once CORE_PLUGIN_DIR . '/classes/gastro-reservation.php';
require_once CORE_PLUGIN_DIR . '/includes/reservation/reservation-helper.php';
require_once CORE_PLUGIN_DIR . '/includes/reservation/reservation-template.php';

require_once CORE_PLUGIN_DIR . '/classes/modules/food-module.php';
require_once CORE_PLUGIN_DIR . '/classes/modules/shortcode-module.php';

require_once CORE_PLUGIN_DIR . '/includes/blueprint-template.php';
require_once CORE_PLUGIN_DIR . '/includes/widgets.php';

function gastro_core_version()
{
	return '1.1.0';
}

/**
 * Initialize Gastro Core plugin
 **/
add_action( 'init', 'gastro_core_init', 9 );


/**
 * Load plugin textdomain.
 */
function myplugin_load_textdomain() {
	load_plugin_textdomain( 'gastro-core', false, basename( dirname( __FILE__ ) ) . '/languages' );
}
add_action( 'plugins_loaded', 'myplugin_load_textdomain' );


function gastro_core_register_modules( $gastro_core )
{
	$gastro_core->register_module( new Gastro_Shortcode_Module( $gastro_core ) );
	$gastro_core->register_module( new Gastro_Food_Module( $gastro_core ) );
}
add_action( 'gastro_core_init', 'gastro_core_register_modules' );


function gastro_core_plugins_init()
{
	// Register reservation
	global $booking_controller;
	$booking_controller = new Gastro_Reservation();

	// Register food menu
	new Gastro_Food_Menu();

	// Register recipe
	new Gastro_Recipe();

	// Register Delivery Plugin
	if ( gastro_core_woocommerce_activated() ) {
		new Gastro_Delivery();
	}
}
add_action( 'init', 'gastro_core_plugins_init', 9 );


function gastro_core_importer_options( $options )
{
	$options['prefix'] = 'gst';
	$options['asset_path'] = CORE_PLUGIN_DIR . '/lib/assets';

	return $options;
}
add_filter( 'gastro_core_importer_options', 'gastro_core_importer_options' );
