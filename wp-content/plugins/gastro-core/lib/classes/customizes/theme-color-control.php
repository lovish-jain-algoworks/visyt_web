<?php

class Gastro_Theme_Color_Control extends WP_Customize_Control
{
	public function __construct( $manager, $id, $args = array(), $options = array() )
	{

		parent::__construct( $manager, $id, $args );
	}

	public function render_content()
	{
		$output = '';

		if ( !empty( $this->label ) ) {
			$output .= '<span class="customize-control-title">' . esc_html( $this->label ) . '</span>';
		}

		if ( !empty( $this->description ) ) {
			$output .= '<span class="description customize-control-description">' . esc_html( $this->description ) . '</span>';
		}

		$output .= '<div class="customize-control-content customize-control-preset-color">';

		foreach ( $this->color_presets() as $key => $preset ) {
			$output .= '<label class="js-input-label  bp-radio-image" data-preset="' . esc_attr( $preset ) . '">';
			$output .=   '<span class="icon icon-customize-color-' . esc_attr( $key ) . '"></span>';
			$output .=   '<input type="radio" />';
			$output .= '</label>';
		}

		$output .= '</div>';

		$output .= '<div class="customize-control-content customize-control-color-list">';
		$color_names = $this->color_names();

		foreach ( $this->settings as $key => $setting ) {
			$index = array_search( $key, array_keys( $this->settings ) );

			if ( 0 == $index ) {
				$output .= '<div class="customize-control-color-section customize-cotrol-color-section--brand">';
				$output .=    '<div class="customize-control customize-control-twist-section">';
				$output .=        '<span class="customize-control-title">' . esc_html__( 'Brand Color', 'gastro-core' ) . '</span>';
				$output .=    '</div>';
			} elseif ( 2 == $index ) {
				$output .= '</div><div class="customize-control-color-section customize-cotrol-color-section--light">';
				$output .=    '<div class="customize-control customize-control-twist-section">';
				$output .=        '<span class="customize-control-title">' . esc_html__( 'Light Scheme', 'gastro-core' ) . '</span>';
				$output .=    '</div>';
			} elseif ( 7 == $index ) {
				$output .= '</div><div class="customize-control-color-section customize-cotrol-color-section--dark">';
				$output .=    '<div class="customize-control customize-control-twist-section">';
				$output .=        '<span class="customize-control-title">' . esc_html__( 'Dark Scheme', 'gastro-core' ) . '</span>';
				$output .=    '</div>';
			} elseif ( 12 == $index ) {
				$output .= '</div><div class="customize-control-color-section customize-cotrol-color-section--custom">';
				$output .=    '<div class="customize-control customize-control-twist-section">';
				$output .=        '<span class="customize-control-title">' . esc_html__( 'Custom Color', 'gastro-core' ) . '</span>';
				$output .=    '</div>';
			}

			$output .= '<span class="customize-control-title">' . esc_html( $color_names[$setting->id] ) . '</span>';
			$output .= '<input type="text" class="js-customize-color" ' . gastro_core_escape_content( $this->get_link( $key ) ) . ' data-default-color="' . esc_attr( $setting->default ) . '" />';

			if ( 14 == $index ) {
				$output .= '</div>';
			}
		}

		$output .= '</div>';

		echo gastro_core_escape_content( $output );
	}

	protected function color_presets()
	{
		return array(
			'default' => '#f8c12e,#260000,#676767,#1d1d1d,#ffffff,#f7f7f7,#eeeeee,#cdcdcd,#ffffff,#1d1d1d,#000000,#333333,#fafafa,#363636'
		);
	}

	protected function color_names()
	{
		return array(
			'bp_color_1' => esc_html__( 'Primary Brand', 'gastro-core' ),
			'bp_color_2' => esc_html__( 'Secondary Brand', 'gastro-core' ),
			'bp_color_3' => esc_html__( 'Primary Text', 'gastro-core' ),
			'bp_color_4' => esc_html__( 'Secondary Text', 'gastro-core' ),
			'bp_color_5' => esc_html__( 'Primary Background', 'gastro-core' ),
			'bp_color_6' => esc_html__( 'Secondary Background', 'gastro-core' ),
			'bp_color_7' => esc_html__( 'Border', 'gastro-core' ),
			'bp_color_8' => esc_html__( 'Primary Text', 'gastro-core' ),
			'bp_color_9' => esc_html__( 'Secondary Text', 'gastro-core' ),
			'bp_color_10' => esc_html__( 'Primary Background', 'gastro-core' ),
			'bp_color_11' => esc_html__( 'Secondary Background', 'gastro-core' ),
			'bp_color_12' => esc_html__( 'Border', 'gastro-core' ),
			'bp_color_13' => esc_html__( 'Custom 1 Color', 'gastro-core' ),
			'bp_color_14' => esc_html__( 'Custom 2 Color', 'gastro-core' ),
		);
	}
}
