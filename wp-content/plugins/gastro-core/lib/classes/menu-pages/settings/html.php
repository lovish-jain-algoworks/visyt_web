<?php

class Gastro_Core_Admin_Setting_Html extends Gastro_Core_Admin_Setting
{
	public function render_setting()
	{
		$this->render_description();
		echo gastro_core_escape_content( $this->html );
	}
}
