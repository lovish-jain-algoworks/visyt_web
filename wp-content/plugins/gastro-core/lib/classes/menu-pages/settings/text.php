<?php

class Gastro_Core_Admin_Setting_Text extends Gastro_Core_Admin_Setting
{
	public function render_setting()
	{
		$class = ( isset( $this->class ) && !empty( $this->class ) ) ? ' class="' . esc_attr( $this->class ) . '"' : '';
		$placeholder = ( isset( $this->placeholder ) && !empty( $this->placeholder ) ) ? ' placeholder="' . esc_attr( $this->placeholder ) . '"' : '';
		?>

		<input<?php echo gastro_core_escape_content( $class ); ?> name="<?php echo esc_attr( $this->input_name ); ?>" type="text" id="<?php echo esc_attr( $this->input_name ); ?>" value="<?php echo gastro_core_escape_content( $this->value ); ?>"<?php echo gastro_core_escape_content( $placeholder ); ?>/>

		<?php
		$this->render_description();
	}
}
