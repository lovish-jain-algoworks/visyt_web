<?php

class Gastro_Widget_Module extends Gastro_Base_Module
{
	const API_WIDGET_SIDEBAR_CREATE = 'widget/sidebar-create';
	const API_WIDGET_SIDEBAR_DELETE = 'widget/sidebar-delete';

	protected $sidebar_names;

	public function __construct( $gastro_core, $options = array() )
	{
		parent::__construct( $gastro_core, $options );

		$this->sidebar_names = array();
	}

	public function get_name()
	{
		return 'widget';
	}

	public function start()
	{
		if ( function_exists( 'gastro_get_title' ) ) {
			$title = gastro_get_title( array( 'size' => 'h3' ), false );
		} else {
			$title = array(
				'prefix' => '',
				'suffix' => ''
			);
		}

		$prefix = isset( $title['prefix'] ) ? $title['prefix'] : '';
		$suffix = isset( $title['suffix'] ) ? $title['suffix'] : '';

		$sidebar = array(
			'class' => 'gastro_theme js-dynamic-sidebar',
			'description' => esc_html__( 'Custom Widget', 'gastro-core' ),
			'before_title' => $prefix,
			'after_title' => $suffix
		);

		$sidebar['class'] = 'gastro_core-dynamic js-dynamic-sidebar';
		$dynamic_sidebars = get_option( 'gastro_core_widget_dynamic_sidebars', array() );

		foreach ( $dynamic_sidebars as $sidebar_name ) {
			$sidebar['name'] = $sidebar_name;
			$sidebar['id'] = $this->process_sidebar_name( $sidebar_name );

			$this->widget_register_sidebar( $sidebar );
		}

		add_action( 'admin_enqueue_scripts', array( $this, 'widgets_admin_enqueue_scripts' ) );
		add_action( 'wp_ajax_gastro_core_widget_api', array( $this, 'widget_api' ) );

		// Enable shortcode in widget_text
		add_filter( 'widget_text', 'do_shortcode' );
		// Add extra optiom to WP existing widget
		add_filter( 'in_widget_form', array( $this, 'add_menu_widget_extra_options' ), 10, 3 );
		// Save extra options for menu widget
		add_filter( 'widget_update_callback', array( $this, 'save_widget_menu_extra_options' ), 10, 2 );
		// Add class to menu widget according to the extra options
		add_filter( 'dynamic_sidebar_params', array( $this, 'widget_menu_extra_control' ) );
	}

	public function widgets_admin_enqueue_scripts( $hook )
	{
		if ( $hook !== 'widgets.php' ) {
			return;
		}

		$options = apply_filters( 'blueprint_options', null );

		wp_enqueue_media();
		do_action( 'gastro_core_widgets_scripts', array(
			'jquery',
			'underscore',
			'backbone'
		), $options, gastro_core_translation() );
	}

	public function widget_register_sidebar( $sidebar, $theme_sidebar = false )
	{
		if ( false === isset( $sidebar['name'] ) ) {
			return false;
		}

		if ( false === ( $theme_sidebar || $this->is_valid_sidebar_name( $sidebar['name'] ) ) ) {
			return false;
		}


		register_sidebar( $sidebar );

		if ( !$theme_sidebar ) {
			$this->sidebar_names[] = $sidebar['name'];
		}
	}

	public function widget_api()
	{
		if ( false === ( isset( $_POST['api_action'] ) && isset( $_POST['sidebar_name'] ) ) ) {
			return $this->api_error_response( array(
				'error' => esc_html__( 'bad_request', 'gastro-core' ),
				'message' => esc_html__( 'Bad Request', 'gastro-core' )
			) );
		}

		$api_action = $_POST['api_action'];
		$sidebar_name = $_POST['sidebar_name'];

		if ( 'create' === $api_action ) {
			$error = $this->widget_create_sidebar( $sidebar_name );

			if ( $error ) {
				$this->api_error_response( array(
					'api_action' => $api_action,
					'sidebar_name' => $sidebar_name,
					'error' => $error['error'],
					'message' => $error['message']
				) );
			} else {
				wp_send_json_success( array( 'api_action' => $api_action, 'result' => $sidebar_name ) );
			}
		} else if ( 'delete' === $api_action ) {
			$error = $this->widget_delete_sidebar( $sidebar_name );

			if ( $error ) {
				$this->api_error_response(array(
					'api_action' => $api_action,
					'sidebar_name' => $sidebar_name,
					'error' => $error['error'],
					'message' => $error['message']
				));
			} else {
				wp_send_json_success( array( 'api_action' => $api_action, 'result' => $sidebar_name ) );
			}
		}
	}

	public function widget_create_sidebar( $sidebar_name )
	{
		$error = null;

		if ( $this->is_valid_sidebar_name( $sidebar_name ) ) {
			$this->sidebar_names[] = $sidebar_name;

			$result = update_option( 'gastro_core_widget_dynamic_sidebars', $this->sidebar_names, true );

			if ( !$result ) {
				$error = array(
					'error' => esc_html__( 'update_option_failed', 'gastro-core' ),
					'message' => esc_html__( 'Failed to update sidebar', 'gastro-core' )
				);
			}

			return $result;
		} else {
			$error = array(
				'error' => esc_html__( 'invalid_sidebar_name', 'gastro-core' ),
				'message' => esc_html__( 'Please try a different sidebar name', 'gastro-core' )
			);
		}

		return $error;
	}

	public function widget_delete_sidebar( $sidebar_name )
	{
		$error = null;

		$is_theme_sidebar = $this->is_theme_sidebar( $sidebar_name );
		$is_sidebar = in_array( $sidebar_name, $this->sidebar_names );

		if ( $is_theme_sidebar || ( false === $is_sidebar ) ) {
			$error = array(
				'error' => esc_html__( 'invalid_sidebar_name', 'gastro-core' ),
				'message' => esc_html__( 'Please try a different sidebar name', 'gastro-core' )
			);
		} else {
			$key = array_search( $sidebar_name, $this->sidebar_names );
			unset( $this->sidebar_names[$key] );

			$result = update_option( 'gastro_core_widget_dynamic_sidebars', $this->sidebar_names, true );

			if ( !$result ) {
				$error = array(
					'error' => esc_html__( 'update_option_failed', 'gastro-core' ),
					'message' => esc_html__( 'Failed to update sidebar', 'gastro-core' )
				);
			}

			return $result;
		}

		return $error;
	}

	/**
	 * These sidebar names are not able to be translated, since "name" can also be used to call dynamic sidebar.
	 */
	protected function get_default_sidebars()
	{
		return array(
			'gastro-sidebar',
			'gastro-topbar-1',
			'gastro-topbar-2',
			'gastro-topbar-3',
			'gastro-topbar-4',
			'gastro-navbar-stacked-left',
			'gastro-navbar-stacked-right',
			'gastro-navbar-full',
			'gastro-navbar-offcanvas',
			'gastro-navbar-side',
			'gastro-headerwidget-1',
			'gastro-headerwidget-2',
			'gastro-headerwidget-3',
			'gastro-headerwidget-4'
		);
	}

	protected function process_sidebar_name( $sidebar_name )
	{
		return str_replace( ' ', '-', strtolower( $sidebar_name ) );
	}

	protected function is_valid_sidebar_name( $sidebar_name )
	{
		if ( empty( $sidebar_name ) )
			return false;

		if ( $this->is_theme_sidebar( $sidebar_name ) )
			return false;

		$processed_name = $this->process_sidebar_name( $sidebar_name );
		$processed_sidebar_names = array_map( array( $this, 'process_sidebar_name' ), $this->sidebar_names );

		if ( in_array( $processed_name, $processed_sidebar_names ) ) {
			return false;
		}

		return true;
	}

	protected function is_theme_sidebar( $sidebar_name )
	{
		$processed_name = $this->process_sidebar_name( $sidebar_name );

		$theme_sidebars = $this->get_default_sidebars();

		if ( in_array( $processed_name, $theme_sidebars ) )
			return true;

		return false;
	}

	public function handle_api_action( $endpoint, $params )
	{
		switch ( $endpoint ) {
			case self::API_WIDGET_SIDEBAR_CREATE:
				if ( !isset( $params['name'] ) ) {
					return array(
						'error' => 'bad_request',
						'error_message' => 'Bad Request : missing name parameter'
					);
				}

				return $this->widget_create_sidebar( $params['name'] );
			case self::API_WIDGET_SIDEBAR_DELETE:
				if ( !isset( $params['name'] ) ) {
					return array(
						'error' => 'bad_request',
						'error_message' => 'Bad Request : missing name parameter'
					);
				}

				return $this->widget_delete_sidebar( $params['name'] );
			default:
				return false;
		}
	}

	public function add_menu_widget_extra_options( $widget, $return, $instance )
	{
		if ( 'nav_menu' == $widget->id_base ) {
			$output = '';
			$inline = isset( $instance['inline'] ) ? $instance['inline'] : '';
			$anchor = isset( $instance['anchor'] ) ? $instance['anchor'] : '';

			$output .= '<p>';
			$output .= 	'<input class="checkbox" type="checkbox" id="' . $widget->get_field_id( 'inline' ) . '" name="' . $widget->get_field_name( 'inline' ) . '" ' . checked( true , $inline, false ) . '/>';
			$output .= 	'<label for="' . $widget->get_field_id('inline') . '">' . esc_html__( 'Display Inline', 'gastro-core' ) . '</label>';
			$output .= '</p>';

			$output .= '<p>';
			$output .= 	'<input class="checkbox" type="checkbox" id="' . $widget->get_field_id( 'anchor' ) . '" name="' . $widget->get_field_name( 'anchor' ) . '" ' . checked( true , $anchor, false ) . '/>';
			$output .= 	'<label for="' . $widget->get_field_id('anchor') . '">' . esc_html__( 'Display Menu In Anchor Style', 'gastro-core' ) . '</label>';
			$output .= '</p>';

			echo gastro_core_escape_content( $output );
		}
	}

	public function save_widget_menu_extra_options( $instance, $new_instance )
	{
		// Is the instance a nav menu and are inline enabled?
		if ( isset( $new_instance['nav_menu'] ) ) {
			if ( !empty( $new_instance['inline'] ) ) {
				$new_instance['inline'] = 1;
			}

			if ( !empty( $new_instance['anchor'] ) ) {
				$new_instance['anchor'] = 1;
			}
		}

		return $new_instance;
	}

	public function add_widget_menu_extra_class( $nav_menu_args, $nav_menu, $args )
	{
		$nav_menu_args['menu_class'] = 'menu inline anchor';

		return $nav_menu_args;
	}

	public function add_widget_menu_inline_class( $nav_menu_args, $nav_menu, $args )
	{
		$nav_menu_args['menu_class'] = 'menu inline';

		return $nav_menu_args;
	}

	public function add_widget_menu_anchor_class( $nav_menu_args, $nav_menu, $args )
	{
		$nav_menu_args['menu_class'] = 'menu anchor';

		return $nav_menu_args;
	}

	public function widget_menu_extra_control( $params )
	{
		$widget_settings = get_option( 'widget_nav_menu' );
		if ( !empty( $widget_settings[$params[1]['number']]['inline'] ) && !empty( $widget_settings[$params[1]['number']]['anchor'] ) ) {
			add_filter( 'widget_nav_menu_args', array( $this, 'add_widget_menu_extra_class' ), 10, 3 );
		} else if ( !empty( $widget_settings[$params[1]['number']]['inline'] ) ) {
			add_filter( 'widget_nav_menu_args', array( $this, 'add_widget_menu_inline_class' ), 10, 3 );
		} else if ( !empty( $widget_settings[$params[1]['number']]['anchor'] ) ) {
			add_filter( 'widget_nav_menu_args', array( $this, 'add_widget_menu_anchor_class' ), 10, 3 );
		} else {
			remove_filter( 'widget_nav_menu_args', array( $this, 'add_widget_menu_extra_class' ), 10, 3 );
			remove_filter( 'widget_nav_menu_args', array( $this, 'add_widget_menu_inline_class' ), 10, 3 );
			remove_filter( 'widget_nav_menu_args', array( $this, 'add_widget_menu_anchor_class' ), 10, 3 );
		}

		return $params;
	}
}
