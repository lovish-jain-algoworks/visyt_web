<?php

function gastro_core_option( $name )
{
	global $gastro_core;
	return $gastro_core['core_' . $name];
}

function gastro_core_template( $template, $args = array(), $absolute_path = false )
{
	global $gastro_core;

	if ( $args ) {
		$gastro_core->set_template_args( $template, $args );
	}

	if ( !$absolute_path ) {
		$template_file = $gastro_core['core_path'] . 'templates/' . $template . '.php';
	} else {
		$template_file = $absolute_path;
	}

	load_template( $template_file, false );
}


function gastro_core_template_style_custom( $values = array() )
{
	global $gastro_core;

	$default_template = $gastro_core['core_path'] . 'templates/style-custom.php';

	$values = apply_filters( 'gastro_core_style_custom_values', $values );
	$template_path = apply_filters( 'gastro_core_style_custom_template', $default_template );

	ob_start();
	gastro_core_template( 'style-custom', $values, $template_path );
	return ob_get_clean();
}


function gastro_core_template_blueprint_style_custom( $values = array() )
{
	global $gastro_core;

	$default_template = $gastro_core['core_path'] . 'templates/style-custom-blueprint.php';

	$values = apply_filters( 'gastro_core_style_custom_values', $values );
	$template_path = apply_filters( 'gastro_core_blueprint_style_custom_template', $default_template );

	ob_start();
	gastro_core_template( 'style-custom-blueprint', $values, $template_path );
	return ob_get_clean();
}


function gastro_core_template_args( $template )
{
	global $gastro_core;
	return $gastro_core->get_template_args( $template );
}


function gastro_core_let_to_num( $size )
{
	$l = substr( $size, -1 );
	$ret = substr( $size, 0, -1 );

	switch ( strtoupper( $l ) ) {
		case 'P':
			$ret *= 1024;
		case 'T':
			$ret *= 1024;
		case 'G':
			$ret *= 1024;
		case 'M':
			$ret *= 1024;
		case 'K':
			$ret *= 1024;
	}

	return $ret;
}


function gastro_core_system_reports()
{
	$reports = array(
		'memory_limit' => array(
			'value' => WP_MEMORY_LIMIT,
			'recommended' => '64M'
		),
		'max_execution_time' => array(
			'value' => ini_get( 'max_execution_time' ),
			'recommended' => 120
		)
	);

	$reports['max_execution_time']['status'] = ( $reports['max_execution_time']['value'] >= 120 );
	$reports['memory_limit']['status'] = ( gastro_core_let_to_num( $reports['memory_limit']['value'] ) >= 67108864 );

	return $reports;
}


function gastro_core_import_demo_status()
{
	$reports = gastro_core_system_reports();

	$reports['related_configs'] = array();
	$reports['import_demo_status'] = true;

	if ( !$reports['memory_limit']['status'] ) {
		$reports['related_configs'][] = 'Memory Limit';
		$reports['import_demo_status'] = false;
	}

	if ( !$reports['max_execution_time']['status'] ) {
		$reports['related_configs'][] = 'Max Executation Time';
		$reports['import_demo_status'] = false;
	}

	return $reports;
}


function gastro_core_style_custom_dir()
{
	$upload_dir = wp_upload_dir();
	$filename = apply_filters( 'gastro_style_custom_filename', 'style-custom.css' );

	$path = trailingslashit( $upload_dir['basedir'] ) . $filename;
	$url = trailingslashit( $upload_dir['baseurl'] ) . $filename;

	return apply_filters( 'gastro_core_style_custom_dir', array(
		'path' => $path,
		'url' => $url
	) );
}


function gastro_core_scan_template_files( $path )
{
	$files  = @scandir( $path );
	$output = array();

	if ( !empty( $files ) ) {
		foreach ( $files as $key => $value ) {
			if ( !in_array( $value, array( '.', '..' ) ) ) {
				if ( is_dir( $path . '/' . $value ) ) {
					$sub_folder = gastro_core_scan_template_files( $path . '/' . $value );
					foreach ( $sub_folder as $sub_file ) {
						if ( '.php' === substr( $sub_file, -4 ) ) {
							$output[] = $value . '/' . $sub_file;
						}
					}
				} else {
					if ( '.php' === substr( $value, -4 ) ) {
						$output[] = $value;
					}
				}
			}
		}
	}
	return $output;
}


function gastro_core_get_template_file_version( $file )
{
	$fp = fopen( $file, 'r' );
	$file_data = fread( $fp, 8192 );
	fclose( $fp );

	$file_data = str_replace( "\r", "\n", $file_data );
	$version   = '';

	if ( preg_match( '/^[ \t\/*#@]*' . preg_quote( '@version', '/' ) . '(.*)$/mi', $file_data, $match ) && $match[1] )
		$version = _cleanup_header_comment( $match[1] );

	return $version ;
}


function gastro_core_escape_content( $content )
{
	return apply_filters( 'gastro_core_escape_content', $content );
}

function gastro_core_sanitize_text_value( $text )
{
	return $text;
}

function gastro_core_sanitize_url_value( $url )
{
	return $url;
}

function gastro_core_sanitize_color_value( $value )
{
	preg_match( "/^bp_color_(\\d\\d?)$/", $value, $sync_match );
	preg_match( "/^#([a-f]|[A-F]|[0-9]){3}(([a-f]|[A-F]|[0-9]){3})?$/", $value, $hex_match );

	if ( 'transparent' === $value || ( $sync_match && 0 < $sync_match[1] && $sync_match[1] < 15 ) || $hex_match ) {
		return $value;
	} else {
		return 'default';
	}
}

function gastro_core_sanitize_number_value( $number )
{
	if ( is_numeric( $number ) ) {
		return $number;
	} else {
		return (int)$number;
	}
}

function gastro_core_sanitize_boolean_value( $boolean )
{
	if ( true === $boolean || 'true' === $boolean || 1 === $boolean ) {
		return true;
	} else {
		return false;
	}
}

function gastro_core_sanitize_component_value( $value )
{
	$multi_values = !is_array( $value ) ? explode( ',', $value ) : $value;
	return !empty( $multi_values ) ? $multi_values : array();
}

function gastro_core_woocommerce_activated()
{
	return class_exists( 'woocommerce' );
}

function gastro_core_scrape_instagram( $username, $access_token, $count = null )
{
	//if (false === ($instagram = get_transient('instagram-photos-'.sanitize_title_with_dashes($username)))) {
	// get id section
	$post_count = '';
	if ( !empty( $count ) ) {
		$post_count .= '&count=' . $count;
	}

	$remote = wp_remote_get( 'https://api.instagram.com/v1/users/self/media/recent/?access_token=' . trim( $access_token ) . $post_count );

	if ( is_wp_error( $remote ) )
		return new WP_Error('site_down', esc_html__( 'Unable to communicate with Instagram.', 'gastro-core' ) );

	if ( 200 != wp_remote_retrieve_response_code( $remote ) )
		return new WP_Error( 'invalid_response', esc_html__('Instagram did not return a 200.', 'gastro-core' ) );

	$user_content = json_decode( $remote['body'], true );
	$images = $user_content['data'];
	$instagram = array();
	foreach ( $images as $image ) {
		$instagram[] = array(
			'caption' 		=> $image['caption']['text'],
			'link' 			=> $image['link'],
			'time'			=> $image['created_time'],
			'comments' 		=> $image['comments']['count'],
			'likes' 		=> $image['likes']['count'],
			'tags'			=> $image['tags'],
			'url'			=> $image['images']['standard_resolution']['url'],
			'width'			=> $image['images']['standard_resolution']['width'],
			'height'		=> $image['images']['standard_resolution']['height'],
			'thumbnail' 	=> $image['images']['thumbnail'],
		);
	}

	$instagram = base64_encode( serialize( $instagram ) );
	set_transient( 'instagram-photos-' . sanitize_title_with_dashes( $username ), $instagram, apply_filters( 'null_instagram_cache_time', HOUR_IN_SECONDS*2 ) );
	// }

	$instagram = unserialize( base64_decode( $instagram ) );

	return $instagram;
}
