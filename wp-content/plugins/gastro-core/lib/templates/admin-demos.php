<?php
	if ( isset( $_GET['demo-flush']) ) {
		delete_option( 'bp_import_' . $_GET['demo-flush'] );
	}
 ?>

<div class="js-demo-intro bp-admin-demos-intro bp-row">
	<div class="bp-col-12">
		<div class="bp-admin-title"><?php esc_html_e( 'Import Demo', 'gastro-core' ); ?></div>
		<div class="bp-admin-subtitle">
			<?php esc_html_e( 'Choose a demo to import to your site. This process may take a few minutes, please do not close this window.', 'gastro-core' ); ?>
		</div>
		<div class="bp-row">
			<div class="bp-col-8">
				<div class="bp-box">
					<h3 class="bp-box-header"><?php esc_html_e( "Important Notes", 'gastro-core' ); ?></h3>
					<ul class="bp-list">
						<li><?php esc_html_e( 'Please make sure that the requirements of the server on System Status is fullfilled.', 'gastro-core' ); ?></li>
						<li><?php esc_html_e( 'Customize Settings will be overwritten by demo settings.', 'gastro-core' ); ?></li>
						<li><?php esc_html_e( 'Plugin data i.e. Slider Revolution, and WP Google Map will not be imported', 'gastro-core' ); ?></li>
						<li><?php esc_html_e( 'No existing posts, pages, images, custom post types will be deleted, and/or modified.', 'gastro-core' ); ?></li>
					</ul>
				</div>
			</div>
			<div class="bp-col-4">
				<div class="bp-box">
					<?php $reports = gastro_core_import_demo_status(); ?>
					<h3 class="bp-box-header"><?php esc_html_e( "Importer Status", 'gastro-core' ); ?></h3>
					<ul class="bp-list bp-list--none">
						<li>
							<?php if ( !$reports['import_demo_status'] ) : ?>
								<?php esc_html_e( 'Importer might not work properly. Please checkout', 'gastro-core' ); ?>
								<a target="_blank" href="<?php echo admin_url( 'admin.php?page=system-status' ) ?>"><?php esc_html_e( "System Status", 'gastro-core' ); ?></a>
							<?php else : ?>
								<?php esc_html_e( 'Everything looks good.', 'gastro-core' ); ?>
							<?php endif; ?>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="bp-demos" class="bp-admin-import-view"></div>
