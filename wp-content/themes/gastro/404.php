<?php
/**
 * The main template file
 *
 * @package gastro
 * @version 1.0.0
 */
?>


<?php get_header(); ?>

<div class="gst-content gst-content--no-header">
	<div class="gst-content-wrapper">
		<main class="gst-404-page">
			<div class="gst-404-header gst-s-text-color"><?php echo esc_html( '404' ); ?></div>
			<?php gastro_get_title( array(
					'style' => 'plain',
					'size' => 'h2',
					'text' => esc_html__( 'PAGE NOT FOUND', 'gastro' )
			) ); ?>
			<div class="gst-404-content">
				<?php esc_html_e( 'The page you are looking for does not exist. It may have been moved, or removed. Perhaps you can return back to the homepage and see if you can find what you are looking for.', 'gastro' ); ?>
			</div>
			<?php gastro_template_button( array(
				'button_size' => 'medium',
				'button_link' => get_home_url(),
				'button_label' => esc_html__( 'GO TO HOMEPAGE', 'gastro' ),
				'button_target_self' => true
			) ); ?>
		</main>
	</div>
</div><!-- #primary -->

<?php get_footer(); ?>
