<?php
/**
 * The main template file
 *
 * @package gastro
 * @version 1.0.0
 */
?>


<?php
	$custom_archive = gastro_mod( 'recipe_custom_archive' );
	$custom_archive = apply_filters( 'wpml_object_id', $custom_archive, 'page', TRUE );
?>
<?php if ( $custom_archive && $page = get_post( $custom_archive ) ) : ?>
	<?php gastro_set_global_post( $page ); ?>
	<?php get_header(); ?>
	<?php get_template_part( 'templates/page-content' ); ?>
	<?php get_footer(); ?>
<?php else : ?>
	<?php $opts = gastro_archive_recipe_options(); ?>
	<?php get_header(); ?>
	<div class="<?php echo esc_attr( $opts['content_class'] ); ?>">
		<div class="gst-content-wrapper">
			<?php get_template_part( 'templates/archive-title' ); ?>
			<div class="<?php echo esc_attr( $opts['container_class'] ); ?> js-dynamic-navbar">
				<main id="main" class="<?php echo esc_attr( gastro_main_page_class( $opts['sidebar'], $opts['sidebar_position'] ) ); ?> blueprint-inactive" itemprop="mainContentOfPage" itemscope="itemscope" itemtype="https://schema.org/Blog">
					<?php gastro_template_item( $opts['entries_args'] ); ?>
				</main>
				<?php if ( $opts['sidebar'] ) : ?>
					<aside class="<?php echo gastro_sidebar_class( $opts['sidebar_position'], $opts['sidebar_fixed'] ); ?>" itemscope="itemscope" itemtype="https://schema.org/WPSideBar">
						<?php gastro_template_sidebar_background(); ?>
						<div class="gst-widgets">
							<?php if ( is_active_sidebar( $opts['sidebar_select'] ) ) : ?>
								<ul class="gst-widgets-list">
									<?php dynamic_sidebar( $opts['sidebar_select'] ); ?>
								</ul>
							<?php endif; ?>
						</div>
					</aside>
				<?php endif; ?>
			</div>
		</div>
	</div><!-- #primary -->
	<?php get_footer(); ?>
<?php endif; ?>
