<?php

$dir = get_template_directory();

require_once $dir . '/lib/class-tgm-plugin-activation.php';
require_once $dir . '/includes/gastro.php';
require_once $dir . '/includes/restaurant.php';

function gastro_version()
{
	return '1.1.0';
}

function gastro_after_setup_theme()
{
	global $content_width;
	$theme_defaults = get_theme_mod( 'theme_defaults' );

	if ( !$theme_defaults ) {
		$options = gastro_theme_options();

		foreach ( $options as $key => $value ) {
			set_theme_mod( $key, $value );
		}

		set_theme_mod( 'theme_defaults', true );
	}

	$post_settings = get_theme_mod( 'post_settings' );

	if ( empty( $post_settings ) ) {
		set_theme_mod( 'post_settings', gastro_default_post_options() );
	}

	// Theme support
	add_theme_support( 'title-tag' );
	add_theme_support( 'custom-header' );
	add_theme_support( 'custom-background' );
	add_theme_support( 'post-thumbnails' );
	add_theme_support( 'automatic-feed-links' );
	add_theme_support( 'woocommerce' );
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption'
	) );

	// Add post format
	add_theme_support( 'post-formats', array(
		'gallery',
		'quote',
		'video',
		'audio'
	) );

	// added tiny mce editor styling
	add_editor_style();

	// Add excerpt to support page post type
	add_post_type_support( 'page', 'excerpt' );

	// Add attribute to support post post type
	add_post_type_support( 'post', 'page-attributes' );

	register_nav_menus( array(
		'gst-primary-menu'  => esc_html__( 'Primary Menu', 'gastro' ),
		'gst-mobile-menu'  => esc_html__( 'Mobile Menu', 'gastro' )
	) );

	// Set up content max width
	$content_max_width = gastro_mod( 'content_max_width' );

	if ( !empty( $content_max_width ) ) {
		$content_width = $content_max_width;
	} else {
		$content_width = apply_filters( 'default_content_max_width', 1100 );
	}
}
add_action( 'after_setup_theme', 'gastro_after_setup_theme' );


function gastro_register_initial_sidebar()
{
	// register dynamic sidebars
	$gastro_sidebars = array(
		'gastro-sidebar' => esc_html__( 'Gastro Sidebar', 'gastro' ),
		'gastro-topbar-1' => esc_html__( 'Topbar 1', 'gastro' ),
		'gastro-topbar-2' => esc_html__( 'Topbar 2', 'gastro' ),
		'gastro-topbar-3' => esc_html__( 'Topbar 3', 'gastro' ),
		'gastro-topbar-4' => esc_html__( 'Topbar 4', 'gastro' ),
		'gastro-navbar-stacked-left' => esc_html__( 'Stacked Navbar Left', 'gastro' ),
		'gastro-navbar-stacked-right' => esc_html__( 'Stacked Navbar Right', 'gastro' ),
		'gastro-navbar-full' => esc_html__( 'Full Navbar', 'gastro' ),
		'gastro-navbar-offcanvas' => esc_html__( 'Offcanvas Navbar', 'gastro' ),
		'gastro-navbar-side' => esc_html__( 'Side Navbar', 'gastro' ),
		'gastro-headerwidget-1' => esc_html__( 'Header Widget 1', 'gastro' ),
		'gastro-headerwidget-2' => esc_html__( 'Header Widget 2', 'gastro' ),
		'gastro-headerwidget-3' => esc_html__( 'Header Widget 3', 'gastro' ),
		'gastro-headerwidget-4' => esc_html__( 'Header Widget 4', 'gastro' )
	);

	$widget_title = gastro_get_title( array( 'size' => 'h3' ), false );
	$sidebar = array(
		'class' => 'gastro_theme js-dyanmic-sidebar',
		'description' => esc_html__( 'Custom Widget', 'gastro' ),
		'before_title' => $widget_title['prefix'],
		'after_title' => $widget_title['suffix']
	);

	foreach ( $gastro_sidebars as $id => $name ) {
		$sidebar['id'] = $id;
		$sidebar['name'] = $name;
		register_sidebar( $sidebar );
	}
}
add_action( 'widgets_init', 'gastro_register_initial_sidebar' );


// TGM Register
function gastro_tgmpa_register()
{
	$template_directory = get_template_directory();

	$plugins = array(
		array(
			'name' => esc_html__( 'Gastro Core', 'gastro' ),
			'slug' => 'gastro-core',
			'version' => '1.1.0',
			'source' => $template_directory . '/lib/plugins/gastro-core.zip',
			'required' => true
		),
		array(
			'name' => esc_html__( 'Envato Market', 'gastro' ),
			'slug' => 'envato-market',
			'source' => $template_directory . '/lib/plugins/envato-market.zip',
			'required' => false
		),
		array(
			'name' => esc_html__( 'Slider Revolution', 'gastro' ),
			'slug' => 'revslider',
			'source' => $template_directory . '/lib/plugins/revslider.zip',
			'required' => false
		),
		array(
			'name' => esc_html__( 'Contact Form 7', 'gastro' ),
			'slug' => 'contact-form-7',
			'required' => false
		),
		array(
			'name' => esc_html__( 'MailChimp for WordPress', 'gastro' ),
			'slug' => 'mailchimp-for-wp',
			'required' => false
		),
		array(
			'name' => esc_html__( 'WooCommerce', 'gastro' ),
			'slug' => 'woocommerce',
			'required' => false
		),
		array(
			'name' => esc_html__( 'WP Google Maps', 'bateaux' ),
			'slug' => 'wp-google-maps',
			'required' => false
		),
		array(
			'name' => esc_html__( 'Yoast SEO', 'gastro' ),
			'slug' => 'wordpress-seo',
			'required' => false
		)
	);

	$configs = array(
		'id' => 'gastro-tgmpa',
		'is_automatic' => true
	);

	$plugins = apply_filters( 'gastro_tgmpa_plugins', $plugins );
	$configs = apply_filters( 'gastro_tgmpa_configs', $configs );

	tgmpa( $plugins, $configs );
}
add_action( 'tgmpa_register', 'gastro_tgmpa_register' );
