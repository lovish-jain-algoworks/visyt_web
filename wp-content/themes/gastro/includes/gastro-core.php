<?php

function gastro_module( $gastro_core )
{
	wp_register_script(
		'gastro-admin-common',
		gastro_asset( 'js/gst.admin-common.js' ),
		array( 'jquery', 'jquery-ui-widget' ),
		gastro_version(),
		true
	);

	wp_register_style(
		'gastro-admin-common',
		gastro_asset( 'css/gst.admin-common.css' ),
		null,
		gastro_version()
	);
}
add_action( 'gastro_core_init', 'gastro_module' );


function gastro_blueprint_scripts( $dependencies = array() , $options, $strings )
{
	array_unshift( $dependencies, 'gastro-admin-common' );

	wp_enqueue_script(
		'gastro-blueprint',
		gastro_asset( 'js/gst.blueprint.js' ),
		$dependencies,
		gastro_version(),
		true
	);

	wp_enqueue_style( 'gastro-admin-common' );

	wp_localize_script( 'gastro-blueprint', 'BPOptions', $options );
	wp_localize_script( 'gastro-blueprint', 'BPStrings', $strings );
}
add_action( 'gastro_core_blueprint_scripts', 'gastro_blueprint_scripts', 10, 3 );


function gastro_customize_scripts( $dependencies = array() , $options )
{
	array_unshift( $dependencies, 'gastro-admin-common' );

	wp_enqueue_script(
		'gastro-customize',
		gastro_asset( 'js/gst.customize.js' ),
		$dependencies,
		gastro_version(),
		true
	);

	wp_enqueue_style( 'gastro-admin-common' );
}
add_action( 'gastro_core_customize_scripts', 'gastro_customize_scripts', 10, 2 );


function gastro_customize_preview_scripts()
{
	wp_enqueue_script( 'gastro-preview' );
	wp_localize_script( 'gastro-preview', 'Gastro', get_theme_mods() );
}
add_action( 'gastro_core_customize_preview_scripts', 'gastro_customize_preview_scripts' );


function gastro_admin_scripts( $dependencies = array(), $options  = array(), $strings = array() )
{
	array_unshift( $dependencies, 'gastro-admin-common' );

	wp_enqueue_script(
		'gastro-admin',
		gastro_asset( 'js/gst.admin.js' ),
		$dependencies,
		gastro_version(),
		true
	);

	wp_enqueue_style( 'gastro-admin-common' );

	wp_localize_script( 'gastro-admin', 'BPOptions', $options );
	wp_localize_script( 'gastro-admin', 'BPStrings', $strings );
}
add_action( 'gastro_core_admin_scripts', 'gastro_admin_scripts', 10, 3 );


// script for taxonomy page to upload images
function gastro_taxonomy_scripts( $dependencies = array(), $options  = array() )
{
	wp_enqueue_media();
	wp_enqueue_script( 'wp-color-picker' );
	wp_enqueue_style( 'wp-color-picker' );

	wp_enqueue_script(
		'gastro-taxonomy',
		gastro_asset( 'js/gst.taxonomy.js' ),
		$dependencies,
		gastro_version(),
		true
	);

	$options = apply_filters( 'taxonomy_pages_options', array(
		'placeholder' => gastro_placeholder(),
		'multiple' => 'false',
		'string' => array(
			'choose' => esc_html__( 'Choose an image', 'gastro' ),
			'use' => esc_html__( 'Use image', 'gastro' )
		)
	) );

	wp_localize_script( 'gastro-taxonomy', 'taxonomyObj', $options );
}
add_action( 'gastro_core_taxonomy_scripts', 'gastro_taxonomy_scripts', 10, 2 );


// Common admin menu pages (not theme dashboard)
function gastro_menu_pages_scripts( $dependencies = array(), $options = array() )
{
	array_unshift( $dependencies, 'gastro-admin-common' );

	wp_enqueue_script(
		'gastro-common',
		gastro_asset( 'js/gst.common.js' ),
		$dependencies,
		gastro_version(),
		true
	);

	wp_enqueue_style( 'gastro-admin-common' );
}
add_action( 'gastro_core_menu_pages_scripts', 'gastro_menu_pages_scripts', 10, 2 );


// Common admin page (not theme dashboard)
function gastro_booking_admin_scripts( $dependencies = array(), $options = array() )
{
	array_unshift( $dependencies, 'gastro-admin-common', 'jquery-ui-widget' );

	wp_enqueue_script(
		'gastro-bookingadmin',
		gastro_asset( 'js/gst.bookingadmin.js' ),
		$dependencies,
		gastro_version(),
		true
	);

	wp_enqueue_style( 'gastro-admin-common' );

	wp_localize_script( 'gastro-bookingadmin', 'bookingAdminObject', $options );
}
add_action( 'gastro_core_booking_admin_scripts', 'gastro_booking_admin_scripts', 10, 2 );


function gastro_widgets_scripts( $dependencies = array(), $options = array(), $strings = array() )
{
	array_unshift( $dependencies, 'gastro-admin-common' );

	wp_enqueue_script(
		'gastro-widgets',
		gastro_asset( 'js/gst.widgets.js' ),
		$dependencies,
		gastro_version(),
		true
	);

	wp_enqueue_style( 'gastro-admin-common' );
	wp_localize_script( 'gastro-widgets', 'BPOptions', $options );
	wp_localize_script( 'gastro-widgets', 'BPStrings', $strings );
}
add_action( 'gastro_core_widgets_scripts', 'gastro_widgets_scripts', 10, 3 );


function gastro_post_scripts( $dependencies = array(), $options = array() )
{
	wp_enqueue_script(
		'gastro-tinymce',
		gastro_asset( 'js/gst.tinymce.js' ),
		$dependencies,
		gastro_version(),
		true
	);
}
add_action( 'gastro_core_post_scripts', 'gastro_post_scripts', 10, 2 );


// Filters
function gastro_app_name()
{
	return 'gastro';
}
add_filter( 'gastro_app_name', 'gastro_app_name' );


function gastro_blueprint_options( $options )
{
	$options['placeholders'] = gastro_asset( 'images/placeholder.png' );

	return $options;
}
add_filter( 'blueprint_options', 'gastro_blueprint_options' );


function gastro_mce_plugins( $plugins )
{
	$plugins['gastro'] = gastro_asset( 'js/gst.tinymce.js' );

	return $plugins;
}
add_filter( 'mce_external_plugins', 'gastro_mce_plugins' );


function gastro_mce_buttons( $buttons )
{
    $buttons[] = 'gastro-gallery';
    $buttons[] = 'gastro-image';
    $buttons[] = 'gastro-video';
    $buttons[] = 'gastro-blockquote';
    $buttons[] = 'gastro-button';

    return $buttons;
}
add_filter( 'mce_buttons', 'gastro_mce_buttons' );


function gastro_default_customize_options( $options )
{
	return $options;
}
add_filter( 'gastro_core_default_customize_options', 'gastro_default_customize_options' );


function gastro_style_custom_template()
{
	return trailingslashit(get_template_directory()) . 'templates/style-custom.php';
}
add_filter( 'gastro_core_style_custom_template', 'gastro_style_custom_template' );


function gastro_blueprint_style_custom_template()
{
	return trailingslashit(get_template_directory()) . 'templates/style-custom-blueprint.php';
}
add_filter( 'gastro_core_blueprint_style_custom_template', 'gastro_blueprint_style_custom_template' );
