<?php

function gastro_default_options()
{
	return array(
		'minified_asset' => true,
		'asset_uri' => apply_filters( 'gastro_asset_uri', get_template_directory_uri() . '/dist' )
	);
}

function gastro_theme_options( $key = '' )
{
	$default_options = array(
		'bp_color_1' => '#f8c12e',
		'bp_color_2' => '#260000',
		'bp_color_3' => '#676767',
		'bp_color_4' => '#1d1d1d',
		'bp_color_5' => '#ffffff',
		'bp_color_6' => '#f7f7f7',
		'bp_color_7' => '#eeeeee',
		'bp_color_8' => '#cdcdcd',
		'bp_color_9' => '#ffffff',
		'bp_color_10' => '#1d1d1d',
		'bp_color_11' => '#000000',
		'bp_color_12' => '#333333',
		'bp_color_13' => '#fafafa',
		'bp_color_14' => '#363636',
		'default_color_scheme' => 'light',
		'topbar_color_scheme' => 'dark',
		'navbar_color_scheme' => 'light',
		'header_widget_color_scheme' => 'dark',
		'page_title_color_scheme' => 'default',
		'sidebar_color_scheme' => 'default',
		'logo_homescreen_icon' => '',
		'navbar_offcanvas_cursor' => '',
		'navbar_logo_section' => '',
		'logo_type' => 'text',
		'logo' => '',
		'logo_width' => 120,
		'logo_text_title' => 'GASTRO',
		'logo_typography' => 'secondary',
		'logo_font_color' => 'default',
		'logo_font_size' => 24,
		'logo_letter_spacing' => 0.1,
		'mobile_navbar_logo_section' => '',
		'mobile_navbar_logo' => '',
		'mobile_navbar_logo_width' => 60,
		'fixed_navbar_logo_section' => '',
		'fixed_navbar_logo' => '',
		'fixed_navbar_logo_light_scheme' => '',
		'fixed_navbar_logo_dark_scheme' => '',
		'fixed_navbar_logo_width' => 60,
		'site_layout' => 'wide',
		'frame_color' => 'default',
		'frame_width' => 30,
		'content_max_width' => 1200,
		'side_padding' => 10,
		'header_on_frame' => 0,
		'sidebar' => 0,
		'sidebar_position' => 'right',
		'sidebar_select' => '',
		'sidebar_fixed' => 0,
		'sidebar_background_color' => 'default',
		'sidebar_width' => 25,
		'sidebar_top_padding' => 60,
		'content_background' => 0,
		'content_background_color' => 'default',
		'content_background_image' => '',
		'content_background_repeat' => 'repeat',
		'content_background_size' => 'cover',
		'content_background_position' => 'center center',
		'content_background_attachment' => 1,
		'body_background' => 0,
		'body_background_color' => 'default',
		'body_background_image' => '',
		'body_background_repeat' => 'repeat',
		'body_background_size' => 'cover',
		'body_background_position' => 'center center',
		'body_background_attachment' => 1,
		'boxed_shadow' => '0 0 30px 0 rgba(0, 0, 0, 0.1)',
		'site_responsive' => 1,
		'enable_mobile_parallax' => 0,
		'style_typography' => 'primary',
		'style_font_size' => 14,
		'heading_style' => 'plain',
		'heading_typography' => 'secondary',
		'heading_size_h1' => 64,
		'heading_size_h2' => 48,
		'heading_size_h3' => 36,
		'heading_size_h4' => 20,
		'heading_size_h5' => 16,
		'heading_size_h6' => 14,
		'button_style' => 'layer',
		'button_hover_style' => 'inverse',
		'button_size' => 'medium',
		'button_typography' => 'secondary',
		'button_uppercase' => 1,
		'button_border' => 1,
		'button_radius' => 0,
		'carousel_arrow_background' => 'transparent',
		'carousel_arrow_style' => 'angle',
		'cookies_notice' => 0,
		'cookies_notice_color_scheme' => 'dark',
		'cookies_notice_background_color' => 'default',
		'cookies_notice_background_opacity' => 100,
		'cookies_notice_message' => 'We use cookies to deliver you the best experience. By browsing our website you agree to our use of cookies. [bp_link url="/"]Learn More[/bp_link]',
		'preload_style' => 'none',
		'preload_background_color' => 'default',
		'preload_logo' => '',
		'preload_logo_width' => 80,
		'back_to_top' => 0,
		'back_to_top_background' => 'square',
		'back_to_top_background_color' => 'default',
		'back_to_top_style' => 'angle',
		'back_to_top_arrow_color' => 'default',
		'form_style' => 'line',
		'navbar_position' => 'top',
		'navbar_style' => 'standard',
		'navbar_fullwidth' => 1,
		'side_navbar_style' => 'fixed',
		'navbar_menu_position' => 'right',
		'side_navbar_menu_alignment' => 'left',
		'inline_navbar_menu_position' => 'inner',
		'minimal_navbar_menu_style' => 'full',
		'navbar_size' => 'custom',
		'navbar_height' => 90,
		'navbar_menu_hover_style' => 'default',
		'navbar_stacked_options' => '',
		'navbar_stacked_lineheight' => 90,
		'navbar_stacked_background_color' => 'default',
		'navbar_stacked_opacity' => 100,
		'navbar_menu_border' => '',
		'navbar_menu_border_thickness' => 1,
		'navbar_menu_border_color' => 'default',
		'navbar_offset' => '',
		'navbar_logo_offset_top' => '',
		'navbar_menu_offset_top' => '',
		'navbar_component' => '',
		'mega_menu_separator' => 1,
		'navbar_search' => 1,
		'navbar_cart' => 0,
		'navbar_cart_icon' => 'et-bag',
		'navbar_background_custom' => 0,
		'navbar_background_color' => 'default',
		'navbar_opacity' => 100,
		'navbar_menu_custom' => 0,
		'navbar_menu_color' => 'default',
		'navbar_menu_active_color' => 'default',
		'navbar_menu_hover_color' => 'default',
		'navbar_menu_typography' => 'secondary',
		'navbar_menu_uppercase' => 1,
		'navbar_menu_font_size' => 12,
		'navbar_menu_letter_spacing' => 0.1,
		'navbar_menu_separator' => 'none',
		'dropdown_menu' => 0,
		'dropdown_menu_uppercase' => 0,
		'dropdown_menu_font_size' => 12,
		'dropdown_menu_letter_spacing' => '',
		'dropdown_menu_min_width' => 220,
		'dropdown_color_scheme' => 'dark',
		'dropdown_background_color' => 'default',
		'dropdown_opacity' => 100,
		'dropdown_menu_color' => 'default',
		'dropdown_hover_color' => 'default',
		'sidenav_background_custom' => 0,
		'sidenav_background_color' => 'default',
		'sidenav_background_image' => '',
		'sidenav_background_repeat' => 'repeat',
		'sidenav_background_size' => 'cover',
		'sidenav_background_position' => 'center center',
		'sidenav_background_attachment' => 1,
		'sidenav_menu_custom' => 0,
		'sidenav_menu_color' => 'default',
		'sidenav_menu_active_color' => 'default',
		'sidenav_menu_hover_color' => 'default',
		'sidenav_menu_typography' => 'secondary',
		'sidenav_menu_uppercase' => 1,
		'sidenav_menu_font_size' => 15,
		'sidenav_menu_letter_spacing' => -0.03,
		'navbar_full_background' => 0,
		'navbar_full_background_color' => 'default',
		'navbar_full_opacity' => 100,
		'navbar_full_background_image' => '',
		'navbar_full_background_repeat' => 'repeat',
		'navbar_full_background_size' => 'cover',
		'navbar_full_background_position' => 'center center',
		'navbar_full_menu_custom' => 0,
		'navbar_full_menu_axis' => 'vertical',
		'navbar_full_menu_color' => 'default',
		'navbar_full_menu_active_color' => 'default',
		'navbar_full_menu_hover_color' => 'default',
		'navbar_full_menu_typography' => 'secondary',
		'navbar_full_menu_font_size' => 30,
		'navbar_full_menu_letter_spacing' => -0.03,
		'mobile_navbar_style' => 'classic',
		'mobile_navbar_menu_typography' => 'secondary',
		'mobile_navbar_menu_uppercase' => 1,
		'mobile_navbar_menu_font_size' => 14,
		'mobile_navbar_menu_letter_spacing' => '',
		'mobile_navbar_advance' => 0,
		'mobile_navbar_background_color' => 'default',
		'mobile_navbar_opacity' => 100,
		'mobile_navbar_background_image' => '',
		'mobile_navbar_background_repeat' => 'repeat',
		'mobile_navbar_background_size' => 'cover',
		'mobile_navbar_background_position' => 'center center',
		'mobile_navbar_menu_color' => 'default',
		'mobile_navbar_menu_active_color' => 'default',
		'mobile_navbar_menu_hover_color' => 'default',
		'fixed_navbar' => 0,
		'fixed_navbar_hide' => 0,
		'fixed_navbar_style' => 'custom',
		'fixed_navbar_height' => 70,
		'fixed_navbar_transition' => 'show',
		'fixed_navbar_transition_point' => '',
		'fixed_navbar_bottom_border' => '',
		'fixed_navbar_bottom_border_thickness' => 1,
		'fixed_navbar_bottom_border_color' => '#eeeeee',
		'fixed_navbar_advance' => 0,
		'fixed_navbar_background_color' => 'default',
		'fixed_navbar_opacity' => 100,
		'fixed_navbar_menu_font_size' => 12,
		'fixed_navbar_menu_color' => 'default',
		'fixed_navbar_menu_active_color' => 'default',
		'fixed_navbar_menu_hover_color' => 'default',
		'topbar' => 0,
		'enable_mobile_topbar' => 0,
		'topbar_column' => 2,
		'topbar_height' => 36,
		'topbar_separator' => 0,
		'topbar_bottom_border' => '',
		'topbar_bottom_border_thickness' => 0,
		'topbar_bottom_border_color' => 'default',
		'topbar_advance' => 0,
		'topbar_background_color' => 'default',
		'topbar_background_opacity' => 100,
		'topbar_text_color' => 'default',
		'topbar_link_color' => 'default',
		'topbar_link_hover_color' => 'default',
		'header_action_button' => 0,
		'action_type' => 'link',
		'fixed_nav_action_button' => 1,
		'action_mobile_display' => 0,
		'action_button_style_section' => '',
		'action_button_text' => 'reservation',
		'action_button_icon' => '',
		'action_button_icon_position' => 'before',
		'action_button_style' => 'layer',
		'action_button_hover_style' => 'brand',
		'action_button_border' => 1,
		'action_button_radius' => 0,
		'action_button_setting_section' => '',
		'action_link' => '/',
		'action_target_self' => 0,
		'header_widget_column' => 4,
		'header_widget_alignment' => 'left',
		'header_widget_max_height' => '',
		'header_widget_advance' => 0,
		'header_widget_separator' => 0,
		'header_widget_background_color' => 'default',
		'header_widget_background_opacity' => 100,
		'header_widget_background_image' => '',
		'header_widget_background_repeat' => 'repeat',
		'header_widget_background_size' => 'cover',
		'header_widget_background_position' => 'center center',
		'header_widget_text_color' => 'default',
		'header_widget_link_color' => 'default',
		'header_widget_link_hover_color' => 'default',
		'action_social_component' => array(
			'facebook',
			'instagram',
			'tripadvisor',
			'phone'
		),
		'footer' => 1,
		'footer_parallax' => 0,
		'footer_id' => '',
		'page_title_full_width' => 0,
		'breadcrumb_setting_section' => '',
		'breadcrumb' => 0,
		'breadcrumb_separator' => 'angle-right',
		'breadcrumb_text_color' => 'default',
		'breadcrumb_background_color' => 'default',
		'breadcrumb_background_opacity' => 100,
		'page_title_setting_section' => '',
		'page_title' => 1,
		'breadcrumb_position' => 'top',
		'page_title_alignment' => 'center',
		'page_title_text_color' => 'default',
		'page_title_background_custom' => 0,
		'page_title_background_color' => 'default',
		'page_title_background_opacity' => 100,
		'page_title_background_image' => '',
		'page_title_background_repeat' => 'repeat',
		'page_title_background_size' => 'cover',
		'page_title_background_position' => 'center center',
		'page_title_label_section' => 0,
		'page_title_blog_label' => 'Blog',
		'page_title_shop_label' => 'Products',
		'page_title_archive_label' => 'Archives of ',
		'page_title_category_label' => 'Archives of ',
		'page_title_tag_label' => 'Archives of #',
		'page_title_search_label' => 'Search Results for: ',
		'page_title_author_label' => 'All posts of ',
		'page_title_day_label' => 'Archive of day: ',
		'page_title_month_label' => 'Archive of month: ',
		'page_title_year_label' => 'Archive of year: ',
		'blog_subtitle' => "Edit this subtitle in customizer's archive blog section",
		'blog_custom_archive' => '',
		'blog_full_width' => 0,
		'blog_style' => 'plain',
		'blog_layout' => 'masonry',
		'blog_columns' => 3,
		'blog_alignment' => 'left',
		'blog_featured_media' => 'auto',
		'blog_image_size' => 'large',
		'blog_image_ratio' => 'auto',
		'blog_image_hover' => 'none',
		'blog_category_style' => 'bar',
		'blog_border' => 0,
		'blog_color_section' => '',
		'blog_color_scheme' => 'default',
		'blog_background' => 'default',
		'blog_opacity' => 100,
		'blog_spacing_section' => '',
		'blog_spacing' => 30,
		'blog_inner_spacing' => 0,
		'blog_typography_section' => '',
		'blog_title_uppercase' => 0,
		'blog_title_font_size' => 20,
		'blog_title_letter_spacing' => -0.03,
		'blog_title_line_height' => '',
		'blog_advance_section' => '',
		'blog_load' => 'pagination',
		'blog_filter' => 0,
		'blog_filter_alignment' => 'left',
		'blog_filter_sorting' => 'default',
		'blog_excerpt_content' => 'excerpt',
		'blog_excerpt_length' => '',
		'blog_more_message' => 'Read More',
		'blog_link' => 'post',
		'blog_link_new_tab' => 0,
		'blog_component' => array(
			'media',
			'category',
			'tag',
			'title',
			'excerpt',
			'link',
			'icon'
		),
		'recipe_slug' => 'recipe',
		'recipe_custom_archive' => '',
		'recipe_subtitle' => "Edit this subtitle in customizer's recipe section",
		'recipe_items' => 12,
		'recipe_full_width' => 0,
		'recipe_style' => 'overlay',
		'recipe_layout' => 'grid',
		'recipe_columns' => 3,
		'recipe_alignment' => 'left',
		'recipe_image_size' => 'large',
		'recipe_image_ratio' => 'auto',
		'recipe_image_hover' => 'zoom',
		'recipe_category_style' => 'bar',
		'recipe_border' => 0,
		'recipe_sidebar' => 1,
		'recipe_sidebar_position' => 'right',
		'recipe_sidebar_select' => '',
		'recipe_sidebar_fixed' => false,
		'recipe_color_section' => '',
		'recipe_color_scheme' => 'default',
		'recipe_background' => 'default',
		'recipe_opacity' => 100,
		'recipe_spacing_section' => '',
		'recipe_spacing' => 16,
		'recipe_inner_spacing' => 40,
		'recipe_typography_section' => '',
		'recipe_title_uppercase' => 0,
		'recipe_title_font_size' => 22,
		'recipe_title_letter_spacing' => -1,
		'recipe_title_line_height' => '',
		'recipe_advance_section' => '',
		'recipe_load' => 'pagination',
		'recipe_filter' => 1,
		'recipe_filter_alignment' => 'center',
		'recipe_filter_sorting' => 'default',
		'recipe_excerpt_content' => 'excerpt',
		'recipe_excerpt_length' => '',
		'recipe_more_message' => 'View Recipe',
		'recipe_link' => 'post',
		'recipe_link_new_tab' => 1,
		'recipe_component' => array(
			'media',
			'category',
			'title',
			'link',
			'icon',
			'customfield'
		),
		'shop_subtitle' => "Edit this subtitle in customizer's woocommerce shop section",
		'shop_full_width' => 0,
		'shop_product' => 12,
		'shop_column' => 3,
		'shop_spacing' => 30,
		'shop_image_size' => 'medium_large',
		'shop_image_ratio' => 'auto',
		'shop_sidebar' => 1,
		'shop_sidebar_position' => 'right',
		'shop_sidebar_select' => '',
		'shop_sidebar_fixed' => 1,
		'shop_typography_section' => '',
		'shop_title_uppercase' => 1,
		'shop_title_letter_spacing' => 0.1,
		'shop_title_font_size' => 16,
		'shop_component' => array(
			'image',
			'subtitle',
			'badge'
		),
		'product_image_action' => 'lightbox',
		'product_variation_mode' => 'radio',
		'product_thumbnail_position' => 'bottom',
		'product_thumbnail_ratio' => 100,
		'product_thumbnail_item' => 4,
		'product_detail_position' => 'right',
		'product_detail_width' => 50,
		'product_component' => '',
		'product_related' => 1,
		'product_related_column' => 4,
		'product_share' => 1,
		'product_sidebar' => 0,
		'product_sidebar_position' => 'right',
		'product_sidebar_select' => '',
		'product_sidebar_fixed' => false,
		'social_auto_color' => 0,
		'social_icon_style' => 'plain',
		'social_icon_hover_style' => 'plain',
		'social_facebook' => 'https://www.facebook.com/twisttheme/',
		'social_twitter' => 'https://twitter.com/ThemeTwist',
		'social_youtube' => 'https://www.youtube.com',
		'social_vimeo' => '',
		'social_instagram' => 'https://www.instagram.com',
		'social_linkedin' => '',
		'social_google-plus' => '',
		'social_skype' => '',
		'social_pinterest' => 'https://www.pinterest.com',
		'social_tripadvisor' => '',
		'social_flickr' => '',
		'social_tumblr' => '',
		'social_dribbble' => '',
		'social_behance' => '',
		'social_stumbleupon' => '',
		'social_email' => 'support@twisttheme.com',
		'social_phone' => '',
		'social_xing' => '',
		'social_share_component' => array(
			'facebook',
			'twitter',
			'pinterest',
			'email'
		),
		'use_style_custom_version' => true,
		'style_custom_version' => ''
	);

	if ( !empty( $key ) ) {
		return isset( $default_options[$key] ) ? $default_options[$key] : '';
	} else {
		return $default_options;
	}
}


function gastro_default_post_options()
{
	return array(
		'featured_media_layout' => 'standard',
		'featured_media_parallax' => true,
		'featured_media_scheme' => 'dark',
		'breadcrumb' => false,
		'breadcrumb_color_scheme' => 'default',
		'breadcrumb_color' => 'default',
		'breadcrumb_background' => 'default',
		'breadcrumb_opacity' => 100,
		'breadcrumb_alignment' => 'left',
		'header_alignment' => 'left',
		'header_vertical' => 'bottom',
		'title_font' => 'secondary',
		'title_size' => 42,
		'title_max_width' => '60%',
		'title_letter_spacing' => '',
		'title_line_height' => '',
		'sidebar' => false,
		'sidebar_id' => '',
		'sidebar_position' => 'right',
		'sidebar_fixed' => false,
		'sidebar_custom_color' => 'default',
		'sidebar_background_color' => 'default',
		'sidebar_color_scheme' => 'light',
		'category_style' => 'bar',
		'category' => true,
		'author' => true,
		'date' => true,
		'tag' => true,
		'author_box' => false,
		'comment_color_scheme' => 'default',
		'comment_background_color' => 'default',
		'social' => true,
		'social_components' => array( 'facebook', 'twitter', 'pinterest'),
		'social_counter' => false,
		'social_divider' => false,
		'social_size' => 15,
		'social_alignment' => 'center',
		'social_auto_color' => false,
		'social_color' => 'bp_color_4',
		'social_icon_style' => 'border',
		'social_icon_hover_style' => 'fill',
		'social_icon_hover_color' => 'bp_color_4',
		'navigation' => true,
		'navigation_style' => 'bar',
		'navigation_border_top' => true,
		'navigation_border_bottom' => true,
		'navigation_alignment' => 'center',
		'related' => true,
		'related_title' => 'Related Posts',
		'related_items' => 4,
		'related_column' => 4,
		'related_thumbnail' => false,
		'related_date' => true,
		'related_entry_background' => 'transparent',
		'related_color_scheme' => 'default',
		'related_background_color' => 'default'
	);
}


function gastro_default_font_options( $options = array() )
{
	if ( !isset ($options['primary']) || empty( $options['primary'] ) ) {
		$options['primary'] = array(
			'type' => 'google_font',
			'family' => 'Roboto',
			'style' => 'regular',
			'category' => 'sans-serif',
			'variants' => array(
				'100',
				'100italic',
				'300',
				'300italic',
				'regular',
				'italic',
				'500',
				'500italic',
				'700',
				'700italic',
				'900',
				'900italic'
			)
		);
	}

	if ( !isset( $options['secondary'] ) || empty( $options['secondary'] ) ) {
		$options['secondary'] = array(
			'type' => 'google_font',
			'family' => 'Roboto',
			'style' => '500',
			'category' => 'sans-serif',
			'variants' => array(
				'100',
				'100italic',
				'300',
				'300italic',
				'regular',
				'italic',
				'500',
				'500italic',
				'700',
				'700italic',
				'900',
				'900italic'
			)
		);
	}

	return $options;
}
add_filter( 'gastro_fonts', 'gastro_default_font_options' );


function gastro_customize_options( $options )
{
	return $options;
}


function gastro_body_class( $classes )
{
	$opts = array(
		'topbar' => gastro_mod( 'topbar' ),
		'site_layout' => gastro_mod( 'site_layout' ),
		'header_on_frame' => gastro_mod( 'header_on_frame' ),
		'responsive' => gastro_mod( 'site_responsive' ),
		'navbar_position' => gastro_mod( 'navbar_position' ),
		'navbar_style' => gastro_mod( 'navbar_style' ),
		'minimal_navbar_menu_style' => gastro_mod( 'minimal_navbar_menu_style' ),
		'sidenav_style' => gastro_mod( 'side_navbar_style' )
	);

	$classes[] = 'gst-layout gst-layout--' . $opts['site_layout'];

	if ( 'boxed' === $opts['site_layout'] ) {
		$classes[] = 'gst-s-bg-bg';
	} elseif ( 'frame' === $opts['site_layout'] ) {
		if ( $opts['header_on_frame'] ) {
			$classes[] = 'header-on-frame';
		}
	}

	$page_id = gastro_get_page_id();
	$bp_data = gastro_bp_data( $page_id );

	if ( gastro_is_blueprint_active( $bp_data ) ) {
		$builder = $bp_data['builder'];

		if ( isset( $builder['responsive'] ) ) {
			$opts['responsive'] = ( 'disable' === $builder['responsive'] ) ? false : true;
		}

		if ( isset( $builder['nav_style'] ) && 'default' !== $builder['nav_style'] ) {
			$navbar_style = explode( '-', $builder['nav_style'] );
			$opts['navbar_position'] = $navbar_style[0];

			if ( 'top' !== $opts['navbar_position'] ) {
				$opts['sidenav_style'] = $navbar_style[1];
			} else {
				$opts['navbar_style'] = $navbar_style[1];
			}
		}

		$opts['nav_menu'] = isset( $builder['nav_menu'] ) ? $builder['nav_menu'] : 'default';
	} else {
		$opts['nav_menu'] = 'default';
	}

	// add responsive class
	if ( $opts['responsive'] ) {
		$classes[] = 'gst-layout-responsive';
	}

	if ( 'none' === $opts['nav_menu'] ) {
		$classes[] = 'gst-layout--no-navbar';
	} else if ( 'top' === $opts['navbar_position'] ) {
		$classes[] = 'gst-layout--topnav';
		$classes[] = 'gst-layout--topnav-' . $opts['navbar_style'];

		if ( 'minimal' === $opts['navbar_style'] || 'split' === $opts['navbar_style'] ) {
			$classes[] = 'collapsed-' . $opts['minimal_navbar_menu_style'];
		}
	} else if ( 'top' !== $opts['navbar_position'] ) {
		$classes[] = 'gst-layout--sidenav';
		$classes[] = 'gst-layout--sidenav-' . esc_attr( $opts['sidenav_style'] ) . ' gst-layout--sidenav-' . esc_attr( $opts['sidenav_style'] ) . '-' . esc_attr( $opts['navbar_position'] );
	}

	return $classes;
}
add_filter( 'body_class', 'gastro_body_class' );


if ( !function_exists( 'gastro_header_content_options' ) ) :
function gastro_header_content_options( $opts = array(), $post_id = '' )
{
	$opts['responsive'] = gastro_mod( 'site_responsive' );
	$opts['topbar'] = gastro_mod( 'topbar' );
	$opts['topbar_height'] = gastro_mod( 'topbar_height' );
	$opts['enable_mobile_topbar'] = gastro_mod( 'enable_mobile_topbar' );
	$opts['topbar_column'] = gastro_mod( 'topbar_column' );
	$opts['topbar_color_scheme'] = gastro_mod( 'topbar_color_scheme' );
	$opts['site_layout'] = gastro_mod( 'site_layout' );
	$opts['frame_width'] = gastro_mod( 'frame_width' );
	$opts['preload_style'] = gastro_mod( 'preload_style' );
	$opts['preload_logo'] = gastro_mod( 'preload_logo' );
	$opts['sidenav_style'] = gastro_mod( 'side_navbar_style' );
	$opts['navbar_style'] = gastro_mod( 'navbar_style' );
	$opts['navbar_fullwidth'] = gastro_mod( 'navbar_fullwidth' );
	$opts['navbar_position'] = gastro_mod( 'navbar_position' );
	$opts['footer_parallax'] = gastro_mod( 'footer_parallax' );
	$opts['color_scheme'] = gastro_mod( 'default_color_scheme' );
	$opts['carousel_arrow_background'] = gastro_mod( 'carousel_arrow_background' );
	$opts['carousel_arrow_style'] = gastro_mod( 'carousel_arrow_style' );
	$opts['navbar_background_color'] = gastro_mod( 'navbar_background_color' );
	$opts['navbar_opacity'] = gastro_mod( 'navbar_opacity' );
	$opts['navbar_transparent'] = ( 'transparent' === $opts['navbar_background_color'] || ( 0 == $opts['navbar_opacity'] && 'default' !== $opts['navbar_background_color'] ) ) ? true : false;
	$opts['header_transparent'] = ''; // opacity of navigation bar less than 100

	$opts['topbar_attr'] = array(
		'class' => array( 'gst-topbar', 'gst-' . $opts['topbar_color_scheme'] . '-scheme' ),
		'data_attr' => array(
			'height' => $opts['topbar_height'],
			'mobile' => $opts['enable_mobile_topbar']
		)
	);
	$wrapper_class = array ( 'gst-wrapper', 'gst-p-bg-bg' );
	$wrapper_style = array();
	$wrapper_id = array();

	// Case Blueprint Activated
	$bp_data = gastro_bp_data( $post_id );
	if ( gastro_is_blueprint_active( $bp_data ) ) {
		$builder = $bp_data['builder'];
		$opts['nav_menu'] = isset( $builder['nav_menu'] ) ? $builder['nav_menu'] : 'default';
		$opts['nav_menu_mobile'] = isset( $builder['nav_menu_mobile'] ) ? $builder['nav_menu_mobile'] : 'default';

		// Overide global settings from customizer
		if ( isset( $builder['preload'] ) && 'default' !== $builder['preload'] ) {
			$opts['preload_style'] = $builder['preload'];
		}
		if ( isset( $builder['footer_parallax'] ) && 'default' !== $builder['footer_parallax'] ) {
			$opts['footer_parallax'] = $builder['footer_parallax'];
		}

		if ( !$opts['footer_parallax'] && isset( $builder['page_custom_background'] )
			&& $builder['page_custom_background'] && isset( $builder['page_background_color'] ) ) {
			$wrapper_style['background-color'] = gastro_c( $builder['page_background_color'] );
		}

		if ( isset( $builder['nav_style'] ) && 'default' !== $builder['nav_style'] ) {
			$navbar = explode( '-', $builder['nav_style'] );
			$opts['navbar_position'] = $navbar[0];

			if ( 'top' === $opts['navbar_position'] ) {
				$opts['navbar_style'] = $navbar[1];

				if ( 'inline' === $opts['navbar_style'] ) {
					$opts['inline_navbar_menu_position'] = $navbar[2];
				} elseif ( 'minimal' === $opts['navbar_style'] ) {
					$opts['minimal_navbar_menu_style'] = $navbar[2];
				}
			} else {
				$opts['sidenav_style'] = $navbar[1];
			}
		}

		if ( isset( $builder['nav_transparent'] ) && $builder['nav_transparent'] && 'false' !== $builder['nav_transparent'] ) {
			$opts['navbar_transparent'] = true;
		}

		if ( isset( $builder['nav_full'] ) && 'default' !== $builder['nav_full'] ) {
			$opts['navbar_fullwidth'] = ( 'enable' === $builder['nav_full'] ) ? true : false;
		}

		if ( isset( $builder['css_class'] ) ) {
			$wrapper_class[] = $builder['css_class'];
		}

		if ( isset( $builder['css_id'] ) ) {
			$wrapper_id[] = $builder['css_id'];
		}
	} else {
		$opts['nav_menu'] = 'default';
		$opts['nav_menu_mobile'] = 'default';
	}

	$opts['body_data'] = array(
		'scheme' => $opts['color_scheme'],
		'layout' => $opts['site_layout'],
		'arrow_style' => $opts['carousel_arrow_style'],
		'arrow_background' => $opts['carousel_arrow_background']
	);

	// Frame Layout
	if ( 'frame' === $opts['site_layout'] ) {
		$opts['body_data']['frame_width'] = $opts['frame_width'];
	}

	// Parallax Footer Case
	if ( $opts['footer_parallax'] ) {
		$wrapper_class[] = 'gst-wrapper--parallax-footer';
	}

	// Container Class
	if ( !$opts['navbar_fullwidth'] || 'false' === $opts['navbar_fullwidth'] ) {
		$opts['container_class'] = 'gst-container';
	} else {
		$opts['container_class'] = 'gst-container--fullwidth';
	}

	// If top navbar is transparent or opacity of top navbar less than 100
	if ( 'none' !== $opts['nav_menu'] ) {
		// Topbar Mobile
		if ( $opts['topbar'] && $opts['enable_mobile_topbar'] ) {
			$opts['topbar_mobile'] = 'true';
			$wrapper_class[] = 'mobile-topbar-enable';
		} else {
			$opts['topbar_mobile'] = 'false';
		}

		if ( 'top' === $opts['navbar_position'] && ( ( $opts['navbar_transparent'] && 'false' !== $opts['navbar_transparent'] ) || ( 'transparent' === $opts['navbar_background_color'] || ( 100 > $opts['navbar_opacity'] && 'default' !== $opts['navbar_background_color'] ) ) ) ) {
			$wrapper_class[] = 'gst-wrapper--header-transparent';
			$opts['header_transparent'] = true;
		}
	}

	// Wrap Up wrapper attribute
	$opts['wrapper_attr'] = array(
		'class' => $wrapper_class,
		'id' => $wrapper_id,
		'style_attr' => $wrapper_style
	);

	return $opts;
}
endif;


if ( !function_exists( 'gastro_navbar_options' ) ) :
function gastro_navbar_options( $opts = array() )
{
	$defaults = array(
		'is_item' => false,
		'extra_class' => '',
		'nav_menu' => 'default',
		'mobile_navbar_logo' => '',
		'logo_on' => true,
		'append_inline_style' => false,
		'logo' => gastro_mod( 'logo' ),
		'logo_type' => gastro_mod( 'logo_type' ),
		'logo_text_title' => gastro_mod( 'logo_text_title' ),
		'fixed_navbar_logo' => gastro_mod( 'fixed_navbar_logo' ),
		'fixed_navbar_logo_light' => gastro_mod( 'fixed_navbar_logo_light_scheme' ),
		'fixed_navbar_logo_dark' => gastro_mod( 'fixed_navbar_logo_dark_scheme' ),
		'navbar_position' => gastro_mod( 'navbar_position' ),
		'navbar_style' => gastro_mod( 'navbar_style' ),
		'navbar_size' => gastro_mod( 'navbar_size' ),
		'navbar_menu_border_width' => gastro_mod( 'navbar_menu_border_thickness' ),
		'navbar_stacked_lineheight' => gastro_mod( 'navbar_stacked_lineheight' ),
		'navbar_fullwidth' => gastro_mod( 'navbar_fullwidth' ),
		'navbar_menu_position' => gastro_mod( 'navbar_menu_position' ),
		'minimal_navbar_menu_style' => gastro_mod( 'minimal_navbar_menu_style' ),
		'inline_navbar_menu_position' => gastro_mod( 'inline_navbar_menu_position' ),
		'navbar_full_menu_axis' => gastro_mod( 'navbar_full_menu_axis' ),
		'navbar_menu_hover_style' => gastro_mod( 'navbar_menu_hover_style' ),
		'navbar_color_scheme' => gastro_mod( 'navbar_color_scheme' ),
		'dropdown_color_scheme' => gastro_mod( 'dropdown_color_scheme' ),
		'fixed_navbar' => gastro_mod( 'fixed_navbar' ),
		'fixed_navbar_style' => gastro_mod( 'fixed_navbar_style' ),
		'fixed_navbar_height' => gastro_mod( 'fixed_navbar_height' ),
		'fixed_navbar_hide' => gastro_mod( 'fixed_navbar_hide' ),
		'fixed_navbar_transition' => gastro_mod( 'fixed_navbar_transition' ),
		'fixed_navbar_transition_point' => gastro_mod( 'fixed_navbar_transition_point' ),
		'navbar_background_color' => gastro_mod( 'navbar_background_color' ),
		'navbar_opacity' => gastro_mod( 'navbar_opacity' ),
		'fixed_navbar_background_color' => gastro_mod( 'fixed_navbar_background_color' ),
		'fixed_navbar_opacity' => gastro_mod( 'fixed_navbar_opacity' ),
		'header_action_button' => gastro_mod( 'header_action_button' ),
		'action_type' =>gastro_mod( 'action_type' ),
		'action_button_style' => gastro_mod( 'action_button_style' ),
		'action_button_radius' => gastro_mod( 'action_button_radius' ),
		'action_button_thickness' => gastro_mod( 'action_button_border' ),
		'action_button_hover_style' => gastro_mod( 'action_button_hover_style' ),
		'action_button_icon' => gastro_mod( 'action_button_icon' ),
		'action_button_icon_position' => gastro_mod( 'action_button_icon_position' ),
		'action_button_text' => gastro_mod( 'action_button_text' ),
		'action_link' => gastro_mod( 'action_link' ),
		'action_target_self' => gastro_mod( 'action_target_self' ),
		'header_widget_col' => gastro_mod( 'header_widget_column' ),
		'header_widget_alignment' => gastro_mod( 'header_widget_alignment' ),
		'header_widget_color_scheme' => gastro_mod( 'header_widget_color_scheme' ),
		'action_social_component' => gastro_mod( 'action_social_component' ),
		'search_on' => gastro_mod( 'navbar_search' ),
		'cart_on' => gastro_mod( 'navbar_cart' )
	);

	if ( 'transparent' === $defaults['navbar_background_color'] || ( 0 == $defaults['navbar_opacity'] && 'default' !== $defaults['navbar_background_color'] ) ) {
		$defaults['navbar_transparent'] = true;
	} else {
		$defaults['navbar_transparent'] = false;
	}

	$defaults['navbar_stacked_lineheight'] = !empty( $defaults['navbar_stacked_lineheight'] ) ? $defaults['navbar_stacked_lineheight'] : 56;
	$defaults['navbar_full_menu_axis'] = !empty( $defaults['navbar_full_menu_axis'] ) ? $defaults['navbar_full_menu_axis'] : 'horizontal';
	$defaults['fixed_navbar_transparent'] = false;
	$defaults['two_schemes_logo'] = false;

	$opts = array_merge( $defaults, $opts );

	$opts['nav_menu_args'] = array(
		'navbar_menu' => $opts['nav_menu'],
		'navbar_style' => $opts['navbar_style'],
		'menu_position' => 'top',
		'search_on' => $opts['search_on'],
		'cart_on' => $opts['cart_on']
	);

	$opts['collapsed_classes'] = array( 'gst-collapsed-menu' );

	$navbar_classes = array(
		'gst-navbar',
		'gst-navbar--' . $opts['navbar_position'],
		'gst-navbar--' . $opts['navbar_style'],
		'gst-highlight-' . $opts['navbar_menu_hover_style'],
		$opts['extra_class']
	);
	$navbar_inner_style_attr = array();
	$navbar_style_attr = array();
	$navbar_data_attr = array(
		'position' => $opts['navbar_position'],
		'style' => $opts['navbar_style']
	);

	// Do inline style for navigation menu item
	if ( $opts['append_inline_style'] ) {
		if ( 'default' !== $opts['navbar_background_color'] ) {
			$navbar_inner_style_attr['background-color'] = gastro_hex_to_rgba( $opts['navbar_background_color'], (int)$opts['navbar_opacity']/100 );

			if ( 'transparent' !== $opts['navbar_background_color'] || 0 != $opts['navbar_opacity'] ) {
				$opts['navbar_transparent'] = false;
			}
		}

		if ( isset( $opts['navbar_border_thickness'] ) ) {
			$navbar_inner_style_attr['border-bottom-width'] = (int)$opts['navbar_border_thickness'] . 'px';
			if ( 0 == $opts['navbar_border_thickness'] ) {
				$opts['navbar_menu_border_width'] = 0;
			}
		}

		if ( isset( $opts['navbar_border_color'] ) && 'default' !== $opts['navbar_border_color'] ) {
			$navbar_inner_style_attr['border-bottom-color'] = gastro_c( $opts['navbar_border_color'] );
		}

		if ( isset( $opts['navbar_height'] ) ) {
			$navbar_style_attr['height'] = (int)$opts['navbar_height'] . 'px';
			$navbar_style_attr['line-height'] = (int)$opts['navbar_height'] . 'px';
		}
	}

	// in case there is border width
	if ( $opts['navbar_menu_border_width'] > 0 ) {
		$navbar_classes[] = 'with-border';
	}

	// Container Class
	if ( !$opts['navbar_fullwidth'] || 'false' === $opts['navbar_fullwidth'] ) {
		$opts['container_class'] = 'gst-container';
	} else {
		$opts['container_class'] = 'gst-container--fullwidth';
	}

	// Add dropdown scheme class
	if ( !( ( 'split' === $opts['navbar_style'] || 'minimal' === $opts['navbar_style'] ) && 'full' === $opts['minimal_navbar_menu_style'] ) ) {
		if ( 'default' !== $opts['dropdown_color_scheme'] && $opts['navbar_color_scheme'] !== $opts['dropdown_color_scheme'] ) {
			$navbar_classes[] = 'gst-navbar-dropdown-' . $opts['dropdown_color_scheme'] . '-scheme';
		}
	}

	if ( 'inline' === $opts['navbar_style'] ) {
		$navbar_classes[] = 'gst-navbar--inline--' . $opts['inline_navbar_menu_position'];
	} elseif ( 'minimal' === $opts['navbar_style'] || 'split' === $opts['navbar_style'] ) {
		$navbar_data_attr['collapsed_style'] = $opts['minimal_navbar_menu_style'];
		$navbar_classes[] = $opts['minimal_navbar_menu_style'];
		$opts['collapsed_classes'][] = 'gst-collapsed-menu--' . $opts['minimal_navbar_menu_style'];

		if ( 'full' === $opts['minimal_navbar_menu_style'] ) {
			$navbar_classes[] = 'gst-navbar-collapsed-fixed';
			$opts['collapsed_classes'][] = 'gst-p-bg-bg ' . $opts['navbar_full_menu_axis'];
			$opts['nav_menu_args']['menu_position'] = 'full';
		} else if ( 'offcanvas' === $opts['minimal_navbar_menu_style'] ) {
			$navbar_classes[] = 'gst-navbar-collapsed-fixed';
			$opts['collapsed_classes'][] = 'gst-highlight-' . $opts['navbar_menu_hover_style'];
			$opts['collapsed_classes'][] = 'gst-' . $opts['navbar_color_scheme'] . '-scheme';
			$opts['nav_menu_args']['menu_position'] = 'side';
		}
	} else {
		$navbar_classes[] = 'gst-navbar--' . $opts['navbar_menu_position'];
	}

	// Not stacked style the input size class
	if ( 'stacked' !== $opts['navbar_style'] ) {
		$navbar_classes[] =  'gst-navbar--' . $opts['navbar_size'];
	} else {
		$navbar_data_attr['stacked_menu_height'] = $opts['navbar_stacked_lineheight'];
	}

	// Fixed Navigation Bar
	if ( $opts['fixed_navbar'] ) {
		$navbar_data_attr['fixed'] = 'true';

		if ( $opts['fixed_navbar_hide'] && !$opts['is_item'] ) {
			$navbar_data_attr['autohide'] = 'true';
		}

		if ( 'custom' === $opts['fixed_navbar_style'] && !$opts['is_item'] ) {
			$navbar_data_attr['transition'] = 'custom-' . $opts['fixed_navbar_transition'];
			$navbar_data_attr['transition_point'] = $opts['fixed_navbar_transition_point'];
			$navbar_data_attr['height_fixed'] = $opts['fixed_navbar_height'];

			if ( 'transparent' === $opts['fixed_navbar_background_color'] || ( 0 == $opts['fixed_navbar_opacity'] && 'default' !== $opts['fixed_navbar_background_color'] ) ) {
				$opts['fixed_navbar_transparent'] = true;
				$navbar_classes[] = 'fixed-transparent';
			}
		} else {
			$navbar_data_attr['transition'] = 'default';

			if ( $opts['navbar_transparent'] && 'false' !== $opts['navbar_transparent'] ) {
				$opts['fixed_navbar_transparent'] = true;
				$navbar_classes[] = 'fixed-transparent';
			}
		}

		// Check if have fixed navbar logo
		if ( !empty( $opts['fixed_navbar_logo'] ) ) {
			$navbar_classes[] = 'has-fixed-logo';
		}
	}

	// If Navigation bar transparent case
	if ( $opts['navbar_transparent'] && 'false' !== $opts['navbar_transparent'] ) {
		$navbar_classes[] = 'transparent';
	}

	// Check if has 2 scheme logos
	if ( ( ( $opts['navbar_transparent'] && 'false' !== $opts['navbar_transparent'] ) || ( $opts['fixed_navbar_transparent'] && 'false' !== $opts['fixed_navbar_transparent'] ) ) && !empty( $opts['fixed_navbar_logo_light'] ) && !empty( $opts['fixed_navbar_logo_dark'] ) ) {
		$opts['two_schemes_logo'] = true;
		$navbar_classes[] = 'has-two-schemes-logo';
	}

	$opts['navbar_attribute'] = array(
		'class' => $navbar_classes,
		'style_attr' => $navbar_style_attr,
		'data_attr' => $navbar_data_attr,
	);

	$opts['navbar_inner_style'] = $navbar_inner_style_attr;

	// if not item then add micro data
	if ( !$opts['is_item'] ) {
		$opts['navbar_attribute']['data_attr']['role'] = 'navigation';
		$opts['navbar_attribute']['itemscope'] = true;
		$opts['navbar_attribute']['itemtype'] = 'https://schema.org/SiteNavigationElement';
	} else {
		$opts['navbar_attribute']['data_attr']['role'] = 'item';
	}

	$opts['header_widgets_class'] = array(
		'gst-header-widgets',
		'gst-' . $opts['header_widget_alignment'] . '-align',
		'gst-' . $opts['header_widget_color_scheme'] . '-scheme'
	);

	$opts['action_button_args'] = array(
		'button_style' => $opts['action_button_style'],
		'button_hover' => $opts['action_button_hover_style'],
		'button_radius' => $opts['action_button_radius'],
		'button_thickness' => $opts['action_button_thickness'],
		'button_icon' => $opts['action_button_icon'],
		'button_icon_position' => $opts['action_button_icon_position'],
		'button_label' => $opts['action_button_text'],
		'button_link' => ( 'link' === $opts['action_type'] ) ? $opts['action_link'] : '#' ,
		'button_target_self' => $opts['action_target_self'],
		'button_extra_class' => 'gst-navbar-widget',
		'button_link_extra_class' => 'js-header-' . $opts['action_type'] . '-btn'
	);

	return $opts;
}
endif;


if ( !function_exists( 'gastro_navbar_mobile_options' ) ) :
function gastro_navbar_mobile_options( $opts = array() )
{
	$defaults = array(
		'is_item' => false,
		'extra_class' => '',
		'nav_menu' => 'default',
		'nav_menu_mobile' => 'default',
		'logo_on' => true,
		'append_inline_style' => false,
		'mobile_navbar_logo' => gastro_mod( 'mobile_navbar_logo' ),
		'logo' => gastro_mod( 'logo' ),
		'logo_type' => gastro_mod( 'logo_type' ),
		'logo_text_title' => gastro_mod( 'logo_text_title' ),
		'fixed_navbar_logo' => gastro_mod( 'fixed_navbar_logo' ),
		'fixed_navbar_logo_light' => gastro_mod( 'fixed_navbar_logo_light_scheme' ),
		'fixed_navbar_logo_dark' => gastro_mod( 'fixed_navbar_logo_dark_scheme' ),
		'navbar_position' => gastro_mod( 'navbar_position' ),
		'mobile_navbar_style' => gastro_mod( 'mobile_navbar_style' ),
		'navbar_color_scheme' => gastro_mod( 'navbar_color_scheme' ),
		'navbar_fullwidth' => gastro_mod( 'navbar_fullwidth' ),
		'fixed_navbar' => gastro_mod( 'fixed_navbar' ),
		'fixed_navbar_style' => gastro_mod( 'fixed_navbar_style' ),
		'fixed_navbar_height' => gastro_mod( 'fixed_navbar_height' ),
		'fixed_navbar_hide' => gastro_mod( 'fixed_navbar_hide' ),
		'fixed_navbar_transition' => gastro_mod( 'fixed_navbar_transition' ),
		'fixed_navbar_transition_point' => gastro_mod( 'fixed_navbar_transition_point' ),
		'navbar_background_color' => gastro_mod( 'navbar_background_color' ),
		'navbar_opacity' => gastro_mod( 'navbar_opacity' ),
		'fixed_navbar_background_color' => gastro_mod( 'fixed_navbar_background_color' ),
		'fixed_navbar_opacity' => gastro_mod( 'fixed_navbar_opacity' ),
		'header_action_button' => gastro_mod( 'header_action_button' ),
		'action_mobile_display' => gastro_mod( 'action_mobile_display' ),
		'action_type' =>gastro_mod( 'action_type' ),
		'action_button_style' => gastro_mod( 'action_button_style' ),
		'action_button_radius' => gastro_mod( 'action_button_radius' ),
		'action_button_thickness' => gastro_mod( 'action_button_border' ),
		'action_button_hover_style' => gastro_mod( 'action_button_hover_style' ),
		'action_button_icon' => gastro_mod( 'action_button_icon' ),
		'action_button_icon_position' => gastro_mod( 'action_button_icon_position' ),
		'action_button_text' => gastro_mod( 'action_button_text' ),
		'action_link' => gastro_mod( 'action_link' ),
		'action_target_self' => gastro_mod( 'action_target_self' ),
		'action_social_component' => gastro_mod( 'action_social_component' ),
		'search_on' => gastro_mod( 'navbar_search' ),
		'cart_on' => gastro_mod( 'navbar_cart' )
	);
	$defaults['fixed_navbar_transparent'] = false;
	$defaults['two_schemes_logo'] = false;

	if ( 'transparent' === $defaults['navbar_background_color'] || ( 0 == $defaults['navbar_opacity'] && 'default' !== $defaults['navbar_background_color'] ) ) {
		$defaults['navbar_transparent'] = true;
	} else {
		$defaults['navbar_transparent'] = false;
	}

	$defaults['mobile_navbar_style'] = !empty( $defaults['mobile_navbar_style'] ) ? $defaults['mobile_navbar_style'] : 'full';
	$opts = array_merge( $defaults, $opts );

	$opts['nav_menu_args'] = array(
		'navbar_menu' => $opts['nav_menu'],
		'navbar_menu_mobile' => $opts['nav_menu_mobile'],
		'navbar_style' => $opts['mobile_navbar_style'],
		'menu_position' => $opts['mobile_navbar_style'],
		'search_on' => $opts['search_on'],
		'cart_on' => $opts['cart_on']
	);

	$navbar_mobile_classes = array(
		'gst-navbar--mobile',
		'gst-navbar--mobile--' . $opts['navbar_position'],
		'gst-navbar--mobile--' . $opts['mobile_navbar_style'],
		$opts['extra_class']
	);
	$navbar_mobile_style_attr = array();
	$navbar_inner_style_attr = array();
	$navbar_mobile_data_attr = array(
		'position' => $opts['navbar_position'],
		'style' => $opts['mobile_navbar_style'],
		'collapsed_style' => $opts['mobile_navbar_style']
	);

	// Do inline style for navigation menu item
	if ( $opts['append_inline_style'] ) {
		if ( 'default' !== $opts['navbar_background_color'] ) {
			$navbar_inner_style_attr['background-color'] = gastro_hex_to_rgba( $opts['navbar_background_color'], $opts['navbar_opacity'] );

			if ( 'transparent' !== $opts['navbar_background_color'] || 0 != $opts['navbar_opacity'] ) {
				$opts['navbar_transparent'] = false;
			}
		}

		if ( isset( $opts['navbar_border_thickness'] ) ) {
			$navbar_inner_style_attr['border-bottom-width'] = $opts['navbar_border_thickness'] . 'px';
		}

		if ( isset( $opts['navbar_border_color'] ) && 'default' !== $opts['navbar_border_color'] ) {
			$navbar_inner_style_attr['border-bottom-color'] = gastro_c( $opts['navbar_border_color'] );
		}

		if ( isset( $opts['navbar_height'] ) ) {
			$navbar_mobile_style_attr['height'] = (int)$opts['navbar_height'] . 'px';
			$navbar_mobile_style_attr['line-height'] = (int)$opts['navbar_height'] . 'px';
		}
	}

	// Container Class
	if ( !$opts['navbar_fullwidth'] || 'false' === $opts['navbar_fullwidth'] ) {
		$opts['container_class'] = 'gst-container';
	} else {
		$opts['container_class'] = 'gst-container--fullwidth';
	}

	// Fixed Navigation Bar
	if ( $opts['fixed_navbar'] ) {
		$navbar_mobile_data_attr['fixed'] = 'true';

		if ( $opts['fixed_navbar_hide'] && !$opts['is_item'] ) {
			$navbar_mobile_data_attr['autohide'] = 'true';
		}

		if ( 'custom' === $opts['fixed_navbar_style'] && !$opts['is_item'] ) {
			$navbar_mobile_data_attr['transition'] = 'custom-' . $opts['fixed_navbar_transition'];
			$navbar_mobile_data_attr['transition_point'] = $opts['fixed_navbar_transition_point'];
			$navbar_mobile_data_attr['height_fixed'] = $opts['fixed_navbar_height'];

			if ( 'transparent' === $opts['fixed_navbar_background_color'] || ( 0 == $opts['fixed_navbar_opacity'] && 'default' !== $opts['fixed_navbar_background_color'] ) ) {
				$opts['fixed_navbar_transparent'] = true;
				$navbar_mobile_classes[] = 'fixed-transparent';
			}
		} else {
			$navbar_mobile_data_attr['transition'] = 'default';

			if ( $opts['navbar_transparent'] && 'false' !== $opts['navbar_transparent'] ) {
				$opts['fixed_navbar_transparent'] = true;
				$navbar_mobile_classes[] = 'fixed-transparent';
			}
		}

		// Check if have fixed navbar logo
		if ( !empty( $opts['fixed_navbar_logo'] ) ) {
			$navbar_mobile_classes[] = 'has-fixed-logo';
		}
	}

	// If Navigation bar transparent case
	if ( $opts['navbar_transparent'] && 'false' !== $opts['navbar_transparent'] ) {
		$navbar_mobile_classes[] = 'transparent';
	}

	// Check if has 2 scheme logos
	if ( ( ( $opts['navbar_transparent'] && 'false' !== $opts['navbar_transparent'] ) || ( $opts['fixed_navbar_transparent'] && 'false' !== $opts['fixed_navbar_transparent'] ) ) && !empty( $opts['fixed_navbar_logo_light'] ) && !empty( $opts['fixed_navbar_logo_dark'] ) ) {
		$opts['two_schemes_logo'] = true;
		$navbar_mobile_classes[] = 'has-two-schemes-logo';
	}

	$opts['navbar_mobile_attr'] = array(
		'class' => $navbar_mobile_classes,
		'style_attr' => $navbar_mobile_style_attr,
		'data_attr' => $navbar_mobile_data_attr
	);

	$opts['navbar_inner_style'] = $navbar_inner_style_attr;

	// if not item then add micro data
	if ( !$opts['is_item'] ) {
		$opts['navbar_mobile_attr']['data_attr']['role'] = 'navigation';
		$opts['navbar_mobile_attr']['itemscope'] = true;
		$opts['navbar_mobile_attr']['itemtype'] = 'https://schema.org/SiteNavigationElement';
	} else {
		$opts['navbar_mobile_attr']['data_attr']['role'] = 'item';
	}

	$opts['action_button_args'] = array(
		'button_style' => $opts['action_button_style'],
		'button_hover' => $opts['action_button_hover_style'],
		'button_radius' => $opts['action_button_radius'],
		'button_thickness' => $opts['action_button_thickness'],
		'button_icon' => $opts['action_button_icon'],
		'button_icon_position' => $opts['action_button_icon_position'],
		'button_label' => $opts['action_button_text'],
		'button_link' => ( 'link' === $opts['action_type'] ) ? $opts['action_link'] : '#' ,
		'button_target_self' => $opts['action_target_self'],
		'button_extra_class' => 'gst-navbar-widget',
		'button_link_extra_class' => 'js-header-' . $opts['action_type'] . '-btn'
	);

	return $opts;
}
endif;


if ( !function_exists( 'gastro_side_navbar_options' ) ) :
function gastro_side_navbar_options( $opts = array() )
{
	$defaults = array(
		'is_item' => false,
		'nav_menu' => 'default',
		'mobile_navbar_logo' => '',
		'logo' => gastro_mod( 'logo' ),
		'logo_type' => gastro_mod( 'logo_type' ),
		'logo_text_title' => gastro_mod( 'logo_text_title' ),
		'fixed_navbar_logo' => gastro_mod( 'fixed_navbar_logo' ),
		'fixed_navbar_logo_light' => gastro_mod( 'fixed_navbar_logo_light_scheme' ),
		'fixed_navbar_logo_dark' => gastro_mod( 'fixed_navbar_logo_dark_scheme' ),
		'navbar_position' => gastro_mod( 'navbar_position' ),
		'sidenav_style' => gastro_mod( 'side_navbar_style' ),
		'navbar_fullwidth' => gastro_mod( 'navbar_fullwidth' ),
		'side_navbar_menu_alignment' => gastro_mod( 'side_navbar_menu_alignment' ),
		'navbar_menu_hover_style' => gastro_mod( 'navbar_menu_hover_style' ),
		'navbar_color_scheme' => gastro_mod( 'navbar_color_scheme' ),
		'navbar_full_menu_axis' => gastro_mod( 'navbar_full_menu_axis' ),
		'search_on' => gastro_mod( 'navbar_search' ),
		'cart_on' => gastro_mod( 'navbar_cart' )
	);
	$opts['two_schemes_logo'] = false;
	$defaults['navbar_full_menu_axis'] = !empty( $defaults['navbar_full_menu_axis'] ) ? $defaults['navbar_full_menu_axis'] : 'horizontal';
	$opts = array_merge( $defaults, $opts );

	$opts['nav_menu_args'] = array(
		'navbar_menu' => $opts['nav_menu'],
		'navbar_style' => $opts['sidenav_style'],
		'menu_position' => 'side',
		'search_on' => $opts['search_on'],
		'cart_on' => $opts['cart_on']
	);

	// Navbar Attribute
	$opts['collapsed_classes'] = array( 'gst-collapsed-menu' );
	$navbar_data_attr = array(
		'position' => $opts['navbar_position'],
		'style' => $opts['sidenav_style']
	);
	$navbar_classes = array(
		'gst-side-navbar',
		'gst-side-navbar--' . $opts['navbar_position'],
		'gst-side-navbar--' . $opts['sidenav_style'],
		'gst-' . $opts['side_navbar_menu_alignment'] . '-align',
		'gst-highlight-' . $opts['navbar_menu_hover_style']
	);

	if ( 'minimal' === $opts['sidenav_style'] && !empty( $opts['fixed_navbar_logo_light'] ) && !empty( $opts['fixed_navbar_logo_dark'] ) ) {
		$opts['two_schemes_logo'] = true;
		$navbar_classes[] = 'has-two-schemes-logo';
	}

	if ( 'fixed' !== $opts['sidenav_style'] ) {
		$navbar_data_attr['collapsed_style'] =  $opts['sidenav_style'];
		$opts['collapsed_classes'][] = 'gst-collapsed-menu--' . $opts['sidenav_style'];

		if ( 'full' === $opts['sidenav_style'] ) {
			$opts['collapsed_classes'][] = 'gst-p-bg-bg';
			$opts['collapsed_classes'][] = $opts['navbar_full_menu_axis'];
			$opts['nav_menu_args']['menu_position'] = 'full';
		}
	}

	$opts['navbar_attribute'] = array(
		'class' => $navbar_classes,
		'data_attr' => $navbar_data_attr
	);

	// if not item then add micro data
	if ( !$opts['is_item'] ) {
		$opts['navbar_attribute']['data_attr']['role'] = 'navigation';
		$opts['navbar_attribute']['itemscope'] = true;
		$opts['navbar_attribute']['itemtype'] = 'https://schema.org/SiteNavigationElement';
	} else {
		$opts['navbar_attribute']['data_attr']['role'] = 'item';
	}

	return $opts;
}
endif;


if ( !function_exists( 'gastro_footer_options' ) ) :
function gastro_footer_options( $opts = array(), $post_id = '' )
{
	$opts['back_to_top'] = gastro_mod( 'back_to_top' );
	$back_to_top_background = gastro_mod( 'back_to_top_background' );
	$back_to_top_style = gastro_mod( 'back_to_top_style' );
	$opts['back_to_top_background'] = !empty( $back_to_top_background ) ? $back_to_top_background : 'square' ;
	$opts['back_to_top_style'] = !empty( $back_to_top_style ) ? $back_to_top_style : 'ln-chevron';
	$opts['cookies_notice'] = gastro_mod( 'cookies_notice' );
	$opts['cookies_notice_color_scheme'] = gastro_mod( 'cookies_notice_color_scheme' );
	$opts['cookies_notice_style_attr'] = array();
	$opts['cookies_notice_message'] = gastro_mod( 'cookies_notice_message' );
	$cookies_bg_color = gastro_mod( 'cookies_notice_background_color' );
	if ( 'default' !== $cookies_bg_color ) {
		$cookies_bg_opacity = gastro_mod( 'cookies_notice_background_opacity' );
		$opts['cookies_notice_style_attr']['background-color'] = gastro_hex_to_rgba( $cookies_bg_color, $cookies_bg_opacity/100 );
	}

	$bp_data = gastro_bp_data( $post_id );
	if ( gastro_is_blueprint_active( $bp_data ) ) {
		$defaults = array(
			'full_page_scroll' => false,
			'half_page_scroll' => false,
			'back_to_top' => 'default',
			'footer' => 'footer',
			'footer_id' => ''
		);
		$builder = array_merge( $defaults, $bp_data['builder'] );

		if ( !$builder['full_page_scroll'] && !$builder['half_page_scroll'] ) {
			if ( 'default' !== $builder['back_to_top'] ) {
				$opts['back_to_top'] = ( 'enable' === $builder['back_to_top'] ) ? true : false;
			}
			$opts['footer_active'] = ( 'none' !== $builder['footer'] ) ? true : false;
		} else {
			$opts['footer_active'] = false;
		}

		$opts['footer_id'] = !empty( $builder['footer_id'] ) ? $builder['footer_id'] : gastro_mod( 'footer_id' );
	} else {
		$opts['footer_active'] = gastro_mod( 'footer' );
		$opts['footer_id'] = gastro_mod( 'footer_id' );
	}

	$opts['footer_id'] = apply_filters( 'wpml_object_id', $opts['footer_id'], 'twcbp_block', TRUE );

	return $opts;
}
endif;


if ( !function_exists( 'gastro_post_options' ) ) :
function gastro_post_options( $opts = array(), $post_id = '' )
{
	$defaults = array(
		'builder' => array(
			'active' => false,
		),
		'sidebar' => false,
		'sidebar_select' => '',
		'sidebar_position' => 'right',
		'sidebar_fixed' => false,
		'sidebar_background_color' => 'transparent',
		'sidebar_color_scheme' => '',
		'class' => array( 'gst-content' ),
		'id' => array( 'main' ),
		'style_attr' => array()
	);

	$bp_data = get_post_meta( $post_id, 'bp_data', true );
	$bp_data = is_array( $bp_data ) ? $bp_data : array();
	$opts = array_merge( $defaults, $bp_data );

	$opts['blueprint_active'] = $opts['builder']['active'];

	if ( !$opts['blueprint_active'] ) {
		$mode = get_post_meta( $post_id, 'bp_post_settings_mode', true );
		if ( 'custom' === $mode ) {
			$opts = get_post_meta( $post_id, 'bp_post_settings', true );
			if ( empty( $opts ) ) {
				$opts = gastro_mod( 'post_settings', gastro_default_post_options() );
			}
		} else {
			$opts = gastro_mod( 'post_settings', gastro_default_post_options() );
		}

		array_walk( $opts, 'gastro_sanitize_boolean_value_walker' );
		$breadcrumb_color_scheme = isset( $opts['breadcrumb_color_scheme'] ) ? $opts['breadcrumb_color_scheme'] : 'default';

		$opts['breadcrumb_args'] = array(
			'breadcrumb_on' => $opts['breadcrumb'],
			'title_on' => false,
			'subtitle_on' => false,
			'breadcrumb_position' => 'top',
			'color_scheme' => $breadcrumb_color_scheme,
			'dynamic_nav' => false,
			'breadcrumb_color' => $opts['breadcrumb_color'],
			'background_color' => $opts['breadcrumb_background'],
			'background_opacity' => $opts['breadcrumb_opacity'],
			'alignment' => $opts['breadcrumb_alignment']
		);

		$opts['social_args'] = array(
			'style' => 'icon',
			'divider' => $opts['social_divider'],
			'components' => $opts['social_components'],
			'size' => $opts['social_size'],
			'auto_color' => $opts['social_auto_color'],
			'color' => $opts['social_color'],
			'icon_style' => $opts['social_icon_style'],
			'icon_hover_style' => $opts['social_icon_hover_style'],
			'icon_hover_color' => $opts['social_icon_hover_color'],
			'counter' => $opts['social_counter']
		);

		$opts['nav_args'] = array(
			'type' => 'navigation',
			'style' => $opts['navigation_style'],
			'border_top' => $opts['navigation_border_top'],
			'border_bottom' => $opts['navigation_border_bottom'],
			'alignment' => $opts['navigation_alignment']
		);

		$opts['related_post_args'] = array(
			'type' => 'gst-relatedpost',
			'style' => 'grid',
			'no_of_items' => $opts['related_items'],
			'no_of_columns' => $opts['related_column'],
			'thumbnail_on' => $opts['related_thumbnail'],
			'date_on' => $opts['related_date'],
			'background_color' => $opts['related_entry_background'],
			'heading' => $opts['related_title']
		);

		$opts['class'] = array( 'gst-content' );
		$opts['id'] = 'main';
		$opts['itemprop'] = 'mainContentOfPage';
		$opts['itemscope'] = true;
		$opts['itemtype'] = 'https://schema.org/Article';
		$opts['is_gradient'] = false;
		$opts['content_wrapper_style'] = array();
		$opts['layout_class'] = array( 'gst-post' );

		if ( 'none' === $opts['featured_media_layout'] || 'standard' === $opts['featured_media_layout'] ) {
			$content_extra_class = 'gst-content--no-header';
		} else {
			$content_extra_class = 'gst-content--with-header';

			if ( 'gradient' === $opts['featured_media_layout'] ) {
				// if gradient then override the layout to fullwidth
				$opts['featured_media_layout'] = 'fullwidth';
				$opts['is_gradient'] = true;
			}
		}

		if ( $opts['sidebar'] ) {
			$opts['layout_class'][] = 'gst-post--with-sidebar';
		} else {
			$opts['layout_class'][] = 'gst-post--no-sidebar';
		}

		if ( $opts['social'] ) {
			$opts['layout_class'][] = 'has-share';
		} else {
			$opts['layout_class'][] = 'no-share';
		}

		$opts['featured_media_args'] = false;
		$opts['featured_post_format'] = false;
		$opts['featured_media_attr'] = false;
		$opts['featured_background_attr'] = false;

		if ( 'none' !== $opts['featured_media_layout'] ) {
			$post_format = get_post_format( $post_id );
			$featured_media = get_post_meta( $post_id, 'bp_post_format_settings', true );
			$thumbnail_id = get_post_thumbnail_id( $post_id );
			$background_attr = false;

			if ( !$post_format ) {
				$post_format = 'standard';
			}

			if ( empty( $thumbnail_id ) && ( 'standard' === $post_format || empty( $featured_media ) ) ) {
				$opts['featured_media_layout'] = 'none';
			}

			$media_args = array(
				'post_format' => $post_format,
				'image_id' => $thumbnail_id,
				'image_size' => 'full'
			);

			if ( true === is_array( $featured_media ) ) {
				$media_args = array_merge( $media_args, $featured_media );
			}

			$opts['layout_class'][] = 'gst-post-format--' . $post_format;
			$media_class = array(
				'gst-post-media',
				'gst-post-media--' . $post_format
			);
			$media_data_attr = array();

			if ( 'fullwidth' === $opts['featured_media_layout'] ) {
				$media_class[] = 'js-dynamic-navbar';

				if ( 'standard' === $post_format ) {
					$background_attr = array(
						'background_id' => $thumbnail_id,
						'overlay_on' => true
					);

					if ( $opts['featured_media_parallax'] ) {
						$background_attr['parallax_speed'] = 5;
						$background_attr['content_fade'] = true;
					}
				} elseif ( 'audio' === $post_format ) {
					if ( isset( $featured_media['audio'] ) && !empty( $featured_media['audio'] ) ) {
						$media_class[] = 'gst-post-media--audio-selfhosted';
					} elseif ( !empty( $featured_media['audio-external'] ) ) {
						$media_class[] = 'gst-post-media--audio-external';
						$content_extra_class = 'gst-content--no-header';
					}
				} elseif ( 'quote' === $post_format ) {
					$media_data_attr['scheme'] = 'dark';
				}

				$media_data_attr['scheme'] = $opts['featured_media_scheme'];
			}

			$opts['featured_media_attr'] = array(
				'class' => $media_class,
				'data_attr' => $media_data_attr
			);

			$opts['class'][] = $content_extra_class;
			$opts['featured_media_args'] = $media_args;
			$opts['featured_post_format'] = $post_format;
			$opts['featured_background_attr'] = $background_attr;
		}

		$opts['layout_class'][] = 'gst-post-featured--' . $opts['featured_media_layout'];
		if ( $opts['is_gradient'] ) {
			$opts['layout_class'][] = 'gradient';
		}

		// Comment part
		$opts['comment_attr'] = array(
			'class' => array( 'gst-comment' ),
			'style_attr' => array(),
			'id' => array( 'comments' )
		);

		if ( 'default' !== $opts['comment_color_scheme'] && !$opts['sidebar'] ) {
			$opts['comment_attr']['class'][] = 'gst-' . $opts['comment_color_scheme'] . '-scheme';
		}

		if ( 'default' !== $opts['comment_background_color'] && !$opts['sidebar'] ) {
			$opts['comment_attr']['style_attr']['background-color'] = gastro_c( $opts['comment_background_color'] );
		}

		// Related post part
		$opts['related_attr'] = array(
			'class' => array( 'gst-post-related' ),
			'style_attr' => array()
		);

		if ( 'default' !== $opts['related_color_scheme'] && !$opts['sidebar'] ) {
			$opts['related_attr']['class'][] = 'gst-' . $opts['related_color_scheme'] . '-scheme';
		}

		if ( 'default' !== $opts['related_background_color'] && !$opts['sidebar'] ) {
			$opts['related_attr']['style_attr']['background-color'] = gastro_c( $opts['related_background_color'] );
		}

		$opts['blueprint_active'] = false;
	}

	$opts['post_id'] = $post_id;

	return $opts;
}
endif;


if ( !function_exists( 'gastro_page_content_options' ) ) :
function gastro_page_content_options( $post_id = '' )
{
	$defaults = array(
		'builder' => array(
			'active' => false
		),
		'sidebar' => false,
		'sidebar_select' => '',
		'sidebar_position' => 'right',
		'sidebar_fixed' => false,
		'sidebar_background_color' => 'transparent',
		'sidebar_color_scheme' => '',
		'class' => array( 'gst-content' ),
		'id' => array( 'main' ),
		'style_attr' => array(),
		'itemprop' => 'mainContentOfPage'
	);

	$bp_data = get_post_meta( $post_id, 'bp_data', true );
	$bp_data = is_array( $bp_data ) ? $bp_data : array();
	$opts = array_merge( $defaults, $bp_data );
	$opts['blueprint_active'] = $opts['builder']['active'];

	if ( !$opts['blueprint_active'] ) {
		$opts['builder'] = array(
			'header' => gastro_mod( 'page_title' ) ? 'title' : 'none',
			'title' => array(
				'background_color' => 'default'
			)
		);
	}

	return $opts;
}
endif;


if ( !function_exists( 'gastro_product_options' ) ) :
function gastro_product_options( $opts = array(), $post_id = '' )
{
	$defaults = array(
		'builder' => array(
			'active' => false,
		),
		'sidebar' => gastro_mod( 'product_sidebar' ),
		'sidebar_select' => gastro_mod( 'product_sidebar_select' ),
		'sidebar_position' => gastro_mod( 'product_sidebar_position' ),
		'sidebar_fixed' => gastro_mod( 'product_sidebar_fixed' ),
		'sidebar_background_color' => 'transparent',
		'sidebar_color_scheme' => '',
		'page_custom_background' => false,
		'class' => array( 'gst-content' ),
		'id' => array( 'main' ),
		'style_attr' => array(),
		'content_wrapper_style' => array(),
		'footer_parallax' => gastro_mod( 'footer_parallax' ),
		'main_container_class' => 'gst-container'
	);

	$bp_data = get_post_meta( $post_id, 'bp_data', true );
	$bp_data = is_array( $bp_data ) ? $bp_data : array();
	$opts = array_merge( $defaults, $bp_data );
	$builder = $opts['builder'];

	if ( $opts['builder']['active'] && 'false' !== $opts['builder']['active'] ) {
		$opts['class'][] = 'product-blueprint-active';
		if ( 'none' !== $builder['sidebar'] && isset( $builder['sidebar_id'] ) ) {
			$opts['sidebar'] = true;
			$opts['sidebar_select'] = $builder['sidebar_id'];
			$opts['sidebar_position'] = $builder['sidebar'];
			$opts['sidebar_fixed'] = $builder['sidebar_fixed'];

			if ( isset( $builder['sidebar_full'] ) && $builder['sidebar_full'] ) {
				$opts['main_container_class'] = 'gst-container--fullwidth';
			}
		}

		if ( 'none' === $builder['header'] ) {
			$opts['class'][] = 'gst-content--no-header';
		} else {
			$opts['class'][] = 'gst-content--with-header';
			$opts['content_wrapper_style'] = gastro_get_spacing_style( $opts['builder'], 'padding' );
		}

		// Overide Footer parallax global setting
		if ( isset( $builder['footer_parallax'] ) && 'default' !== $builder['footer_parallax'] ) {
			$opts['footer_parallax'] = ( 'enable' === $builder['footer_parallax'] ) ? true : false;
		}

		if ( isset( $builder['sidebar_background_color'] ) ) {
			$opts['sidebar_background_color'] = $builder['sidebar_background_color'];
		}

		if ( isset( $builder['sidebar_color_scheme'] ) ) {
			$opts['sidebar_color_scheme'] = $builder['sidebar_color_scheme'];
		}

		if ( isset( $builder['page_custom_background'] ) ) {
			$opts['page_custom_background'] = $builder['page_custom_background'];
		}

		if ( $opts['footer_parallax'] && $opts['page_custom_background'] && isset( $builder['page_background_color'] ) ) {
			$opts['style_attr']['background-color'] = gastro_c( $builder['page_background_color'] );
		}
	}

	return $opts;
}
endif;


if ( !function_exists( 'gastro_blueprint_content_options' ) ) :
function gastro_blueprint_content_options( $opts = array() )
{
	$builder = $opts['builder'];
	$builder_defaults = array(
		'header' => 'none',
		'sidebar' => false,
		'sidebar_id' => '',
		'half_page_scroll' => false,
		'full_page_scroll' => false,
		'footer_parallax' => gastro_mod( 'footer_parallax' )
	);
	$builder = array_merge( $builder_defaults, $opts['builder'] );
	$opts['content_wrapper_style'] = gastro_get_spacing_style( $builder, 'padding' );
	$opts['half_page_scroll'] = $builder['half_page_scroll'];
	$opts['full_page_scoll'] = $builder['full_page_scroll'];
	$opts['footer_parallax'] = $builder['footer_parallax'];
	$opts['scroll_page'] = $opts['full_page_scoll'] || $opts['half_page_scroll'];
	$opts['main_container_class'] = 'gst-container';
	$opts['main_wrapper_class'] = array( 'gst-main-wrapper' );

	if ( !$opts['scroll_page'] && 'none' !== $builder['sidebar'] && !empty( $builder['sidebar_id'] ) ) {
		$opts['sidebar'] = true;
		$opts['sidebar_select'] = $builder['sidebar_id'];

		if ( isset( $builder['sidebar_full'] ) && $builder['sidebar_full'] ) {
			$opts['main_container_class'] = 'gst-container--fullwidth';
		}

		if ( isset( $builder['sidebar_fixed'] ) ) {
			$opts['sidebar_fixed'] = $builder['sidebar_fixed'];
		}

		if ( isset( $builder['sidebar'] ) ) {
			$opts['sidebar_position'] = $builder['sidebar'];
		}

		if ( isset( $builder['sidebar_background_color'] ) ) {
			$opts['sidebar_background_color'] = $builder['sidebar_background_color'];
		}

		if ( isset( $builder['sidebar_color_scheme'] ) ) {
			$opts['sidebar_color_scheme'] = $builder['sidebar_color_scheme'];
		}
	} else {
		$opts['sidebar'] = false;
	}

	if ( $opts['scroll_page'] ) {
		$opts['class'][] = 'gst-content--with-header';
	} else {
		if ( 'none' === $builder['header'] ) {
			$opts['class'][] = 'gst-content--no-header';
		} else {
			$opts['class'][] = 'gst-content--with-header';
		}
	}

	if ( $opts['full_page_scoll'] ) {
		$opts['main_wrapper_class'][] = 'gst-scrollpage';
		$opts['main_wrapper_class'][] = 'gst-scrollpage--full';
		$opts['main_wrapper_class'][] = 'gst-scrollpage--full--' . $builder['scroll_direction'];
	}

	if ( $opts['half_page_scroll'] ) {
		$opts['main_wrapper_class'][] = 'gst-scrollpage';
		$opts['main_wrapper_class'][] = 'gst-scrollpage--half';
	}

	if ( $opts['footer_parallax'] && isset( $builder['page_custom_background'] )
		&& $builder['page_custom_background'] && isset( $builder['page_background_color'] ) ) {
		$opts['style_attr']['background-color'] = gastro_c( $builder['page_background_color'] );
	}

	$section_indexes = array();
	foreach ( $opts['sections'] as $key => $section ) {
		$section_indexes[$key] = $section['index'];
	}

	array_multisort( $section_indexes, SORT_ASC, $opts['sections'] );

	return $opts;
}
endif;


if ( !function_exists( 'gastro_section_content_options' ) ) :
function gastro_section_content_options( $opts = array() )
{
	$defaults = array(
		'section' => array(),
		'index' => 1,
		'sidebar' => false,
		'scroll_page' => false,
		'half_page_scroll' => false
	);

	$section_defaults = array(
		'blocks' => array(),
		'sid' => array(),
		'full_width' => false,
		'fit_height' => false,
		'fit_height_percent' => 100,
		'fit_height_offset' => '',
		'vertical' => 'middle',
		'color_scheme' => 'default',
		'border_thickness' => 0
	);

	$opts = array_merge( $defaults, $opts );
	$section = array_merge( $section_defaults, $opts['section'] );
	$opts['section_wrapper_attr'] = array(
		'class' => 'gst-section-wrapper gst-' . $section['vertical'] . '-vertical',
		'style_attr' => gastro_get_spacing_style( $section, 'padding' )
	);
	$opts['container_class'] = ( $section['full_width'] || $opts['half_page_scroll'] ) ? 'gst-container--fullwidth' : 'gst-container';

	$section_class = array( 'gst-section', 'js-dynamic-navbar', 'gst-p-border-border' );
	$section_data = array( 'index' => $opts['index'] );

	if ( 'default' !== $section['color_scheme'] && gastro_mod( 'default_color_scheme' ) !== $section['color_scheme'] ) {
		$section_class[] = 'gst-' . $section['color_scheme'] . '-scheme';
		$section_data['scheme'] = $section['color_scheme'];
	}

	$section_style = array();
	if ( $section['fit_height'] && !$opts['scroll_page'] ) {
		$section_class[] = 'gst-section--fit-height';
		$section_data['screen_percent'] = (int)$section['fit_height_percent'];
		$section_style['height'] = (int)$section['fit_height_percent'] . 'vh';
		$section_style['line-height'] = (int)$section['fit_height_percent'] . 'vh';

		if ( !empty( $section['fit_height_offset'] ) ) {
			$section_data['screen_offset'] = $section['fit_height_offset'];
		}
	}

	$hidden_classes = array(
		'pc_hidden' => 'gst-pc-hidden',
		'tablet_landscape_hidden' => 'gst-tablet-landscape-hidden',
		'tablet_hidden' => 'gst-tablet-hidden',
		'mobile_hidden' => 'gst-mobile-hidden',
		'force_mobile_center' => 'gst-force-center-mobile'
	);

	foreach ( $hidden_classes as $option => $hidden_class ) {
		if ( isset( $section[$option] ) && $section[$option] ) {
			$section_class[] = $hidden_class;
		}
	}

	if ( $section['border_thickness'] > 0 && 'transparent' !== $section['border_color'] ) {
		$section_style['border-bottom-width'] = $section['border_thickness'] . 'px';
		$section_style['border-bottom-style'] = 'solid';
		$section_style['border-bottom-color'] = gastro_c( $section['border_color'] );
	}

	$margin = gastro_get_spacing_style( $section, 'margin' );
	$section_style = array_merge( $section_style, $margin );

	$opts['section_attr'] = array(
		'id' => $section['sid'],
		'class' => $section_class,
		'style_attr' => $section_style,
		'data_attr' => $section_data,
	);

	return $opts;
}
endif;


if ( !function_exists( 'gastro_index_options' ) ) :
function gastro_index_options( $opts = array() )
{
	global $wp_query;
	$opts['sidebar'] = gastro_mod( 'sidebar' );
	$opts['sidebar_position'] = gastro_mod( 'sidebar_position' );
	$opts['content_class'] = 'gst-content';
	$opts['content_class'] .= ( gastro_mod( 'page_title' ) ) ? ' gst-content--with-header' : ' gst-content--no-header';
	$opts['container_class'] = ( gastro_mod( 'blog_full_width' ) ) ? 'gst-container--fullwidth' : 'gst-container';

	$opts['entries_args'] = array(
		'type' => 'gst-blog',
		'style' => gastro_mod( 'blog_style' ),
		'layout' => is_home() ? gastro_mod( 'blog_layout' ) : 'grid',
		'no_of_columns' => gastro_mod( 'blog_columns' ),
		'alignment' => gastro_mod( 'blog_alignment' ),
		'featured_media' => gastro_mod( 'blog_featured_media' ),
		'image_size' => gastro_mod( 'blog_image_size' ),
		'image_ratio' => gastro_mod( 'blog_image_ratio' ),
		'image_hover' => gastro_mod( 'blog_image_hover' ),
		'category_style' => gastro_mod( 'blog_category_style' ),
		'border' => gastro_mod( 'blog_border' ),
		'entry_color_scheme' => gastro_mod( 'blog_color_scheme' ),
		'background_color' => gastro_mod( 'blog_background' ),
		'background_opacity' => gastro_mod( 'blog_opacity' ),
		'spacing' => gastro_mod( 'blog_spacing' ),
		'inner_spacing' => gastro_mod( 'blog_inner_spacing' ),
		'title_uppercase' => gastro_mod( 'blog_title_uppercase' ),
		'title_letter_spacing' => gastro_mod( 'blog_title_letter_spacing' ) . 'em',
		'title_size' => gastro_mod( 'blog_title_font_size' ),
		'title_line_height' => gastro_mod( 'blog_title_line_height' ),
		'pagination' => true,
		'pagination_style' => gastro_mod( 'blog_load' ),
		'filter' => gastro_mod( 'blog_filter' ),
		'filter_alignment' => gastro_mod( 'blog_filter_alignment' ),
		'filter_sorting' => gastro_mod( 'blog_filter_sorting' ),
		'excerpt_content' => gastro_mod( 'blog_excerpt_content' ),
		'excerpt_length' => gastro_mod( 'blog_excerpt_length' ),
		'more_message' => gastro_mod( 'blog_more_message' ),
		'entry_link' => gastro_mod( 'blog_link' ),
		'link_new_tab' => gastro_mod( 'blog_link_new_tab' ),
		'components' => gastro_mod( 'blog_component' ),
		'query' => $wp_query,
		'query_args' => $wp_query->query_vars
	);

	return $opts;
}
endif;


if ( !function_exists( 'gastro_archive_recipe_options' ) ) :
function gastro_archive_recipe_options( $opts = array() )
{
	global $wp_query;
	$opts['sidebar'] = gastro_mod( 'recipe_sidebar' );
	$opts['sidebar_position'] = gastro_mod( 'recipe_sidebar_position' );
	$opts['sidebar_select'] = gastro_mod( 'recipe_sidebar_select' );
	$opts['sidebar_fixed'] = gastro_mod( 'recipe_sidebar_fixed' );
	$opts['content_class'] = 'gst-content';
	$opts['content_class'] .= ( gastro_mod( 'page_title' ) ) ? ' gst-content--with-header' : ' gst-content--no-header';
	$opts['container_class'] = ( gastro_mod( 'recipe_full_width' ) ) ? 'gst-container--fullwidth' : 'gst-container';

	$opts['entries_args'] = array(
		'type' => 'gst-recipe',
		'style' => gastro_mod( 'recipe_style' ),
		'layout' => gastro_mod( 'recipe_layout' ),
		'no_of_columns' => gastro_mod( 'recipe_columns' ),
		'alignment' => gastro_mod( 'recipe_alignment' ),
		'image_size' => gastro_mod( 'recipe_image_size' ),
		'image_ratio' => gastro_mod( 'recipe_image_ratio' ),
		'image_hover' => gastro_mod( 'recipe_image_hover' ),
		'category_style' => gastro_mod( 'recipe_category_style' ),
		'border' => gastro_mod( 'recipe_border' ),
		'entry_color_scheme' => gastro_mod( 'recipe_color_scheme' ),
		'background_color' => gastro_mod( 'recipe_background' ),
		'background_opacity' => gastro_mod( 'recipe_opacity' ),
		'spacing' => gastro_mod( 'recipe_spacing' ),
		'inner_spacing' => gastro_mod( 'recipe_inner_spacing' ),
		'title_uppercase' => gastro_mod( 'recipe_title_uppercase' ),
		'title_letter_spacing' => gastro_mod( 'recipe_title_letter_spacing' ) . 'em',
		'title_size' => gastro_mod( 'recipe_title_font_size' ),
		'title_line_height' => gastro_mod( 'recipe_title_line_height' ),
		'pagination' => true,
		'pagination_style' => gastro_mod( 'recipe_load' ),
		'filter' => gastro_mod( 'recipe_filter' ),
		'filter_alignment' => gastro_mod( 'recipe_filter_alignment' ),
		'filter_sorting' => gastro_mod( 'recipe_filter_sorting' ),
		'excerpt_content' => gastro_mod( 'recipe_excerpt_content' ),
		'excerpt_length' => gastro_mod( 'recipe_excerpt_length' ),
		'more_message' => gastro_mod( 'recipe_more_message' ),
		'entry_link' => gastro_mod( 'recipe_link' ),
		'link_new_tab' => gastro_mod( 'recipe_link_new_tab' ),
		'components' => gastro_mod( 'recipe_component' ),
		'query' => $wp_query,
		'query_args' => $wp_query->query_vars
	);

	return $opts;
}
endif;


if ( !function_exists( 'gastro_archive_title_options' ) ) :
function gastro_archive_title_options( $opts = array() )
{
	$opts['default'] = true;
	$opts['breadcrumb_inline_style'] = false;
	$opts['breadcrumb_color'] = gastro_mod( 'breadcrumb_text_color' );
	$opts['breadcrumb_on'] = gastro_mod( 'breadcrumb' );
	$opts['title_on'] = gastro_mod( 'page_title' );
	$opts['color_scheme'] = gastro_mod( 'page_title_color_scheme' );
	$opts['subtitle_on'] = false;
	$opts['breadcrumb_position'] = gastro_mod( 'breadcrumb_position' );
	$opts['alignment'] = gastro_mod( 'page_title_alignment' );
	$opts['full_width'] = gastro_mod( 'page_title_full_width' );

	if ( is_home() ) {
		$blog_title = gastro_mod( 'page_title_blog_label' );
		$opts['title_content'] = $blog_title;
		$opts['subtitle_on'] = true;
		$opts['subtitle'] = gastro_mod( 'blog_subtitle' );
	} elseif ( is_archive() && !( is_tax() || is_category() || is_tag() || is_author() || is_year() || is_month() || is_day() ) ) {
		$post_type = get_post_type();
		if ( is_post_type_archive( 'gst_recipe' ) ) {
			$opts['title_content'] = apply_filters( 'gastro_recipe_title', gastro_mod( 'recipe_slug' ) );
			$opts['subtitle_on'] = true;
			$opts['subtitle'] = gastro_mod( 'recipe_subtitle' );
		} elseif ( true === gastro_is_woocommerce_activated() && is_shop() ) {
			$shop_title = gastro_mod( 'page_title_shop_label' );
			$opts['title_content'] = $shop_title;
			$opts['subtitle_on'] = true;
			$opts['subtitle'] = gastro_mod( 'shop_subtitle' );

			// function woocommerce_page_title //
			if ( is_search() ) {
				$search_title = gastro_mod( 'page_title_search_label' );
				$opts['title_content'] = sprintf( $search_title . ' &ldquo;%s&rdquo;', get_search_query() );

				if ( get_query_var( 'paged' ) ) {
					$opts['title_content'] .= sprintf( '&nbsp;&ndash; ' . esc_html__( 'Page', 'gastro' ) . ' %s', get_query_var( 'paged' ) );
				}
			}
		} else {
			$blog_title = gastro_mod( 'page_title_blog_label' );
			$opts['title_content'] = $blog_title;
			$opts['subtitle_on'] = true;
			$opts['subtitle'] = gastro_mod( 'blog_subtitle' );
		}
	} elseif ( is_archive() && is_tax() && !is_category() ) {
		$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
		$archive_title = gastro_mod( 'page_title_archive_label' );
		$opts['title_content'] = $archive_title . $term->name;

		if ( !empty( $term->description ) ) {
			$opts['subtitle_on'] = true;
			$opts['subtitle'] = $term->description;
		}
	} elseif ( is_category() ) {
		$category_title = gastro_mod( 'page_title_category_label' );
		$opts['title_content'] = $category_title . single_cat_title( '', false );
		$description = category_description();

		if ( !empty( $description ) ) {
			$opts['subtitle_on'] = true;
			$opts['subtitle'] = $description;
		}
	} elseif ( is_tag() ) {
		$tag_title = gastro_mod( 'page_title_tag_label' );
		$opts['title_content'] = $tag_title . single_tag_title( '', false );
		$description = tag_description();

		if ( !empty( $description ) ) {
			$opts['subtitle_on'] = true;
			$opts['subtitle'] = $description;
		}
	} elseif ( is_author() ) {
		$autor_title = gastro_mod( 'page_title_author_label' );
		$opts['title_content'] = $autor_title . get_the_author_meta( 'display_name' );
	} elseif ( is_day() ) {
		$day_title = gastro_mod( 'page_title_day_label' );
		$opts['title_content'] = $day_title . get_the_date();
	} elseif ( is_month() ) {
		$month_title = gastro_mod( 'page_title_month_label' );
		$opts['title_content'] = $month_title . get_the_date( 'F Y' );
	} elseif ( is_year() ) {
		$year_title = gastro_mod( 'page_title_month_label' );
		$opts['title_content'] = $year_title . get_the_date( 'Y' );
	} elseif ( is_search() ) {
		$search_title = gastro_mod( 'page_title_search_label' );

		global $wp_query;
		$total_results = $wp_query->found_posts;
		$opts['title_content'] = $search_title . get_search_query( false );
		if ( 0 != $total_results ) {
			if ( 1 == $total_results ) {
					$opts['subtitle'] = esc_attr( $total_results ) . esc_html__( ' result found', 'gastro' );
			} else {
					$opts['subtitle'] = esc_attr( $total_results ) . esc_html__( ' results found', 'gastro' );
			}
		} else {
			$opts['subtitle'] = esc_html__( 'no result found', 'gastro' );
		}
	}

	return $opts;
}
endif;


if ( !function_exists( 'gastro_page_hero_options' ) ) :
function gastro_page_hero_options( $opts = array() )
{
	$defaults = array(
		'components' => array( 'title', 'subtitle', 'firstbutton', 'secondbutton', 'divider' ),
		'full_width' => false,
		'fit_height' => false,
		'fit_height_percent' => 100,
		'fit_height_offset' => '',
		'height' => '',
		'content_max_width' => '',
		'divider_width' => '',
		'bannertext_style' => 'flip',
		'bannertext_loop' => true,
		'bannertext_duration' => 3000,
		'bannertext_size' => 36,
		'bannertext_cursor' => '|',
		'bannertext_color' => 'default',
		'bannertext_background_color' => 'default',
		'title_uppercase' => false,
		'title_letter_spacing' => '',
		'title_font' => 'secondary',
		'topsubtitle_font' => 'primary',
		'subtitle_font' => 'primary',
		'bannertext_font' => 'secondary',
		'layout' => 'stacked',
		'horizontal' => 'center',
		'vertical' => 'middle',
		'alignment' => 'center',
		'topsubtitle' => 'Edit Top Subtitle',
		'title' => 'Edit Title',
		'bannertext' => 'Text 1, Text 2, Text 3',
		'subtitle' => 'Edit Subtitle Here',
		'media_type' => 'image',
		'image_id' => '',
		'image_url' => '',
		'image_circle' => false,
		'image_link' => '',
		'image_size' => 'medium',
		'image_max_width' => '',
		'image_popup' => false,
		'icon_style' => 'plain',
		'icon_hover_style' => 'fill',
		'icon_size' => 'medium',
		'icon_color' => 'default',
		'icon_hover_color' => 'default',
		'icon' => 'et-anchor',
		'icon_link' => '',
		'media_target_self' => false,
		'content_fade' => false,
		'parallax_speed' => 2,
		'color_scheme' => 'default',
		'background_type' => 'image',
		'background_id' => '',
		'background_url' => '',
		'background_color' => 'default',
		'background_opacity' => 90,
		'background_size' => 'cover',
		'background_position' => 'center center',
		'background_repeat' => 'repeat',
		'background_animation' => 'none',
		'firstbutton_style' => 'layer',
		'firstbutton_color' => 'brand',
		'firstbutton_hover' => 'inverse',
		'firstbutton_thickness' => 1,
		'firstbutton_radius' => 0,
		'firstbutton_size' => 'medium',
		'firstbutton_label' => 'Button',
		'firstbutton_icon' => 'et-anchor',
		'firstbutton_icon_position' => 'before',
		'firstbutton_link' => '/',
		'firstbutton_target_self' => false,
		'secondbutton_style' => 'layer',
		'secondbutton_color' => 'brand',
		'secondbutton_hover' => 'inverse',
		'secondbutton_thickness' => 1,
		'secondbutton_radius' => 0,
		'secondbutton_size' => 'medium',
		'secondbutton_label' => 'Button',
		'secondbutton_icon' => 'et-anchor',
		'secondbutton_icon_position' => 'before',
		'secondbutton_link' => '/',
		'secondbutton_target_self' => false,
	);
	$opts = array_merge( $defaults, $opts );
	$opts['image_on'] = isset( $opts['image_on'] ) ? $opts['image_on'] : in_array( 'image', $opts['components'] );
	$opts['topsubtitle_on'] = isset( $opts['topsubtitle_on'] ) ? $opts['topsubtitle_on'] : in_array( 'topsubtitle', $opts['components'] );
	$opts['subtitle_on'] = isset( $opts['subtitle_on'] ) ? $opts['subtitle_on'] : in_array( 'subtitle', $opts['components'] );
	$opts['firstbutton_on'] = isset( $opts['firstbutton_on'] ) ? $opts['firstbutton_on'] : in_array( 'firstbutton', $opts['components'] );
	$opts['secondbutton_on'] = isset( $opts['secondbutton_on'] ) ? $opts['secondbutton_on'] : in_array( 'secondbutton', $opts['components'] );
	$opts['title_on'] = isset( $opts['title_on'] ) ? $opts['title_on'] : in_array( 'title', $opts['components'] );
	$opts['bannertext_on'] = isset( $opts['bannertext_on'] ) ? $opts['bannertext_on'] : in_array( 'bannertext', $opts['components'] );
	$opts['divider_on'] = isset( $opts['divider_on'] ) ? $opts['divider_on'] : in_array( 'divider', $opts['components'] );
	$opts['body_on'] = $opts['topsubtitle_on'] || $opts['subtitle_on'] || $opts['title_on'] || $opts['bannertext_on'] || $opts['firstbutton_on'] || $opts['secondbutton_on'];
	$opts['content_wrapper_style'] = array();
	$opts['title_style'] = array(
		'letter-spacing' => is_numeric( $opts['title_letter_spacing'] ) ? $opts['title_letter_spacing'] .'px' : $opts['title_letter_spacing']
	);

	if ( $opts['title_uppercase']  ) {
		$opts['title_style']['text-transform'] = 'uppercase';
	}

	$opts['content_wrapper_style']['max-width'] = is_numeric( $opts['content_max_width'] ) ? $opts['content_max_width'] .'px' : $opts['content_max_width'];

	$opts['class'] = array( 'gst-content-header', 'js-dynamic-navbar', 'gst-page-hero', 'gst-page-hero--' . $opts['layout'] );
	$opts['style_attr'] = gastro_get_spacing_style( $opts, 'margin' );
	$opts['data_attr'] = array( 'role' => 'header' );
	$opts['content_style'] = gastro_get_spacing_style( $opts, 'padding' );

	if ( 'default' !== $opts['color_scheme'] && gastro_mod( 'default_color_scheme' ) !== $opts['color_scheme'] ) {
		$opts['class'][] = 'gst-' . $opts['color_scheme'] . '-scheme';
		$opts['data_attr']['scheme'] = $opts['color_scheme'];
	}

	$opts['container_class'] = ( !$opts['full_width'] ) ? 'gst-container' : 'gst-container--fullwidth';

	if ( $opts['fit_height'] ) {
		$opts['class'][] = 'gst-page-hero--fit-height';
		$opts['data_attr']['screen_percent'] = (int)$opts['fit_height_percent'];
		$opts['style_attr']['height'] = (int)$opts['fit_height_percent'] . 'vh';
		$opts['data_attr']['screen_offset'] = $opts['fit_height_offset'];
	} else {
		$opts['style_attr']['height'] = is_numeric( $opts['height'] ) ? $opts['height'] .'px' : $opts['height'];
	}

	$opts['divider_style'] = array(
		'max-width' => is_numeric( $opts['divider_width'] ) ? $opts['divider_width'] .'px' : $opts['divider_width']
	);

	$hidden_classes = array(
		'pc_hidden' => 'gst-pc-hidden',
		'tablet_landscape_hidden' => 'gst-tablet-landscape-hidden',
		'tablet_hidden' => 'gst-tablet-hidden',
		'mobile_hidden' => 'gst-mobile-hidden',
		'force_mobile_center' => 'gst-force-center-mobile'
	);

	foreach ( $hidden_classes as $option => $hidden_class ) {
		if ( isset( $opts[$option] ) && $opts[$option] ) {
			$opts['class'][] = $hidden_class;
		}
	}

	if ( isset( $opts['animation'] ) && 'none' !== $opts['animation'] ) {
		$opts['class'][] = 'anmt-item anmt-' . $opts['animation'];

		if ( isset( $opts['stagger'] ) && $opts['stagger'] ) {
			$opts['class'][] = 'stagger';
		}
	}

	if ( isset( $opts['css_class'] ) ) {
		$opts['class'][] = $opts['css_class'];
	}

	if ( isset( $opts['css_id'] ) ) {
		$opts['id'] = $opts['css_id'];
	}

	$opts['bannertext_attr'] = array(
		'class' => 'gst-item js-item-bannertext gst-bannertext gst-bannertext--' . $opts['bannertext_style'],
		'data_attr' => array(
			'item' => 'bannertext',
			'loop' => $opts['bannertext_loop'],
			'duration' => $opts['bannertext_duration'],
			'words' => $opts['bannertext']
		)
	);

	if ( 'type' === $opts['bannertext_style'] ) {
		$opts['bannertext_attr']['data_attr']['cursor'] = $opts['bannertext_cursor'];
	}

	$opts['bannertext_dynamic_class'] = array( 'gst-bannertext-dynamic', 'gst-p-brand-color', 'gst-' . $opts['bannertext_font'] . '-font' );
	if ( 48 < $opts['bannertext_size'] ) {
		$opts['bannertext_dynamic_class'][] = 'font-style-big';
	}

	$opts['bannertext_dynamic_style'] = array(
		'color' => gastro_c( $opts['bannertext_color'] ),
		'background-color' => gastro_c( $opts['bannertext_background_color'] ),
		'font-size' => $opts['bannertext_size'] . 'px'
	);

	return $opts;
}
endif;


if ( !function_exists( 'gastro_page_title_options' ) ) :
function gastro_page_title_options( $opts = array() )
{
	$defaults = array(
		'default' => false,
		'dynamic_nav' => true,
		'components' => array( 'breadcrumb', 'title', 'subtitle' ),
		'subtitle' => 'Enter Subtitle Here',
		'full_width' => false,
		'breadcrumb_position' => 'top',
		'title_color' => 'default',
		'breadcrumb_inline_style' => false,
		'breadcrumb_color' => 'default',
		'breadcrumb_background_color' => 'default',
		'breadcrumb_opacity' => 100,
		'alignment' => 'left',
		'color_scheme' => 'default',
		'background_id' => '',
		'background_url' => '',
		'background_color' => 'default',
		'background_opacity' => 90,
		'background_size' => 'cover',
		'background_position' => 'center center',
		'background_repeat' => 'repeat',
		'title_font' => 'secondary',
		'subtitle_font' => 'primary',
		'breadcrumb_font' => 'primary'
	);
	$opts = array_merge( $defaults, $opts );
	$opts['breadcrumb_on'] = isset( $opts['breadcrumb_on'] ) ? $opts['breadcrumb_on'] : in_array( 'breadcrumb', $opts['components'] );
	$opts['title_on'] = isset( $opts['title_on'] ) ? $opts['title_on'] : in_array( 'title', $opts['components'] );
	$opts['subtitle_on'] = isset( $opts['subtitle_on'] ) ? $opts['subtitle_on'] : in_array( 'subtitle', $opts['components'] );
	$opts['container_class'] = ( !$opts['full_width'] ) ? 'gst-container' : 'gst-container--fullwidth';

	$opts['class'] = array( 'gst-content-header', 'gst-page-title', 'gst-page-title--' . $opts['breadcrumb_position'], 'gst-' . $opts['alignment'] . '-align' );
	$padding = gastro_get_spacing_style( $opts, 'padding' );
	$margin = gastro_get_spacing_style( $opts, 'margin' );
	$opts['style_attr'] = array_merge( $padding, $margin  );
	$opts['data_attr'] = array( 'role' => 'header' );
	$opts['breadcrumb_class'] = 'gst-page-title-breadcrumb gst-' . $opts['breadcrumb_font'] . '-font gst-p-text-color';
	$opts['breadcrumb_class'] .= ( 'default' !== $opts['breadcrumb_color'] ) ? ' custom-color' : '';
	$opts['breadcrumb_style'] = array( 'color' => gastro_c( $opts['breadcrumb_color'] ) );
	$opts['title_style'] = array( 'color' => gastro_c( $opts['title_color'] ) );

	if ( $opts['dynamic_nav'] ) {
		$opts['class'][] = 'js-dynamic-navbar';
	}

	if ( 'default' !== $opts['color_scheme'] && gastro_mod( 'default_color_scheme' ) !== $opts['color_scheme'] ) {
		$opts['class'][] = 'gst-' . $opts['color_scheme'] . '-scheme';
		$opts['data_attr']['scheme'] = $opts['color_scheme'];
	}

	if ( $opts['default'] ) {
		$opts['class'][] = 'gst-page-title--default';
	}

	if ( 'inline' === $opts['breadcrumb_position'] ) {
		$opts['class'][] = 'gst-page-title--'. $opts['alignment'];
	} else {
		$opts['breadcrumb_style']['background-color'] = gastro_hex_to_rgba( $opts['breadcrumb_background_color'], $opts['breadcrumb_opacity'] / 100 );
	}

	$hidden_classes = array(
		'pc_hidden' => 'gst-pc-hidden',
		'tablet_landscape_hidden' => 'gst-tablet-landscape-hidden',
		'tablet_hidden' => 'gst-tablet-hidden',
		'mobile_hidden' => 'gst-mobile-hidden',
		'force_mobile_center' => 'gst-force-center-mobile'
	);

	foreach ( $hidden_classes as $option => $hidden_class ) {
		if ( isset( $opts[$option] ) && $opts[$option] ) {
			$opts['class'][] = $hidden_class;
		}
	}

	if ( isset( $opts['animation'] ) && 'none' !== $opts['animation'] ) {
		$opts['class'][] = 'anmt-item anmt-' . $opts['animation'];

		if ( isset( $opts['stagger'] ) && $opts['stagger'] ) {
			$opts['class'][] = 'stagger';
		}
	}

	if ( isset( $opts['css_class'] ) ) {
		$opts['class'][] = $opts['css_class'];
	}

	if ( isset( $opts['css_id'] ) ) {
		$opts['id'] = $opts['css_id'];
	}

	return $opts;
}
endif;


if ( !function_exists( 'gastro_entry_options' ) ) :
function gastro_entry_options( $opts = array(), $post_id = '' )
{
	foreach ( $opts as $key => $value ) {
		if ( 'true' === $value ) {
			$opts[$key] = true;
		} elseif ( 'false' === $value ) {
			$opts[$key] = false;
		}
	}

	if ( $bp_data = gastro_bp_data( $post_id ) ) {
		$entry_setting = $bp_data['builder'];
	} else {
		$entry_setting = array();
	}

	$single_width = isset( $entry_setting['width'] ) ? $entry_setting['width'] : 1;
	$single_height = isset( $entry_setting['height'] ) ? $entry_setting['height'] : 1;

	$defaults = array(
		'post_type' => 'post',
		'post_taxonomy' => 'category',
		'post_tag' => 'tag',
		'style' => 'plain',
		'layout' => 'grid',
		'media_on' => true,
		'author_on' => true,
		'category_on' => true,
		'tag_on' => true,
		'title_on' => true,
		'excerpt_on' => true,
		'comment_on' => true,
		'rating_on' => false,
		'date_on' => true,
		'link_on' => true,
		'icon_on' => true,
		'customfield_on' => false,
		'featured_media' => 'image',
		'image_size' => 'medium_large',
		'image_ratio' => 'auto',
		'image_lazy_load' => false,
		'image_hover' => 'none',
		'media_style' => array(),
		'category_style' => 'plain',
		'no_of_columns' => 2,
		'spacing' => 30,
		'inner_spacing' => 0,
		'entry_color_scheme' => 'default',
		'background_color' => 'default',
		'background_opacity' => 100,
		'title_uppercase' => false,
		'title_letter_spacing' => '',
		'title_line_height' => '',
		'title_size' => 16,
		'title_font' => 'secondary',
		'meta_font' => 'primary',
		'excerpt_content' => 'excerpt',
		'excerpt_length' => '',
		'more_message' => 'Read More',
		'entry_link' => 'post',
		'link_new_tab' => false,
		'fade' => false,
		'variable_width' => false,
		'carousel_height' => '',
	);

	$opts = array_merge( $defaults, $opts );

	// entry setting
	$opts['entry_link'] = ( isset( $entry_setting['entry_link'] ) && 'default' !== $entry_setting['entry_link'] ) ? $entry_setting['entry_link'] : $opts['entry_link'];
	$opts['alternate_link'] = isset( $entry_setting['alternate_link'] ) ? $entry_setting['alternate_link'] : '';
	$opts['image_hover'] = ( isset( $entry_setting['image_hover'] ) && 'default' !== $entry_setting['image_hover'] ) ? $entry_setting['image_hover'] : $opts['image_hover'];
	$opts['no_of_columns'] = !empty( $opts['no_of_columns'] ) ? $opts['no_of_columns'] : 2;
	$opts['entry_class'] = array( 'gst-entry' );
	$opts['entry_style'] = array();
	$opts['header_style'] = array();
	$opts['body_style'] = array();
	$opts['inner_attr'] = array(
		'class' => array( 'gst-entry-inner gst-p-border-border' ),
		'style_attr' => array()
	);
	$opts['media_wrapper_style'] = array();

	$opts['title_attr'] = array(
		'class' => array(
			'gst-entry-title',
			'gst-s-text-color',
			'gst-' . $opts['title_font'] . '-font'
		),
		'style_attr' => array(
			'font-size' => $opts['title_size'] . 'px',
			'letter-spacing' => is_numeric( $opts['title_letter_spacing'] ) ? $opts['title_letter_spacing'] .'px' : $opts['title_letter_spacing'],
			'line-height' => $opts['title_line_height'],
			'text-transform' => $opts['title_uppercase'] ? 'uppercase' : ''
		)
	);

	$opts['link_target'] = ( $opts['link_new_tab'] ) ? '_blank' : '_self';
	$opts['category_class'] = 'gst-entry-category gst-entry-category--' . $opts['category_style'] . ' gst-' . $opts['meta_font'] . '-font';

	if ( 'plain' === $opts['category_style'] ) {
		$opts['category_class'] .= ' gst-p-brand-color';
	} else if ( 'bar' === $opts['category_style'] ) {
		$opts['category_class'] .= ' gst-p-text-color';
		$opts['category_line_class'] = 'gst-entry-category-line gst-p-brand-bg';
	} else if ( 'line' === $opts['category_style'] ) {
		$opts['category_class'] .= ' gst-p-text-color';
		$opts['category_line_class'] = 'gst-entry-category-line gst-p-text-bg';
	}

	if ( isset( $opts['animation'] ) && 'none' !== $opts['animation'] ) {
		$opts['inner_attr']['class'][] = 'anmt-item anmt-' . $opts['animation'];

		if ( isset( $opts['stagger'] ) && $opts['stagger'] ) {
			$opts['inner_attr']['class'][] = 'stagger';
		}
	}

	if ( 'alternate' === $opts['entry_link'] ) {
		$opts['permalink'] = empty( $opts['alternate_link'] ) ? get_permalink( $post_id ) : $opts['alternate_link'];
	} else {
		$opts['permalink'] = get_permalink( $post_id );
	}

	if ( 'masonry' === $opts['layout'] && 'overlay' === $opts['style'] ) {
		$opts['inner_attr']['style_attr']['width'] = 'calc(100% - ' . $opts['spacing'] . 'px)';
		$opts['inner_attr']['style_attr']['margin-bottom'] = $opts['spacing'] . 'px';
		$opts['media_wrapper_style']['margin'] = -$opts['spacing'] / 2 . 'px ' . -$opts['spacing'] / 2 . 'px ' . -$opts['spacing'] / 2 . 'px';
		$masonry_width = ( $single_width > $opts['no_of_columns'] ) ? $opts['no_of_columns'] : $single_width;
		$opts['image_column_width'] = ( $opts['no_of_columns'] != 5 ) ? 12 / $opts['no_of_columns'] * $masonry_width : $masonry_width . '-5';
		$opts['entry_class'][] = 'gst-col-' . $opts['image_column_width'];

		if ( 'auto' !== $opts['image_ratio'] ) {
			$opts['image_ratio'] = gastro_adjust_ratio( $masonry_width, $single_height, $opts['image_ratio'] );
		}
	} else {
		$opts['entry_style']['margin-bottom'] = $opts['spacing'] . 'px';

		if ( 'zigzag' !== $opts['layout'] ) {
			$opts['entry_style']['padding'] = '0 ' . $opts['spacing'] / 2 . 'px';
			if ( 'carousel' === $opts['layout'] && ( $opts['fade'] || ( 'overlay' === $opts['style'] && $opts['variable_width'] ) ) ) {
				$opts['image_column_width'] = 12;
			} else {
				$opts['image_column_width'] = ( $opts['no_of_columns'] != 5 ) ? 12 / $opts['no_of_columns'] : '1-5';
				$opts['entry_class'][] = 'gst-col-' . $opts['image_column_width'];
			}
		} else {
			$opts['image_column_width'] = 12;
		}
	}

	// override background color from entry setting
	if ( isset( $entry_setting['background_custom'] ) && $entry_setting['background_custom'] ) {
		$opts['background_color'] = isset( $entry_setting['background_color'] ) ? $entry_setting['background_color'] : $opts['background_color'];
		$opts['background_color'] = isset( $entry_setting['background_opacity'] ) ? $entry_setting['background_opacity'] : $opts['background_opacity'];
		$opts['entry_color_scheme'] = isset( $entry_setting['entry_color_scheme'] ) ? $entry_setting['entry_color_scheme'] : $opts['entry_color_scheme'];
	}

	$background_color = gastro_hex_to_rgba( $opts['background_color'], $opts['background_opacity'] / 100 );
	if ( 'overlay' === $opts['style'] ) {
		$opts['inner_attr']['style_attr']['color'] = gastro_contrast_color( $opts['background_color'] );
		$opts['inner_attr']['style_attr']['background-color'] = $background_color;

		if ( 'transparent' !== $background_color && 'default' !== $background_color ) {
			$opts['inner_attr']['class'][] = 'with-background';
		}

		// animation on hover (content on hover)
		if ( 'none' !== $opts['image_hover'] ) {
			$opts['inner_attr']['class'][] = 'anmt-image-' . $opts['image_hover'];
		}

		if ( 0 != $opts['inner_spacing'] ) {
			$opts['body_style']['padding-left'] = $opts['inner_spacing'] / 2 . 'px';
			$opts['body_style']['padding-right'] = $opts['inner_spacing'] / 2 . 'px';
		}
	} else {
		$opts['body_style']['background-color'] = $background_color;
		if ( 0 != $opts['inner_spacing'] ) {
			$opts['body_style']['padding'] = $opts['inner_spacing'] / 2 . 'px';
		}
	}

	if ( 'default' !== $opts['entry_color_scheme'] ) {
		$opts['entry_class'][] = 'gst-entry-' . $opts['entry_color_scheme'] . '-scheme';
	}

	// Featured Media
	$thumb_id = get_post_thumbnail_id( $post_id );
	$opts['featured_media_args'] = array(
		'image_id' => $thumb_id,
		'image_link' => $opts['permalink'],
		'image_size' => $opts['image_size'],
		'image_ratio' => $opts['image_ratio'],
		'image_hover' => $opts['image_hover'],
		'image_column_width' => $opts['image_column_width'],
		'media_style' => $opts['media_style'],
		'wrapper_style' => $opts['media_wrapper_style'],
		'image_lazy_load' => $opts['image_lazy_load'],
		'media_target_self' => !$opts['link_new_tab']
	);

	if ( 'overlay' === $opts['style'] ) {
		$opts['featured_media_args']['post_format'] = 'standard';
	} elseif ( 'image' === $opts['featured_media'] ) {
		$opts['featured_media_args']['post_format'] = 'standard';
	} else {
		$post_format = get_post_format( $post_id ) ? get_post_format( $post_id ) : 'standard';
		$opts['featured_media_args']['post_format'] = $post_format;

		if ( 'standard' !== $post_format ) {
			$post_type_settings = get_post_meta( $post_id, 'bp_post_format_settings', true );
			if ( is_array( $post_type_settings ) ) {
				$opts['featured_media_args'] = array_merge( $opts['featured_media_args'], $post_type_settings );
			}
		}
	}

	return $opts;
}
endif;


if ( !function_exists( 'gastro_entry_product_options' ) ) :
function gastro_entry_product_options( $opts = array() )
{
	foreach ( $opts as $key => $value ) {
		if ( 'true' === $value ) {
			$opts[$key] = true;
		} elseif ( 'false' === $value ) {
			$opts[$key] = false;
		}
	}

	$defaults = array(
		'extra_class' => array(),
		'style' => 'list',
		'animation' => 'none',
		'stagger' => false,
		'enable_link' => true,
		'no_of_columns' => 4,
		'spacing' => '',
		'image_on' => true,
		'subtitle_on' => true,
		'badge_on' => true,
		'image_size' => 'medium_large',
		'image_ratio' => 'auto',
		'image_lazy_load' => false,
		'image_circle' => false,
		'title_font' => 'secondary',
		'title_size' => 14,
		'title_uppercase' => 0,
		'title_letter_spacing' => '',
		'title_line_height' => '',
		'subtitle_font' => 'primary',
		'subtitle_size' => 12,
		'subtitle_uppercase' => false,
		'subtitle_letter_spacing' => '',
		'subtitle_line_height' => '',
		'price_font' => 'secondary',
		'price_font_size' => 14,
	);
	$opts = array_merge( $defaults, $opts );
	$opts['entry_class'] = array( 'gst-entry' );
	$opts['entry_class'] = array_merge( $opts['extra_class'], $opts['entry_class'] );
	$opts['entry_inner_class'] = array( 'gst-entry-inner', 'gst-p-border-border' );
	$opts['entry_inner_style'] = array();
	$opts['entry_style'] = array();
	$opts['menu_line_style'] = array();
	$opts['price_style'] = array(
		'font-size' => $opts['price_font_size'] . 'px'
	);

	$opts['media_args'] = array(
		'image_size' => $opts['image_size'],
		'image_ratio' => $opts['image_ratio'],
		'image_circle' => $opts['image_circle'],
		'image_lazy_load' => $opts['image_lazy_load']
	);

	if ( !empty( $opts['animation'] ) && 'none' !== $opts['animation'] ) {
		$opts['entry_inner_class'][] = 'anmt-item anmt-' . $opts['animation'];

		if ( $opts['stagger'] ) {
			$opts['entry_inner_class'][] = 'stagger';
		}
	}

	if ( 'grid' === $opts['style'] ) {
		$column_width = ( 5 == $opts['no_of_columns'] ) ? '1-5' : 12/$opts['no_of_columns'];
		$opts['entry_class'][] = 'gst-col-' . $column_width;
		$opts['media_args']['image_column_width'] = $column_width;

		if ( !empty( $opts['spacing'] ) || ( is_numeric( $opts['spacing'] ) && 0 == $opts['spacing'] ) ) {
			$opts['entry_style']['padding'] = '0 ' . $opts['spacing']/2 . 'px';
			$opts['entry_style']['margin-bottom'] = $opts['spacing'] . 'px';
		}
	} else if ( !empty( $opts['spacing'] ) || ( is_numeric( $opts['spacing'] ) && 0 == $opts['spacing'] ) ) {
		$opts['entry_inner_style']['padding-bottom'] = $opts['spacing']/2 . 'px';
		$opts['entry_style']['margin-bottom'] = $opts['spacing']/2 . 'px';
	}

	$opts['title_style'] = array(
		'font-size' => $opts['title_size'] . 'px',
		'letter-spacing' => is_numeric( $opts['title_letter_spacing'] ) ? $opts['title_letter_spacing'] .'px' : $opts['title_letter_spacing'],
		'line-height' => $opts['title_line_height'],
		'text-transform' => $opts['title_uppercase'] ? 'uppercase' : ''
	);

	$opts['subtitle_attr'] = array(
		'class' => array(
			'gst-entry-subtitle',
			'gst-' . $opts['subtitle_font'] . '-font'
		),
		'style_attr' => array(
			'font-size' => $opts['subtitle_size'] . 'px',
			'letter-spacing' => is_numeric( $opts['subtitle_letter_spacing'] ) ? $opts['subtitle_letter_spacing'] .'px' : $opts['subtitle_letter_spacing'],
			'line-height' => $opts['subtitle_line_height'],
			'text-transform' => $opts['subtitle_uppercase'] ? 'uppercase' : ''
		)
	);

	return $opts;
}
endif;


if ( !function_exists( 'gastro_entry_productcat_options' ) ) :
function gastro_entry_productcat_options( $opts = array() )
{
	foreach ( $opts as $key => $value ) {
		if ( 'true' === $value ) {
			$opts[$key] = true;
		} elseif ( 'false' === $value ) {
			$opts[$key] = false;
		}
	}

	$defaults = array(
		'taxonomy' => 'product_cat',
		'extra_class' => array(),
		'style' => 'overlay',
		'layout' => 'grid',
		'animation' => 'none',
		'stagger' => false,
		'category_obj' => false,
		'no_of_columns' => 4,
		'spacing' => '',
		'image_size' => 'large',
		'image_ratio' => 'auto',
		'image_lazy_load' => false,
		'image_hover' => 'none',
		'title_font' => 'secondary',
		'title_size' => 16,
		'title_uppercase' => 0,
		'title_letter_spacing' => '',
	);
	$opts = array_merge( $defaults, $opts );
	$column_width = ( 5 == $opts['no_of_columns'] ) ? '1-5' : 12/$opts['no_of_columns'];
	$opts['entry_class'] = array( 'gst-entry', 'gst-productcat-entry', 'gst-productcat-entry--' . $opts['style'], 'gst-col-' . $column_width );
	$opts['entry_class'] = array_merge( $opts['extra_class'], $opts['entry_class'] );

	if ( !empty( $opts['spacing'] ) ) {
		$opts['entry_style'] = array( 'padding' => '0 '. $opts['spacing']/2 . 'px' );

		if ( 'carousel' !== $opts['layout'] ) {
			$opts['entry_style']['margin-bottom'] = $opts['spacing'] . 'px';
		}
	}

	$opts['entry_inner_class'] = array( 'gst-entry-inner' );
	if ( 'none' !== $opts['image_hover'] ) {
		$opts['entry_inner_class'][] = ' anmt-image-' . $opts['image_hover'];
	}

	if ( !empty( $opts['animation'] ) && 'none' !== $opts['animation'] ) {
		$opts['entry_inner_class'][] = 'anmt-item anmt-' . $opts['animation'];

		if ( $opts['stagger'] ) {
			$opts['entry_inner_class'][] = 'stagger';
		}
	}

	$opts['title_attr'] = array(
		'class' => array(
			'gst-entry-title',
			'gst-' . $opts['title_font'] . '-font'
		),
		'style_attr' => array(
			'font-size' => $opts['title_size'] . 'px',
			'letter-spacing' => is_numeric( $opts['title_letter_spacing'] ) ? $opts['title_letter_spacing'] .'px' : $opts['title_letter_spacing'],
			'text-transform' => $opts['title_uppercase'] ? 'uppercase' : ''
		)
	);

	$opts['media_args'] = array(
		'image_size' => $opts['image_size'],
		'image_ratio' => $opts['image_ratio'],
		'image_hover' => $opts['image_hover'],
		'image_column_width' => $column_width,
		'image_lazy_load' => $opts['image_lazy_load']
	);

	return $opts;
}
endif;


if ( !function_exists( 'gastro_entry_instagram_options' ) ) :
function gastro_entry_instagram_options( $opts = array() )
{
	array_walk( $opts, 'gastro_sanitize_boolean_value_walker' );

	$defaults = array(
		'image' => array(),
		'link_label' => __( 'View Post', 'gastro' ),
		'caption_on' => true,
		'image_index' => 0,
		'style' => 'grid',
		'no_of_columns' => 3,
		'image_ratio' => 'auto',
		'image_lazy_load' => false,
		'on_image_click' => 'none',
		'image_hover' => 'none',
		'spacing' => 15,
		'variable_width' => false,
		'height' => 500,
		'fade' => false
	);
	$opts = array_merge( $defaults, $opts );

	$opts['item_attr'] = array(
		'class' => array( 'gst-gallery-item' ),
		'style_attr' => array()
	);

	$opts['body_attr'] = array(
		'class' => array( 'gst-gallery-body' ),
		'style_attr' => array()
	);

	$opts['media_wrapper_style'] = array();

	if ( isset( $opts['animation'] ) && 'none' !== $opts['animation'] ) {
		$opts['body_attr']['class'][] = 'anmt-item anmt-' . $opts['animation'];

		if ( isset( $opts['stagger'] ) && $opts['stagger'] ) {
			$opts['body_attr']['class'][] = 'stagger';
		}
	}

	if ( 'masonry' === $opts['style'] ) {
		$masonry_size = array(
			'width' => 1,
			'height' => 1
		);
		$opts['body_attr']['style_attr']['width'] = 'calc(100% - ' . $opts['spacing'] . 'px)';
		$opts['body_attr']['style_attr']['margin-bottom'] = $opts['spacing'] . 'px';
		$opts['media_wrapper_style']['margin'] = -$opts['spacing'] / 2 . 'px ' . -$opts['spacing'] / 2 . 'px ' . -$opts['spacing'] / 2 . 'px';

		if ( 'auto' !== $opts['image_ratio'] ) {
			$opts['image_ratio'] = gastro_adjust_ratio( $masonry_size['width'], $masonry_size['height'], $opts['image_ratio'] );
		}

		if ( 5 == $opts['no_of_columns'] ) {
			$image_column_width = $masonry_size['width'] . '-5';
			$opts['item_attr']['class'][] = 'gst-col-' . $image_column_width;
		} else {
			$image_column_width = 12 / $opts['no_of_columns'] * $masonry_size['width'];
			$opts['item_attr']['class'][] = 'gst-col-'. $image_column_width;
		}
	} elseif ( 'carousel' === $opts['style'] ) {
		$opts['item_attr']['style_attr']['padding'] = '0 '. $opts['spacing']/2 .'px';
		if ( !$opts['variable_width'] && !$opts['fade'] ) {
			if ( 5 == $opts['no_of_columns'] ) {
				$image_column_width = '1-5';
				$opts['item_attr']['class'][] = 'gst-col-' . $image_column_width;
			} else {
				$image_column_width = 12 / $opts['no_of_columns'];
				$opts['item_attr']['class'][] = 'gst-col-'. $image_column_width;
			}
		} elseif ( $opts['variable_width'] ) {
			$opts['image']['style_attr'] = 'max-height:' . $opts['height'] .'px;';
		}
	} elseif ( 'grid' === $opts['style'] ) {
		$opts['item_attr']['style_attr']['padding'] = '0 '. $opts['spacing']/2 .'px';
		$opts['item_attr']['style_attr']['margin-bottom'] = $opts['spacing'] . 'px';
		if ( 5 == $opts['no_of_columns'] ) {
			$image_column_width = '1-5';
			$opts['item_attr']['class'][] = 'gst-col-' . $image_column_width;
		} else {
			$image_column_width = 12 / $opts['no_of_columns'];
			$opts['item_attr']['class'][] = 'gst-col-'. $image_column_width;
		}
	}

	return $opts;
}
endif;


if ( !function_exists( 'gastro_entry_gallery_options' ) ) :
function gastro_entry_gallery_options( $opts = array() )
{
	array_walk( $opts, 'gastro_sanitize_boolean_value_walker' );

	$defaults = array(
		'responsive' => true,
		'image' => '',
		'style' => 'grid',
		'title_on' => false,
		'caption_on' => false,
		'image_title' => '',
		'image_caption' => '',
		'no_of_columns' => 3,
		'scroll_height' => 500,
		'image_size' => 'large',
		'image_ratio' => 'auto',
		'image_lazy_load' => false,
		'on_image_click' => 'popup',
		'image_hover' => 'none',
		'media_style' => array(),
		'spacing' => 15,
		'variable_width' => false,
		'fade' => false,
		'thumbnail' => false
	);
	$opts = array_merge( $defaults, $opts );
	$opts['responsive'] = ( 'scroll' === $opts['style'] ) ? false : $opts['responsive'];
	$opts['item_attr'] = array(
		'class' => array( 'gst-gallery-item' ),
		'style_attr' => array()
	);

	$opts['body_attr'] = array(
		'class' => array( 'gst-gallery-body' ),
		'style_attr' => array()
	);

	$opts['media_wrapper_style'] = array();

	$image_column_width = '';
	$height = ( is_numeric( $opts['scroll_height'] ) || false !== strpos( $opts['scroll_height'], 'px' ) ) ? ( int )$opts['scroll_height'] : 500;
	$attachment = get_post( $opts['image'] );
	if ( !empty( $attachment ) ) {
		$opts['image_title'] = $attachment->post_title;
		$opts['image_caption'] = $attachment->post_excerpt;

		if ( isset( $opts['animation'] ) && 'none' !== $opts['animation'] ) {
			$opts['body_attr']['class'][] = 'anmt-item anmt-' . $opts['animation'];

			if ( isset( $opts['stagger'] ) && $opts['stagger'] ) {
				$opts['body_attr']['class'][] = 'stagger';
			}
		}

		if ( 'scroll' === $opts['style'] ) {
			$opts['media_style']['max-height'] = $height .'px';
		} elseif ( 'masonry' === $opts['style'] ) {
			$masonry_size = gastro_get_masonry_size( $opts['image'], $opts['no_of_columns'] );
			$opts['body_attr']['style_attr']['width'] = 'calc(100% - ' . $opts['spacing'] . 'px)';
			$opts['body_attr']['style_attr']['margin-bottom'] = $opts['spacing'] . 'px';
			$opts['media_wrapper_style']['margin'] = -$opts['spacing'] / 2 . 'px ' . -$opts['spacing'] / 2 . 'px ' . -$opts['spacing'] / 2 . 'px';

			if ( 'auto' !== $opts['image_ratio'] ) {
				$opts['image_ratio'] = gastro_adjust_ratio( $masonry_size['width'], $masonry_size['height'], $opts['image_ratio'] );
			}

			if ( 5 == $opts['no_of_columns'] ) {
				$image_column_width = $masonry_size['width'] . '-5';
				$opts['item_attr']['class'][] = 'gst-col-' . $image_column_width;
			} else {
				$image_column_width = 12 / $opts['no_of_columns'] * $masonry_size['width'];
				$opts['item_attr']['class'][] = 'gst-col-'. $image_column_width;
			}
		} elseif ( 'carousel' === $opts['style'] ) {
			$opts['item_attr']['style_attr']['padding'] = '0 '. $opts['spacing']/2 .'px';
			if ( !$opts['variable_width'] && !$opts['fade'] && !$opts['thumbnail'] ) {
				if ( 5 == $opts['no_of_columns'] ) {
					$image_column_width = '1-5';
					$opts['item_attr']['class'][] = 'gst-col-' . $image_column_width;
				} else {
					$image_column_width = 12 / $opts['no_of_columns'];
					$opts['item_attr']['class'][] = 'gst-col-'. $image_column_width;
				}
			} elseif ( $opts['variable_width'] ) {
				$opts['media_style']['max-height'] = $height .'px';
			}
		} else {
			$opts['item_attr']['style_attr']['padding'] = '0 '. $opts['spacing']/2 .'px';
			$opts['item_attr']['style_attr']['margin-bottom'] = $opts['spacing'] . 'px';
			if ( 5 == $opts['no_of_columns'] ) {
				$image_column_width = '1-5';
				$opts['item_attr']['class'][] = 'gst-col-' . $image_column_width;
			} else {
				$image_column_width = 12 / $opts['no_of_columns'];
				$opts['item_attr']['class'][] = 'gst-col-'. $image_column_width;
			}
		}

		$opts['image_args'] = array(
			'image_id' => $opts['image'],
			'image_ratio' => $opts['image_ratio'],
			'image_size' => $opts['image_size'],
			'image_hover' => $opts['image_hover'],
			'image_column_width' => $image_column_width,
			'media_style' => $opts['media_style'],
			'wrapper_style' => $opts['media_wrapper_style'],
			'image_lazy_load' => $opts['image_lazy_load']
		);
	}

	return $opts;
}
endif;


if ( !function_exists( 'gastro_style_custom_options' ) ) :
function gastro_style_custom_options( $opts )
{
	// Fonts
	$font_indexes = array(
		'primary',
		'secondary',
		'custom_a',
		'custom_b',
		'custom_c',
		'custom_d',
		'custom_e',
		'custom_f',
		'custom_g',
		'custom_h'
	);

	if ( !isset( $opts['fonts'] ) ) {
		$opts['fonts'] = array();
	}

	if ( !isset( $opts['custom_fonts'] ) ) {
		$opts['custom_fonts'] = array();
	}

	foreach ( $font_indexes as $index ) {
		$font = array();
		$fallback_font = 'sans-serif';
		$value = $opts['font_' . $index];

		if ( !empty( $value ) ) {
			if ( 'default' !== $value['type'] ) {
				if ( isset( $value['category'] ) && !in_array( $value['category'], array( 'sans-serif', 'serif' ) ) ) {
					$fallback_font = 'sans-serif';
				}

				$font['font-family'] = "'" . $value['family'] . "', " . $fallback_font;
				 // remove italic out of 'style' to get font-weight
				if ( false !== strpos( $value['style'], 'italic' ) ) {
					$font['font-style'] = 'italic';
					$value['style'] = str_replace( 'italic', '', $value['style'] );
				} else {
					$font['font-style'] = 'normal';
				}

				if ( is_numeric( $value['style'] ) || in_array( $value['style'], array( 'normal', 'bold' ) ) ) {
					$font['font-weight'] = $value['style'];
				} else {
					$font['font-weight'] = 'normal';
				}

				if ( 'custom' === $value['type'] ) {
					$opts['custom_fonts'][] = $value;
				}
			} else {
				$font['font-family'] = $value['family'];
			}
		}

		$opts['fonts'][$index] = $font;
	}

	// Color Scheme
	$opts['primary_brand'] = $opts['bp_color_1'];
	$opts['secondary_brand'] = $opts['bp_color_2'];
	$opts['primary_brand_contrast'] = gastro_contrast_color( $opts['bp_color_1'] );
	$opts['secondary_brand_contrast'] = gastro_contrast_color( $opts['bp_color_2'] );
	if ( 'light' === $opts['default_color_scheme'] ) {
		$button_primary_text_color = $opts['bp_color_5'];
		$opts['primary_text'] = $opts['bp_color_3'];
		$opts['secondary_text'] = $opts['bp_color_4'];
		$opts['primary_background'] = $opts['bp_color_5'];
		$opts['secondary_background'] = $opts['bp_color_6'];
		$opts['primary_border'] = $opts['bp_color_7'];
	} elseif ( 'dark' === $opts['default_color_scheme'] ) {
		$button_primary_text_color = $opts['bp_color_10'];
		$opts['primary_text'] = $opts['bp_color_8'];
		$opts['secondary_text'] = $opts['bp_color_9'];
		$opts['primary_background'] = $opts['bp_color_10'];
		$opts['secondary_background'] = $opts['bp_color_11'];
		$opts['primary_border'] = $opts['bp_color_12'];
	}

	$opts['primary_text_contrast'] = gastro_contrast_color( $opts['primary_text'] );
	$opts['secondary_text_contrast'] = gastro_contrast_color( $opts['secondary_text'] );
	$opts['primary_background_contrast'] = gastro_contrast_color( $opts['primary_background'] );
	$color_set = array(
		'default' => array(
			'primary_brand' => $opts['primary_brand'],
			'primary_brand_contrast' => $opts['primary_brand_contrast'],
			'secondary_brand' => $opts['secondary_brand'],
			'secondary_brand_contrast' => $opts['secondary_brand_contrast'],
			'primary_text' => $opts['primary_text'],
			'primary_text_contrast' => $opts['primary_text_contrast'],
			'secondary_text' => $opts['secondary_text'],
			'secondary_text_contrast' => $opts['secondary_text_contrast'],
			'primary_background' => $opts['primary_background'],
			'primary_background_contrast' => $opts['primary_background_contrast'],
			'secondary_background' => $opts['secondary_background'],
			'primary_border' => $opts['primary_border'],
		),
		'light' => array(
			'primary_brand' => $opts['primary_brand'],
			'primary_brand_contrast' => $opts['primary_brand_contrast'],
			'secondary_brand' => $opts['secondary_brand'],
			'secondary_brand_contrast' => $opts['secondary_brand_contrast'],
			'primary_text' => $opts['bp_color_3'],
			'primary_text_contrast' => gastro_contrast_color( $opts['bp_color_3'] ),
			'secondary_text' => $opts['bp_color_4'],
			'secondary_text_contrast' => gastro_contrast_color( $opts['bp_color_4'] ),
			'primary_background' => $opts['bp_color_5'],
			'primary_background_contrast' => gastro_contrast_color( $opts['bp_color_5'] ),
			'secondary_background' => $opts['bp_color_6'],
			'primary_border' => $opts['bp_color_7'],
		),
		'dark' => array(
			'primary_brand' => $opts['primary_brand'],
			'primary_brand_contrast' => $opts['primary_brand_contrast'],
			'secondary_brand' => $opts['secondary_brand'],
			'secondary_brand_contrast' => $opts['secondary_brand_contrast'],
			'primary_text' => $opts['bp_color_8'],
			'primary_text_contrast' => gastro_contrast_color( $opts['bp_color_8'] ),
			'secondary_text' => $opts['bp_color_9'],
			'secondary_text_contrast' => gastro_contrast_color( $opts['bp_color_9'] ),
			'primary_background' => $opts['bp_color_10'],
			'primary_background_contrast' => gastro_contrast_color( $opts['bp_color_10'] ),
			'secondary_background' => $opts['bp_color_11'],
			'primary_border' => $opts['bp_color_12'],
		)
	);

	$opts['base_sch_args'] = array( 'all_color' => $color_set, 'default_scheme' => $opts['default_color_scheme'] );
	// Button Color
	if ( 'border' === $opts['button_style'] ) {
		$opts['button_text_color'] = $opts['bp_color_1'];
		$opts['button_border_color'] = $opts['bp_color_1'];
		$opts['button_background_color'] = 'transparent';
	} elseif ( 'fill' === $opts['button_style'] || 'layer' === $opts['button_style'] ) {
		$opts['button_text_color'] = $opts['primary_brand_contrast'];
		$opts['button_border_color'] = $opts['bp_color_1'];
		$opts['button_background_color'] = $opts['bp_color_1'];
	}

	if ( 'inverse' === $opts['button_hover_style'] ) {
		if ( 'border' === $opts['button_style'] ) {
			$opts['button_text_hover_color'] = $opts['primary_brand_contrast'];
			$opts['button_border_hover_color'] = $opts['bp_color_1'];
			$opts['button_background_hover_color'] = $opts['bp_color_1'];
		} else {
			$opts['button_text_hover_color'] = $opts['bp_color_1'];
			$opts['button_border_hover_color'] = $opts['bp_color_1'];
			$opts['button_background_hover_color'] = 'transparent';
		}
	} elseif ( 'none' === $opts['button_hover_style'] ) {
		if ( 'border' === $opts['button_style'] ) {
			$opts['button_text_hover_color'] = $opts['bp_color_1'];
			$opts['button_border_hover_color'] = $opts['bp_color_1'];
			$opts['button_background_hover_color'] = 'transparent';
		} else {
			$opts['button_text_hover_color'] = $opts['primary_brand_contrast'];
			$opts['button_border_hover_color'] = $opts['bp_color_1'];
			$opts['button_background_hover_color'] = $opts['bp_color_1'];
		}
	} else {
		if ( 'border' === $opts['button_style'] ) {
			$opts['button_text_hover_color'] = $opts['bp_color_2'];
			$opts['button_border_hover_color'] = $opts['bp_color_2'];
			$opts['button_background_hover_color'] = 'transparent';
		} else {
			$opts['button_text_hover_color'] = $opts['secondary_brand_contrast'];
			$opts['button_border_hover_color'] = $opts['bp_color_2'];
			$opts['button_background_hover_color'] = $opts['bp_color_2'];
		}
	}

	if ( 'small' === $opts['button_size'] ) {
		$opts['button_padding'] = '12.5px 24px';
	} elseif ( 'medium' === $opts['button_size'] ) {
		$opts['button_padding'] = '16.5px 32px';
	} elseif ( 'large' === $opts['button_size'] ) {
		$opts['button_padding'] = '20.5px 44px';
	}

	return $opts;
}
endif;


/**
 * Output CSS properties in style-custom
 */
if ( !function_exists( 'gastro_sch_css' ) ) :
function gastro_sch_css( $args = array(), $blueprint = false )
{
	if ( !isset( $args['selector'] ) || !isset( $args['property'] ) ) {
		return;
	} else {
		$color = $args['all_color'];
		$opacity = isset( $args['opacity'] ) ? $args['opacity'] : 1;
		$parents = isset( $args['parent'] ) ? $args['parent'] : '';
		$default_scheme = isset( $args['default_scheme'] ) ? $args['default_scheme'] : 'light';
		$high_level_selector = isset( $args['is_high_level'] ) ? $args['is_high_level'] : false; // selector is the same level with scheme class
		$blueprint_class = ( $blueprint ) ? '.bp-context ' : '';
		$important = isset( $args['important'] ) ? $args['important'] : false;

		if ( 'dark' === $default_scheme ) {
			$schemes = array( 'default', 'light', 'dark' );
		} else {
			$schemes = array( 'default', 'dark', 'light' );
		}

		$all_prop = array();
		$extra_prop = array();

		foreach ( $schemes as $scheme ) {
			$all_prop[$scheme]['class'] = array();

			if ( is_array( $args['selector'] ) ) {
				foreach ( $args['selector'] as $selector ) {
					if ( 'default' === $scheme ) {
						$all_prop[$scheme]['class'][] = $blueprint_class . $selector;
					} else {
						$all_prop[$scheme]['class'][] = $blueprint_class . '.gst-' . $scheme . '-scheme ' . $selector;

						if ( $high_level_selector ) {
							$all_prop[$scheme]['class'][] = $blueprint_class . '.gst-' . $scheme . '-scheme' . $selector;
						}

						if ( !empty( $parents ) ) {
							if ( is_array( $parents ) ) {
								foreach ( $parents as $parent_class ) {
									$extra_prop[$scheme]['class'][] = $blueprint_class . '.gst-' . $parent_class . '-' . $scheme . '-scheme ' . $selector;
								}
							} else {
								$extra_prop[$scheme]['class'][] = $blueprint_class . '.gst-' . $parents . '-' . $scheme . '-scheme ' . $selector;
							}
						}
					}
				}
			} else {
				if ( 'default' === $scheme ) {
					$all_prop[$scheme]['class'][] = $blueprint_class . $args['selector'];
				} else {
					$all_prop[$scheme]['class'][] = $blueprint_class . '.gst-' . $scheme . '-scheme ' . $args['selector'];

					if ( $high_level_selector ) {
						$all_prop[$scheme]['class'][] = $blueprint_class . '.gst-' . $scheme . '-scheme' . $args['selector'];
					}

					if ( !empty( $parents ) ) {
						if ( is_array( $parents ) ) {
							foreach ( $parents as $parent_class ) {
								$extra_prop[$scheme]['class'][] = $blueprint_class . '.gst-' . $parent_class . '-' . $scheme . '-scheme ' . $args['selector'];
							}
						} else {
							$extra_prop[$scheme]['class'][] = $blueprint_class . '.gst-' . $parents . '-' . $scheme . '-scheme ' . $args['selector'];
						}
					}
				}
			}

			$all_prop[$scheme]['property'] = array();
			foreach ( $args['property'] as $key => $value ) {
				$property = ( !$important ) ? esc_attr( $key ) . ': ' . $color[$scheme][$value] . ';' : esc_attr( $key ) . ': ' . $color[$scheme][$value] . ' !important;';

				if ( 1 != $opacity && 'background-color' === $key ) {
					$property .= ( !$important ) ? ' ' . esc_attr( $key ) . ': ' . gastro_hex_to_rgba( $color[$scheme][$value], $opacity ) . ';' : ' ' . esc_attr( $key ) . ': ' . gastro_hex_to_rgba( $color[$scheme][$value], $opacity ) . ' !important;';
				}

				$all_prop[$scheme]['property'][] = $property;

				if ( !empty( $parents ) && ( 'default' !== $scheme ) ) {
					$extra_prop[$scheme]['property'][] = $property;
				}
			}
		}

		$output = array();
		foreach ( $all_prop as $css_prop ) {
			$output[] = implode( ', ', $css_prop['class'] ) . ' {' . implode( ' ', $css_prop['property'] ) . '}';
		}

		if ( !empty( $parents ) && !empty( $extra_prop ) ) {
			foreach ( $extra_prop as $css_prop ) {
				$output[] = implode( ', ', $css_prop['class'] ) . ' {' . implode( ' ', $css_prop['property'] ) . '}';
			}
		}

		echo implode( "\n", $output );
	}
}
endif;
