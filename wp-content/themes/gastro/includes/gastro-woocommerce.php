<?php

// Remove tag <a> of product entry
remove_action( 'woocommerce_before_shop_loop_item', 'woocommerce_template_loop_product_link_open', 10, 0 );
remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_product_link_close', 5, 0 );
remove_action( 'woocommerce_before_subcategory', 'woocommerce_template_loop_category_link_open', 10, 0 );
remove_action( 'woocommerce_after_subcategory', 'woocommerce_template_loop_category_link_close', 10, 0 );

// Remove produt meta from product content
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40, 0 );
add_action( 'woocommerce_after_single_product_summary', 'woocommerce_template_single_meta', 13, 0 );

function gastro_woocommerce_product_share()
{
	if ( gastro_mod( 'product_share' ) ) {
		echo '<div class="gst-product-share gst-share">';
		gastro_template_share( array( 'style' => 'icon' ) );
		echo '</div>';
	}
}
add_action( 'woocommerce_share', 'gastro_woocommerce_product_share' );


function gastro_woocommerce_cart_fragments( $fragments )
{
	global $woocommerce;

	ob_start();
	$fragments['span.gst-menu-cart-count'] = '<span class="gst-menu-cart-count">' . esc_html( $woocommerce->cart->cart_contents_count ) . '</span>';
	ob_end_clean();

	return $fragments;
}
add_filter( 'woocommerce_add_to_cart_fragments', 'gastro_woocommerce_cart_fragments' );


function gastro_woocommerce_breadcrumb( $defaults )
{
	$page_id = gastro_get_page_id();
	$bp_data = gastro_bp_data( $page_id );

	if ( gastro_is_blueprint_active( $bp_data ) ) {
		$builder = $bp_data['builder'];
		$container_class =  ( $builder['sidebar_full'] && 'none' !== $builder['sidebar'] && isset( $builder['sidebar_id'] ) ) ? 'gst-container--fullwidth' : 'gst-container';
	} else {
		$container_class = 'gst-container';
	}

	$defaults['delimiter'] = '<span class="twf twf-angle-right"></span>';
	$defaults['wrap_after'] = '</div></header>';
	$defaults['wrap_before'] = '<header class="woocommerce-breadcrumb gst-content-header"><div class="' . esc_attr( $container_class ) . '">';

	return $defaults;
}
add_filter( 'woocommerce_breadcrumb_defaults', 'gastro_woocommerce_breadcrumb' );


if ( !function_exists( 'gastro_wc_dropdown_variation_attribute_options_html' ) ) :
function gastro_wc_dropdown_variation_attribute_options_html( $html, $args )
{
	$output = '';
	$special_radio_attributes = apply_filters( 'radio_product_attributes', array() );
	$options               = $args['options'];
	$product               = $args['product'];
	$attribute             = $args['attribute'];
	$name                  = $args['name'] ? $args['name'] : 'attribute_' . sanitize_title( $attribute );
	$id                    = $args['id'] ? $args['id'] : sanitize_title( $attribute );
	$class                 = $args['class'];

	if ( empty( $options ) && ! empty( $product ) && ! empty( $attribute ) ) {
		$attributes = $product->get_variation_attributes();
		$options    = $attributes[$attribute];
	}

	if ( !empty( $options ) ) {
		if ( 'radio' === gastro_mod( 'product_variation_mode' ) || in_array( $args['attribute'], $special_radio_attributes ) ) {
			$output .= '<div class="variations-radio" name="' . esc_attr( $id ) . '">';
			if ( $product && taxonomy_exists( $attribute ) ) {
				$terms = wc_get_product_terms( $product->get_id(), $attribute, array(
					'fields' => 'all',
				) );
				foreach ( $terms as $term ) {
					if ( in_array( $term->slug, $options ) ) {
						$output .= '<label data-value="' . esc_attr( $term->slug ) . '">';
						$output .=   '<input class="variations-radio-input" type="radio" name="' . esc_attr( $id ) . '" value="' . esc_attr( $term->slug ) . '" ' . checked( sanitize_title( $args['selected'] ), $term->slug, false ) . '>';
						$output .= 	 '<div class="variations-radio-option">';
						if ( strpos( $attribute, 'color' ) || strpos( $attribute, 'colour' ) ) {
							$color = get_term_meta( $term->term_id, 'color_code', true );
							if ( $color ) {
								$output .= '<div class="variations-radio-color" style="background-color:' . esc_attr( $color ) . '"></div>';
							} else {
								$output .= '<div class="variations-radio-text">' . esc_html( apply_filters( 'woocommerce_variation_option_name', $term->name ) ) . '</div>';
							}
						} else {
							$thumbnail_id = get_term_meta( $term->term_id, 'thumbnail_id', true );
							$attachment = wp_get_attachment_image_src( $thumbnail_id, 'thumbnail' );
							if ( $attachment ) {
								$thumbnail_url = $attachment[0];
								$thumbnail_width = $attachment[1];
								$thumbnail_height = $attachment[2];
								$output .= '<img src="' . esc_url( $thumbnail_url ) . '" width="' . esc_attr( $thumbnail_width ) . '" height="' . esc_attr( $thumbnail_height ) . '">';
							} else {
								$output .= '<div class="variations-radio-text">' . esc_html( apply_filters( 'woocommerce_variation_option_name', $term->name ) ) . '</div>';
							}
						}
						$output .=   '</div>';
						$output .= '</label>';
					}
				}
			} else {
				foreach ( $options as $option ) {
					$output .= '<label data-value="' . esc_attr( $option ) . '">';
					$output .=   '<input class="variations-radio-input" type="radio" name="' . esc_attr( $id ) . '" value="' . esc_attr( $option ) . '" ' . checked( sanitize_title( $args['selected'] ), $option, false ) . '>';
					$output .=   '<div class="variations-radio-option"><div class="variations-radio-text">' . esc_html( apply_filters( 'woocommerce_variation_option_name', $option ) ) . '</div></div>';
					$output .= '</label>';
				}
			}
			$output .= '</div>';
			$output .=  $html;
		}
	}

	return $output;
}
endif;
add_filter( 'woocommerce_dropdown_variation_attribute_options_html', 'gastro_wc_dropdown_variation_attribute_options_html', 10, 2 );
