<?php

$template_directory = get_template_directory();
require_once $template_directory . '/classes/gastro-theme.php';

require_once $template_directory . '/includes/gastro-core.php';
require_once $template_directory . '/includes/gastro-helper.php';
require_once $template_directory . '/includes/gastro-items.php';
require_once $template_directory . '/includes/gastro-navmenu.php';
require_once $template_directory . '/includes/gastro-options.php';
require_once $template_directory . '/includes/gastro-template.php';

if ( gastro_is_woocommerce_activated() ) {
	require_once $template_directory . '/includes/gastro-woocommerce.php';
}

function gastro_init()
{
	global $gastro;
	global $gastro_core;
	global $wp_customize;

	$gastro = apply_filters( 'gastro_options', gastro_default_options() );
	$fonts = apply_filters( 'gastro_fonts', null );

	$version = gastro_version();
	$style_custom = get_theme_mod( 'style_custom' );

	if ( !$wp_customize && $style_custom ) {
		if ( get_theme_mod( 'style_custom_version' ) ) {
			$style_version = get_theme_mod( 'style_custom_version' );
		} else {
			$style_version = $version;
		}

		$style_version = apply_filters( 'gastro_style_custom_version', $style_version );
		$style_custom = preg_replace( "/^http:|https:/i", '', $style_custom );
	} else {
		$style_version = $version;
		$style_custom = get_template_directory_uri() . '/style-custom.css';
	}

	$style_custom = apply_filters( 'gastro_style_custom', $style_custom, $style_version );

	wp_register_script(
		'gastro-vendors',
		gastro_asset( 'js/gst.vendors.js' ),
		array( 'jquery', 'jquery-ui-widget' ),
		$version,
		true
	);

	wp_register_script(
		'gastro',
		gastro_asset( 'js/gst.main.js' ),
		array( 'gastro-vendors' ),
		$version,
		true
	);

	wp_register_script(
		'gastro-preview',
		gastro_asset( 'js/gst.preview.js' ),
		array( 'jquery' ),
		$version,
		true
	);

	wp_register_style(
		'gastro',
		gastro_asset( 'css/gst.main.css' ),
		null,
		$version
	);

	wp_register_style(
		'gastro-custom',
		$style_custom,
		null,
		$style_version
	);

	if ( !class_exists( 'Gastro_Core' ) ) {
		$gastro_core = new Gastro_Theme( 'gastro', array() );
		$gastro_core['core_asset_uri'] = get_template_directory_uri();

		do_action( 'gastro_core_init', $gastro_core );

		$gastro_core->start();
	}
}
add_action( 'init', 'gastro_init', 8 );


function gastro_scripts()
{
	global $gastro;
	global $gastro_core;
	global $wp_customize;

	$customize_module = $gastro_core->get_module( 'customize' );

	wp_enqueue_script( 'comment-reply' );
	wp_enqueue_script( 'gastro' );
	wp_localize_script( 'gastro', 'GastroOptions', array(
		'ajax_url' => admin_url( 'admin-ajax.php' ),
		'ajax_nonce' => wp_create_nonce( 'ajax-nonce' )
	) );

	wp_enqueue_style( 'gastro' );

	if ( $wp_customize  && $customize_module ) {
		$values = $customize_module->get_customize_values( $wp_customize );
		wp_add_inline_style( 'gastro', gastro_core_template_style_custom( $values ) );
	} else {
		wp_enqueue_style( 'gastro-custom' );
	}
}
add_action( 'wp_enqueue_scripts', 'gastro_scripts' );


function gastro_asset( $asset, $minify = false )
{
	global $gastro;

	$info = pathinfo( $asset );
	$image = in_array( $info['extension'], array( 'png', 'jpg' ) );

	if ( !$image && ($minify || $gastro['minified_asset']) ) {
		$pos = strrpos( $asset, '.' );

		if ( $pos !== false ) {
			$asset = substr_replace($asset, '.min.', $pos, 1 );
		}
	}

	return $gastro['asset_uri'] . '/' . $asset;
}


function gastro_head()
{
	$logo_homescreen = gastro_mod( 'logo_homescreen_icon' );
	if ( !empty( $logo_homescreen ) ) {
		echo '<link rel="apple-touch-icon" href="' . esc_url( $logo_homescreen ) . '" />';
	}

	if ( gastro_mod( 'site_responsive' ) ) {
		echo '<meta name="viewport" content="width=device-width, initial-scale=1.0">';
	}

	echo '<!--[if lte IE 9]><style type="text/css">.gst-opacity1 { opacity: 1; }</style><![endif]-->';
}
add_action( 'wp_head', 'gastro_head' );
