<?php

function add_script()
{
	 wp_enqueue_script( 'custom-script', get_stylesheet_directory_uri() . '/includes/js/custom.js', array( 'jquery' ) );

}
add_action( 'wp_enqueue_scripts', 'add_script' );

add_shortcode( 'restaurant', 'get_restaurant' );

function get_restaurant(){
    ob_start();
    get_template_part('restaurant_template');
    return ob_get_clean();  
}
?>
