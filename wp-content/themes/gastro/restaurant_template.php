<?php /* Template Name: restaurant */ 
   
  ?>
<html>
     <head>
     	<style type="text/css">
     		.slider{
     			position: relative;
     		}
     		.prev{
     			  position: absolute;
     			  float: left;
     			  top:50%;
     			
     		}
     		.next{
     			position: absolute;
     			float:right;
     			top:50%;
     			
     		}
     		.column {
				  float: left;
				  width: 25%;
				  padding: 10px;
				   /* Should be removed. Only for demonstration */
				}

				/* Clear floats after the columns */
				.row:after {
				  content: "";
				  display: table;
				  clear: both;
				}
     	</style>
   	 </head>
   	 <body>
   	 	<!--search form to get data-->
   	 	<h2>RESTAURANTS</h2>
   	 	<?php
   	 	
   	 	 $url = "http://dev.algoworks.com/user/country_list"; 
  
		// Initialize a CURL session. 
		$ch = curl_init();  
		  
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
		    'app_id: web',
		    
		)); 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,2);
  
		//grab URL and pass it to the variable. 
		curl_setopt($ch, CURLOPT_URL, $url); 
  
        $result = curl_exec($ch); 
        $country_list = json_decode($result);
        
   	 	 ?>
   	 	<form action ="" method ="post">
   	 		<div class="row">
			  <div class="column">
			  	   <?php if(isset($country_list->results) && !empty($country_list->results)){?>
			        <select>
			        	<option value="" selected disabled>Select Country</option>
					    
					    <?php
					     foreach ($country_list->results as $list) {
					     	echo "<option value=$list->name>$list->name</option>";
					     }

					    ?>


					</select> 
				<?php }?>
			  </div>
			  <div class="column" >
			        <select>
					    <option>1</option>
					    <option>2</option>
					</select>
			  </div>
			  <div class="column">
			        <select>
					    <option>1</option>
					    <option>2</option>
					</select>
			  </div>
			   <div class="column" >
			  	  <input type="submit" value="Search">
			    </div>
			</div>
		    
         </form>
		<div class="slider demo">
		  <div><img src="http://placehold.it/150x150"></div>
		  <div><img src="http://placehold.it/150x150"></div>
		  <div><img src="http://placehold.it/150x150"></div>
		  <div><img src="http://placehold.it/150x150"></div>
		  <div><img src="http://placehold.it/150x150"></div>
		  <div><img src="http://placehold.it/150x150"></div>
		  <div><img src="http://placehold.it/150x150"></div>
		  <div><img src="http://placehold.it/150x150"></div>
		  <div><img src="http://placehold.it/150x150"></div>
		</div>
   	 
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
   	 	
   	 	<script type="text/javascript">
   	 		$(document).ready(function(){
  $('.demo').slick({
  	      infinite: false,
  slidesToShow: 3,
  slidesToScroll: 1,
  arrows: true,
  dots: true,
  dotsClass: 'slick-dots',
  
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
        infinite: true,
        dots: true
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
  });
       
});
   	 	</script>
     </body>
</html>
