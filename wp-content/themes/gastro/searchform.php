<?php
/**
 * The main template file
 *
 * @package gastro
 * @version 1.0.0
 */
?>

<form role="search" method="get" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<div class="gst-search-form">
		<input type="text" value="" placeholder="<?php esc_html_e( 'Search', 'gastro' ); ?>" name="s" />
	</div>
</form>
