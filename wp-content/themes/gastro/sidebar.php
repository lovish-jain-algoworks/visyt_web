<?php
/**
 * The main template file
 *
 * @package gastro
 * @version 1.0.0
 */
?>

<?php $sidebar_select = esc_attr( gastro_mod( 'sidebar_select' ) ); ?>

<aside class="<?php echo gastro_sidebar_class( gastro_mod( 'sidebar_position' ), gastro_mod( 'sidebar_fixed' ) ); ?>" itemscope="itemscope" itemtype="https://schema.org/WPSideBar">
	<?php gastro_template_sidebar_background(); ?>
	<div class="gst-widgets">
		<?php if ( is_active_sidebar( $sidebar_select ) ) : ?>
			<ul class="gst-widgets-list">
				<?php dynamic_sidebar( $sidebar_select ); ?>
			</ul>
		<?php endif; ?>
	</div>
</aside>
