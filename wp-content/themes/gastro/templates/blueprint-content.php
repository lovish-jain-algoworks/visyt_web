<?php
/**
 * The template file
 *
 * @package gastro/templates
 * @version 1.0.0
 */
?>


<?php
	$args = gastro_template_args( 'blueprint-content' );
	$opts = gastro_blueprint_content_options( $args );
?>

<main <?php echo gastro_a( $opts ); ?>>
	<article id="post-<?php the_ID(); ?>" <?php post_class( 'gst-content-wrapper' ); ?> <?php echo gastro_s( $opts['content_wrapper_style'] ); ?>>
		<?php if ( !$opts['scroll_page'] ) : ?>
			<?php gastro_template_blueprint_page_title( $opts['builder'] ); ?>
		<?php endif; ?>

		<?php if ( $opts['sidebar'] ) : ?>
			<div class="gst-content-wrapper-inner">
				<div class="<?php echo esc_attr( $opts['main_container_class'] ); ?>">
		<?php endif; ?>

		<div class="<?php echo esc_attr( gastro_main_page_class( $opts['sidebar'], $opts['sidebar_position'] ) ); ?>">
			<div class="<?php echo esc_attr( implode( ' ', $opts['main_wrapper_class'] ) ); ?>">
				<?php
					$s_index = 0;
					foreach( $opts['sections'] as $section ) {
						$section = gastro_filter_section_content( $section );

						foreach ( $section as $section_content ) {
							$section_args = array(
								'section' => $section_content,
								'index' => $s_index,
								'sidebar' => $opts['sidebar'],
								'scroll_page' => $opts['scroll_page'],
								'half_page_scroll' => $opts['half_page_scroll']
							);
							gastro_template( 'section-content.php', $section_args );
							$s_index++;
						}
					}
				?>
			</div>
		</div>

		<?php if ( $opts['sidebar'] ) : ?>
					<aside class="<?php echo gastro_sidebar_class( $opts['sidebar_position'], $opts['sidebar_fixed'], $opts['sidebar_color_scheme'] ); ?>" itemscope="itemscope" itemtype="https://schema.org/WPSideBar">
						<?php gastro_template_sidebar_background( $opts['sidebar_background_color'], $opts['builder']['page_custom_background'] ); ?>
						<div class="gst-widgets">
							<ul class="gst-widgets-list">
								<?php dynamic_sidebar( $opts['sidebar_select'] ); ?>
							</ul>
						</div>
					</aside>
				</div>
			</div>
		<?php endif; ?>

	</article>
</main>
