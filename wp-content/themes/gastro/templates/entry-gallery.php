<?php
/**
 * The template file
 *
 * @package gastro/templates
 * @version 1.0.0
 */
?>


<?php
	$args = gastro_template_args( 'entry-gallery' );
	$opts = gastro_entry_gallery_options( $args );
?>

<div <?php echo gastro_a( $opts['item_attr'] ); ?>>
	<div <?php echo gastro_a( $opts['body_attr'] ); ?>>
		<?php if ( 'none' ===  $opts['on_image_click'] ) : ?>
			<div class="gst-gallery-media">
				<?php echo gastro_template_media( $opts['image_args'], false, $opts['responsive'] ); ?>
			</div>
		<?php else : ?>
			<?php $media = gastro_template_media( $opts['image_args'], true, $opts['responsive'] ); ?>
			<a class="gst-gallery-media" href="<?php echo esc_url( $media['image_url'] ); ?>" data-index="<?php echo esc_attr( $opts['image_index'] ); ?>">
				<?php echo gastro_escape_content( $media['html'] ); ?>
			</a>
		<?php endif; ?>

		<?php if ( ( $opts['title_on'] && !empty( $opts['image_title'] ) ) || ( $opts['caption_on'] && !empty( $opts['image_caption'] ) ) ) : ?>
			<div class="gst-gallery-description">
				<div class="gst-gallery-description-inner">

					<?php if ( $opts['title_on'] && !empty( $opts['image_title'] ) ) : ?>
						<div class="gst-gallery-title">
							<?php echo esc_html( $opts['image_title'] ); ?>
						</div>
					<?php endif; ?>

					<?php if ( $opts['caption_on'] && !empty( $opts['image_caption'] ) ) : ?>
						<div class="gst-gallery-subtitle">
							<?php echo esc_html( $opts['image_caption'] ); ?>
						</div>
					<?php endif; ?>

				</div>
			</div>
		<?php endif; ?>
	</div>
</div>
