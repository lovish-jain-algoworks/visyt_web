<?php
/**
 * The template file
 *
 * @package gastro/templates
 * @version 1.0.0
 */
?>


<?php
	$args = gastro_template_args( 'entry-product' );
	$opts = gastro_entry_product_options( $args );
	$product_id = get_the_ID();
	$content = get_the_content();
	$categories = gastro_term_names( gastro_get_taxonomy() );
	$article_filter = implode( ', ', $categories['slug'] );
	$opts['media_args']['image_id'] = get_post_thumbnail_id( $product_id );
?>

<article <?php post_class( $opts['entry_class'] ); ?> <?php echo gastro_s( $opts['entry_style'] ) . ' data-filter="' . esc_attr( strtolower( $article_filter ) ) . '"'; ?>>
	<div class="<?php echo esc_attr( implode( ' ', $opts['entry_inner_class'] ) ); ?>" <?php echo gastro_s( $opts['entry_inner_style'] ); ?>>
		<?php woocommerce_show_product_loop_sale_flash(); ?>
		<?php if ( $opts['image_on'] ) : ?>
			<?php if ( $opts['enable_link'] ) : ?>
				<a href="<?php the_permalink(); ?>" class="gst-entry-header">
					<?php echo gastro_template_media( $opts['media_args'] ); ?>
				</a>
			<?php else : ?>
				<div class="gst-entry-header">
					<?php echo gastro_template_media( $opts['media_args'] ); ?>
				</div>
			<?php endif; ?>
		<?php endif; ?>
		<div class="gst-entry-body">
			<div class="gst-entry-body-inner">
				<div class="gst-entry-body-content">
					<div class="gst-entry-title gst-s-text-color">
						<div class="gst-entry-title-inner">
							<div class="gst-entry-name">
								<h4 class="gst-<?php echo esc_attr( $opts['title_font'] ); ?>-font" <?php echo gastro_s( $opts['title_style'] ); ?>>
									<?php if ( $opts['enable_link'] ) : ?>
										<a href="<?php the_permalink(); ?>">
											<?php echo gastro_escape_content( get_the_title() ); ?>
										</a>
									<?php else : ?>
										<?php echo gastro_escape_content( get_the_title() ); ?>
									<?php endif; ?>
								</h4>
							</div>
							<?php if ( $opts['badge_on'] ) : ?>
								<?php $tags = get_the_terms( $product_id, 'product_tag' ); ?>
								<?php if ( $tags ) : ?>
									<div class="gst-entry-badge">
										<?php
											foreach ( $tags as $tag ) {
												$tag_thumbnail = get_term_meta( $tag->term_id, 'thumbnail_id', true );
												if ( !empty( $tag_thumbnail ) ) {
													echo gastro_template_media( array( 'image_id' => (int)$tag_thumbnail, 'image_size' => 'medium' ) );
												}
											}
										?>
									</div>
								<?php endif; ?>
							<?php endif; ?>
						</div><?php // close entry title inner ?>
						<?php if ( 'grid' === $opts['style'] ) : ?>
							<div class="gst-entry-price gst-<?php echo esc_attr( $opts['price_font'] ); ?>-font" <?php echo gastro_s( $opts['price_style'] ); ?>>
								<?php woocommerce_template_loop_price(); ?>
							</div>
						<?php endif; ?>
					</div><?php // close entry title ?>
					<?php if ( $opts['subtitle_on'] && !empty( $content ) ) : ?>
						<div <?php echo gastro_a( $opts['subtitle_attr'] ); ?>><?php the_excerpt(); ?></div>
					<?php endif; ?>
				</div><?php // close entry body wrapper ?>
				<?php if ( 'list' === $opts['style'] ) : ?>
					<div class="gst-entry-body-meta">
						<div class="gst-entry-price gst-s-text-color gst-<?php echo esc_attr( $opts['price_font'] ); ?>-font" <?php echo gastro_s( $opts['price_style'] ); ?>>
							<?php woocommerce_template_loop_price(); ?>
						</div>
						<div class="gst-entry-button gst-p-brand-color">
							<?php woocommerce_template_loop_add_to_cart(); ?>
						</div>
					</div>
				<?php endif; ?>
			</div><?php // close entry body inner ?>
		</div><?php // close entry body ?>
		<?php if ( 'grid' === $opts['style'] ) : ?>
			<div class="gst-entry-button gst-p-brand-color">
				<?php woocommerce_template_loop_add_to_cart(); ?>
			</div>
		<?php endif; ?>
	</div><?php // close entry inner ?>
</article><?php // close entry ?>
