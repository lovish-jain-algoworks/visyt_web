<?php
/**
 * The template file
 *
 * @package gastro/templates
 * @version 1.0.0
 */
?>


<?php
	$args = gastro_template_args( 'entry-productcat' );
	$opts = gastro_entry_productcat_options( $args );
	$category = $opts['category_obj'];
	$thumbnail_args = $opts['media_args'];
	$thumbnail_args['image_id'] = get_woocommerce_term_meta( $category->term_id, 'thumbnail_id', true );
?>

<?php if ( $category ) : ?>
	<article <?php wc_product_cat_class( $opts['entry_class'], $category ); ?> <?php echo gastro_s( $opts['entry_style'] ) . ' data-filter="' . esc_attr( strtolower( $category->name ) ) . '"'; ?>>
		<a href="<?php echo get_term_link( $category->slug, $opts['taxonomy'] ); ?>" class="<?php echo esc_attr( implode( ' ', $opts['entry_inner_class'] ) ); ?>">
			<div class="gst-entry-header">
				<?php echo gastro_template_media( $thumbnail_args ); ?>
			</div>
			<div class="gst-entry-body">
				<div class="gst-entry-body-inner">
					<h4 <?php echo gastro_a( $opts['title_attr'] ); ?>>
						<?php echo esc_html( $category->name ); ?>
					</h4>
				</div>
			</div><?php // close entry body ?>
		</a><?php // close entry inner ?>
	</article><?php // close entry ?>
<?php endif; ?>
