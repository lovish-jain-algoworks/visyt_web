<?php
/**
 * The template file
 *
 * @package gastro/templates
 * @version 1.0.0
 */
?>


<?php
	$post_id = get_the_ID();
	$args = gastro_template_args( 'entry' );
	$opts = gastro_entry_options( $args, $post_id );
	$title = get_the_title();
	$excerpt = get_the_excerpt();
	$excerpt = gastro_limit_excerpt( $excerpt, $opts['excerpt_length'] );
	$categories = gastro_term_names( gastro_get_taxonomy( $opts['post_taxonomy'] ) );
	$article_filter = implode( ', ', $categories['slug'] );
?>

<article <?php post_class( $opts['entry_class'] ); ?> <?php echo gastro_s( $opts['entry_style'] ) . ' data-filter="' . esc_attr( strtolower( $article_filter ) ) . '"'; ?>>
	<div <?php echo gastro_a( $opts['inner_attr'] ); ?>>
		<?php if ( is_sticky() ) : ?>
			<div class="gst-sticky-tag gst-p-brand-bg gst-p-brand-contrast-color"><?php esc_html_e( 'Featured', 'gastro' ); ?></div>
		<?php endif; ?>
		<?php if ( ( $opts['media_on'] || 'overlay' === $opts['style'] || 'zigzag' === $opts['layout'] ) && $opts['featured_media_args']['image_id'] ) : ?>
			<div class="gst-entry-header" <?php echo gastro_s( $opts['header_style'] ); ?>>
				<?php $opts['media_empty_available'] = ( 'zigzag' === $opts['layout'] ) ? false : true; ?>
				<?php gastro_template_featured_media( $opts['featured_media_args'] ); ?>
			</div>
		<?php endif; ?>

		<div class="gst-entry-body" <?php echo gastro_s( $opts['body_style'] ); ?>>
			<div class="gst-entry-body-inner">
				<div class="gst-entry-body-content">

					<?php if ( $opts['category_on'] && !empty( $categories['link'] ) ) : ?>
						<div class="<?php echo esc_attr( $opts['category_class'] ); ?>">
							<?php if ( 'plain' !== $opts['category_style'] ) : ?>
								<div class="<?php echo esc_attr( $opts['category_line_class'] ); ?>"></div>
							<?php endif; ?>
							<?php echo gastro_escape_content( implode( ',&nbsp;', $categories['link'] ) ); ?>
						</div>
					<?php endif; ?>

					<?php if ( $opts['title_on'] && !empty( $title ) ) : ?>
						<h4 <?php echo gastro_a( $opts['title_attr'] ); ?>>
							<a href="<?php echo esc_url( $opts['permalink'] ) ; ?>" target="<?php echo esc_attr( $opts['link_target'] ); ?>">
								<?php echo gastro_escape_content( $title ); ?>
							</a>
						</h4>
					<?php endif; ?>
					<?php if ( $opts['rating_on'] ) : ?>
						<?php $rating = gastro_get_average_ratings( $post_id ); ?>
						<?php if ( $rating ) : ?>
							<div class="comment-rating" title="<?php echo sprintf( esc_attr__( 'Rated %s out of 5', 'gastro' ), $rating ); ?>">
								<span style="width:<?php echo esc_attr( $rating/5*100 ); ?>%;">
									<strong><?php echo esc_html( $rating ); ?></strong>
									<?php esc_html_e( ' out of ', 'gastro' ); ?>
									<span><?php esc_html_e( '5', 'gastro' ); ?></span>
								</span>
							</div>
						<?php endif; ?>
					<?php endif; ?>
					<?php if ( $opts['excerpt_on'] ) : ?>
						<?php if ( 'content' === $opts['excerpt_content'] ) : ?>
							<?php $content = get_the_content(); ?>
							<?php if ( !empty( $content ) ) : ?>
								<div class="gst-entry-excerpt"><?php the_content( esc_html( $opts['more_message'] ) ); ?></div>
							<?php endif; ?>
						<?php else : ?>
							<?php if ( !empty( $excerpt ) ) : ?>
								<div class="gst-entry-excerpt"><?php echo do_shortcode( $excerpt ); ?></div>
							<?php endif; ?>
						<?php endif; ?>
					<?php endif; ?>

					<?php if ( $opts['link_on'] && 'overlay' !== $opts['style'] ) : ?>
						<div class="gst-entry-link gst-p-brand-color">
							<a href="<?php echo esc_url( $opts['permalink'] ) ?>" target="<?php echo esc_attr( $opts['link_target'] ); ?>">
								<?php echo esc_html( $opts['more_message'] ); ?>
							</a>
						</div>
					<?php endif; ?>

					<?php if ( $opts['date_on'] || $opts['author_on'] || $opts['tag_on'] || $opts['comment_on'] || $opts['customfield_on'] ) : ?>
						<div class="gst-entry-meta gst-<?php echo esc_attr( $opts['meta_font'] ); ?>-font">

							<?php if ( $opts['date_on'] ) : ?>
								<div class="gst-entry-meta-date">
									<?php if ( $opts['icon_on'] ) : ?>
										<i class="twf twf-clock"></i>
									<?php endif; ?>
									<?php echo get_the_date( apply_filters( 'gastro_entry_date_format', 'F j, Y' ) ); ?>
								</div>
							<?php endif; ?>

							<?php if ( $opts['author_on'] ) : ?>
								<div class="gst-entry-meta-author">
									<?php if ( $opts['icon_on'] ) : ?>
										<i class="twf twf-user"></i>
									<?php else : ?>
										<?php esc_html_e( 'By', 'gastro' ); ?>
									<?php endif; ?>
									<?php the_author_posts_link(); ?>
								</div>
							<?php endif; ?>

							<?php if ( $opts['tag_on'] ) : ?>
								<?php $tags = get_the_term_list( $post_id, $opts['post_tag'], '', ', ', '' ); ?>
								<?php if ( $tags ) : ?>
									<div class="gst-entry-meta-tag">
										<?php if ( $opts['icon_on'] ) : ?>
											<i class="twf twf-bookmark-o"></i>
										<?php else : ?>
											<?php esc_html_e( 'Tagged in', 'gastro' ); ?>
										<?php endif; ?>
										<?php echo gastro_escape_content( $tags ); ?>
									</div>
								<?php endif; ?>
							<?php endif; ?>

							<?php if ( $opts['customfield_on'] ) : ?>
								<?php
									$prep_time = get_post_meta( $post_id, 'recipe_prep', true );
									$cook_time = get_post_meta( $post_id, 'recipe_cook', true );
									$serving = get_post_meta( $post_id, 'recipe_serves', true );
									$difficulty = get_post_meta( $post_id, 'recipe_difficulty', true );
								?>
								<?php if ( !empty( $prep_time ) ) : ?>
									<div class="gst-entry-meta-preptime">
										<?php if ( $opts['icon_on'] ) : ?>
											<i class="twf twf-clock"></i>
										<?php else : ?>
											<span class="gst-entry-meta-label"><?php esc_html_e( 'Prep', 'gastro' ); ?></span>
										<?php endif; ?>
										<?php echo esc_html( $prep_time ); ?>
									</div>
								<?php endif; ?>
								<?php if ( !empty( $cook_time ) ) : ?>
									<div class="gst-entry-meta-cooktime">
										<?php if ( $opts['icon_on'] ) : ?>
											<i class="twf twf-fire"></i>
										<?php else : ?>
											<span class="gst-entry-meta-label"><?php esc_html_e( 'Cook', 'gastro' ); ?></span>
										<?php endif; ?>
										<?php echo esc_html( $cook_time ); ?>
									</div>
								<?php endif; ?>
								<?php if ( !empty( $serving ) ) : ?>
									<div class="gst-entry-meta-serving">
										<?php if ( $opts['icon_on'] ) : ?>
											<i class="twf twf-user"></i>
										<?php else : ?>
											<span class="gst-entry-meta-label"><?php esc_html_e( 'Serves', 'gastro' ); ?></span>
										<?php endif; ?>
										<?php echo esc_html( $serving ); ?>
									</div>
								<?php endif; ?>
								<?php if ( !empty( $difficulty ) ) : ?>
									<div class="gst-entry-meta-difficulty">
										<?php if ( $opts['icon_on'] ) : ?>
											<i class="twf twf-gauge"></i>
										<?php else : ?>
											<span class="gst-entry-meta-label"><?php esc_html_e( 'Difficulty', 'gastro' ); ?></span>
										<?php endif; ?>
										<?php echo esc_html( $difficulty ); ?>
									</div>
								<?php endif; ?>
							<?php endif; ?>

							<?php if ( $opts['comment_on'] ) : ?>
								<?php $comments = get_comments_number(); ?>
								<?php if ( 1 <= $comments ) : ?>
									<div class="gst-entry-meta-comment">
										<?php if ( $opts['icon_on'] ) : ?>
											<i class="twf twf-comment-o"></i>
										<?php endif; ?>
										<?php echo esc_html( $comments ); ?>
										<?php ( 1 == $comments ) ? esc_html_e( ' comment', 'gastro' ) : esc_html_e( ' comments', 'gastro' ); ?>
									</div>
								<?php endif; ?>
							<?php endif; ?>

						</div><?php // close entry meta ?>
					<?php endif; ?>

					<?php if ( $opts['link_on'] && 'overlay' === $opts['style'] ) : ?>
						<div class="gst-entry-link gst-p-brand-color">
							<a href="<?php echo esc_url( $opts['permalink'] ) ?>" target="<?php echo esc_attr( $opts['link_target'] ); ?>">
								<?php echo esc_html( $opts['more_message'] ); ?>
							</a>
						</div>
					<?php endif; ?>

				</div><?php // close entry body content ?>
			</div><?php // close entry body inner ?>
		</div><?php // close entry body ?>
	</div><?php // close entry inner ?>
	<div class="gst-p-brand-bg gst-entry-layer"></div>
</article><?php // close entry ?>
