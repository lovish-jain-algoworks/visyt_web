<?php
/**
 * The template file
 *
 * @package gastro/templates
 * @version 1.0.0
 */
?>


<?php $post_id = gastro_get_page_id(); ?>
<?php $opts = gastro_header_content_options( array(), $post_id ); ?>

<?php if ( 'frame' === $opts['site_layout'] ) : ?>
	<span class="gst-frame gst-s-bg-bg gst-frame--top"></span>
	<span class="gst-frame gst-s-bg-bg gst-frame--right"></span>
	<span class="gst-frame gst-s-bg-bg gst-frame--bottom"></span>
	<span class="gst-frame gst-s-bg-bg gst-frame--left"></span>
<?php endif; ?>

<body <?php body_class(); ?> <?php echo gastro_d( $opts['body_data'] ); ?> itemscope="itemscope" itemtype="https://schema.org/WebPage">
	<?php if ( !empty( $opts['preload_style'] ) && 'none' !== $opts['preload_style'] ) : ?>
		<div class="gst-page-load gst-p-bg-bg">
			<div class="gst-page-load-spinner">
				<?php echo gastro_template_preload( $opts['preload_style'], $opts['preload_logo'] ); ?>
			</div>
		</div>
	<?php endif; ?>

	<div <?php echo gastro_a( $opts['wrapper_attr'] ); ?>>

		<?php if ( 'none' !== $opts['nav_menu'] ) : ?>
			<?php if ( 'none' !== $opts['nav_menu'] && 'top' !== $opts['navbar_position'] ) : ?>
				<?php gastro_template( 'side-navbar-' . $opts['sidenav_style'], $opts ); ?>
			<?php endif; ?>

			<header class="gst-header" data-transparent="<?php echo esc_attr( $opts['header_transparent'] ); ?>" itemscope="itemscope" itemtype="https://schema.org/WPHeader">
				<?php if ( $opts['topbar'] ) : ?>
					<div <?php echo gastro_a( $opts['topbar_attr'] ); ?>>
						<div class="gst-topbar-inner">
							<div class="<?php echo esc_attr( $opts['container_class'] ); ?>">
								<div class="gst-row">

									<?php for ( $i = 1; $i <= $opts['topbar_column']; $i++ ) : ?>
									<div class="gst-topbar-column gst-p-border-border gst-col-<?php echo esc_attr( 12 / $opts['topbar_column'] ); ?>">
										<?php if ( is_active_sidebar( 'gastro-topbar-' . $i ) ) : ?>
											<div class="gst-widgets">
												<ul class="gst-widgets-list">
													<?php dynamic_sidebar( 'gastro-topbar-' . $i ); ?>
												</ul>
											</div>
										<?php endif; ?>
									</div>
									<?php endfor; ?>
								</div>
							</div>
						</div>
					</div>
				<?php endif; ?>

				<?php if ( 'none' !== $opts['nav_menu'] && 'top' === $opts['navbar_position'] ) : ?>
					<?php gastro_template( 'navbar-' . $opts['navbar_style'], $opts ); ?>
				<?php endif; ?>
				<?php if ( $opts['responsive'] ) : ?>
					<?php gastro_template( 'navbar-mobile', $opts ); ?>
				<?php endif; ?>
			</header>

		<?php endif; ?>
