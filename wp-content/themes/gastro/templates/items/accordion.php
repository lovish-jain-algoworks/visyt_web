<?php
/**
 * Item template file
 *
 * @package gastro/templates/items
 * @version 1.0.0
 */
?>


<?php
	$args = gastro_template_args( 'items/accordion' );
	$opts = gastro_item_accordion_options( $args );
?>

<div <?php echo gastro_a( $opts ); ?>>
	<?php if ( !empty( $opts['data'] ) && is_array( $opts['data'] ) ) : ?>
		<?php foreach ( $opts['data'] as $index => $data ): ?>
			<?php
				$panel_class = 'gst-accordion-panel gst-p-border-border';
				if ( $opts['default_active'] == $index + 1 && !$opts['close_by_default'] ) {
					$panel_class .= ' active';
				}
			?>
			<div class="<?php echo esc_attr( $panel_class ); ?>" data-index="<?php echo esc_attr( $index + 1 ); ?>">
				<a href="#" <?php echo gastro_a( $opts['heading_attr'] ); ?> data-index="<?php echo esc_attr( $index + 1 ); ?>">
					<span class="gst-accordion-title gst-<?php echo esc_attr( $opts['title_font'] ); ?>-font"><?php echo do_shortcode( $data['label'] ); ?></span>

					<?php if ( $opts['icon_on'] ): ?>
						<span class="gst-accordion-icon twf twf-minimal-plus"></span>
					<?php endif; ?>
				</a>
				<?php $content = $opts['spaces'][$index]; ?>
				<div <?php echo gastro_a( $opts['body_attr'] ); ?> data-index="<?php echo esc_attr( $index + 1 ); ?>">
					<?php gastro_template_background( $opts ); ?>
					<?php if ( !empty( $content['items'] ) && is_array( $content['items'] ) ) : ?>
						<?php foreach( $content['items'] as $item ) : ?>
							<?php gastro_template_item( $item ); ?>
						<?php endforeach; ?>
					<?php endif; ?>
				</div>
			</div>

		<?php endforeach; ?>
	<?php endif; ?>
</div>
