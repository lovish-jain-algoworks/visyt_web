<?php
/**
 * Item template file
 *
 * @package gastro/templates/items
 * @version 1.0.0
 */
?>


<?php
	$args = gastro_template_args( 'items/action' );
	$opts = gastro_item_action_options( $args );
?>

<div <?php echo gastro_a( $opts ); ?>>
	<?php if ( 'inline' === $opts['style'] && 'right' === $opts['image_position'] ): ?>
		<div class="gst-action-button">
			<?php gastro_template_button( $opts ); ?>
		</div>
		<div class="gst-action-body" <?php echo gastro_s( $opts['body_style'] ); ?>>
			<div class="gst-action-title gst-s-text-color gst-<?php echo esc_attr( $opts['title_font'] ); ?>-font">
				<?php echo do_shortcode( $opts['title'] ); ?>
			</div>

			<?php if ( $opts['subtitle_on'] ): ?>
				<div class="gst-action-subtitle">
					<?php echo do_shortcode( $opts['subtitle'] ); ?>
				</div>
			<?php endif; ?>
		</div>
		<?php if ( $opts['image_on'] ): ?>
			<div class="gst-action-media" <?php echo gastro_s( $opts['action_media_style'] ); ?>>
				<?php echo gastro_template_media( $opts ); ?>
			</div>
		<?php endif; ?>

	<?php else: ?>

		<?php if ( $opts['image_on'] ): ?>
			<div class="gst-action-media" <?php echo gastro_s( $opts['action_media_style'] ); ?>>
				<?php echo gastro_template_media( $opts ); ?>
			</div>
		<?php endif; ?>
		<div class="gst-action-body" <?php echo gastro_s( $opts['body_style'] ); ?>>
			<div class="gst-action-title gst-s-text-color gst-<?php echo esc_attr( $opts['title_font'] ); ?>-font">
				<?php echo do_shortcode( $opts['title'] ); ?>
			</div>

			<?php if ( $opts['subtitle_on'] ): ?>
				<div class="gst-action-subtitle">
					<?php echo do_shortcode( $opts['subtitle'] ); ?>
				</div>
			<?php endif; ?>
		</div>
		<div class="gst-action-button">
			<?php gastro_template_button( $opts ); ?>
		</div>
	<?php endif; ?>
</div>
