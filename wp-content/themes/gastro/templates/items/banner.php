<?php
/**
 * Item template file
 *
 * @package gastro/templates/items
 * @version 1.0.0
 */
?>


<?php
	$args = gastro_template_args( 'items/banner' );
	$opts = gastro_item_banner_options( $args );
?>

<?php if ( !empty( $opts['banner_link'] ) ): ?>
	<a href="<?php echo gastro_escape_url( $opts['banner_link'] ); ?>" <?php echo gastro_a( $opts ); ?> target="<?php echo esc_attr( $opts['link_target'] ); ?>">
<?php else : ?>
	<div <?php echo gastro_a( $opts ); ?>>
<?php endif; ?>
	<?php gastro_template_background( $opts ); ?>
	<div class="gst-banner-content" <?php echo gastro_s( $opts['content_style'] ); ?>>
		<div <?php echo gastro_a( $opts['content_inner_attr'] ); ?>>
			<?php if ( $opts['media_on'] ) : ?>
				<div class="gst-banner-media">
					<?php echo gastro_template_media( $opts ); ?>
				</div>
			<?php endif; ?>
			<?php if ( $opts['title_on'] ) : ?>
				<div <?php echo gastro_a( $opts['title_attr'] ); ?>>
					<?php echo do_shortcode( $opts['title'] ); ?>
				</div>
			<?php endif; ?>
			<?php if ( $opts['subtitle_on'] ) : ?>
				<div <?php echo gastro_a( $opts['subtitle_attr'] ); ?>>
					<?php echo do_shortcode( $opts['subtitle'] ); ?>
				</div>
			<?php endif; ?>
			<?php if ( $opts['button_on'] ) : ?>
				<div class="gst-banner-button">
					<?php gastro_template_button( $opts ); ?>
				</div>
			<?php endif; ?>
		</div>
	</div>
<?php if ( !empty( $opts['banner_link'] ) ): ?>
	</a>
<?php else : ?>
	</div>
<?php endif; ?>
