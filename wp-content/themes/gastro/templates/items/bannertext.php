<?php
/**
 * Item template file
 *
 * @package gastro/templates/items
 * @version 1.0.0
 */
?>


<?php
	$args = gastro_template_args( 'items/bannertext' );
	$opts = gastro_item_bannertext_options( $args );
?>

<div <?php echo gastro_a( $opts ); ?>>

	<?php if ( $opts['fronttext_on'] ) : ?>
		<span <?php echo gastro_a( $opts['static_attr'] ); ?>><?php echo do_shortcode( $opts['fronttext'] ); ?></span>
	<?php endif; ?>

	<span <?php echo gastro_a( $opts['dynamic_attr'] ); ?>>
		<span class="gst-bannertext-dynamic-inner"></span>
	</span>

	<?php if ( $opts['backtext_on'] ) : ?>
		<span <?php echo gastro_a( $opts['static_attr'] ); ?>><?php echo do_shortcode( $opts['backtext'] ); ?></span>
	<?php endif; ?>

</div>
