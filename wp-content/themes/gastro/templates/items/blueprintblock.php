<?php
/**
 * Item template file
 *
 * @package gastro/templates/items
 * @version 1.0.0
 */
?>


<?php
	$args = gastro_template_args( 'items/blueprintblock' );
	$opts = gastro_item_blueprintblock_options( $args );
?>

<div <?php echo gastro_a( $opts ); ?>>
	<?php if ( !empty( $opts['blueprintblock_id'] ) && $opts['is_publish'] ) : ?>
		<?php $blocks = $opts['bp_data']['sections'][0]['blocks']; ?>
		<?php if ( !empty( $blocks ) && is_array( $blocks ) ) : ?>
			<?php foreach ( $blocks as $index => $block ) : ?>
				<?php
					$row_class = 'gst-row';
					if ( isset( $block['gutter_space'] ) && !empty( $block['gutter_space'] ) ) {
						$row_class .= ' gst-row--' . $block['gutter_space'];
					}
				?>
				<div class="<?php echo esc_attr( $row_class ); ?>">
					<?php if ( !empty( $block['spaces'] ) && is_array( $block['spaces'] ) ) : ?>
						<?php foreach ( $block['spaces'] as $space_index => $space ) : ?>
							<?php $layout = isset( $block['layout'] ) ? $block['layout'] : '1'; ?>
							<div class="<?php echo esc_attr( gastro_space_class( $layout, $space_index ) ); ?>"><?php foreach ( $space['items'] as $item ) { gastro_template_item( $item ); } ?></div>
						<?php endforeach; ?>
					<?php endif; ?>
				</div>
			<?php endforeach; ?>
		<?php else : ?>
			<?php echo gastro_get_dummy_template( esc_html__( 'blueprint block', 'gastro' ), esc_html__( 'Selected blueprint block is empty.', 'gastro' ) ); ?>
		<?php endif; ?>
	<?php else : ?>
		<?php echo gastro_get_dummy_template( esc_html__( 'blueprint block', 'gastro' ), esc_html__( 'Please select a blueprint block.', 'gastro' ) ); ?>
	<?php endif; ?>
</div>
