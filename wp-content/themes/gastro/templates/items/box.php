<?php
/**
 * Item template file
 *
 * @package gastro/templates/items
 * @version 1.0.0
 */
?>


<?php
	$args = gastro_template_args( 'items/box' );
	$opts = gastro_item_box_options( $args );
?>

<div <?php echo gastro_a( $opts ); ?>>
	<?php gastro_template_background( $opts ); ?>
	<div <?php echo gastro_a( $opts['inner_attr'] ); ?>>
		<div class="gst-box-content gst-<?php echo esc_attr( $opts['vertical_align'] ); ?>-vertical" <?php echo gastro_s( $opts['content_style'] ); ?>>
			<div class="gst-box-body" <?php echo gastro_s( $opts['body_style'] ); ?>>
				<?php if ( !empty( $opts['spaces'] ) && is_array( $opts['spaces'] ) ) : ?>
					<?php foreach( $opts['spaces'] as $space ) : ?>
						<?php if ( !empty( $space['items'] ) && is_array( $space['items'] ) ) : ?>
							<?php foreach( $space['items'] as $item ) : ?>
								<?php gastro_template_item( $item ); ?>
							<?php endforeach; ?>
						<?php endif; ?>
					<?php endforeach; ?>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>
