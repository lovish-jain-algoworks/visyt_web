<?php
/**
 * Item template file
 *
 * @package gastro/templates/items
 * @version 1.0.0
 */
?>


<?php
	$args = gastro_template_args( 'items/button' );
	$opts = gastro_item_button_options( $args );
	gastro_template_button( $opts );
?>
