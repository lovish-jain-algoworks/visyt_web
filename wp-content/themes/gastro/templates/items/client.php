<?php
/**
 * Item template file
 *
 * @package gastro/templates/items
 * @version 1.0.0
 */
?>


<?php
	$args = gastro_template_args( 'items/client' );
	$opts = gastro_item_client_options( $args );
?>

<div <?php echo gastro_a( $opts ); ?>>
	<div <?php echo gastro_a( $opts['content_attr'] ); ?>>
		<?php foreach ( $opts['formatted'] as $i => $row ) : ?>
			<?php if ( 'grid' === $opts['style'] ) : ?>
				<?php
					if ( $i == ceil( $opts['no_of_items'] / $opts['no_of_columns'] ) - 1 ) {
						$row_style = array( 'margin' => '0 '. -$opts['spacing'] / 2 .'px' );
					} else {
						$row_style = array( 'margin' => '0 '. -$opts['spacing'] / 2 .'px ' . $opts['spacing'] . 'px' );
					}
				?>
				<div class="gst-row" <?php echo gastro_s( $row_style ); ?>>
			<?php endif; ?>

				<?php foreach ( $row as $index => $data ): ?>
					<?php
						$data['image_hover'] = $opts['image_hover'];
						$data['image_column_width'] = $opts['column_width'];
					?>
					<div <?php echo gastro_a( $opts['item_attr'] );?>>
						<div class="gst-client-media">
							<?php echo gastro_template_media( $data ); ?>
						</div>
					</div>
				<?php endforeach; ?>

			<?php if ( 'grid' === $opts['style'] ) : ?>
				</div><?php // close row ?>
			<?php endif; ?>
		<?php endforeach; ?>
	</div>
</div>
