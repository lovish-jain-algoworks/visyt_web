<?php
/**
 * Item template file
 *
 * @package gastro/templates/items
 * @version 1.0.0
 */
?>


<?php
	$args = gastro_template_args( 'items/comment' );
	$opts = gastro_item_comment_options( $args );
?>

<div <?php echo gastro_a( $opts ); ?>>
	<?php if ( comments_open() || get_comments_number() ): ?>
		<?php comments_template(); ?>
	<?php else : ?>
		<?php echo gastro_get_dummy_template( esc_html__( 'comment', 'gastro' ), esc_html__( 'Please activate comment.', 'gastro' ) ); ?>
	<?php endif; ?>
</div>
