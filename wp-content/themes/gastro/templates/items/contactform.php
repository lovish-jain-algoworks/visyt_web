<?php
/**
 * Item template file
 *
 * @package gastro/templates/items
 * @version 1.0.0
 */
?>


<?php
	$args = gastro_template_args( 'items/contactform' );
	$opts = gastro_item_contactform_options( $args );
?>

<div <?php echo gastro_a( $opts ); ?>>
	<?php
		if ( shortcode_exists( 'contact-form-7' ) ) {
			if ( !empty( $opts['contactform'] ) ) {
				echo do_shortcode( $opts['contactform'] );
			} else {
				echo gastro_get_dummy_template( esc_html__( 'contact form', 'gastro' ), esc_html__( 'Please input Contact Form Shortcode.', 'gastro' ) );
			}
		} else {
			echo gastro_get_dummy_template( esc_html__( 'contact form', 'gastro' ), esc_html__( 'Please activate Contact Form 7 plug in.', 'gastro' ) );
		}
	?>
</div>
