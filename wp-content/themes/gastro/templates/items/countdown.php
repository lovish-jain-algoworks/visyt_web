<?php
/**
 * Item template file
 *
 * @package gastro/templates/items
 * @version 1.0.0
 */
?>


<?php
	$args = gastro_template_args( 'items/countdown' );
	$opts = gastro_item_countdown_options( $args );
?>

<div <?php echo gastro_a( $opts ); ?>>

	<?php if ( $opts['week_on'] ) : ?>
		<div class="gst-countdown-item gst-countdown-week" data-label="week">
			<div <?php echo gastro_a( $opts['number_attr'] ); ?>></div>
			<span <?php echo gastro_a( $opts['label_attr'] ); ?>><?php esc_html_e( 'weeks', 'gastro' ); ?></span>
		</div>
	<?php endif; ?>

	<div class="gst-countdown-item gst-countdown-day" data-label="day">
		<div <?php echo gastro_a( $opts['number_attr'] ); ?>></div>
		<span <?php echo gastro_a( $opts['label_attr'] ); ?>>
			<?php esc_html_e( 'days', 'gastro' ); ?>
		</span>
	</div>
	<div class="gst-countdown-item gst-countdown-hour" data-label="hour">
		<div <?php echo gastro_a( $opts['number_attr'] ); ?>></div>
		<span <?php echo gastro_a( $opts['label_attr'] ); ?>>
			<?php esc_html_e( 'hours', 'gastro' ); ?>
		</span>
	</div>
	<div class="gst-countdown-item gst-countdown-minute" data-label="minute">
		<div <?php echo gastro_a( $opts['number_attr'] ); ?>></div>
		<span <?php echo gastro_a( $opts['label_attr'] ); ?>>
			<?php esc_html_e( 'minutes', 'gastro' ); ?>
		</span>
	</div>
	<div class="gst-countdown-item gst-countdown-second" data-label="second">
		<div <?php echo gastro_a( $opts['number_attr'] ); ?>></div>
		<span <?php echo gastro_a( $opts['label_attr'] ); ?>>
			<?php esc_html_e( 'seconds', 'gastro' ); ?>
		</span>
	</div>
</div>
