<?php
/**
 * Item template file
 *
 * @package gastro/templates/items
 * @version 1.0.0
 */
?>


<?php
	$args = gastro_template_args( 'items/custompost' );
	$opts = gastro_item_custompost_options( $args );

	if ( is_single() ) {
		$opts['query_args']['post__not_in'] = array( get_the_ID() );
	}
?>

<?php if ( !empty( $opts['post_type'] ) ) : ?>
	<?php if ( post_type_exists( $opts['post_type'] ) ) : ?>
		<?php $query = new WP_Query( $opts['query_args'] ); ?>
		<?php if ( $query->have_posts() ): ?>
			<div <?php echo gastro_a( $opts ); ?>>
				<?php if ( 'carousel' !== $opts['layout'] && $opts['filter'] ): ?>
					<?php gastro_template_filter( $query, $opts['filter_args'] ); ?>
				<?php endif; ?>

				<div <?php echo gastro_a( $opts['content_attr'] ); ?>>
					<?php while( $query->have_posts() ) : $query->the_post(); ?>
						<?php $opts['index'] = $query->current_post; ?>
						<?php gastro_template( 'entry', $opts ); ?>
					<?php endwhile; ?>
				</div>

				<?php if ( 'carousel' !== $opts['layout'] && $opts['pagination'] ) : ?>
					<?php gastro_template_pagination( $query, $opts ); ?>
				<?php endif; ?>

				<?php wp_reset_postdata(); ?>
			</div>
		<?php else : ?>
			<?php echo gastro_get_dummy_template( esc_html__( 'custom post type', 'gastro' ), esc_html__( 'Custom post type is now empty.', 'gastro' ) ); ?>
		<?php endif; ?>
	<?php else : ?>
		<?php echo gastro_get_dummy_template( esc_html__( 'custom post type', 'gastro' ), esc_html__( 'Custom post type you entered does not exist.', 'gastro' ) ); ?>
	<?php endif; ?>
<?php else : ?>
	<?php echo gastro_get_dummy_template( esc_html__( 'custom post type', 'gastro' ), esc_html__( 'Please input custom post type name.', 'gastro' ) ); ?>
<?php endif; ?>
