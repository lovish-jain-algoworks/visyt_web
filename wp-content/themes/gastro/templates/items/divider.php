<?php
/**
 * Item template file
 *
 * @package gastro/templates/items
 * @version 1.0.0
 */
?>


<?php
	$args = gastro_template_args( 'items/divider' );
	$opts = gastro_item_divider_options( $args );
?>

<div <?php echo gastro_a( $opts ); ?>>
	<div class="gst-divider-line gst-p-border-bg gst-p-border-border" <?php echo gastro_s( $opts['line_style'] ); ?>>
	<?php if ( $opts['text_on'] ) : ?>
		<span class="gst-divider-text gst-p-border-color gst-p-bg-bg" <?php echo gastro_s( $opts['text_style'] ); ?>>
			<?php echo do_shortcode( $opts['text'] ); ?>
		</span>
	<?php endif; ?>
	</div>
</div>
