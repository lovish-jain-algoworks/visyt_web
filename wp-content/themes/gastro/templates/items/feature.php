<?php
/**
 * Item template file
 *
 * @package gastro/templates/items
 * @version 1.0.0
 */
?>


<?php
	$args = gastro_template_args( 'items/feature' );
	$opts = gastro_item_feature_options( $args );
?>

<div <?php echo gastro_a( $opts ); ?>>
	<div class="gst-feature-content">
		<?php foreach ( $opts['formatted'] as $i => $row ) : ?>
			<?php
				if ( $i == ceil( $opts['no_of_items'] / $opts['no_of_columns'] ) - 1 ) {
					$row_style = array( 'margin' => '0 '. -$opts['spacing'] / 2 .'px' );
				} else {
					$row_style = array( 'margin' => '0 '. -$opts['spacing'] / 2 .'px ' . $opts['spacing'] . 'px' );
				}
			?>
			<div class="gst-row" <?php echo gastro_s( $row_style ); ?>>
				<?php foreach ( $row as $index => $data ): ?>
					<?php $item_attr = $opts['item_attr']; ?>
					<?php if ( 'image' === $data['media_type'] ) : ?>
						<?php $item_attr['class'][] = 'with-image'; ?>
					<?php endif; ?>
					<div <?php echo gastro_a( $item_attr ); ?>>
						<div class="gst-feature-item-inner">

							<?php if ( $opts['media_on'] && 'right' !== $opts['style'] ): ?>
								<div class="gst-feature-media">
									<?php $data['image_column_width'] = $opts['column_width']; ?>
									<?php echo gastro_template_media( $data ); ?>
								</div>
							<?php endif; ?>

							<?php if ( $opts['title_on'] && 'inline' === $opts['style'] ): ?>
								<div class="gst-feature-title gst-s-text-color gst-<?php echo esc_attr( $opts['title_font'] ); ?>-font" <?php echo gastro_s( $opts['title_style'] ); ?>><?php echo do_shortcode( $data['title'] ); ?></div>
							<?php endif; ?>

							<?php if ( ( $opts['title_on'] && 'inline' !== $opts['style'] ) || $opts['description_on'] || $opts['button_on'] ) : ?>
								<div class="gst-feature-body">

									<?php if ( $opts['title_on'] && 'inline' !== $opts['style'] ): ?>
										<div class="gst-feature-title gst-s-text-color gst-<?php echo esc_attr( $opts['title_font'] ); ?>-font" <?php echo gastro_s( $opts['title_style'] ); ?>><?php echo do_shortcode( $data['title'] ); ?></div>
									<?php endif; ?>

									<?php if ( $opts['description_on'] ): ?>
										<div class="gst-feature-description"><?php echo do_shortcode( $data['description'] ); ?></div>
									<?php endif; ?>

									<?php if ( $opts['button_on'] ): ?>
										<div class="gst-feature-button">
											<?php
												$data['button_style'] = 'plain';
												$data['button_hover'] = 'brand';
												$data['button_size'] = 'small';
												gastro_template_button( $data );
											?>
										</div>
									<?php endif; ?>

								</div><?php // close gst-feature-body ?>
							<?php endif; ?>

							<?php if ( $opts['media_on'] && 'right' === $opts['style'] ): ?>
								<div class="gst-feature-media">
									<?php $data['image_column_width'] = $opts['column_width']; ?>
									<?php echo gastro_template_media( $data ); ?>
								</div>
							<?php endif; ?>
						</div><?php // close item inner ?>
					</div><?php // close item ?>
				<?php endforeach;?>
			</div><?php // close row ?>
		<?php endforeach;?>
	</div><?php // close gst-feature-content ?>
</div>
