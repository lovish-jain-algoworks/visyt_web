<?php
/**
 * Item template file
 *
 * @package gastro/templates/items
 * @version 1.0.0
 */
?>


<?php
	$args = gastro_template_args( 'items/featuredmenu' );
	$opts = gastro_item_featuredmenu_options( $args );
?>

<?php if ( !empty( $opts['post_id'] ) ) : ?>
	<div <?php echo gastro_a( $opts ); ?>>
		<div class="gst-featuredmenu-media">
			<?php foreach ( $opts['post_id'] as $index => $id ) : ?>
				<?php
					$post_thumbnail_id = get_post_thumbnail_id( $id );
					$image_style = array( 'background' => gastro_get_background_image( array( 'id' => $post_thumbnail_id ) ) );
					$image_class = 'gst-featuredmenu-image js-featuredmenu-image';
					if ( 0 == $index ) {
						$image_class .= ' active';
					}
				?>
				<div class="<?php echo esc_attr( $image_class ); ?>" <?php echo gastro_s( $image_style ); ?> data-index="<?php echo esc_attr( $index + 1 ); ?>"></div>
			<?php endforeach; ?>
		</div><?php // close gst-featuredmenu-media ?>
		<div class="gst-featuredmenu-inner" <?php echo gastro_s( $opts['inner_style'] ); ?>>
			<?php if ( $opts['out_content_intro'] ) : ?>
				<div class="gst-featuredmenu-intro">
					<?php if ( $opts['header_on'] ) : ?>
						<h3 class="gst-featuredmenu-header gst-secondary-font gst-s-text-color">
							<?php echo do_shortcode( $opts['header'] ); ?>
						</h3>
					<?php endif; ?>
					<?php if ( $opts['subheader_on'] ) : ?>
						<div class="gst-featuredmenu-subheader">
							<?php echo do_shortcode( $opts['subheader'] ); ?>
						</div>
					<?php endif; ?>
				</div><?php // Close Menu Intro gst-featuredmenu-intro ?>
			<?php endif; ?>
			<div class="gst-featuredmenu-wrapper">
				<div class="gst-featuredmenu-content gst-s-text-border" <?php echo gastro_s( $opts['content_style'] ); ?>>
					<div class="gst-featuredmenu-overlay gst-p-bg-bg"></div>
					<?php if ( $opts['in_content_intro'] ) : ?>
						<div class="gst-featuredmenu-intro">
							<?php if ( $opts['header_on'] ) : ?>
								<h3 class="gst-featuredmenu-header gst-secondary-font gst-s-text-color">
									<?php echo do_shortcode( $opts['header'] ); ?>
								</h3>
							<?php endif; ?>
							<?php if ( $opts['subheader_on'] ) : ?>
								<div class="gst-featuredmenu-subheader">
									<?php echo do_shortcode( $opts['subheader'] ); ?>
								</div>
							<?php endif; ?>
						</div><?php // Close Menu Intro gst-featuredmenu-intro ?>
					<?php endif; ?>
					<div class="gst-featuredmenu-content-inner">
						<?php foreach ( $opts['post_id'] as $menu_index => $menu_id ) : ?>
							<?php
								$entry_attr = $opts['entry_attr'];
								$entry_attr['data_attr']['target'] = $menu_index + 1;
								$entry_attr['data_attr']['index'] = $menu_index + 1;
								$price = get_post_meta( $menu_id, 'gst_menu_price', true );
								$menu_price = ( 'before' === $opts['currency_position'] ) ? $opts['currency'] . $price : $price . $opts['currency'];
								$tags = get_the_terms( $menu_id, 'gst_menu_tag' );
								if ( 0 == $menu_index ) {
									$entry_attr['class'][] = 'active';
								}
							?>
							<div <?php echo gastro_a( $entry_attr ); ?>>
								<div class="gst-featuredmenu-entry-inner">
									<div class="gst-featuredmenu-entry-content">
										<div class="gst-featuredmenu-title">
											<div class="gst-featuredmenu-name gst-s-text-color gst-secondary-font">
												<div class="gst-featuredmenu-name-inner gst-p-brand-bg"></div>
												<h4><?php echo gastro_escape_content( get_the_title( $menu_id ) ); ?></h4>
											</div>
											<?php if ( $tags && $opts['badge_on'] ) : ?>
												<div class="gst-featuredmenu-badge">
													<?php
														foreach ( $tags as $tag ) {
															$tag_thumbnail = get_term_meta( $tag->term_id, 'thumbnail_id', true );
															if ( !empty( $tag_thumbnail ) ) {
																echo gastro_template_media( array(
																	'image_id' => (int)$tag_thumbnail,
																	'image_size' => 'medium'
																) );
															}
														}
													?>
												</div>
											<?php endif; ?>
										</div><?php // Close Menu Title gst-featuredmenu-title ?>
										<div class="gst-featuredmenu-subtitle gst-p-text-color">
											<?php echo apply_filters( 'the_content', get_post_field( 'post_content', $menu_id ) ); ?>
										</div>
									</div><?php // Close Entry Content gst-featuredmenu-entry-content ?>
									<?php if ( $opts['excerpt_on'] ) : ?>
										<div class="gst-featuredmenu-excerpt gst-s-text-color"><?php echo get_the_excerpt( $menu_id ); ?></div>
									<?php endif; ?>
									<?php if ( $opts['price_on'] ) : ?>
										<div class="gst-featuredmenu-price gst-s-text-color gst-secondary-font"><?php echo esc_html( $menu_price ); ?></div>
									<?php endif; ?>
								</div><?php // Close Entry Inner gst-featuredmenu-entry-inner ?>
							</div><?php // Close Entry ?>
						<?php endforeach; ?>
					</div><?php // Close Content Inner ?>
					<?php if ( $opts['navigation_on'] ) : ?>
						<div class="gst-featuredmenu-navigation gst-p-brand-bg js-featuredmenu-navigation" data-target="2">
							<i class="twf twf-ln-chevron-right gst-p-brand-contrast-color"></i>
						</div>
					<?php endif; ?>
				</div><?php // Close Content gst-featuredmenu-content ?>
			</div><?php // Close Wrapper gst-featuredmenu-wrapper ?>
		</div><?php // Close Inner gst-featuredmenu-inner ?>
	</div>
<?php else : ?>
	<?php echo gastro_get_dummy_template( esc_html__( 'featured menu', 'gastro' ), esc_html__( 'Menu is now empty.', 'gastro' ) ); ?>
<?php endif; ?>
