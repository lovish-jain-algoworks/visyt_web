<?php
/**
 * Item template file
 *
 * @package gastro/templates/items
 * @version 1.0.0
 */
?>


<?php
	$args = gastro_template_args( 'items/featuredpost' );
	$opts = gastro_item_featuredpost_options( $args );
?>

<?php if ( 'header' === $opts['role'] ) : ?>
	<header <?php echo gastro_a( $opts['header_attr'] ); ?>>
		<?php gastro_template_background( $opts ); ?>
		<div class="<?php echo esc_attr( $opts['container_class'] ); ?>">
<?php endif; ?>

			<div <?php echo gastro_a( $opts ); ?>>
				<?php if ( !empty( $opts['post_id'] ) ) : ?>
					<div <?php echo gastro_a( $opts['content_attr'] ); ?>>

						<?php foreach ( $opts['post_id'] as $index => $post_id ) : ?>

								<?php if ( 'publish' === $opts['post_status'][$index] && !empty( $post_id ) ) : ?>
									<article class="gst-entry">
										<div class="gst-entry-inner gst-p-border-border" <?php echo gastro_s( $opts['entry_inner_style'] ); ?>>

											<div class="gst-entry-header" <?php echo gastro_s( $opts['header_style'][$index] ); ?>></div>
											<?php if ( 'overlay' === $opts['carousel_background_type'] ) : ?>
												<div class="gst-overlay"></div>
											<?php endif; ?>
											<div class="gst-entry-body" <?php echo gastro_s( $opts['body_style'] ); ?>>
												<div class="gst-entry-body-inner gst-p-border-border">

													<?php if ( 'quote' === $opts['post_format'][$index] ) : ?>
														<a class="gst-entry-content" href="<?php echo esc_url( $opts['post_link'][$index] ); ?>">
															<h2 <?php echo gastro_a( $opts['title_attr'] ); ?>>
																<?php echo esc_html( $opts['post_meta'][$index]['quote'] ); ?>
															</h2>
															<div class="gst-entry-author"><?php echo esc_html( $opts['post_meta'][$index]['author'] ); ?></div>
														</a>
													<?php else : ?>
														<?php if ( $opts['category_on'] ) : ?>
															<?php $category = get_the_category( $post_id ); ?>
															<?php if ( !empty( $category ) ) : ?>
																<div class="gst-entry-category gst-p-brand-color gst-<?php echo esc_attr( $opts['meta_font'] ); ?>-font">
																	<?php the_category( ',&nbsp;', '', $post_id ); ?>
																</div>
															<?php endif; ?>
														<?php endif; ?>
														<div class="gst-entry-content">
															<h2 <?php echo gastro_a( $opts['title_attr'] ); ?>>
																<a href="<?php echo esc_url( $opts['post_link'][$index] ); ?>">
																	<?php echo gastro_escape_content( $opts['post'][$index]->post_title ); ?>
																</a>
															</h2>
															<?php if ( $opts['author_on'] ) : ?>
																<div class="gst-entry-author gst-<?php echo esc_attr( $opts['meta_font'] ); ?>-font">
																	<i><?php esc_html_e( 'By', 'gastro' ); ?></i>
																	<?php echo get_the_author_meta( 'display_name', $opts['post'][$index]->post_author ); ?>
																</div>
															<?php endif; ?>

															<?php if ( $opts['excerpt_on'] ) : ?>
																<?php $excerpt = !empty( $opts['post'][$index]->post_excerpt ) ? $opts['post'][$index]->post_excerpt : $opts['post'][$index]->post_content; ?>
																<?php if ( !empty( $excerpt ) ) : ?>
																	<div class="gst-entry-excerpt">
																		<?php echo do_shortcode( $excerpt ); ?>
																	</div>
																<?php endif; ?>
															<?php endif; ?>
														</div><?php // close entry content gst-entry-content ?>
													<?php endif; ?>
												</div><?php // close entry body inner gst-entry-body-inner ?>
											</div><?php // close entry body gst-entry-body ?>
										</div><?php // close entry inner gst-entry-inner ?>
									</article><?php // close entry gst-entry ?>
								<?php endif; ?>

						<?php endforeach; ?>
					</div><?php // close featuredpost content ?>
				<?php else : ?>
					<?php echo gastro_get_dummy_template( esc_html__( 'featured post', 'gastro' ), esc_html__( 'Please input post ID.', 'gastro' ) ); ?>
				<?php endif; ?>
			</div><?php // close element ?>

<?php if ( 'header' === $opts['role'] ) : ?>
		</div><?php // close container ?>
	</header>
<?php endif; ?>
