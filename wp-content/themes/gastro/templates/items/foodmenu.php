<?php
/**
 * Item template file
 *
 * @package gastro/templates/items
 * @version 1.0.0
 */
?>


<?php
	$args = gastro_template_args( 'items/foodmenu' );
	$opts = gastro_item_foodmenu_options( $args );
	$query = new WP_Query( $opts['query_args'] );
?>

<?php if ( $query->have_posts() ): ?>
	<div <?php echo gastro_a( $opts ); ?>>
		<div class="gst-foodmenu-inner">
			<?php while( $query->have_posts() ) : $query->the_post(); ?>
				<?php
					$current_post = $query->current_post;
					$is_row_first = ( 0 == $current_post % $opts['no_of_columns'] ) ? true : false;
					$is_row_last = ( $current_post % $opts['no_of_columns'] == $opts['no_of_columns'] - 1 || ( int )( $query->query['posts_per_page'] ) - 1 == $current_post ||
						( ( ( int )$query->found_posts < ( int )( $query->query['posts_per_page'] ) || -1 == ( int )$query->query['posts_per_page'] ) && ( int )$query->found_posts - 1 == $current_post ) );

					$menu_id = get_the_ID();
					$content = get_the_content();
					$meta_value = get_post_custom( $menu_id );
					$opts['media_args']['image_id'] = get_post_thumbnail_id( $menu_id );

					if ( !empty( $meta_value ) ) {
						$is_featured = isset( $meta_value['gst_menu_featured'] ) ? $meta_value['gst_menu_featured'][0] : false;
						$price_value = isset( $meta_value['gst_menu_price'] ) ? $meta_value['gst_menu_price'][0] : '';
						$link = isset( $meta_value['gst_menu_link'] ) ? $meta_value['gst_menu_link'][0] : '';
					} else {
						$is_featured = false;
						$price_value = '';
						$link = '';
					}

					$entry_class = $opts['entry_class'];
					if ( $is_featured ) {
						$entry_class[] = 'featured';
					}
					$price = ( 'before' === $opts['currency_position'] ) ? $opts['currency'] . $price_value : $price_value . $opts['currency'];
				?>

				<?php if ( !$opts['has_column_style'] || ( $opts['has_column_style'] && $is_row_first ) ) : ?>
					<div <?php echo gastro_a( $opts['row_attr'] ); ?>>
				<?php endif; ?>

					<div <?php post_class( $entry_class ); ?> <?php echo gastro_s( $opts['entry_style'] ); ?>>
						<div <?php echo gastro_a( $opts['entry_inner_attr'] ); ?>>
							<?php if ( $is_featured && 'mouseover' === $opts['style'] ) : ?>
								<div class="gst-entry-star gst-p-bg-bg"><i class="twf twf-star gst-p-brand-color"></i></div>
							<?php endif; ?>
							<?php if ( $opts['image_on'] ) : ?>
								<div class="gst-entry-header">
									<?php if ( $opts['link_on'] && !empty( $link ) ) : ?>
										<a href="<?php echo esc_url( $link ); ?>" target="<?php echo esc_attr( $opts['link_target'] ); ?>"/>
											<?php echo gastro_template_media( $opts['media_args'] ); ?>
										</a>
									<?php else : ?>
										<?php echo gastro_template_media( $opts['media_args'] ); ?>
									<?php endif; ?>
								</div><?php // close header gst-entry-header ?>
							<?php endif; ?>
							<div class="gst-entry-body gst-s-text-border" <?php echo gastro_s( $opts['menu_body_style'] ); ?>>
								<?php if ( 'mouseover' === $opts['style'] ) : ?>
									<div class="gst-overlay" <?php echo gastro_s( $opts['overlay_style'] ); ?>></div>
								<?php endif; ?>
								<div class="gst-entry-body-inner">
									<div class="gst-entry-body-wrapper">
										<div class="gst-entry-body-content">
											<div <?php echo gastro_a( $opts['menu_title_attr'] ); ?>>
												<div class="gst-entry-title-inner">
													<div class="gst-entry-title-wrapper">
														<div class="gst-entry-name gst-s-text-color">
															<div class="gst-entry-name-inner gst-p-brand-bg"></div>
															<?php if ( $opts['link_on'] && !empty( $link ) ) : ?>
																<a class="gst-entry-name-text" href="<?php echo esc_url( $link ); ?>" target="<?php echo esc_attr( $opts['link_target'] ); ?>">
																	<?php echo gastro_escape_content( get_the_title() ); ?>
																</a>
															<?php else : ?>
																<div class="gst-entry-name-text"><?php echo gastro_escape_content( get_the_title() ); ?></div>
															<?php endif; ?>
														</div><?php // close title name gst-entry-name ?>
														<?php if ( $opts['badge_on'] ) : ?>
															<?php $tags = get_the_terms( $menu_id, 'gst_menu_tag' ); ?>
															<?php if ( $tags ) : ?>
																<div class="gst-entry-badge">
																	<?php
																		foreach ( $tags as $tag ) {
																			$tag_thumbnail = get_term_meta( $tag->term_id, 'thumbnail_id', true );
																			if ( !empty( $tag_thumbnail ) ) {
																				echo gastro_template_media( array(
																					'image_id' => (int)$tag_thumbnail,
																					'image_size' => 'medium'
																				) );
																			}
																		}
																	?>
																</div>
															<?php endif; ?>
														<?php endif; ?>
													</div><?php // close title wrapper gst-entry-title-wrapper ?>
													<?php if ( $opts['title_price_on'] && !empty( $price_value ) ) : ?>
														<?php if ( $opts['menu_line_on'] ) : ?>
															<div class="gst-entry-line">
																<div class="gst-entry-line-inner gst-p-border-border" <?php echo gastro_s( $opts['menu_line_style'] ); ?>></div>
															</div>
														<?php endif; ?>
														<div <?php echo gastro_a( $opts['menu_price_attr'] ); ?>>
															<?php echo esc_html( $price ); ?>
														</div>
													<?php endif; ?>
												</div><?php // close entry title inner gst-entry-title-inner ?>
											</div><?php // close entry title gst-entry-title ?>
											<?php if ( $opts['subtitle_on'] && !empty( $content ) ) : ?>
												<div <?php echo gastro_a( $opts['menu_subtitle_attr'] ); ?>><?php the_content(); ?></div>
											<?php endif; ?>
										</div><?php // close entry body content gst-entry-body-content ?>
										<?php if ( $opts['footer_price_on'] && !empty( $price_value ) ) : ?>
											<div class="gst-entry-footer">
												<div <?php echo gastro_a( $opts['menu_price_attr'] ); ?>>
													<?php echo esc_html( $price ); ?>
												</div>
											</div>
										<?php endif; ?>
									</div><?php // close entry body wrapper gst-entry-body-wrapper ?>
								</div><?php // close entry body inner gst-entry-body-inner ?>
							</div><?php // close entry body gst-entry-body ?>
						</div><?php // close entry inner gst-entry-inner ?>
					</div><?php // close entry gst-entry ?>

				<?php if ( !$opts['has_column_style'] || ( $opts['has_column_style'] && $is_row_last ) ) : ?>
					</div><?php // close row ?>
				<?php endif; ?>
			<?php endwhile; ?>
			<?php wp_reset_postdata(); ?>
		</div><?php // close menu inner gst-foodmenu-inner ?>
	</div>
<?php else : ?>
	<?php echo gastro_get_dummy_template( esc_html__( 'food menu', 'gastro' ), esc_html__( 'Menu is now empty.', 'gastro' ) ); ?>
<?php endif; ?>
