<?php
/**
 * Item template file
 *
 * @package gastro/templates/items
 * @version 1.0.0
 */
?>


<?php
	$args = gastro_template_args( 'items/gallery' );
	$opts = gastro_item_gallery_options( $args );
?>
<?php if ( !empty( $opts['image_id'] ) ) : ?>
	<div <?php echo gastro_a( $opts ); ?>>
		<div <?php echo gastro_a( $opts['content_attr'] ); ?>>
			<?php $images = ( !$opts['pagination'] || 'false' === $opts['pagination'] ) ? $opts['image_id'] : array_slice( $opts['image_id'], ( $opts['query_args']['paged'] - 1 ) * $opts['no_of_items'], $opts['no_of_items'] ); ?>
			<?php foreach ( $images as $index => $image_id ) : ?>
				<?php $opts['image'] = $image_id; ?>
				<?php $opts['image_index'] = $index; ?>
				<?php gastro_template( 'entry-gallery.php', $opts ); ?>
			<?php endforeach; ?>
		</div>

		<?php if ( $opts['thumbnail'] && 'carousel' === $opts['style'] ) : ?>
			<div class="gst-gallery-thumbnail gst-gallery-thumbnail--bottom" data-thumbnail='<?php echo esc_attr( $opts['thumbnail_columns'] ); ?>'>
				<?php foreach ( $opts['image_id'] as $index => $image_id ): ?>
					<?php $thumbnail_image_args = array(
						'image_id' => $image_id,
						'image_ratio' => '1x1',
						'image_column_width' => $opts['thumbnail_column_width']
					); ?>
					<div class="gst-gallery-thumbnail-item gst-col-<?php echo esc_attr( $opts['thumbnail_column_width'] ); ?>">
						<?php echo gastro_template_media( $thumbnail_image_args ); ?>
					</div>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>

		<?php if ( !empty( $opts['caption'] ) ) : ?>
			<?php // this is gallery caption not image caption ?>
			<div class="gst-gallery-caption">
				<?php echo do_shortcode( $opts['caption'] ); ?>
			</div>
		<?php endif; ?>

		<?php if ( $opts['pagination'] && 'carousel' !== $opts['style'] && 'scroll' !== $opts['style'] ) : ?>
			<?php gastro_template_pagination( $opts['query_args'], $opts ); ?>
		<?php endif; ?>
	</div>
<?php else :?>
		<?php echo gastro_get_dummy_template( esc_html__( 'gallery', 'gastro' ), esc_html__( 'Gallery is now empty, please upload images.', 'gastro' ) ); ?>
<?php endif; ?>
