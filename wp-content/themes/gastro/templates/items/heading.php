<?php
/**
 * Item template file
 *
 * @package gastro/templates/items
 * @version 1.0.0
 */
?>


<?php
	$args = gastro_template_args( 'items/heading' );
	$opts = gastro_item_heading_options( $args );
	gastro_get_title( $opts );
?>
