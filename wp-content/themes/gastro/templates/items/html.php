<?php
/**
 * Item template file
 *
 * @package gastro/templates/items
 * @version 1.0.0
 */
?>


<?php
	$args = gastro_template_args( 'items/html' );
	$opts = gastro_item_default_options( $args );
?>

<div <?php echo gastro_a( $opts ); ?>>
	<?php echo do_shortcode( html_entity_decode( $opts['html'] ) ); ?>
</div>
