<?php
/**
 * Item template file
 *
 * @package gastro/templates/items
 * @version 1.0.0
 */
?>


<?php
	$args = gastro_template_args( 'items/image' );
	$opts = gastro_item_image_options( $args );
?>

<div <?php echo gastro_a( $opts ); ?>>
	<div class="gst-image-container">
		<?php echo gastro_template_media( $opts ); ?>
	</div>
<?php if ( $opts['caption_on'] && !empty( $opts['caption'] ) ) : ?>
	<div class="gst-image-caption">
		<?php echo do_shortcode( $opts['caption'] ); ?>
	</div>
<?php endif; ?>
</div>
