<?php
/**
 * Item template file
 *
 * @package gastro/templates/items
 * @version 1.0.0
 */
?>


<?php
	$args = gastro_template_args( 'items/interactive' );
	$opts = gastro_item_interactive_options( $args );
?>

<div <?php echo gastro_a( $opts ); ?>>
	<div class="gst-interactive-space gst-interactive-space--normal <?php echo esc_attr( $opts['normal_class'] ); ?>">
		<?php gastro_template_background( $opts['data'][0] ); ?>
		<div class="gst-interactive-content" <?php echo gastro_s( $opts['space_style'] ); ?>>
			<?php if ( !empty( $opts['spaces'][0]['items'] ) && is_array( $opts['spaces'][0]['items'] ) ) : ?>
				<?php foreach ( $opts['spaces'][0]['items'] as $items ) : ?>
					<?php gastro_template_item( $items ); ?>
				<?php endforeach; ?>
			<?php endif; ?>
		</div>
	</div>
	<div class="gst-interactive-space gst-interactive-space--hover <?php echo esc_attr( $opts['hover_class'] ); ?>">
		<?php gastro_template_background( $opts['data'][1] ); ?>
		<div class="gst-interactive-content" <?php echo gastro_s( $opts['space_style'] ); ?>>
			<?php if ( !empty( $opts['spaces'][1]['items'] ) && is_array( $opts['spaces'][1]['items'] ) ) : ?>
				<?php foreach ( $opts['spaces'][1]['items'] as $items ) : ?>
					<?php gastro_template_item( $items ); ?>
				<?php endforeach; ?>
			<?php endif; ?>
		</div>
	</div>
</div>
