<?php
/**
 * Item template file
 *
 * @package gastro/templates/items
 * @version 1.0.0
 */
?>


<?php
	$args = gastro_template_args( 'items/map' );
	$opts = gastro_item_map_options( $args );
?>

<div <?php echo gastro_a( $opts ); ?>>
	<?php if ( shortcode_exists( 'wpgmza' ) ) : ?>

		<?php if ( !empty( $opts['map_id'] ) ) : ?>
			<?php echo do_shortcode( '[wpgmza id="'. $opts['map_id'] .'"]' ); ?>
		<?php else : ?>
			<?php echo gastro_get_dummy_template( esc_html__( 'map', 'gastro' ), esc_html__( 'Please input Map ID.', 'gastro' ) ); ?>
		<?php endif; ?>
	<?php else : ?>
		<?php echo gastro_get_dummy_template( esc_html__( 'map', 'gastro' ), esc_html__( 'Please install plugin "WP Google Maps".', 'gastro' ) ); ?>
	<?php endif; ?>
</div>
