<?php
/**
 * Item template file
 *
 * @package gastro/templates/items
 * @version 1.0.0
 */
?>


<?php
	$args = gastro_template_args( 'items/message' );
	$opts = gastro_item_message_options( $args );
?>

<div <?php echo gastro_a( $opts ); ?>>
	<div class="gst-message-content">

		<?php if ( $opts['icon_on'] ): ?>
			<div class="gst-message-icon"><?php echo gastro_template_media( $opts ); ?></div>
		<?php endif; ?>

		<div class="gst-message-text"><?php echo do_shortcode( $opts['text'] ); ?></div>
	</div>

	<?php if ( $opts['button_on'] ): ?>
		<a href="#js-close-all" class="gst-message-close-button twf twf-times"></a>
	<?php endif; ?>

</div>
