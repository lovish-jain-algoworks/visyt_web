<?php
/**
 * Item template file
 *
 * @package gastro/templates/items
 * @version 1.0.0
 */
?>


<?php
	$args = gastro_template_args( 'items/meta' );
	$opts = gastro_item_meta_options( $args );
?>

<div <?php echo gastro_a( $opts ); ?>>
	<ul class="gst-meta-list">
	<?php foreach ( $opts['data'] as $index => $data ) : ?>
		<li class="gst-meta-item">
			<?php if ( $opts['title_on'] || $opts['icon_on'] ) : ?>
				<div class="gst-meta-header  gst-<?php echo esc_attr( $opts['title_font'] ); ?>-font">
					<?php if ( $opts['icon_on'] ) : ?>
						<div class="gst-meta-icon twf twf-<?php echo esc_attr( $data['icon'] ); ?>" <?php echo gastro_s( $opts['icon_style'] ); ?>></div>
					<?php endif; ?>
					<?php if ( $opts['title_on'] ) : ?>
						<div class="gst-meta-title" <?php echo gastro_s( $opts['title_style'] ); ?>>
							<?php echo do_shortcode( $data['title'] ); ?>
						</div>
					<?php endif; ?>
				</div>
		<?php endif; ?>
			<div class="gst-meta-body gst-<?php echo esc_attr( $opts['detail_font'] ); ?>-font">
				<div class="gst-meta-detail" <?php echo gastro_s( $opts['detail_style'] ); ?>>
					<?php echo do_shortcode( $data['detail'] ); ?>
				</div>
			</div>
		</li>
	<?php endforeach; ?>
	</ul>
</div>
