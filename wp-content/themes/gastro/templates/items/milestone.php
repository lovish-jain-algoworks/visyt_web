<?php
/**
 * Item template file
 *
 * @package gastro/templates/items
 * @version 1.0.0
 */
?>


<?php
	$args = gastro_template_args( 'items/milestone' );
	$opts = gastro_item_milestone_options( $args );
?>

<div <?php echo gastro_a( $opts ); ?>>
	<div class="gst-row">

		<?php foreach ( $opts['data'] as $index => $data ) : ?>
			<?php
				$item_class = $opts['item_class'];
				if ( isset( $opts['animation'] ) && 'none' !== $opts['animation'] ) {
					$item_class .= ' anmt-item anmt-' . $opts['animation'];

					if ( isset( $opts['stagger'] ) && $opts['stagger'] ) {
						$item_class .= ' stagger';
					}
				}
			?>
			<div class="<?php echo esc_attr( $item_class ); ?>">
				<div class="gst-milestone-wrapper gst-p-border-border" <?php echo gastro_s( $opts['wrapper_style'] );?>>
					<div class="gst-milestone-body">
						<div class="gst-milestone-body-inner">

							<?php if ( $opts['media_on'] ): ?>
								<div class="gst-milestone-media">
									<?php $data['image_column_width'] = $opts['column_width']; ?>
									<?php echo gastro_template_media( $data ); ?>
								</div>
							<?php endif; ?>

							<div class="gst-milestone-content">
								<div class="gst-milestone-number gst-p-brand-color gst-<?php echo esc_attr( $opts['number_font'] ); ?>-font" <?php echo gastro_s( $opts['number_style'] ); ?>>
									<?php echo esc_html( $data['number'] ); ?>
								</div>
								<div class="gst-milestone-text">
									<div class="gst-milestone-title gst-<?php echo esc_attr( $opts['title_font'] ); ?>-font gst-s-text-color" <?php echo gastro_s( $opts['title_style'] ); ?>>
										<?php echo do_shortcode( $data['title'] ); ?>
									</div>

									<?php if ( $opts['subtitle_on'] ): ?>
										<div class="gst-milestone-subtitle gst-<?php echo esc_attr( $opts['subtitle_font'] ); ?>-font" <?php echo gastro_s( $opts['subtitle_style'] ); ?>>
											<?php echo do_shortcode( $data['subtitle'] ); ?>
										</div>
									<?php endif; ?>

								</div><?php // close text gst-milestone-text ?>
							</div><?php // close content gst-milestone-content ?>
						</div><?php // close body inner gst-milestone-body-inner ?>
					</div><?php // close body gst-milestone-body ?>
				</div><?php // close wrapper gst-milestone-wrapper ?>
			</div><?php // close item gst-milestone-item ?>
		<?php endforeach; ?>

	</div><?php // close row gst-row ?>
</div>
