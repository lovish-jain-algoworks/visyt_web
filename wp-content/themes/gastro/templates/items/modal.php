<?php
/**
 * Item template file
 *
 * @package gastro/templates/items
 * @version 1.0.0
 */
?>


<?php
	$args = gastro_template_args( 'items/modal' );
	$opts = gastro_item_modal_options( $args );
?>

<div <?php echo gastro_a( $opts ); ?>>
	<div class="gst-modal-inner" <?php echo gastro_s( $opts['inner_style'] ); ?>>
		<div class="gst-modal-overlay js-close"></div>
		<div class="gst-modal-wrapper" <?php echo gastro_s( $opts['wrapper_style'] ); ?>>
			<?php gastro_template_background( $opts, 'gst-p-bg-bg' ); ?>
			<div class="gst-modal-content">
				<?php if ( !empty( $opts['spaces'] ) && is_array( $opts['spaces'] ) ) : ?>
					<?php foreach( $opts['spaces'] as $space ) : ?>
						<?php if ( !empty( $space['items'] ) && is_array( $space['items'] ) ) : ?>
							<?php foreach( $space['items'] as $item ) : ?>
								<?php gastro_template_item( $item ); ?>
							<?php endforeach; ?>
						<?php endif; ?>
					<?php endforeach; ?>
				<?php endif; ?>
			</div>
		</div>
	</div>
	<div class="gst-modal-close js-close"><i class="twf twf-ln-cross"></i></div>
</div>
