<?php
/**
 * Item template file
 *
 * @package gastro/templates/items
 * @version 1.0.0
 */
?>


<?php
	$args = gastro_template_args( 'items/navigation' );
	$opts = gastro_item_navigation_options( $args );

	$taxonomy_name = gastro_get_post_taxonomy_name();
	$prev_post = get_adjacent_post( $opts['same_term'], '', true, $taxonomy_name );
	$next_post = get_adjacent_post( $opts['same_term'], '', false, $taxonomy_name );
	$has_prev_post = !empty( $prev_post ) ? true : false;
	$has_next_post = !empty( $next_post ) ? true : false;
?>

<?php if ( $has_prev_post || $has_next_post ) : ?>
	<div <?php echo gastro_a( $opts ); ?>>

		<?php if ( 'minimal' === $opts['style'] ) : ?>
			<?php if ( $has_prev_post ) : ?>
				<div class="gst-navigation-previous">
					<?php previous_post_link(
						'%link',
						'<i class="twf twf-angle-left twf-lg"></i>' . esc_html__( 'PREV', 'gastro' ),
						$opts['same_term'],
						'',
						$taxonomy_name
					); ?>
				</div>
			<?php endif; ?>

			<?php if ( $has_next_post ) : ?>
				<div class="gst-navigation-next">
					<?php next_post_link(
						'%link',
						esc_html__( 'NEXT', 'gastro' ) . '<i class="twf twf-angle-right twf-lg"></i>',
						$opts['same_term'],
						'',
						$taxonomy_name
					); ?>
				</div>
			<?php endif; ?>
		<?php else: ?>
			<?php if ( $has_prev_post ) : ?>
				<div class="gst-navigation-previous">
					<?php previous_post_link(
						'%link',
						'<div class="gst-navigation-content">
							<span class="gst-navigation-label"><i class="twf twf-angle-left twf-lg"></i>' . esc_html__( 'Previous', 'gastro' ) . '</span>
							<span class="gst-navigation-title gst-s-text-color">%title</span>
					 	</div>',
						$opts['same_term'],
						'',
						$taxonomy_name
					); ?>
				</div>
			<?php endif; ?>

			<?php if ( $has_next_post ) : ?>
				<div class="gst-navigation-next">
					<?php next_post_link(
						'%link',
						'<div class="gst-navigation-content">
							<span class="gst-navigation-label">' . esc_html__( 'Next', 'gastro' ) . '<i class="twf twf-angle-right twf-lg"></i></span>
							<span class="gst-navigation-title gst-s-text-color">%title</span>
					 	</div>',
						$opts['same_term'],
						'',
						$taxonomy_name
					); ?>
				</div>
			<?php endif; ?>
		<?php endif; ?>

		<?php if ( 'bar' === $opts['style'] && $opts['index_on'] ) : ?>
			<div class="gst-navigation-middle">
				<?php $post_type = get_post_type(); ?>
				<a class="gst-p-text-color" href="<?php echo get_post_type_archive_link( $post_type ); ?>">
					<i class="twf twf-th-large twf-2x"></i>
				</a>
			</div>
		<?php endif; ?>

	</div>
<?php endif; ?>
