<?php
/**
 * Item template file
 *
 * @package gastro/templates/items
 * @version 1.0.0
 */
?>


<?php
	$args = gastro_template_args( 'items/navigationmenu' );
	$opts = gastro_item_navigation_menu_options( $args );
?>

<div <?php echo gastro_a( $opts ); ?>>
	<div class="gst-navigationmenu-inner">
		<?php gastro_template( 'navbar-' . $opts['style'], $opts ); ?>
		<?php gastro_template( 'navbar-mobile', $opts ); ?>
	</div>
</div>
