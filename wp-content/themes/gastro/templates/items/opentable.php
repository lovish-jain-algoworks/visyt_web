<?php
/**
 * Item template file
 *
 * @package gastro/templates/items
 * @version 1.0.0
 */
?>


<?php
	$args = gastro_template_args( 'items/opentable' );
	$opts = gastro_item_opentable_options( $args );
?>

<?php if ( !empty( $opts['opentable_id'] ) ) : ?>
	<div <?php echo gastro_a( $opts ); ?>>
		<div class="gst-opentable-inner">
			<form class="gst-opentable-form" name="opentable-form" target="_blank" method="get" action="<?php echo esc_url( 'https://secure.opentable.com/ism/interim.aspx' ); ?>">
				<div class="gst-row">
					<div class="form-container gst-opentable-date <?php echo esc_attr( $opts['column_class'] ); ?>">
						<label class="gst-form-label"><i class="twf twf-calendar"></i></label>
						<input type="text" class="gst-form-input" name="startDate" value=""/>
					</div>
					<div class="form-container gst-opentable-time <?php echo esc_attr( $opts['column_class'] ); ?>">
						<label class="gst-form-label"><i class="twf twf-clock"></i></label>
						<input type="text" class="gst-form-input" name="ResTime" value=""/>
					</div>
					<div class="form-container gst-opentable-party <?php echo esc_attr( $opts['column_class'] ); ?>">
						<label class="gst-form-label"><i class="twf twf-user"></i></label>
						<select name="PartySize" class="gst-form-input">
							<?php for ( $i = 1; $i < 21 ; $i++ ) : ?>
								<?php
									if ( 1 == $i ) {
										$option = $i . ' ' . esc_html__( 'person', 'gastro' );
									} elseif ( $i < 20 ) {
										$option = $i . ' ' . esc_html__( 'people', 'gastro' );
									} else {
										$option = $i . '+ ' . esc_html__( 'people', 'gastro' );
									}
								?>
								<option value="<?php echo esc_attr( $i ); ?>"><?php echo esc_html( $option ); ?></option>
							<?php endfor; ?>
						</select>
					</div>
				</div><?php // close row gst-row ?>
				<div class="form-container gst-opentable-submit">
					<input type="submit" class="gst-form-submit" value="<?php echo esc_attr( $opts['button_label'] ); ?>"/>
				</div>
				<input type="hidden" name="RestaurantID" value="<?php echo esc_attr( $opts['opentable_id'] ); ?>">
				<input type="hidden" name="txtDateFormat" value="MM/dd/yyyy">
			</form>
		</div><?php // close inner gst-opentable-inner ?>
	</div>
<?php else : ?>
	<?php echo gastro_get_dummy_template( esc_html__( 'opentable', 'gastro' ), esc_html__( 'Please input opentable ID.', 'gastro' ) ); ?>
<?php endif; ?>
