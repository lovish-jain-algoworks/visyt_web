<?php
/**
 * Item template file
 *
 * @package gastro/templates/items
 * @version 1.0.0
 */
?>


<?php
	$args = gastro_template_args( 'items/pluginslider' );
	$opts = gastro_item_pluginslider_options( $args );
?>

<div <?php echo gastro_a( $opts ); ?>>
	<?php if ( !empty( $opts['pluginslider'] ) ) : ?>
		<?php
			$pluginsliders = explode( ' ', $opts['pluginslider'] );
			$prefix = $pluginsliders[0];
			$plugin = substr( $prefix, 1 );
		?>

		<?php if ( shortcode_exists( $plugin ) ) : ?>
			<?php echo do_shortcode( $opts['pluginslider'] ); ?>
		<?php else : ?>
			<?php echo gastro_get_dummy_template( esc_html__( 'plug in slider', 'gastro' ), esc_html__( 'Please activate plug-in slider.', 'gastro' ) ); ?>
		<?php endif; ?>
	<?php else : ?>
		<?php echo gastro_get_dummy_template( esc_html__( 'plug in slider', 'gastro' ), esc_html__( 'Please input Slider Shortcode.', 'gastro' ) ); ?>
	<?php endif; ?>
</div>
