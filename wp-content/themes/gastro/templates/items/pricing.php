<?php
/**
 * Item template file
 *
 * @package gastro/templates/items
 * @version 1.0.0
 */
?>


<?php
	$args = gastro_template_args( 'items/pricing' );
	$opts = gastro_item_pricing_options( $args );
?>

<div <?php echo gastro_a( $opts ); ?>>
	<div class="gst-row">

		<?php foreach ( $opts['data'] as $index => $data ) : ?>
			<div class="gst-pricing-item gst-p-border-border <?php echo esc_attr( implode( ' ', $opts['item_class'][$index] ) ); ?>" <?php echo gastro_s( $opts['item_style'][$index] ); ?>>
				<div class="gst-pricing-header <?php echo esc_attr( implode( ' ', $opts['heading_class'][$index] ) ); ?>" <?php echo gastro_s( $opts['heading_style'][$index] ); ?>>

					<?php if ( !empty( $data['top_title'] ) ) : ?>
						<div class="gst-pricing-top-title gst-<?php echo esc_attr( $opts['toptitle_font'] ); ?>-font">
							<?php echo do_shortcode( $data['top_title'] ); ?>
						</div>
					<?php endif; ?>

					<?php if ( $opts['image_on'] ) : ?>
						<div class="gst-pricing-media">
							<?php $data['image_column_width'] = $opts['column_width']; ?>
							<?php echo gastro_template_media( $data ); ?>
						</div>
					<?php endif; ?>

					<div class="gst-pricing-price gst-<?php echo esc_attr( $opts['price_font'] ); ?>-font">

						<?php if ( $data['currency'] ) : ?>
							<span class="gst-pricing-currency">
								<?php echo esc_html( $opts['currency'] ); ?>
							</span>
						<?php endif; ?>

						<?php if ( !empty( $data['value'] ) || 0 == $data['value'] ) : ?>
							<?php $decimal = explode( '.', $data['value'] ); ?>
							<?php if ( !empty( $decimal[1] ) ) : ?>
								<span class="gst-pricing-value"><?php echo esc_html( $decimal[0] ); ?></span>
								<span class="gst-pricing-decimal">.<?php echo esc_html( $decimal[1] ); ?></span>
							<?php else : ?>
								<span class="gst-pricing-value"><?php echo esc_html( $data['value'] ); ?></span>
							<?php endif; ?>
						<?php endif; ?>

						<?php if ( !empty( $data['unit'] ) ) : ?>
							<span class="gst-pricing-unit"><?php echo do_shortcode( $data['unit'] ); ?></span>
						<?php endif; ?>

					</div><?php // close price gst-pricing-price ?>
					<?php if ( !empty( $data['title'] ) ) : ?>
						<div class="gst-pricing-title gst-<?php echo esc_attr( $opts['subtitle_font'] ); ?>-font">
							<?php echo do_shortcode( $data['title'] ); ?>
						</div>
					<?php endif; ?>
				</div><?php // close header gst-pricing-header ?>

				<?php if ( count( $data['content'] ) > 0 ) : ?>
					<div class="gst-pricing-body gst-<?php echo esc_attr( $opts['content_font'] ); ?>-font">

						<?php foreach ( $data['content'] as $i => $content ) : ?>
							<div class="gst-pricing-body-content" <?php echo gastro_s( $opts['alternate_style'][$i] ); ?>>
								<div class="gst-pricing-body-text"><?php echo do_shortcode( $content ); ?></div>
							</div>
						<?php endforeach; ?>

					</div><?php // close body gst-pricing-body ?>
				<?php endif; ?>

				<?php if ( $opts['button_on'] ) : ?>
					<div class="gst-pricing-footer">
						<?php gastro_template_button( $data ); ?>
					</div>
				<?php endif; ?>

			</div><?php // close item gst-pricing-item ?>

		<?php endforeach; ?>
	</div><?php // close row gst-row ?>
</div>
