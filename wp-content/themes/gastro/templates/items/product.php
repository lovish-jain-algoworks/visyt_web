<?php
/**
 * Item template file
 *
 * @package gastro/templates/items
 * @version 1.0.0
 */
?>


<?php
	$args = gastro_template_args( 'items/product' );
	$opts = gastro_item_product_options( $args );
	$query = new WP_Query( $opts['query_args'] );
?>

<?php if ( gastro_is_woocommerce_activated() ) : ?>
	<?php if ( $query->have_posts() ): ?>
		<div <?php echo gastro_a( $opts ); ?>>
			<?php if ( $opts['filter'] ): ?>
				<?php gastro_template_filter( $query, $opts['filter_args'] ); ?>
			<?php endif; ?>

			<div <?php echo gastro_a( $opts['content_attr'] ); ?>>
				<?php while( $query->have_posts() ) : $query->the_post(); ?>
					<?php gastro_template( 'entry-product', $opts ); ?>
				<?php endwhile; ?>
			</div>

			<?php if ( $opts['pagination'] ): ?>
				<?php gastro_template_pagination( $query, $opts ); ?>
			<?php endif; ?>

			<?php wp_reset_postdata(); ?>
		</div>
	<?php else : ?>
		<?php echo gastro_get_dummy_template( esc_html__( 'product', 'gastro' ), esc_html__( 'Shop is now empty.', 'gastro' ) ); ?>
	<?php endif; ?>

<?php else : ?>
	<?php echo gastro_get_dummy_template( esc_html__( 'product', 'gastro' ), esc_html__( 'Please install and activate Woocommerce Plugin.', 'gastro' ) ); ?>
<?php endif; ?>
