<?php
/**
 * Item template file
 *
 * @package gastro/templates/items
 * @version 1.0.0
 */
?>


<?php
	$args = gastro_template_args( 'items/productcat' );
	$opts = gastro_item_productcat_options( $args );
?>

<div <?php echo gastro_a( $opts ); ?>>
	<?php if ( gastro_is_woocommerce_activated() ) : ?>
		<?php if ( !empty( $opts['filters'] ) ) : ?>
			<div <?php echo gastro_a( $opts['content_attr'] ); ?>>
				<?php
					$categories = get_terms( $opts['taxonomy'], $opts['category_args'] );
					if ( is_array( $categories ) ) {
						foreach ( $categories as $category ) {
							$slug = $category->slug;
							if ( in_array( 'all', $opts['filters'] ) || in_array( $slug, $opts['filters'] ) ) {
								$opts['category_obj'] = $category;
								gastro_template( 'entry-productcat', $opts );
							}
						}
					} else {
						echo gastro_get_dummy_template( esc_html__( 'product category', 'gastro' ), esc_html__( 'Product Category is now empty.', 'gastro' ) );
					}
				?>
			</div><?php // close content ?>

		<?php else : ?>
			<?php echo gastro_get_dummy_template( esc_html__( 'product category', 'gastro' ), esc_html__( 'Please choose categories.', 'gastro' ) ); ?>
		<?php endif; ?>

	<?php else : ?>
		<?php echo gastro_get_dummy_template( esc_html__( 'product category', 'gastro' ), esc_html__( 'Please install and activate Woocommerce Plugin.', 'gastro' ) ); ?>
	<?php endif; ?>
</div><?php // close element ?>
