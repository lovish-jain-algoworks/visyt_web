<?php
/**
 * Item template file
 *
 * @package gastro/templates/items
 * @version 1.0.0
 */
?>


<?php
	$args = gastro_template_args( 'items/quote' );
	$opts = gastro_item_quote_options( $args );
?>

<blockquote <?php echo gastro_a( $opts ); ?>>
	<?php gastro_template_background( $opts ); ?>
	<?php if ( 'border' === $opts['style'] ) : ?>
		<div class="gst-quote-border gst-quote-border--top"></div>
	<?php endif; ?>

	<div class="gst-quote-inner">
		<?php if ( $opts['icon_on'] ) : ?>
			<div class="gst-quote-icon" <?php echo gastro_s( $opts['item_icon_style'] ); ?>>
				<i class="twf twf-star"></i>
				<i class="twf twf-star"></i>
				<i class="twf twf-star"></i>
				<i class="twf twf-star"></i>
				<i class="twf twf-star"></i>
			</div>
		<?php endif; ?>

		<div class="gst-quote-text gst-s-text-color gst-<?php echo esc_attr( $opts['quote_font'] ); ?>-font">
			<?php echo do_shortcode( $opts['text'] ); ?>
		</div>

		<?php if ( $opts['author_on'] && !empty( $opts['author'] ) ) : ?>
			<div class="gst-quote-author gst-s-text-color">
				<?php echo do_shortcode( $opts['author'] ); ?>
			</div>
		<?php endif; ?>
	</div><?php // close inner ?>

	<?php if ( 'border' === $opts['style'] ) : ?>
		<div class="gst-quote-border gst-quote-border--bottom"></div>
	<?php endif; ?>
</blockquote>
