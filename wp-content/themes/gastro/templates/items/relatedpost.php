<?php
/**
 * Item template file
 *
 * @package gastro/templates/items
 * @version 1.0.0
 */
?>


<?php
	$post_id = get_the_ID();
	$args = gastro_template_args( 'items/relatedpost' );
	$opts = gastro_item_relatedpost_options( $args, $post_id );
?>

<div <?php echo gastro_a( $opts ); ?>>
	<?php if ( $opts['categories'] ) : ?>
		<?php $query = new WP_Query( $opts['relatedpost_query_args'] ); ?>
		<?php if ( $opts['heading_on'] ) : ?>
			<div class="gst-relatedpost-heading">
				<?php gastro_get_title( array( 'text' => $opts['heading'] ) ); ?>
			</div>
		<?php endif; ?>

		<?php if ( $query->have_posts() ): ?>
			<?php while ( $query->have_posts() ) : $query->the_post(); ?>
				<?php $current_post = $query->current_post; ?>
				<?php $is_list_style = ( 'list' === $opts['style'] ); ?>
				<?php $is_row_first = ( !$is_list_style && 0 == $current_post % $opts['no_of_columns'] ); ?>
				<?php $is_row_last = ( !$is_list_style &&
					( $current_post % $opts['no_of_columns'] == $opts['no_of_columns'] - 1 || ( int )( $query->query['posts_per_page'] ) - 1 == $current_post ||
					( ( ( int )$query->found_posts < ( int )( $query->query['posts_per_page'] ) || -1 == ( int )$query->query['posts_per_page'] ) && ( int )$query->found_posts - 1 == $current_post )
				) ); ?>

				<?php if ( 'list' === $opts['style'] || $is_row_first ) : ?>
					<div class="gst-row">
				<?php  endif; ?>

					<div class="gst-entry <?php echo esc_attr( $opts['entry_class'] ); ?>">
						<a href="<?php the_permalink(); ?>" class="gst-entry-inner">

							<?php if ( $opts['thumbnail_on'] ): ?>
								<div class="gst-entry-header">
									<div class="gst-entry-media gst-s-text-color">
										<?php
											$image_args = array(
												'image_id' => get_post_thumbnail_id( get_the_ID() ),
												'image_ratio' => $opts['image_ratio'],
												'image_size' => $opts['image_size'],
												'image_column_width' => $opts['column_size']
											);
											echo gastro_template_media( $image_args );
										?>
									</div>
								</div>
							<?php endif; ?>
							<div class="gst-entry-content">
								<h4 class="gst-entry-title gst-s-text-color gst-secondary-font">
									<?php the_title(); ?>
								</h4>

								<?php if ( $opts['date_on'] ): ?>
									<div class="gst-entry-date">
										<?php echo get_the_date(); ?>
									</div>
								<?php endif; ?>
							</div><?php // close entry content gst-entry-content ?>
						</a><?php // close entry inner gst-entry-inner ?>
					</div><?php // close entry gst-entry ?>

				<?php if ( 'list' === $opts['style'] || $is_row_last ) : ?>
					</div><?php // close row gst-row ?>
				<?php endif; ?>

			<?php endwhile; ?>
			<?php wp_reset_postdata(); ?>
		<?php else : ?>
			<div class="gst-text gst-center-align">
				<h3><?php esc_html_e( 'No related post.', 'gastro' ); ?></h3>
			</div>
		<?php endif; ?>
	<?php endif; ?>
</div>
