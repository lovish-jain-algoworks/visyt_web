<?php
/**
 * Item template file
 *
 * @package gastro/templates/items
 * @version 1.0.0
 */
?>


<?php
	$args = gastro_template_args( 'items/reservation' );
	$opts = gastro_item_reservation_options( $args );
?>
<?php if ( function_exists( 'gastro_booking_print_form' ) ) : ?>
	<div <?php echo gastro_a( $opts ); ?>>
		<div class="gst-reservation-inner">
			<?php echo gastro_booking_print_form( array(), $opts ); ?>
		</div>
	</div>
<?php else : ?>
	<?php echo gastro_get_dummy_template( esc_html__( 'reservation', 'gastro' ), esc_html__( 'Please activate plugin Gastro-Core.', 'gastro' ) ); ?>
<?php endif; ?>
