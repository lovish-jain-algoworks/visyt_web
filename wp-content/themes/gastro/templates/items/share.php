<?php
/**
 * Item template file
 *
 * @package gastro/templates/items
 * @version 1.0.0
 */
?>


<?php
	$args = gastro_template_args( 'items/share' );
	$opts = gastro_item_share_options( $args );
?>

<div <?php echo gastro_a( $opts ); ?>>
	<?php gastro_template_share( $opts ); ?>
</div>
