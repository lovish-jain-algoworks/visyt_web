<?php
/**
 * Item template file
 *
 * @package gastro/templates/items
 * @version 1.0.0
 */
?>


<?php
	$args = gastro_template_args( 'items/skill' );
	$opts = gastro_item_skill_options( $args );
?>

<div <?php echo gastro_a( $opts ); ?>>
	<div class="gst-row">
		<?php foreach ( $opts['data'] as $index => $data ) : ?>
			<div class="gst-skill-item <?php echo esc_attr( $opts['item_class'] ); ?>" <?php echo gastro_d( $opts['item_data'][$index] ); ?>>
				<?php if ( 'bar' === $opts['style'] ) : ?>
					<?php if ( 'horizontal' === $opts['axis'] ) : ?>
						<div class="gst-skill-heading">

							<?php if ( $opts['icon_on'] ) : ?>
								<span class="gst-skill-icon gst-s-text-color twf-<?php echo esc_attr( $data['icon'] ); ?>" <?php echo gastro_s( $opts['icon_style'][$index] ); ?>></span>
							<?php endif; ?>

							<?php if ( !empty( $data['title'] ) ) : ?>
								<span class="gst-skill-title gst-s-text-color"><?php echo do_shortcode( $data['title'] ); ?></span>
							<?php endif; ?>

							<?php if ( $opts['percentage_on'] ) : ?>
								<div class="gst-skill-percentage gst-s-text-color"><?php echo esc_html( $data['percent'] . '%' ); ?></div>
							<?php endif; ?>

						</div><?php // close heading gst-skill-heading ?>
						<div class="gst-skill-bar gst-s-bg-bg" <?php echo gastro_s( $opts['bar_style'][$index] ); ?>>
							<div class="gst-skill-progress gst-p-brand-bg" <?php echo gastro_s( $opts['progress_style'][$index] ); ?>></div>
						</div>
					<?php else : ?>
						<div class="gst-skill-item-inner" <?php echo gastro_s( $opts['inner_style'] ); ?>>
							<div class="gst-skill-bar gst-p-brand-bg" <?php echo gastro_s( $opts['bar_style'][$index] ); ?>>
								<div class="gst-skill-progress gst-s-bg-bg gst-" <?php echo gastro_s( $opts['progress_style'][$index] ); ?>></div>
							</div>
							<div class="gst-skill-heading">

								<?php if ( $opts['icon_on'] ) : ?>
									<span class="gst-skill-icon gst-s-text-color twf-<?php echo esc_attr( $data['icon'] ); ?>" <?php echo gastro_s( $opts['icon_style'][$index] ); ?>></span>
								<?php endif; ?>

								<?php if ( !empty( $data['title'] ) ) : ?>
									<span class="gst-skill-title"><?php echo do_shortcode( $data['title'] ); ?></span>
								<?php endif; ?>

								<?php if ( $opts['percentage_on'] ) : ?>
									<div class="gst-skill-percentage gst-s-text-color"><?php echo esc_html( $data['percent'] . '%' ); ?></div>
								<?php endif; ?>

							</div><?php // close heading gst-skill-heading ?>
						</div><?php // close inner gst-skill-item-inner ?>
					<?php endif; ?>
				<?php else : ?>
					<div class="gst-skill-piechart" <?php echo gastro_s( $opts['piechart_style'] ); ?>>
						<div class="easyPieChart"></div>
						<div class="gst-skill-heading">
							<?php if ( $opts['icon_on'] ) : ?>
								<div class="gst-skill-icon gst-s-text-color twf-<?php echo esc_attr( $data['icon'] ); ?>" <?php echo gastro_s( $opts['icon_style'][$index] ); ?>></div>
							<?php endif; ?>
							<?php if ( $opts['percentage_on'] ) : ?>
								<div class="gst-skill-percentage gst-s-text-color"><?php echo esc_html( $data['percent'] . '%' ); ?></div>
							<?php endif; ?>
						</div><?php // close heading gst-skill-heading ?>
					</div><?php // close piechart gst-skill-piechart ?>
					<?php if ( !empty( $data['title'] ) ) : ?>
						<div class="gst-skill-title gst-s-text-color"><?php echo do_shortcode( $data['title'] ); ?></div>
					<?php endif; ?>
				<?php endif; ?>
			</div><?php // close item gst-skill-item ?>
		<?php endforeach; ?>
	</div><?php // close row gst-row ?>
</div>
