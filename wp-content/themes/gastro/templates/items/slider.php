<?php
/**
 * Item template file
 *
 * @package gastro/templates/items
 * @version 1.0.0
 */
?>


<?php
	$args = gastro_template_args( 'items/slider' );
	$opts = gastro_item_slider_options( $args );
?>

<?php if ( !$opts['is_header'] ) : ?>
	<div <?php echo gastro_a( $opts ); ?>>
<?php else : ?>
	<header <?php echo gastro_a( $opts ); ?>>
<?php endif; ?>

	<?php foreach ( $opts['data'] as $index => $data ) : ?>
		<div <?php echo gastro_a( $opts['item_attribute'][$index] ); ?>>
			<?php gastro_template_background( $data, 'gst-p-bg-bg' ); ?>
			<div class="gst-slider-inner <?php echo esc_attr( $opts['inner_class'] ); ?>">
				<div class="gst-slider-wrapper">
					<div class="gst-slider-content gst-<?php echo esc_attr( $data['vertical'] ); ?>-vertical" <?php echo gastro_s( $opts['content_style'] ); ?>>
						<div class="gst-slider-content-wrapper gst-<?php echo esc_attr( $data['alignment'] ); ?>-align" <?php echo gastro_s( $opts['content_wrapper_style'] ); ?>>

							<?php if ( $opts['image_on'][$index] ) : ?>
								<div class="gst-slider-media">
									<?php echo gastro_template_media( $data ); ?>
								</div>
							<?php endif; ?>
							<div class="gst-slider-body gst-s-text-color">

								<?php if ( $opts['topsubtitle_on'][$index] ) : ?>
									<div class="gst-slider-subtitle--top gst-<?php echo esc_attr( $opts['topsubtitle_font'] ); ?>-font">
										<?php echo do_shortcode( $data['topsubtitle'] ); ?>
									</div>
								<?php endif; ?>

								<?php if ( $opts['title_on'][$index] ) : ?>
									<div class="gst-slider-title gst-<?php echo esc_attr( $opts['title_font'] ); ?>-font" <?php echo gastro_s( $opts['title_style'] ); ?>>
										<?php echo do_shortcode( $data['title'] ); ?>
									</div>
								<?php endif; ?>

								<?php if ( $opts['subtitle_on'][$index] ) : ?>
									<div class="gst-slider-subtitle gst-<?php echo esc_attr( $opts['subtitle_font'] ); ?>-font">
										<?php echo do_shortcode( $data['subtitle'] ); ?>
									</div>
								<?php endif; ?>

								<?php if ( $opts['divider_on'][$index] ) : ?>
									<div class="gst-slider-divider">
										<div class="gst-slider-divider-inner gst-s-text-bg" <?php echo gastro_s( $opts['divider_style'][$index] ); ?>></div>
									</div>
								<?php endif; ?>

								<?php if ( $opts['firstbutton_on'][$index] || $opts['secondbutton_on'][$index] ) : ?>
									<div class="gst-slider-footer">

										<?php if ( $opts['firstbutton_on'][$index] ) : ?>
											<?php gastro_template_button( $data, 'firstbutton' ); ?>
										<?php endif; ?>

										<?php if ( $opts['secondbutton_on'][$index] ) : ?>
											<?php gastro_template_button( $data, 'secondbutton' ); ?>
										<?php endif; ?>

									</div>
								<?php endif; ?>
							</div><?php // close body gst-slider-body ?>
						</div><?php // close content wrapper gst-slider-content-wrapper ?>
					</div><?php // close content gst-slider-content ?>
				</div><?php // close wrapper gst-slider-wrapper ?>
			</div><?php // close inner gst-slider-inner ?>
		</div><?php // close item gst-slider-item ?>
	<?php endforeach; ?>

<?php if ( !$opts['is_header'] ) : ?>
	</div>
<?php else : ?>
	</header>
<?php endif; ?>
