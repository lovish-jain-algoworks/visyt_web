<?php
/**
 * Item template file
 *
 * @package gastro/templates/items
 * @version 1.0.0
 */
?>


<?php
	$args = gastro_template_args( 'items/social' );
	$opts = gastro_item_social_options( $args );
?>

<div <?php echo gastro_a( $opts ); ?>>
	<div class="gst-social-inner">
		<?php echo gastro_get_social_icon( $opts ); ?>
	</div>
</div>
