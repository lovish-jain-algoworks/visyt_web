<?php
/**
 * Item template file
 *
 * @package gastro/templates/items
 * @version 1.0.0
 */
?>


<?php
	$args = gastro_template_args( 'items/space' );
	$opts = gastro_item_space_options( $args );
?>

<div <?php echo gastro_a( $opts ); ?>></div>
