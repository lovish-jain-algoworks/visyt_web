<?php
/**
 * Item template file
 *
 * @package gastro/templates/items
 * @version 1.0.0
 */
?>


<?php
	$args = gastro_template_args( 'items/tab' );
	$opts = gastro_item_tab_options( $args );
?>

<div <?php echo gastro_a( $opts ); ?>>
	<ul <?php echo gastro_a( $opts['nav_attr'] ); ?>>
		<?php if ( !empty( $opts['data'] ) && is_array( $opts['data'] ) ) : ?>
			<?php foreach ( $opts['data'] as $index => $data ) : ?>
				<?php
					$nav_list_class = $opts['nav_list_class'];
					if ( 0 == $index ) {
						$nav_list_class .= ' active';
					}
				?>
				<li class="<?php echo esc_attr( $nav_list_class ); ?>" data-index="<?php echo esc_attr( $index + 1 ); ?>">
					<?php if ( $opts['media_on'] && 'right' !== $opts['tab_position'] ): ?>
						<span class="gst-tab-nav-media">
							<?php echo gastro_template_media( $data ); ?>
						</span>
					<?php endif; ?>
					<a class="gst-tab-nav-title" href="#">
						<span class="gst-tab-nav-line <?php echo esc_attr( $opts['line_class'] ); ?>"></span>
						<span class="gst-tab-nav-title-text"><?php echo do_shortcode( $data['label'] ); ?></span>
					</a>
					<?php if ( $opts['media_on'] && 'right' === $opts['tab_position'] ): ?>
						<span class="gst-tab-nav-media">
							<?php echo gastro_template_media( $data ); ?>
						</span>
					<?php endif; ?>
				</li>
			<?php endforeach; ?>
		<?php endif; ?>
	</ul>
	<div class="<?php echo esc_attr( $opts['body_class'] ); ?>" <?php echo gastro_s( $opts['body_style'] ); ?>>
		<?php gastro_template_background( $opts ); ?>
		<div class="gst-tab-wrapper">
			<?php if ( !empty( $opts['spaces'] ) && is_array( $opts['spaces'] ) ) : ?>
				<?php foreach ( $opts['spaces'] as $i => $space ) : ?>
					<?php
						$content_class = $opts['content_class'];
						if ( 0 == $i ) {
							$content_class .= ' active';
						}
					?>
					<div data-index="<?php echo esc_attr( $i + 1 ); ?>" class="<?php echo esc_attr( $content_class ); ?>">
						<div class="gst-tab-pane">
							<?php if ( !empty( $space['items'] ) && is_array( $space['items'] ) ) : ?>
								<?php foreach ( $space['items'] as $item ) : ?>
									<?php gastro_template_item( $item ); ?>
								<?php endforeach; ?>
							<?php endif; ?>
						</div>
					</div>
				<?php endforeach; ?>
			<?php endif; ?>
		</div>
	</div>
</div>
