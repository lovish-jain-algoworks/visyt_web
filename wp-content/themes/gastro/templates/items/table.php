<?php
/**
 * Item template file
 *
 * @package gastro/templates/items
 * @version 1.0.0
 */
?>


<?php
	$args = gastro_template_args( 'items/table' );
	$opts = gastro_item_table_options( $args );
?>


<div <?php echo gastro_a( $opts ); ?>>
	<table>

		<?php if ( $opts['header_row'] ) : ?>
			<thead class="<?php echo esc_attr( implode( ' ', $opts['header_class'] ) ); ?>" <?php echo gastro_s( $opts['header_style'] ); ?>>
				<tr>

					<?php foreach ( $opts['data_header'] as $i => $data ) : ?>
						<th <?php echo gastro_s( $opts['column_style'][$i] ); ?>><?php echo do_shortcode( $data ); ?></th>
					<?php endforeach; ?>

				</tr>
			</thead>
		<?php endif; ?>

		<tbody class="gst-p-brand-border" <?php echo gastro_s( $opts['body_style'] ); ?>>

			<?php foreach ( $opts['formatted'] as $index => $row ) : ?>
				<tr class="gst-table-body-row <?php echo esc_attr( $opts['tr_class'][$index] ); ?>" <?php echo gastro_s( $opts['tr_style'][$index] ); ?>>

					<?php foreach ( $row as $i => $data ): ?>
						<td <?php echo gastro_s( $opts['column_style'][$i] ); ?>><?php echo do_shortcode( $data ); ?></td>
					<?php endforeach; ?>

				</tr>
			<?php endforeach; ?>

		</tbody>
	</table>
</div>
