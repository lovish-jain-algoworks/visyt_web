<?php
/**
 * Item template file
 *
 * @package gastro/templates/items
 * @version 1.0.0
 */
?>


<?php
	$args = gastro_template_args( 'items/team' );
	$opts = gastro_item_team_options( $args );
?>

<div <?php echo gastro_a( $opts ); ?>>
	<div class="gst-team-inner">
		<div class="gst-team-media">
			<?php echo gastro_template_media( $opts ); ?>
		</div>
		<div class="gst-team-body <?php echo esc_attr( implode( ' ', $opts['body_class'] ) ); ?>" <?php echo gastro_s( $opts['body_style'] ); ?>>

			<?php if ( 'hover' === $opts['style'] ) : ?>
				<?php if ( 'flip' === $opts['mouseover_effect'] ) : ?>
					<?php echo gastro_template_media( $opts ); ?>
				<?php endif; ?>
				<?php if ( !empty( $opts['image_link'] ) ) : ?>
					<?php $link_target = $opts['media_target_self'] ? '_blank' : '_self'; ?>
					<?php $link_target = ( '#' !== substr( $opts['image_link'], 0 , 1 ) ) ? $link_target : '_self'; ?>
					<a class="gst-team-background gst-s-bg-bg gst-p-border-border" href="<?php echo gastro_escape_url( $opts['image_link'] ); ?>" target="<?php echo esc_attr( $link_target ); ?>" <?php echo gastro_s( $opts['hover_style'] ); ?>></a>
				<?php else : ?>
					<div class="gst-team-background gst-s-bg-bg gst-p-border-border" <?php echo gastro_s( $opts['hover_style'] ); ?>></div>
				<?php endif; ?>
			<?php endif; ?>

			<div class="gst-team-body-wrapper">
				<div class="gst-team-body-inner">
					<div class="gst-team-body-content" <?php echo gastro_s( $opts['content_style'] ); ?>>
						<div class="gst-team-header">
							<div class="gst-team-name gst-s-text-color gst-<?php echo esc_attr( $opts['name_font'] ); ?>-font">
								<?php echo do_shortcode( $opts['name'] ); ?>
							</div>
							<div class="gst-team-title gst-<?php echo esc_attr( $opts['title_font'] ); ?>-font">
								<?php echo do_shortcode( $opts['title'] ); ?>
							</div>
						</div>

						<?php if ( $opts['description_on'] ) : ?>
							<div class="gst-team-description">
								<?php echo do_shortcode( $opts['description'] ); ?>
							</div>
						<?php endif; ?>

						<?php if ( $opts['social_on'] && !empty( $opts['socials'] ) ) : ?>
							<div class="gst-team-socials">
								<div class="gst-team-socials-inner">
									<div class="gst-social gst-social--<?php echo esc_attr( $opts['social_style'] ); ?>">
										<div class="gst-social-inner">
											<?php echo gastro_get_social_icon( $opts['social_args'] ); ?>
										</div>
									</div>
								</div>
							</div>
						<?php endif; ?>

					</div><?php // close body content gst-team-body-content ?>
				</div><?php // close body inner gst-team-body-inner ?>
			</div><?php // close body wrapper gst-team-body-wrapper ?>
		</div><?php // close body gst-team-body ?>
	</div><?php // close inner gst-team-inner ?>
</div>
