<?php
/**
 * Item template file
 *
 * @package gastro/templates/items
 * @version 1.0.0
 */
?>


<?php
	$args = gastro_template_args( 'items/testimonial' );
	$opts = gastro_item_testimonial_options( $args );
?>

<div <?php echo gastro_a( $opts ); ?>>

	<?php for ( $i = 0; $i < $opts['no_of_data']; $i++ ) : ?>
		<div class="gst-testimonial-item gst-s-text-color gst-p-bg-bg gst-p-border-border" <?php echo gastro_s( $opts['item_style'] ); ?>>
			<div class="gst-testimonial-item-inner" <?php echo gastro_s( $opts['inner_style'] ); ?>>
				<?php if ( 'top' === $opts['icon_position'] ) : ?>
					<?php if ( $opts['icon_on'] ) : ?>
						<div class="gst-testimonial-icon" <?php echo gastro_s( $opts['item_icon_style'] ); ?>>
							<i class="twf twf-star"></i>
							<i class="twf twf-star"></i>
							<i class="twf twf-star"></i>
							<i class="twf twf-star"></i>
							<i class="twf twf-star"></i>
						</div>
					<?php endif; ?>
					<blockquote class="gst-<?php echo esc_attr( $opts['text_font'] ); ?>-font">
						<?php echo do_shortcode( $opts['data'][$i]['quote'] ); ?>
					</blockquote>
				<?php endif; ?>

				<div class="gst-testimonial-author">
					<?php if ( $opts['avatar_on'] ) : ?>
						<div class="gst-testimonial-author-media"><?php echo gastro_template_media( $opts['data'][$i] ); ?></div>
					<?php endif; ?>

					<?php if ( $opts['author_on'] || $opts['title_on'] ) : ?>
						<div class="gst-testimonial-author-body">

							<?php if ( $opts['author_on'] ) : ?>
								<div class="gst-testimonial-author-name gst-<?php echo esc_attr( $opts['author_font'] ); ?>-font">
									<?php echo do_shortcode( $opts['data'][$i]['author'] ); ?>
								</div>
							<?php endif; ?>

							<?php if ( $opts['title_on'] ) : ?>
								<div class="gst-testimonial-author-title gst-p-text-color gst-<?php echo esc_attr( $opts['title_font'] ); ?>-font">
									<?php echo do_shortcode( $opts['data'][$i]['title'] ); ?>
								</div>
							<?php endif; ?>
						</div>
					<?php endif; ?>
				</div><?php // close author gst-testimonial-author ?>

				<?php if ( 'bottom' === $opts['icon_position'] ) : ?>
					<blockquote class="gst-<?php echo esc_attr( $opts['text_font'] ); ?>-font">
						<?php echo do_shortcode( $opts['data'][$i]['quote'] ); ?>
					</blockquote>
					<?php if ( $opts['icon_on'] ) : ?>
						<div class="gst-testimonial-icon" <?php echo gastro_s( $opts['item_icon_style'] ); ?>>
							<i class="twf twf-star"></i>
							<i class="twf twf-star"></i>
							<i class="twf twf-star"></i>
							<i class="twf twf-star"></i>
							<i class="twf twf-star"></i>
						</div>
					<?php endif; ?>
				<?php endif; ?>

			</div><?php // close item inner gst-testimonial-item-inner ?>
		</div><?php // close item gst-testimonial-item ?>
	<?php endfor; ?>

</div>
