<?php
/**
 * Item template file
 *
 * @package gastro/templates/items
 * @version 1.0.0
 */
?>


<?php
	$args = gastro_template_args( 'items/video' );
	$opts = gastro_item_video_options( $args );
?>

<div <?php echo gastro_a( $opts ); ?>>
	<?php if ( ( 'self-hosted' === $opts['video_type'] && !empty( $opts['video_url'] ) ) || ( 'external' === $opts['video_type'] && !empty( $opts['external_url'] ) ) ) : ?>
		<div class="gst-video-inner" <?php echo gastro_s( $opts['inner_style'] ); ?>>
			<div class="gst-video-content" <?php echo gastro_s( $opts['content_style'] ); ?>>
				<?php echo gastro_template_video( $opts ); ?>
			</div>
		</div>
	<?php else : ?>
		<?php echo gastro_get_dummy_template( esc_html__( 'video', 'gastro' ), esc_html__( 'Video is now empty, please upload video.', 'gastro' ) ); ?>
	<?php endif; ?>
</div>
