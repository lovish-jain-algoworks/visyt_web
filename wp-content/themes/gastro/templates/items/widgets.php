<?php
/**
 * Item template file
 *
 * @package gastro/templates/items
 * @version 1.0.0
 */
?>


<?php
	$args = gastro_template_args( 'items/widgets' );
	$opts = gastro_template_widgets_options( $args );
?>

<div <?php echo gastro_a( $opts ); ?>>
	<?php if ( !empty( $opts['sidebar_id'] ) ) : ?>
		<ul class="gst-widgets-list">
			<?php dynamic_sidebar( $opts['sidebar_id'] ); ?>
		</ul>
	<?php else : ?>
		<?php echo gastro_get_dummy_template( esc_html__( 'widgets', 'gastro' ), esc_html__( 'Please choose a sidebar.', 'gastro' ) ); ?>
	<?php endif; ?>
</div>
