<?php
/**
 * The template file
 *
 * @package gastro/templates
 * @version 1.0.0
 */
?>


<?php
	$args = gastro_template_args( 'navbar-mobile' );
	$opts = gastro_navbar_mobile_options( $args );
	$menu = gastro_template_nav_menu( $opts['nav_menu_args'], true );
?>

<nav <?php echo gastro_a( $opts['navbar_mobile_attr'] ); ?>>
	<div class="gst-navbar-inner gst-<?php echo esc_attr( $opts['navbar_color_scheme'] ); ?>-scheme" <?php echo gastro_s( $opts['navbar_inner_style'] ); ?>>
		<div class="<?php echo esc_attr( $opts['container_class'] ); ?>">
			<div class="gst-navbar-wrapper">
				<?php if ( $opts['logo_on'] ) : ?>
					<div class="gst-navbar-header">
						<?php gastro_template_partial( 'navbar-logo', $opts ); ?>
					</div>
				<?php endif; ?>
				<div class="gst-navbar-content">
					<div class="gst-navbar-content-inner">
						<?php if ( !empty( $menu ) ) : ?>
							<div class="gst-navbar-body">
								<div class="gst-navbar-body-inner">
									<a class="gst-collapsed-button gst-collapsed-button--<?php echo esc_attr( $opts['mobile_navbar_style'] ); ?>" href="#" data-target=".gst-collapsed-menu">
										<span class="gst-lines"></span>
									</a>
									<?php if ( 'full' === $opts['mobile_navbar_style'] ) : ?>
										<div class="gst-collapsed-menu gst-collapsed-menu--<?php echo esc_attr( $opts['mobile_navbar_style'] ); ?> gst-p-bg-bg" data-style="<?php echo esc_attr( $opts['mobile_navbar_style'] ); ?>">
											<div class="gst-collapsed-menu-inner">
												<div class="gst-collapsed-menu-wrapper">
													<div class="gst-collapsed-menu-content">
														<?php echo gastro_escape_content( $menu ); ?>
													</div>
												</div>
											</div>
										</div>
									<?php endif; ?>
								</div><?php // Close navbar body inner ?>
							</div><?php // Close navbar body  ?>
						<?php endif; ?>
						<?php if ( $opts['header_action_button'] && 'headerwidget' !== $opts['action_type'] && $opts['action_mobile_display'] ) : ?>
							<div class="gst-navbar-footer">
								<?php if ( 'social' !== $opts['action_type'] ) : ?>
									<?php gastro_template_button( $opts['action_button_args'] ); ?>
								<?php else : ?>
									<div class="gst-navbar-social">
										<?php echo gastro_get_social_icon( array( 'components' => $opts['action_social_component'] ) ); ?>
									</div>
								<?php endif; ?>
							</div>
						<?php endif; ?>
					</div><?php // Close navbar content inner ?>
				</div><?php // Close navbar content ?>
			</div><?php // Close navbar wrapper ?>
		</div><?php // Close navbar container ?>
		<?php if ( !empty( $menu ) && 'classic' === $opts['mobile_navbar_style'] ) : ?>
			<div class="gst-collapsed-menu gst-collapsed-menu--<?php echo esc_attr( $opts['mobile_navbar_style'] ); ?> gst-p-bg-bg" data-style="<?php echo esc_attr( $opts['mobile_navbar_style'] ); ?>">
				<div class="gst-collapsed-menu-inner">
					<div class="gst-collapsed-menu-wrapper">
						<div class="gst-collapsed-menu-content gst-container">
							<?php echo gastro_escape_content( $menu ); ?>
						</div>
					</div>
				</div>
			</div>
		<?php endif; ?>
	</div><?php // Close navbar inner ?>
</nav>
