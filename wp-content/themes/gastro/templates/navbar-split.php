<?php
/**
 * The template file
 *
 * @package gastro/templates
 * @version 1.0.0
 */
?>


<?php
	$args = gastro_template_args( 'navbar-split' );
	$opts = gastro_navbar_options( $args );
	$menu = gastro_template_nav_menu( $opts['nav_menu_args'] );
?>

<nav <?php echo gastro_a( $opts['navbar_attribute'] ); ?>>
	<div class="gst-navbar-inner gst-<?php echo esc_attr( $opts['navbar_color_scheme'] ); ?>-scheme" <?php echo gastro_s( $opts['navbar_inner_style'] ); ?>>
		<div class="<?php echo esc_attr( $opts['container_class'] ); ?>">
			<div class="gst-navbar-wrapper">
				<div class="gst-navbar-content">
					<div class="gst-navbar-content-inner">
						<?php if ( $opts['header_action_button'] ) : ?>
							<div class="gst-navbar-footer">
								<?php if ( 'social' !== $opts['action_type'] ) : ?>
									<?php gastro_template_button( $opts['action_button_args'] ); ?>
								<?php else : ?>
									<div class="gst-navbar-social">
										<?php echo gastro_get_social_icon( array( 'components' => $opts['action_social_component'] ) ); ?>
									</div>
								<?php endif; ?>
								<?php if ( is_active_sidebar( 'gastro-navbar-split' ) ) : ?>
									<div class="gst-widgets">
										<ul class="gst-widgets-list">
											<?php dynamic_sidebar( 'gastro-navbar-split' ); ?>
										</ul>
									</div>
								<?php endif; ?>
							</div>
						<?php endif; ?>

						<?php if ( !empty( $menu ) ) : ?>
							<div class="gst-navbar-body">
								<div class="gst-navbar-body-inner">
									<a class="gst-collapsed-button gst-collapsed-button--<?php echo esc_attr( $opts['minimal_navbar_menu_style'] ); ?>" href="#" data-target=".gst-collapsed-menu">
										<span class="gst-lines"></span>
									</a>
									<?php if ( ( $opts['search_on'] || $opts['cart_on'] ) && 'split' === $opts['navbar_style'] ) : ?>
										<ul class="gst-extra-menu">
											<?php if ( $opts['search_on'] ) : ?>
												<?php echo gastro_escape_content( gastro_template_menu_search() ); ?>
											<?php endif; ?>
											<?php if ( $opts['cart_on'] ) : ?>
												<?php echo gastro_escape_content( gastro_template_menu_cart() ); ?>
											<?php endif; ?>
										</ul>
									<?php endif; ?>
									<?php if ( 'offcanvas' !== $opts['minimal_navbar_menu_style'] ) : ?>
										<div class="<?php echo esc_attr( implode( ' ', $opts['collapsed_classes'] ) ); ?>">
											<div class="gst-collapsed-menu-inner">
												<div class="gst-collapsed-menu-wrapper">
													<div class="gst-collapsed-menu-content">
														<?php echo gastro_escape_content( $menu ); ?>
													</div>
												</div>
												<?php if ( 'full' === $opts['minimal_navbar_menu_style'] && is_active_sidebar( 'gastro-navbar-full' ) ) : ?>
													<div class="gst-widgets">
														<ul class="gst-widgets-list">
															<?php dynamic_sidebar( 'gastro-navbar-full' ); ?>
														</ul>
													</div>
												<?php endif ;?>
											</div>
										</div>
									<?php endif ;?>
								</div><?php // Close navbar body inner ?>
							</div><?php // Close navbar body ?>
						<?php endif; ?>
					</div><?php // Close navbar content inner ?>
				</div><?php // Close navbar content ?>
				<?php if ( $opts['logo_on'] ) : ?>
					<div class="gst-navbar-header">
						<?php gastro_template_partial( 'navbar-logo', $opts ); ?>
					</div>
				<?php endif; ?>
			</div><?php // Close navbar wrapper ?>
		</div><?php // Close navbar container ?>
	</div><?php // Close navbar inner ?>
	<?php if ( $opts['header_action_button'] && 'headerwidget' === $opts['action_type'] ) : ?>
		<?php gastro_template_partial( 'navbar-header-widgets', $opts ); ?>
	<?php endif ;?>
</nav>

<?php if ( 'offcanvas' === $opts['minimal_navbar_menu_style'] ) : ?>
	<div class="gst-offcanvas-overlay"></div>
	<div class="<?php echo esc_attr( implode( ' ', $opts['collapsed_classes'] ) ); ?>" data-style="<?php echo esc_attr( $opts['minimal_navbar_menu_style'] ); ?>">
		<div class="gst-collapsed-menu-inner gst-p-bg-bg">
			<div class="gst-collapsed-menu-wrapper">
				<div class="gst-collapsed-menu-content">
					<?php echo gastro_escape_content( $menu ); ?>
				</div>
				<?php if ( is_active_sidebar( 'gastro-navbar-offcanvas' ) ) : ?>
					<div class="gst-widgets">
						<ul class="gst-widgets-list">
							<?php dynamic_sidebar( 'gastro-navbar-offcanvas' ); ?>
						</ul>
					</div>
				<?php endif ;?>
			</div>
		</div>
	</div>
<?php endif ;?>
