<?php
/**
 * The template file
 *
 * @package gastro/templates
 * @version 1.0.0
 */
?>


<?php
	$args = gastro_template_args( 'navbar-stacked' );
	$opts = gastro_navbar_options( $args );
	$menu = gastro_template_nav_menu( $opts['nav_menu_args'] );
?>

<nav <?php echo gastro_a( $opts['navbar_attribute'] ); ?>>
	<div class="gst-navbar-inner gst-<?php echo esc_attr( $opts['navbar_color_scheme'] ); ?>-scheme" <?php echo gastro_s( $opts['navbar_inner_style'] ); ?>>
		<div class="gst-navbar-wrapper">
			<?php if ( $opts['logo_on'] ) : ?>
				<div class="gst-navbar-header">
					<div class="<?php echo esc_attr( $opts['container_class'] ); ?>">
						<?php if ( ( 'right' === $opts['navbar_menu_position'] || 'center' === $opts['navbar_menu_position'] ) && is_active_sidebar( 'gastro-navbar-stacked-left' ) ) : ?>
							<div class="gst-widgets navbar-widgets left">
								<ul class="gst-widgets-list">
									<?php dynamic_sidebar( 'gastro-navbar-stacked-left' ); ?>
								</ul>
							</div>
						<?php endif ;?>
						<?php gastro_template_partial( 'navbar-logo', $opts ); ?>
						<?php if ( ( 'left' === $opts['navbar_menu_position'] || 'center' === $opts['navbar_menu_position'] ) && is_active_sidebar( 'gastro-navbar-stacked-right' ) ) : ?>
							<div class="gst-widgets navbar-widgets right">
								<ul class="gst-widgets-list">
									<?php dynamic_sidebar( 'gastro-navbar-stacked-right' ); ?>
								</ul>
							</div>
						<?php endif ;?>
					</div>
				</div>
			<?php endif ;?>
			<div class="gst-navbar-content">
				<div class="<?php echo esc_attr( $opts['container_class'] ); ?>">
					<div class="gst-navbar-content-inner">
						<?php if ( $opts['header_action_button'] ) : ?>
							<div class="gst-navbar-footer">
								<?php if ( 'social' !== $opts['action_type'] ) : ?>
									<?php gastro_template_button( $opts['action_button_args'] ); ?>
								<?php else : ?>
									<div class="gst-navbar-social">
										<?php echo gastro_get_social_icon( array( 'components' => $opts['action_social_component'] ) ); ?>
									</div>
								<?php endif; ?>
							</div>
						<?php endif; ?>
						<?php if ( !empty( $menu ) ) : ?>
							<div class="gst-navbar-body">
								<div class="gst-navbar-body-inner">
									<?php echo gastro_escape_content( $menu ); ?>
								</div>
							</div>
						<?php endif; ?>
					</div><?php // Close content inner ?>
				</div><?php // Close container ?>
			</div><?php // Close navbar content ?>
		</div><?php // Close navbar wrapper ?>
	</div><?php // Close navbar inner ?>
	<?php if ( $opts['header_action_button'] && 'headerwidget' === $opts['action_type'] ) : ?>
		<?php gastro_template_partial( 'navbar-header-widgets', $opts ); ?>
	<?php endif ;?>
</nav>
