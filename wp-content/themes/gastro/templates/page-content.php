<?php
/**
 * The template file
 *
 * @package gastro/templates
 * @version 1.0.0
 */
?>


<?php
	$post_id = gastro_get_page_id();
	$opts = gastro_page_content_options( $post_id );
?>

<?php if ( $opts['blueprint_active'] && !post_password_required() ) : ?>
	<?php gastro_template( 'blueprint-content.php', $opts ); ?>
<?php else: ?>
	<main <?php echo gastro_a( $opts ); ?>>
		<article id="post-<?php echo esc_attr( $post_id ); ?>" <?php post_class( 'gst-content-wrapper' ); ?>>
			<?php gastro_template_blueprint_page_title( $opts['builder'] ); ?>
			<div id="main" class="gst-main gst-main--single blueprint-inactive">
				<div class="gst-main-wrapper">
					<div class="gst-container">
						<?php the_content(); ?>
						<?php wp_link_pages(); ?>
					</div>
				</div>
				<?php if ( comments_open() || get_comments_number() ): ?>
					<div class="gst-container">
						<div class="gst-row">
							<div class="gst-col-12">
								<div id="comments" class="gst-comment">
									<?php comments_template(); ?>
								</div>
							</div>
						</div>
					</div>
				<?php endif; ?>
			</div>
		</article>
	</main>
<?php endif; ?>
