<?php
/**
 * The template file
 *
 * @package gastro/templates
 * @version 1.0.0
 */
?>


<?php
	$args = gastro_template_args( 'page-hero' );
	$opts = gastro_page_hero_options( $args );
?>

<header <?php echo gastro_a( $opts ); ?>>
	<?php gastro_template_background( $opts, 'gst-p-bg-bg' ); ?>
	<div class="gst-page-hero-inner <?php echo esc_attr( $opts['container_class'] ); ?>">
		<div class="gst-page-hero-wrapper gst-<?php echo esc_attr( $opts['horizontal'] ); ?>-align">
			<div class="gst-page-hero-content gst-<?php echo esc_attr( $opts['vertical'] ); ?>-vertical" <?php echo gastro_s( $opts['content_style'] ); ?>>
				<div class="gst-page-hero-content-wrapper gst-<?php echo esc_attr( $opts['alignment'] ); ?>-align" <?php echo gastro_s( $opts['content_wrapper_style'] ); ?>>

					<?php if ( $opts['image_on'] ): ?>
						<div class="gst-page-hero-media">
							<?php echo gastro_template_media( $opts ); ?>
						</div>
					<?php endif; ?>

					<?php if ( $opts['body_on'] ) : ?>
						<div class="gst-page-hero-body">

							<?php if ( $opts['topsubtitle_on'] ): ?>
								<div class="gst-page-hero-subtitle gst-page-hero-subtitle--top gst-<?php echo esc_attr( $opts['topsubtitle_font'] ); ?>-font">
									<?php echo do_shortcode( $opts['topsubtitle'] ); ?>
								</div>
							<?php endif; ?>

							<?php if ( $opts['title_on'] || $opts['bannertext_on'] ) : ?>
								<div class="gst-page-hero-body-title">
									<?php if ( $opts['title_on'] ) : ?>
										<h1 class="gst-page-hero-title gst-s-text-color gst-<?php echo esc_attr( $opts['title_font'] ); ?>-font" <?php echo gastro_s( $opts['title_style'] ); ?> itemprop="headline">
											<?php echo do_shortcode( $opts['title'] ); ?>
										</h1>
									<?php endif; ?>
									<?php if ( $opts['bannertext_on'] ) : ?>
										<h1 <?php echo gastro_a( $opts['bannertext_attr'] ); ?>>
											<span class="<?php echo esc_attr( implode( ' ', $opts['bannertext_dynamic_class'] ) ); ?>" <?php echo gastro_s( $opts['bannertext_dynamic_style'] ); ?>>
												<span class="gst-bannertext-dynamic-inner"></span>
											</span>
										</h1>
									<?php endif; ?>
								</div>
							<?php endif; ?>

							<?php if ( $opts['subtitle_on'] ): ?>
								<div class="gst-page-hero-subtitle gst-page-hero-subtitle--bottom gst-<?php echo esc_attr( $opts['subtitle_font'] ); ?>-font" itemprop="description">
									<?php echo do_shortcode( $opts['subtitle'] ); ?>
								</div>
							<?php endif; ?>

							<?php if ( $opts['divider_on'] ): ?>
								<div class="gst-page-hero-divider"><div class="gst-page-hero-divider-inner gst-s-text-bg" <?php echo gastro_s( $opts['divider_style'] ); ?>></div></div>
							<?php endif; ?>

							<?php if ( $opts['firstbutton_on'] || $opts['secondbutton_on'] ) : ?>
								<div class="gst-page-hero-buttons">

									<?php if ( $opts['firstbutton_on'] ) : ?>
										<?php gastro_template_button( $opts, 'firstbutton' ); ?>
									<?php endif; ?>

									<?php if ( $opts['secondbutton_on'] ) : ?>
										<?php gastro_template_button( $opts, 'secondbutton' ); ?>
									<?php endif; ?>
								</div>
							<?php endif; ?>
						</div><?php // close body ?>
					<?php endif; ?>
				</div><?php // close content wrapper ?>
			</div><?php // close content ?>
		</div><?php // close wrapper ?>
	</div><?php // close inner ?>
</header>
