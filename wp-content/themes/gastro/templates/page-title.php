<?php
/**
 * The template file
 *
 * @package gastro/templates
 * @version 1.0.0
 */
?>


<?php
	$args = gastro_template_args( 'page-title' );
	$opts = gastro_page_title_options( $args );
?>

<?php if ( $opts['breadcrumb_on'] || $opts['title_on'] ) : ?>
	<?php $page_title = isset( $opts['title_content'] ) ? $opts['title_content'] : get_the_title( gastro_get_page_id() ); ?>
	<header <?php echo gastro_a( $opts ); ?>>
		<?php gastro_template_background( $opts, 'gst-s-bg-bg' ); ?>

		<?php if ( !is_front_page() && $opts['breadcrumb_on'] && 'top' === $opts['breadcrumb_position'] ): ?>
			<div class="<?php echo esc_attr( $opts['breadcrumb_class'] ); ?>" <?php echo gastro_s( $opts['breadcrumb_style'] ); ?>>
				<div class="<?php echo esc_attr( $opts['container_class'] ); ?>">
					<?php get_template_part( 'templates/breadcrumb' ); ?>
				</div>
			</div>
		<?php endif; ?>

		<?php if ( $opts['title_on'] ) : ?>
			<div class="<?php echo esc_attr( $opts['container_class'] ); ?>">
				<div class="gst-page-title-wrapper">

					<?php if ( !is_front_page() && $opts['breadcrumb_on'] && 'inline' === $opts['breadcrumb_position'] ): ?>
						<div class="<?php echo esc_attr( $opts['breadcrumb_class'] ); ?>" <?php echo gastro_s( $opts['breadcrumb_style'] ); ?>>
							<?php get_template_part('templates/breadcrumb'); ?>
						</div>
					<?php endif; ?>
					<div class="gst-page-title-content gst-s-text-color" <?php echo gastro_s( $opts['title_style'] ); ?>>
						<?php if ( !empty( $page_title ) ): ?>
							<h1 class="gst-<?php echo esc_attr( $opts['title_font'] ); ?>-font" itemprop="headline">
								<?php echo do_shortcode( $page_title ); ?>
							</h1>
						<?php endif; ?>

						<?php if ( $opts['subtitle_on'] && !empty( $opts['subtitle'] ) ): ?>
							<div class="gst-page-title-subtitle gst-<?php echo esc_attr( $opts['subtitle_font'] ); ?>-font" itemprop="description">
								<?php echo do_shortcode( $opts['subtitle'] ); ?>
							</div>
						<?php endif; ?>
					</div>
				</div>
			</div>
		<?php endif; ?>

		<?php if ( !is_front_page() && $opts['breadcrumb_on'] && 'bottom' === $opts['breadcrumb_position'] ): ?>
			<div class="<?php echo esc_attr( $opts['breadcrumb_class'] ); ?>" <?php echo gastro_s( $opts['breadcrumb_style'] ); ?>>
				<div class="<?php echo esc_attr( $opts['container_class'] ); ?>">
					<?php get_template_part( 'templates/breadcrumb' ); ?>
				</div>
			</div>
		<?php endif; ?>

	</header>
<?php endif; ?>
