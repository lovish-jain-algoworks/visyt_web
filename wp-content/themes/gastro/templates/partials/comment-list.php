<?php
/**
 * Partial template file
 *
 * @package gastro/templates/partials
 * @version 1.0.0
 */
?>

<?php $opts = gastro_template_args( 'comment-list' ); ?>

<li <?php echo comment_class( null, null, null, false ); ?> id="comment-<?php echo get_comment_ID(); ?>" itemprop="review" itemscope="itemscope" itemtype="http://schema.org/Review">
	<div class="comment-article gst-p-border-border">
		<?php if ( 'pingback' === $opts['comment']->comment_type || 'trackback' === $opts['comment']->comment_type ) : ?>
			<div class="comment-body">
				<div class="gst-s-text-color">
					<?php esc_html_e( 'Pingback : ', 'gastro' ); ?>
					<?php echo get_comment_author_link(); ?>
				</div>
				<div class="comment-content">
					<?php echo get_comment_text(); ?>
				</div>
			</div>
		<?php else : ?>
			<?php do_action( 'before_each_comment_body', $opts['comment'] ); ?>
			<div class="comment-body">
				<?php if ( '0' == $opts['comment']->comment_approved ) : ?>
					<p class="comment-await-moderation">
						<?php esc_html_e( 'Your comment is awaiting moderation.', 'gastro' ); ?>
					</p>
				<?php endif; ?>
				<div class="comment-content" itemprop="description">
					<?php echo get_comment_text(); ?>
				</div>
			</div><?php // close comment body ?>
			<?php if ( $opts['comment_open'] || !empty( $opts['edit_comment_link'] ) ) : ?>
			<div class="comment-footer">
				<div class="comment-avatar">
					<?php echo get_avatar( $opts['comment'], 60 ); ?>
				</div>
				<div class="comment-meta">
					<div itemprop="author" class="comment-author gst-secondary-font gst-s-text-color">
						<div class="comment-author-link gst-p-brand-border"><?php echo get_comment_author_link(); ?></div>
					</div>
					<div class="comment-time">
						<time itemprop="datePublished" datetime="<?php echo get_comment_time( 'c' ); ?>">
							<?php echo get_comment_date() . ' ' . esc_html__( 'at', 'gastro' ) . ' ' . get_comment_time(); ?>
						</time>
					</div>
				</div>
				<div class="comment-footer-link gst-p-brand-color">
					<?php $reply_link = get_comment_reply_link( $opts['comment_reply_args'] ); ?>
					<?php if ( $opts['comment_open'] && !empty( $reply_link ) ) : ?>
						<div class="comment-reply"><?php echo gastro_escape_content( $reply_link ); ?></div>
					<?php endif; ?>
					<?php if ( !empty( $opts['edit_comment_link'] ) ) : ?>
						<p class="edit-link">
							<?php if ( $opts['comment_open'] && !empty( $reply_link ) ) { echo '&nbsp;/ '; } ?>
							<a href="<?php echo esc_url( $opts['edit_comment_link'] ); ?>">
								<?php esc_html_e( 'Edit', 'gastro' ); ?>
							</a>
						</p>
					<?php endif; ?>
				</div>
			</div><?php // close comment footer ?>
			<?php endif; ?>
		<?php endif; ?>
		<?php do_action( 'after_each_comment_body', $opts['comment'] ); ?>
	</div>
