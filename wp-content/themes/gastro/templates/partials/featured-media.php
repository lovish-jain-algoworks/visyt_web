<?php
/**
 * Partial template file
 *
 * @package gastro/templates/partials
 * @version 1.0.0
 */
?>

<?php $opts = gastro_template_args( 'featured-media' ); ?>

<?php if ( 'video' == $opts['post_format'] && ( $opts['video'] || !empty( $opts['video-external'] ) ) ) : ?>
	<?php gastro_template_item( array(
		'type' => 'video',
		'video_type' => ( $opts['video'] ) ? 'self-hosted' : 'external',
		'video_url' => wp_get_attachment_url( $opts['video'] ),
		'external_url' => $opts['video-external'],
		'width' => 2560,
		'itemprop' => 'video'
	) ); ?>
<?php elseif ( 'audio' == $opts['post_format'] ) : ?>
	<?php if ( !empty( $opts['audio'] ) ) : ?>
		<?php if ( !empty( $opts['image_id'] ) || !empty( $opts['image_url'] ) ) : ?>
			<div class="gst-audio with-background" itemprop="audio">
				<div class="gst-audio-player pause" <?php echo gastro_s( array( 'background' => gastro_get_background_image( array( 'id' => $opts['image_id'], 'url' => $opts['image_url'] ) ) ) ); ?>>
					<div class="gst-audio-button twf twf-play twf-2x"></div>
				</div>
		<?php else : ?>
			<div class="gst-audio" itemprop="audio">
		<?php endif; ?>
			<?php echo do_shortcode( '[audio src="' . wp_get_attachment_url( $opts['audio'] ) . '"]' ); ?>
		</div><?php // close audio ?>
	<?php elseif ( !empty( $opts['audio-external'] ) ) : ?>
		<?php echo gastro_escape_content( $opts['audio-external'] ); ?>
	<?php endif; ?>
<?php elseif ( 'gallery' == $opts['post_format'] && !empty( $opts['gallery'] ) ) : ?>
	<?php gastro_template_item( array(
		'type' => 'gallery',
		'style' => 'carousel',
		'no_of_columns' => 1,
		'image_size' => $opts['image_size'],
		'image_ratio' => $opts['image_ratio'],
		'image_lazy_load' => $opts['image_lazy_load'],
		'spacing' => 0,
		'adaptive_height' => true,
		'images' => $opts['gallery'],
		'responsive' => $opts['responsive']
	) ); ?>
<?php elseif ( 'quote' == $opts['post_format'] && !empty( $opts['quote'] ) ) : ?>
	<?php gastro_template_item( array(
		'type' => 'quote',
		'background_id' => $opts['image_id'],
		'background_url' => $opts['image_url'],
		'background_color' => '#22222',
		'background_opacity' => 50,
		'text' => $opts['quote'],
		'author' => '&#8211;&nbsp;' . $opts['author']
	) ); ?>
<?php else : ?>
	<?php $opts['itemprop'] = 'image'; ?>
	<?php echo gastro_template_media( $opts, false, $opts['responsive'] ); ?>
<?php endif; ?>
