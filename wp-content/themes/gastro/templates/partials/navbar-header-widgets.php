<?php
/**
 * Partial template file
 *
 * @package gastro/templates/partials
 * @version 1.0.0
 */
?>


<?php $opts = gastro_template_args( 'navbar-header-widgets' ); ?>

<?php if ( $opts && is_array( $opts ) && !empty( $opts ) ) : ?>
	<div class="<?php echo esc_attr( implode( ' ', $opts['header_widgets_class'] ) ); ?>">
		<div class="gst-header-widgets-content">
			<div class="<?php echo esc_attr( $opts['container_class'] ); ?>">
				<div class="gst-row">
					<?php for ( $i = 1; $i <= $opts['header_widget_col']; $i++ ) : ?>
						<div class="gst-header-widgets-column gst-p-border-border gst-col-<?php echo esc_attr( 12 / $opts['header_widget_col'] ); ?>">
							<?php if ( is_active_sidebar( 'gastro-headerwidget-' . $i ) ) : ?>
								<div class="gst-widgets">
									<ul class="gst-widgets-list">
										<?php dynamic_sidebar( 'gastro-headerwidget-' . $i ); ?>
									</ul>
								</div>
							<?php endif; ?>
						</div>
					<?php endfor; ?>
				</div>
			</div>
		</div>
	</div>
<?php endif; ?>
