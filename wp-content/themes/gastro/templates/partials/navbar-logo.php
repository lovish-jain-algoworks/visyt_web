<?php
/**
 * Partial template file
 *
 * @package gastro/templates/partials
 * @version 1.0.0
 */
?>


<?php $opts = gastro_template_args( 'navbar-logo' ); ?>

<?php if ( $opts && is_array( $opts ) && !empty( $opts ) ) : ?>
	<a class="gst-navbar-brand" href="<?php echo esc_url( get_home_url() ); ?>">
		<?php if ( empty( $opts['mobile_navbar_logo'] ) ) : ?>
			<?php if ( 'text' === $opts['logo_type'] && !empty( $opts['logo_text_title'] ) ) : ?>
				<span class="gst-navbar-logo gst-navbar-logo--text"><?php echo do_shortcode( $opts['logo_text_title'] ); ?></span>
			<?php elseif ( !empty( $opts['logo'] ) ) : ?>
				<?php $logo_image = gastro_convert_image( $opts['logo'], 'full' ); ?>
				<img class="gst-navbar-logo gst-navbar-logo--image" src="<?php echo esc_url( $logo_image['url'] ); ?>" alt="<?php esc_attr_e( 'logo', 'gastro' ); ?>" />
			<?php endif; ?>
		<?php else : ?>
			<?php $logo_image = gastro_convert_image( $opts['mobile_navbar_logo'], 'full' ); ?>
			<img class="gst-navbar-logo gst-navbar-logo--image" src="<?php echo esc_url( $logo_image['url'] ); ?>" alt="<?php esc_attr_e( 'logo', 'gastro' ); ?>" />
		<?php endif; ?>

		<?php if ( $opts['two_schemes_logo'] ) : ?>
			<?php $fixed_logo_image_light = gastro_convert_image( $opts['fixed_navbar_logo_light'], 'full' ); ?>
			<?php $fixed_logo_image_dark = gastro_convert_image( $opts['fixed_navbar_logo_dark'], 'full' ); ?>
			<img class="gst-fixed-nav-logo gst-fixed-nav-logo--light" src="<?php echo esc_url( $fixed_logo_image_light['url'] ); ?>" alt="<?php esc_attr_e( 'logo light scheme', 'gastro' ); ?>" />
			<img class="gst-fixed-nav-logo gst-fixed-nav-logo--dark" src="<?php echo esc_url( $fixed_logo_image_dark['url'] ); ?>" alt="<?php esc_attr_e( 'logo dark scheme', 'gastro' ); ?>" />
		<?php endif; ?>
		<?php if ( !empty( $opts['fixed_navbar_logo'] ) ) : ?>
			<?php $fixed_logo_image = gastro_convert_image( $opts['fixed_navbar_logo'], 'full' ); ?>
			<img class="gst-fixed-nav-logo gst-fixed-nav-logo--default" src="<?php echo esc_url( $fixed_logo_image['url'] ); ?>" alt="<?php esc_attr_e( 'logo', 'gastro' ); ?>" />
		<?php endif; ?>
	</a>
<?php endif; ?>
