<?php
/**
 * Partial template file
 *
 * @package gastro/templates/partials
 * @version 1.0.0
 */
?>


<?php $opts = gastro_template_args( 'pagination' ); ?>

<div class="<?php echo esc_attr( $opts['pagination_class'] ); ?>">

<?php if ( 'pagination' === $opts['pagination_style'] ) : ?>
	<div class="gst-pagination-inner">
		<?php echo paginate_links( $opts['pagination_link_args'] ); ?>
	</div>
<?php else : ?>
	<input type="hidden" value="<?php echo esc_attr( json_encode( $opts ) ); ?>" data-posts="<?php echo esc_attr( $opts['all_posts'] ); ?>" />
	<a href="#" class="btnx gst-s-bg-bg gst-p-border-border gst-s-text-color" data-ajax="<?php echo admin_url( 'admin-ajax.php' ); ?>">
		<span class="btnx-text" data-label="<?php esc_attr_e( 'Load More', 'gastro' ); ?>" data-loading_label="<?php esc_attr_e( 'Loading', 'gastro' ); ?>">
			<?php esc_html_e( 'Load More', 'gastro' ); ?>
		</span>
		<?php if ( 'click' === $opts['pagination_style'] ) : ?>
			<?php echo gastro_template_preload( 'three-bounce' ); ?>
		<?php endif; ?>
	</a>
	<?php if ( 'scroll' === $opts['pagination_style'] ) : ?>
		<?php echo gastro_template_preload( 'three-bounce' ); ?>
	<?php endif; ?>
	<div class="gst-pagination-error-msg">
		<?php esc_html_e( 'Fail to load posts. Try to refresh page.', 'gastro' ); ?>
	</div>
<?php endif; ?>
</div>
