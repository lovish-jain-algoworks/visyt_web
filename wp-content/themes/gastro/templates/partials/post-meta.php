<?php
/**
 * Partial template file
 *
 * @package gastro/templates/partials
 * @version 1.0.5
 */
?>


<?php
	$opts = gastro_template_args( 'post-meta' );
	$title = get_the_title();
?>

<?php if ( !empty( $title ) || $opts['category'] || $opts['date'] || $opts['author'] ) : ?>
	<?php if ( $opts['category'] ) : ?>
		<?php $categories = gastro_term_names( gastro_get_taxonomy() ); ?>
		<?php if ( !empty( $categories['link'] ) ) : ?>
			<div class="gst-post-category <?php echo esc_attr( $opts['category_style'] ); ?> gst-<?php echo esc_attr( apply_filters( 'single_post_category_font', 'secondary' ) ); ?>-font">
				<?php if ( 'plain' !== $opts['category_style'] ) : ?>
					<span class="gst-post-category-line gst-p-brand-bg"></span>
				<?php endif; ?>
				<?php foreach ( $categories['link'] as $cat ) : ?>
					<?php echo gastro_escape_content( $cat ); ?>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
	<?php endif; ?>
	<?php
		$title_style = array(
			'font-size' => $opts['title_size'] . 'px',
			'letter-spacing' =>  is_numeric( $opts['title_letter_spacing'] ) ? $opts['title_letter_spacing'] . 'px' : $opts['title_letter_spacing'],
			'max-width' => is_numeric( $opts['title_max_width'] ) ? $opts['title_max_width'] . 'px' : $opts['title_max_width'],
			'line-height' => $opts['title_line_height']
		);
	?>
	<?php if ( !empty( $title ) ) : ?>
		<h1 itemprop="headline" class="gst-post-title post-title gst-<?php echo esc_attr( $opts['title_font'] ); ?>-font" <?php echo gastro_s( $title_style ); ?>>
			<?php echo esc_html( $title ); ?>
		</h1>
	<?php endif; ?>
	<?php
		$prep_time = get_post_meta( $opts['post_id'], 'recipe_prep', true );
		$cook_time = get_post_meta( $opts['post_id'], 'recipe_cook', true );
		$serving = get_post_meta( $opts['post_id'], 'recipe_serves', true );
		$difficulty = get_post_meta( $opts['post_id'], 'recipe_difficulty', true );
	?>
	<div class="gst-post-meta gst-<?php echo esc_attr( apply_filters( 'single_post_meta_font', 'secondary' ) ); ?>-font">
	<?php if ( $opts['date'] ) : ?>
		<div itemprop="datePublished" class="gst-post-date post-date date updated"><?php echo get_the_date(); ?></div>
	<?php endif; ?>
	<?php if ( $opts['author'] ) : ?>
		<div class="author gst-post-author">
			<?php echo esc_html_e( 'By', 'gastro' ); ?>
			<a itemprop="author" href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>">
				<?php echo get_the_author(); ?>
			</a>
		</div>
	<?php endif; ?>
	<?php if ( !empty( $prep_time ) || !empty( $cook_time ) || !empty( $serving ) || !empty( $difficulty ) ) : ?>
		<div class="gst-post-customfield">
			<?php if ( !empty( $prep_time ) ) : ?>
				<div class="gst-post-preptime">
					<i class="twf twf-clock"></i>
					<span itemprop="prepTime"><?php echo esc_html( $prep_time ); ?></span>
				</div>
			<?php endif; ?>
			<?php if ( !empty( $cook_time ) ) : ?>
				<div class="gst-post-cooktime">
					<i class="twf twf-fire"></i>
					<span itemprop="cookTime"><?php echo esc_html( $cook_time ); ?></span>
				</div>
			<?php endif; ?>
			<?php if ( !empty( $serving ) ) : ?>
				<div class="gst-post-serving">
					<i class="twf twf-user"></i>
					<span itemprop="recipeYield"><?php echo esc_html( $serving ); ?></span>
				</div>
			<?php endif; ?>
			<?php if ( !empty( $difficulty ) ) : ?>
				<div class="gst-post-difficulty">
					<i class="twf twf-gauge"></i>
					<span><?php echo esc_html( $difficulty ); ?></span>
				</div>
			<?php endif; ?>
		</div>
	<?php endif; ?>
	</div>
<?php endif; ?>
