<?php
/**
 * Partial template file
 *
 * @package gastro/templates/partials
 * @version 1.0.0
 */
?>


<?php $opts = gastro_template_args( 'social-share' ); ?>

<?php if ( !empty( $opts['components'] ) ) : ?>
	<ul <?php echo gastro_a( $opts['share_attr'] ); ?>>
		<?php if ( $opts['counter'] ) : ?>
			<li class="gst-social-share-count gst-p-border-border gst-s-text-color">
				<?php
					$page_id = gastro_get_page_id();
					$social_count = get_transient( 'gst-social-share-' . $page_id );
					if ( $social_count === false ) {
						$social_count = gastro_get_share_count( $opts['components'], $opts['share_url'] );
						$expiration = 86400;
						set_transient( 'gst-social-share-' . $page_id, $social_count, $expiration );
					}
				?>
				<span class="gst-social-share-count-number"><?php echo esc_html( $social_count ); ?></span>
				<span class="gst-social-share-count-suffix">
					<?php echo ( $social_count > 1 ) ? esc_html__( 'Shares', 'gastro' ) : esc_html__( 'Share', 'gastro' ); ?>
				</span>
			</li>
		<?php endif; ?>
		<?php foreach ( $opts['components'] as $option ) : ?>
			<?php $social_share_icon = ( 'email' === $option ) ? 'envelope' : $option; ?>
			<li class="<?php echo implode( ' ', $opts['item_class'] ); ?>">
				<?php $link_attr = $opts['link_attr']; ?>
				<?php $link_attr['class'][] = 'gst-social-' . esc_attr( $option ); ?>
				<a <?php echo gastro_a( $link_attr ); ?> href="<?php echo esc_url( $opts['social_share'][$option]['link'] ); ?>">
					<?php
						if ( 'minimal' === $opts['style'] ) {
							echo esc_html( $opts['social_share'][$option]['label'] );
						} else {
							$opts['icon_args']['icon'] = $social_share_icon;
							echo gastro_template_icon( $opts['icon_args'] );

							if ( 'button' === $opts['style'] ) { ?>
								<span class="gst-social-share-label">
									<?php echo esc_html( $opts['social_share'][$option]['label'] ); ?>
								</span>
							<?php }
						}
					?>
				</a>
			</li>
		<?php endforeach; ?>
	</ul>
<?php else : ?>
	<?php echo gastro_get_dummy_template( esc_html__( 'share', 'gastro' ), esc_html__( 'Share component is now empty.', 'gastro' ) ); ?>
<?php endif; ?>
