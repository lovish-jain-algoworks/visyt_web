<?php
/**
 * The template file
 *
 * @package gastro/templates
 * @version 1.0.0
 */
?>


<?php
	$args = gastro_template_args( 'side-navbar-fixed' );
	$opts = gastro_side_navbar_options( $args );
	$menu = gastro_template_nav_menu( $opts['nav_menu_args'] );
?>

<nav <?php echo gastro_a( $opts['navbar_attribute'] ); ?>>
	<div class="gst-navbar-inner gst-<?php echo esc_attr( $opts['navbar_color_scheme'] ); ?>-scheme">
		<div class="gst-navbar-wrapper">
			<div class="gst-navbar-header">
				<a class="gst-navbar-brand" href="<?php echo esc_url( get_home_url() ); ?>">
					<?php if ( 'text' === $opts['logo_type'] ) : ?>
						<span class="gst-navbar-logo gst-navbar-logo--text"><?php echo do_shortcode( $opts['logo_text_title'] ); ?></span>
					<?php elseif ( !empty( $opts['logo'] ) ) : ?>
						<?php $logo_image = gastro_convert_image( $opts['logo'], 'full' ); ?>
						<img class="gst-navbar-logo gst-navbar-logo--image" src="<?php echo esc_url( $logo_image['url'] ); ?>" alt="<?php esc_attr_e( 'logo', 'gastro' ); ?>" />
					<?php endif; ?>
				</a>
			</div>
			<?php if ( !empty( $menu ) ) : ?>
				<div class="gst-navbar-body">
					<div class="gst-navbar-body-inner">
						<?php echo gastro_template_nav_menu( $opts['nav_menu_args'] ); ?>
					</div>
				</div>
			<?php endif ;?>
			<?php if ( is_active_sidebar( 'gastro-navbar-side' ) ) : ?>
				<div class="gst-navbar-footer">
					<div class="gst-widgets">
						<ul class="gst-widgets-list">
							<?php dynamic_sidebar( 'gastro-navbar-side' ); ?>
						</ul>
					</div>
				</div>
			<?php endif ;?>
		</div><?php // Close navbar wrapper ?>
	</div><?php // Close navbar inner ?>
</nav>
