<?php
/**
 * The template file
 *
 * @package gastro/templates
 * @version 1.0.0
 */
?>


<?php
	$args = gastro_template_args( 'side-navbar-full' );
	$opts = gastro_side_navbar_options( $args );
	$menu = gastro_template_nav_menu( $opts['nav_menu_args'] );
?>

<nav <?php echo gastro_a( $opts['navbar_attribute'] ); ?>>
	<div class="gst-navbar-inner gst-<?php echo esc_attr( $opts['navbar_color_scheme'] ); ?>-scheme">
		<div class="gst-side-navbar-nav">
			<a class="gst-collapsed-button gst-collapsed-button--full" href="#" data-target=".gst-collapsed-menu">
				<span class="gst-lines"></span>
			</a>
			<div class="gst-side-navbar-logo">
				<a class="gst-navbar-brand" href="<?php echo esc_url( get_home_url() ); ?>">
					<?php if ( 'text' === $opts['logo_type'] ) : ?>
						<span class="gst-nabar-logo gst-navbar-logo--text"><?php echo do_shortcode( substr( $opts['logo_text_title'], 0, 1 ) ); ?></span>
					<?php else : ?>
						<?php if ( !empty( $opts['fixed_navbar_logo'] ) ): ?>
							<?php $logo_image = gastro_convert_image( $opts['fixed_navbar_logo'], 'full' ); ?>
							<img class="gst-navbar-logo gst-navbar-logo--image" src="<?php echo esc_url( $logo_image['url'] ); ?>" alt="<?php esc_attr_e( 'logo', 'gastro' ); ?>" />
						<?php elseif ( !empty( $opts['logo'] ) ) : ?>
							<?php $logo_image = gastro_convert_image( $opts['logo'], 'full' ); ?>
							<img class="gst-navbar-logo gst-navbar-logo--image" src="<?php echo esc_url( $logo_image['url'] ); ?>" alt="<?php esc_attr_e( 'logo', 'gastro' ); ?>" />
						<?php endif; ?>
					<?php endif; ?>
				</a>
			</div>
		</div><?php // Close sidenav nav ?>
		<?php if ( !empty( $menu ) ) : ?>
			<div class="gst-navbar-wrapper">
				<div class="gst-navbar-body">
					<div class="gst-navbar-body-inner">
						<div class="<?php echo esc_attr( implode( ' ', $opts['collapsed_classes'] ) ); ?>">
							<div class="gst-collapsed-menu-inner">
								<div class="gst-collapsed-menu-wrapper">
									<div class="gst-collapsed-menu-content">
										<?php echo gastro_escape_content( $menu ); ?>
									</div>
								</div>
								<?php if ( is_active_sidebar( 'gastro-navbar-full' ) ): ?>
									<div class="gst-widgets">
										<ul class="gst-widgets-list">
											<?php dynamic_sidebar( 'gastro-navbar-full' ); ?>
										</ul>
									</div>
								<?php endif ;?>
							</div>
						</div>
					</div>
				</div>
			</div>
		<?php endif ;?>
	</div><?php // Close navbar inner ?>
</nav>
