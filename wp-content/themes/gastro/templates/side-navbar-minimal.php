<?php
/**
 * The template file
 *
 * @package gastro/templates
 * @version 1.0.0
 */
?>


<?php
	$args = gastro_template_args( 'side-navbar-minimal' );
	$opts = gastro_side_navbar_options( $args );
	$menu = gastro_template_nav_menu( $opts['nav_menu_args'] );

	if ( 'text' === $opts['logo_type'] ) {
		$logo = $opts['logo_text_title'];
	} else {
		$logo_image = gastro_convert_image( $opts['logo'], 'full' );
		$logo = $logo_image['url'];
	}
?>

<nav <?php echo gastro_a( $opts['navbar_attribute'] ); ?>>
	<div class="gst-navbar-inner gst-<?php echo esc_attr( $opts['navbar_color_scheme'] ); ?>-scheme">
		<div class="gst-side-navbar-nav">
			<?php if ( 'left' === $opts['navbar_position'] ) : ?>
				<a class="gst-collapsed-button gst-collapsed-button--minimal" href="#" data-target=".gst-collapsed-menu"><span class="gst-lines"></span></a>
			<?php endif; ?>
				<div class="gst-side-navbar-logo">
					<a class="gst-navbar-brand" href="<?php echo esc_url( get_home_url() ); ?>">

					<?php if ( 'text' === $opts['logo_type'] ) : ?>
						<span class="gst-navbar-logo gst-navbar-logo--text"><?php echo do_shortcode( $logo ); ?></span>
					<?php else : ?>
						<img class="gst-navbar-logo gst-navbar-logo--image" src="<?php echo esc_url( $logo ); ?>" alt="<?php esc_attr_e( 'logo', 'gastro' ); ?>" />
					<?php endif; ?>

					<?php if ( $opts['two_schemes_logo'] ) : ?>
						<?php $fixed_logo_image_light = gastro_convert_image( $opts['fixed_navbar_logo_light'], 'full' ); ?>
						<?php $fixed_logo_image_dark = gastro_convert_image( $opts['fixed_navbar_logo_dark'], 'full' ); ?>
						<img class="gst-fixed-nav-logo gst-fixed-nav-logo--light" src="<?php echo esc_url( $fixed_logo_image_light['url'] ); ?>" alt="<?php esc_attr_e( 'logo light scheme', 'gastro' ); ?>" />
						<img class="gst-fixed-nav-logo gst-fixed-nav-logo--dark" src="<?php echo esc_url( $fixed_logo_image_dark['url'] ); ?>" alt="<?php esc_attr_e( 'logo dark scheme', 'gastro' ); ?>" />
					<?php endif; ?>

				</a>
				</div>
			<?php if ( 'right' === $opts['navbar_position'] ) : ?>
				<a class="gst-collapsed-button" href="#" data-target=".gst-collapsed-menu"><span class="gst-lines"></span></a>
			<?php endif; ?>
		</div>
		<div class="gst-navbar-wrapper">
			<div class="<?php echo esc_attr( implode( ' ', $opts['collapsed_classes'] ) ); ?>">
				<div class="gst-navbar-header">
					<a class="gst-navbar-brand" href="<?php echo esc_url( get_home_url() ); ?>">
						<?php if ( 'text' === $opts['logo_type'] ) : ?>
							<span class="gst-navbar-logo gst-navbar-logo--text"><?php echo esc_html( $logo ); ?></span>
						<?php else : ?>
							<img class="gst-navbar-logo gst-navbar-logo--image" src="<?php echo esc_url( $logo ); ?>" alt="<?php esc_attr_e( 'logo', 'gastro' ); ?>" />
						<?php endif; ?>
					</a>
				</div>
				<?php if ( !empty( $menu ) ) : ?>
					<div class="gst-navbar-body">
						<div class="gst-navbar-body-inner">
							<?php echo gastro_escape_content( $menu ); ?>
						</div>
					</div>
				<?php endif; ?>
				<?php if ( is_active_sidebar( 'gastro-navbar-side' ) ) : ?>
					<div class="gst-navbar-footer">
						<div class="gst-widgets">
							<ul class="gst-widgets-list">
								<?php dynamic_sidebar( 'gastro-navbar-side' ); ?>
							</ul>
						</div>
					</div>
				<?php endif ;?>
			</div><?php // Close collapsed menu ?>
		</div><?php // Close navbar wrapper ?>
	</div><?php // Close navbar inner ?>
</nav>
