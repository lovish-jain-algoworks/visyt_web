<?php $args = gastro_template_args( 'style-custom-blueprint' ); ?>
<?php $args = gastro_style_custom_options( $args ); ?>
<?php $fonts = $args['fonts']; ?>

<?php if ( !empty( $args['custom_fonts'] ) ) : ?>
	<?php $registered_custom_fonts = get_option( 'bp_custom_font' ); ?>
	<?php if ( !empty( $registered_custom_fonts ) ) : ?>
		<?php foreach ( $args['custom_fonts'] as $custom_font ) : ?>
			<?php $custom_font_family = $custom_font['family']; ?>
			<?php if ( isset( $registered_custom_fonts[$custom_font['family']] ) ) : ?>
				@font-face {
					font-family: <?php echo esc_attr( $custom_font_family ); ?>;
				<?php if ( isset( $custom_font['eot_url'] ) ) : ?>
					src: url('<?php echo esc_url( $registered_custom_fonts[$custom_font['family']]['eot_url'] ); ?>');
				<?php endif; ?>
					src: url('<?php echo esc_url( $registered_custom_fonts[$custom_font['family']]['url'] ); ?>');
				}
			<?php endif; ?>
		<?php endforeach; ?>
	<?php endif; ?>
<?php endif; ?>

<?php /*====Design Typography====*/?>
.bp-space {
	<?php echo gastro_s( $fonts[$args['style_typography']], false ); ?>
	font-size: <?php echo esc_attr( (int)$args['style_font_size'] ); ?>px;
}

.bp-space .gst-box-content {
	font-size: <?php echo esc_attr( (int)$args['style_font_size'] ); ?>px;
}
<?php /*=========================*/?>

<?php /*====Heading typography====*/?>
#poststuff .bp-item-content h1,
#poststuff .bp-context-header h1 {
	<?php echo gastro_s( $fonts[$args['heading_typography']], false ); ?>
	font-size: <?php echo esc_attr( $args['heading_size_h1'] ); ?>px;
}

#poststuff .bp-item-content h2,
#poststuff .bp-context-header h2 {
	<?php echo gastro_s( $fonts[$args['heading_typography']], false ); ?>
	font-size: <?php echo esc_attr( $args['heading_size_h2'] ); ?>px;
}

#poststuff .bp-item-content h3,
#poststuff .bp-context-header h3 {
	<?php echo gastro_s( $fonts[$args['heading_typography']], false ); ?>
	font-size: <?php echo esc_attr( $args['heading_size_h3'] ); ?>px;
}

#poststuff .bp-item-content h4,
#poststuff .bp-context-header h4 {
	<?php echo gastro_s( $fonts[$args['heading_typography']], false ); ?>
	font-size: <?php echo esc_attr( $args['heading_size_h4'] ); ?>px;
}

#poststuff .bp-item-content h5 {
	<?php echo gastro_s( $fonts[$args['heading_typography']], false ); ?>
	font-size: <?php echo esc_attr( $args['heading_size_h5'] ); ?>px;
}

#poststuff .bp-item-content h6 {
	<?php echo gastro_s( $fonts[$args['heading_typography']], false ); ?>
	font-size: <?php echo esc_attr( $args['heading_size_h6'] ); ?>px;
}
<?php /*==========================*/?>

<?php /*=====Theme typography=====*/?>
<?php foreach ( $fonts as $key => $value ) : ?>
.gst-<?php echo esc_attr( $key ); ?>-font,
#poststuff .bp-item-content .gst-<?php echo esc_attr( $key ); ?>-font,
#poststuff .bp-context-header .gst-<?php echo esc_attr( $key ); ?>-font {
	<?php echo gastro_s( $value, false ); ?>
}
<?php endforeach; ?>
<?php /*==========================*/?>

<?php /*===Layout and Max-width===*/?>
.bp-context-region,
.bp-context {
	background-color: <?php echo esc_attr( $args['primary_background'] ); ?>;
}

<?php if ( 'boxed' === $args['site_layout'] ) : ?>
.bp--preview .bp-context {
	max-width: <?php echo esc_attr( $args['content_max_width'] ); ?>px;
	margin: 0 auto;
}

	<?php if ( 'default' !== $args['body_background_color'] ) : ?>
	.bp-context-region {
		background-color: <?php echo esc_attr( gastro_customizer_get_color( $args['body_background_color'], $args ) ); ?>;
	}
	<?php else : ?>
	.bp-context-region {
		background-color: <?php echo esc_attr( $args['secondary_background'] ); ?>;
	}
	<?php endif; ?>

	<?php if ( !empty( $args['boxed_shadow'] ) ): ?>
	.bp--preview .bp-context {
		box-shadow: <?php echo esc_attr( $args['boxed_shadow'] ); ?>;
	}
	<?php endif; ?>
<?php else : ?>
.bp--preview .bp-context .bp-block {
	max-width: <?php echo esc_attr( $args['content_max_width'] + 200 ); ?>px;
	max-width: calc( <?php echo esc_attr( $args['content_max_width'] . "px + " . (int)$args['side_padding'] . "%" ); ?> );
}
<?php endif; ?>
.bp--preview .bp-context .bp-block {
	padding-right: <?php echo esc_attr( $args['side_padding'] / 2 ); ?>%;
	padding-left: <?php echo esc_attr( $args['side_padding'] / 2 ); ?>%;
}
<?php /*==========================*/?>

<?php /*=====Layout Background====*/?>
<?php if ( 'default' !== $args['content_background_color'] ) : ?>
.bp-context {
	background-color: <?php echo esc_attr( gastro_customizer_get_color( $args['content_background_color'], $args ) ); ?>;
}
<?php endif; ?>

<?php if ( !empty( $args['content_background_image'] ) ) : ?>
.bp--preview .bp-context {
	<?php echo gastro_get_background_image( array(
		'url' => $args['content_background_image'],
		'size' => $args['content_background_size'],
		'position' => $args['content_background_position'],
		'repeat' => $args['content_background_repeat'],
		'fixed' => $args['content_background_attachment']
	) ); ?>
}
<?php endif; ?>
<?php /*==========================*/?>

<?php /*=======Theme Color========*/?>
.bp-context a,
.bp-context a:hover.gst-s-text-color,
.bp-context a:hover.gst-p-text-color {
	color: <?php echo esc_attr( $args['primary_brand'] ); ?>;
}

.bp-context a:hover,
.bp-context a:focus,
.bp-context a:active,
.bp-context a:hover.gst-p-brand-color {
	color: <?php echo esc_attr( $args['secondary_brand'] ); ?>;
}

.bp-context-sections,
.bp-context-header {
	color: <?php echo esc_attr( $args['primary_text'] ); ?>;
}

.bp-context .bp-link,
.bp-context .bp-link:hover.gst-s-text-color,
.bp-context .bp-link:hover.gst-p-text-color {
	color: <?php echo esc_attr( $args['primary_brand'] ); ?>;
}

.bp-context .bp-link:hover,
.bp-context .bp-link:focus,
.bp-context .bp-link:active,
.bp-context .bp-link:hover.gst-p-brand-color {
	color: <?php echo esc_attr( $args['secondary_brand'] ); ?>;
}

.bp-context .btnx {
	color: <?php echo esc_attr( $args['primary_brand'] ); ?>;
}

.bp-context .btnx:hover,
.bp-context .btnx:focus {
	color: <?php echo esc_attr( $args['secondary_brand'] ); ?>;
}

<?php
gastro_sch_css( array_merge( $args['base_sch_args'], array(
	'selector' => '.bp-link:hover.gst-p-bg-color',
	'property' => array(
		'color' => 'secondary_background'
	)
) ), true );
gastro_sch_css( array_merge( $args['base_sch_args'], array(
	'selector' => array( 'h1', 'h2', 'h3', 'h4', 'h5', 'h6' ),
	'property' => array(
		'color' => 'secondary_text'
	)
) ), true );
?>
<?php /*=========================*/?>

<?php /*===Main Color Selector===*/?>
#poststuff .bp-context .gst-light-scheme,
#poststuff .bp-context .gst-entry-light-scheme,
#poststuff .bp-context .gst-slider-light-scheme {
	color: <?php echo esc_attr( $args['bp_color_3'] ); ?>;
}

#poststuff .bp-context .gst-dark-scheme,
#poststuff .bp-context .gst-entry-dark-scheme,
#poststuff .bp-context .gst-slider-dark-scheme {
	color: <?php echo esc_attr( $args['bp_color_8'] ); ?>;
}

.gst-p-brand-color {
	color: <?php echo esc_attr( $args['primary_brand'] ); ?>;
}

.gst-p-brand-bg {
	background-color: <?php echo esc_attr( $args['primary_brand'] ); ?>;
}

.gst-p-brand-border {
	border-color: <?php echo esc_attr( $args['primary_brand'] ); ?>;
}

.gst-s-brand-color {
	color: <?php echo esc_attr( $args['secondary_brand'] ); ?>;
}

.gst-s-brand-bg {
	background-color: <?php echo esc_attr( $args['secondary_brand'] ); ?>;
}

.gst-s-brand-border {
	border-color: <?php echo esc_attr( $args['secondary_brand'] ); ?>;
}

.gst-p-brand-contrast-color {
	color: <?php echo esc_attr( $args['primary_brand_contrast'] ); ?>;
}

.gst-s-brand-contrast-color {
	color: <?php echo esc_attr( $args['secondary_brand_contrast'] ); ?>;
}

<?php
gastro_sch_css( array_merge( $args['base_sch_args'], array(
	'selector' => '.gst-p-text-contrast-color',
	'property' => array(
		'color' => 'primary_text_contrast'
	),
	'parent' => array( 'slider' )
) ), true );
gastro_sch_css( array_merge( $args['base_sch_args'], array(
	'selector' => '.gst-s-text-contrast-color',
	'property' => array(
		'color' => 'secondary_text_contrast'
	),
	'parent' => array( 'slider' )
) ), true );
gastro_sch_css( array_merge( $args['base_sch_args'], array(
	'selector' => '.gst-p-text-color',
	'property' => array(
		'color' => 'primary_text'
	),
	'parent' => array( 'slider', 'entry' )
) ), true );
gastro_sch_css( array_merge( $args['base_sch_args'], array(
	'selector' => '.gst-p-text-bg',
	'property' => array(
		'background-color' => 'primary_text'
	),
	'parent' => array( 'slider', 'entry' )
) ), true );
gastro_sch_css( array_merge( $args['base_sch_args'], array(
	'selector' => '.gst-s-text-color',
	'property' => array(
		'color' => 'secondary_text'
	),
	'parent' => array( 'slider', 'entry' )
) ), true );
gastro_sch_css( array_merge( $args['base_sch_args'], array(
	'selector' => '.gst-s-text-bg',
	'property' => array(
		'background-color' => 'secondary_text'
	),
	'parent' => array( 'slider', 'entry' )
) ), true );
gastro_sch_css( array_merge( $args['base_sch_args'], array(
	'selector' => '.gst-s-text-bg.gst-overlay',
	'property' => array(
		'background-color' => 'secondary_text'
	),
	'opacity' => 0.8,
	'parent' => array( 'slider', 'entry' )
) ), true );
gastro_sch_css( array_merge( $args['base_sch_args'], array(
	'selector' => '.gst-s-text-border',
	'property' => array(
		'border-color' => 'secondary_text'
	),
	'parent' => array( 'slider', 'entry' )
) ), true );
gastro_sch_css( array_merge( $args['base_sch_args'], array(
	'selector' => '.gst-p-bg-color',
	'property' => array(
		'color' => 'primary_background'
	),
	'parent' => array( 'slider', 'entry' )
) ), true );
gastro_sch_css( array_merge( $args['base_sch_args'], array(
	'selector' => '.gst-p-bg-bg',
	'property' => array(
		'background-color' => 'primary_background'
	),
	'parent' => array( 'slider', 'entry' )
) ), true );
gastro_sch_css( array_merge( $args['base_sch_args'], array(
	'selector' => '.gst-p-bg-border',
	'property' => array(
		'border-color' => 'primary_background'
	),
	'parent' => array( 'slider', 'entry' )
) ), true );
gastro_sch_css( array_merge( $args['base_sch_args'], array(
	'selector' => '.gst-s-bg-color',
	'property' => array(
		'color' => 'secondary_background'
	),
	'parent' => array( 'slider', 'entry' )
) ), true );
gastro_sch_css( array_merge( $args['base_sch_args'], array(
	'selector' => '.gst-s-bg-bg',
	'property' => array(
		'background-color' => 'secondary_background'
	),
	'parent' => array( 'slider', 'entry' )
) ), true );
gastro_sch_css( array_merge( $args['base_sch_args'], array(
	'selector' => '.gst-s-bg-border',
	'property' => array(
		'border-color' => 'secondary_background'
	),
	'parent' => array( 'slider', 'entry' )
) ), true );
gastro_sch_css( array_merge( $args['base_sch_args'], array(
	'selector' => '.gst-p-border-color',
	'property' => array(
		'color' => 'primary_border'
	),
	'parent' => array( 'slider', 'entry' )
) ), true );
gastro_sch_css( array_merge( $args['base_sch_args'], array(
	'selector' => '.gst-p-border-bg',
	'property' => array(
		'background-color' => 'primary_border'
	),
	'parent' => array( 'slider', 'entry' )
) ), true );
gastro_sch_css( array_merge( $args['base_sch_args'], array(
	'selector' => '.gst-p-border-border',
	'property' => array(
		'border-color' => 'primary_border'
	),
	'parent' => array( 'slider', 'entry' ),
	'is_high_level' => true
) ), true );
gastro_sch_css( array_merge( $args['base_sch_args'], array(
	'selector' => '.gst-overlay',
	'property' => array(
		'background-color' => 'primary_background'
	),
	'opacity' => 0.8,
	'parent' => array( 'slider', 'entry' )
) ), true );
?>

<?php /*=========================*/?>

<?php /*=======Brand Button======*/?>
<?php
gastro_sch_css( array_merge( $args['base_sch_args'], array(
	'selector' => array( '.gst-button--border > .btnx', '.gst-button--fill.gst-button-hover--inverse > .btnx:hover' ),
	'property' => array(
		'color' => 'primary_brand',
		'border-color' => 'primary_brand'
	),
	'parent' => 'slider'
) ), true );
gastro_sch_css( array_merge( $args['base_sch_args'], array(
	'selector' => '.gst-button--border.gst-button-hover--brand > .btnx:hover',
	'property' => array(
		'color' => 'secondary_brand',
		'border-color' => 'secondary_brand'
	),
	'parent' => 'slider'
) ), true );
gastro_sch_css( array_merge( $args['base_sch_args'], array(
	'selector' => array(
		'.gst-form-group input[type=submit]',
		'.gst-button--layer.gst-button-color--brand > .btnx',
		'.gst-button--fill.gst-button-color--brand > .btnx',
		'.gst-button--border.gst-button-hover--inverse > .btnx:hover'
	),
	'property' => array(
		'color' => 'primary_brand_contrast',
		'border-color' => 'primary_brand',
		'background-color' => 'primary_brand'
	),
	'parent' => 'slider'
) ), true );
gastro_sch_css( array_merge( $args['base_sch_args'], array(
	'selector' => array(
		'.gst-form-group input[type=submit]:hover',
		'.gst-button--fill.gst-button-hover--brand > .btnx:hover'
	),
	'property' => array(
		'color' => 'secondary_brand_contrast',
		'border-color' => 'secondary_brand',
		'background-color' => 'secondary_brand'
	),
	'parent' => 'slider'
) ), true );
?>
<?php /*=========================*/?>

<?php /*=======Basic Button======*/?>
<?php
gastro_sch_css( array_merge( $args['base_sch_args'], array(
	'selector' => array(
		'.gst-button-color--basic.gst-button--border > .btnx',
		'.gst-button-color--basic.gst-button--fill.gst-button-hover--inverse > .btnx:hover'
	),
	'property' => array(
		'color' => 'secondary_text',
		'border-color' => 'secondary_text'
	),
	'parent' => 'slider'
) ), true );
gastro_sch_css( array_merge( $args['base_sch_args'], array(
	'selector' => array(
		'.gst-button-color--basic.gst-button--layer > .btnx',
		'.gst-button-color--basic.gst-button--fill > .btnx',
		'.gst-button-color--basic.gst-button--border.gst-button-hover--inverse > .btnx:hover'
	),
	'property' => array(
		'color' => 'secondary_text_contrast',
		'border-color' => 'secondary_text',
		'background-color' => 'secondary_text'
	),
	'parent' => 'slider'
) ), true );
gastro_sch_css( array_merge( $args['base_sch_args'], array(
	'selector' => '.gst-button-color--basic.gst-button--border.gst-button-hover--brand > .btnx:hover',
	'property' => array(
		'color' => 'primary_brand',
		'border-color' => 'primary_brand',
	),
	'parent' => 'slider'
) ), true );
gastro_sch_css( array_merge( $args['base_sch_args'], array(
	'selector' => '.gst-button-color--basic.gst-button--fill.gst-button-hover--brand > .btnx:hover',
	'property' => array(
		'color' => 'primary_brand_contrast',
		'border-color' => 'primary_brand',
		'background-color' => 'primary_brand'
	),
	'parent' => 'slider'
) ), true );
?>
<?php /*=========================*/?>

<?php /*=====Tab & Accordion=====*/?>
.gst-tab--button .gst-tab-nav .active {
	background-color: <?php echo esc_attr( $args['primary_brand'] ); ?>;
}

.gst-tab--button .gst-tab-nav .active > .gst-tab-nav-title {
	color: <?php echo esc_attr( $args['primary_brand_contrast'] ); ?>;
}
<?php /*=========================*/?>

<?php /*==========Entry==========*/?>
<?php $brand_shadow_color = gastro_hex_to_rgba( $args['primary_brand'], 0.8 ); ?>
.gst-entries--layer .gst-entry-inner {
	-webkit-box-shadow: 5px 5px 0px 0px <?php echo esc_attr( $brand_shadow_color ); ?>;
	-moz-box-shadow: 5px 5px 0px 0px <?php echo esc_attr( $brand_shadow_color ); ?>;
	box-shadow: 5px 5px 0px 0px <?php echo esc_attr( $brand_shadow_color ); ?>;
}

<?php
gastro_sch_css( array_merge( $args['base_sch_args'], array(
	'selector' => '.gst-entry-flash span.onsale',
	'property' => array(
		'color' => 'secondary_text_contrast'
	)
) ), true );
gastro_sch_css( array_merge( $args['base_sch_args'], array(
	'selector' => array(
		'.gst-entry-title a:hover',
		'.gst-entry-meta a:hover',
		'.gst-relatedpost .gst-entry:hover .gst-entry-title'
	),
	'property' => array(
		'color' => 'primary_brand'
	)
) ), true );
?>

.gst-entry .more-link {
	color: <?php echo esc_attr( $args['primary_brand'] ); ?>;
}

.gst-entry .more-link:hover {
	color: <?php echo esc_attr( $args['secondary_brand'] ); ?>;
}
<?php /*=========================*/?>

<?php /*========Utilities========*/?>
<?php
gastro_sch_css( array_merge( $args['base_sch_args'], array(
	'selector' => '.slick-dots li.slick-active button:before',
	'property' => array(
		'color' => 'secondary_text'
	),
	'parent' => 'slider'
) ), true );
gastro_sch_css( array_merge( $args['base_sch_args'], array(
	'selector' => '.gst-carousel-arrow',
	'property' => array(
		'color' => 'secondary_text_contrast',
		'background-color' => 'secondary_text',
	),
	'opacity' => 0.7,
	'parent' => 'slider'
) ), true );
gastro_sch_css( array_merge( $args['base_sch_args'], array(
	'selector' => '.gst-carousel-arrow.transparent',
	'property' => array(
		'color' => 'secondary_text'
	),
	'parent' => 'slider'
) ), true );
gastro_sch_css( array_merge( $args['base_sch_args'], array(
	'selector' => '.gst-pagination .page-numbers',
	'property' => array(
		'color' => 'primary_text',
	)
) ), true );
gastro_sch_css( array_merge( $args['base_sch_args'], array(
	'selector' => array(
		'.gst-pagination .page-numbers',
		'.gst-filter-list > .active'
	),
	'property' => array(
		'color' => 'secondary_text',
	)
) ), true );
gastro_sch_css( array_merge( $args['base_sch_args'], array(
	'selector' => array(
		'.gst-filter-list > .active:after',
		'.page-numbers.current:after'
	),
	'property' => array(
		'background-color' => 'secondary_text',
	)
) ), true );
gastro_sch_css( array_merge( $args['base_sch_args'], array(
	'selector' => '.with-border',
	'property' => array(
		'border-color' => 'primary_border',
	)
) ), true );
?>
<?php /*=========================*/?>

<?php /*====Unsolvable Color=====*/?>
<?php if ( 'light' === $args['default_color_scheme'] ) : ?>
.gst-button--layer > .btnx {
	box-shadow: 4px 4px #e4e4e4;
}

.gst-dark-scheme .gst-button--layer > .btnx,
.gst-slider-dark-scheme .gst-button--layer > .btnx {
	box-shadow: 4px 4px #454545;
}

.gst-light-scheme .gst-button--layer > .btnx,
.gst-slider-light-scheme .gst-button--layer > .btnx {
	box-shadow: 4px 4px #e4e4e4;
}

.slick-dots li button:before {
	color: #ccc;
}

.gst-dark-scheme .slick-dots li button:before,
.gst-slider-dark-scheme .slick-dots li button:before {
	color: #999;
}

.gst-light-scheme .slick-dots li button:before,
.gst-slider-light-scheme .slick-dots li button:before {
	color: #ccc;
}

.gst-pricing-item.highlighted {
	box-shadow: 0 2px 8px rgba(0, 0, 0, 0.15);
}

.gst-dark-scheme .gst-pricing-item.highlighted {
	box-shadow: 0 2px 8px rgba(0, 0, 0, 0.4);
}

.gst-light-scheme .gst-pricing-item.highlighted {
	box-shadow: 0 2px 8px rgba(0, 0, 0, 0.15);
}

.gst-with-shadow {
	box-shadow: 0 2px 8px rgba(0, 0, 0, 0.15);
}

.gst-dark-scheme .gst-with-shadow {
	box-shadow: 0 2px 8px rgba(0, 0, 0, 0.3);
}

.gst-light-scheme .gst-with-shadow {
	box-shadow: 0 2px 8px rgba(0, 0, 0, 0.15);
}

.gst-with-hover-shadow:hover {
	box-shadow: 0 10px 24px rgba(0, 0, 0, 0.1);
}

.gst-dark-scheme .gst-with-hover-shadow:hover {
	box-shadow: 0 10px 24px rgba(0, 0, 0, 0.2);
}

.gst-light-scheme .gst-with-hover-shadow:hover {
	box-shadow: 0 10px 24px rgba(0, 0, 0, 0.1);
}
<?php else : ?>
.gst-button--layer > .btnx {
	box-shadow: 4px 4px #454545;
}

.gst-light-scheme .gst-button--layer > .btnx,
.gst-slider-light-scheme .gst-button--layer > .btnx {
	box-shadow: 4px 4px #e4e4e4;
}

.gst-dark-scheme .gst-button--layer > .btnx,
.gst-slider-dark-scheme .gst-button--layer > .btnx {
	box-shadow: 4px 4px #454545;
}

.slick-dots li button:before {
	color: #999;
}

.gst-light-scheme .slick-dots li button:before,
.gst-slider-light-scheme .slick-dots li button:before {
	color: #ccc;
}

.gst-dark-scheme .slick-dots li button:before,
.gst-slider-dark-scheme .slick-dots li button:before {
	color: #999;
}

.gst-pricing-item.highlighted {
	box-shadow: 0 2px 8px rgba(0, 0, 0, 0.4);
}

.gst-light-scheme .gst-pricing-item.highlighted {
	box-shadow: 0 2px 8px rgba(0, 0, 0, 0.15);
}

.gst-dark-scheme .gst-pricing-item.highlighted {
	box-shadow: 0 2px 8px rgba(0, 0, 0, 0.4);
}

.gst-with-shadow {
	box-shadow: 0 2px 8px rgba(0, 0, 0, 0.3);
}

.gst-light-scheme .gst-with-shadow {
	box-shadow: 0 2px 8px rgba(0, 0, 0, 0.15);
}

.gst-dark-scheme .gst-with-shadow {
	box-shadow: 0 2px 8px rgba(0, 0, 0, 0.3);
}

.gst-with-hover-shadow:hover {
	box-shadow: 0 10px 24px rgba(0, 0, 0, 0.2);
}

.gst-light-scheme .gst-with-hover-shadow:hover {
	box-shadow: 0 10px 24px rgba(0, 0, 0, 0.1);
}

.gst-dark-scheme .gst-with-hover-shadow:hover {
	box-shadow: 0 10px 24px rgba(0, 0, 0, 0.2);
}
<?php endif; ?>
<?php /*=========================*/?>

<?php /*=======Design Button=====*/?>
.bp-context .btnx {
	<?php echo gastro_s( $fonts[$args['button_typography']], false ); ?>
<?php if ( $args['button_uppercase'] ) : ?>
	text-transform: uppercase;
<?php else : ?>
	text-transform: none;
<?php endif; ?>
}

.gst-button > .btnx {
	border-radius: <?php echo esc_attr( $args['button_radius'] ); ?>px;
	border-width: <?php echo esc_attr( $args['button_border'] ); ?>px;
}
<?php /*=========================*/?>

<?php /*==Breadcrumb Text Color==*/?>
<?php if ( 'default' !== $args['breadcrumb_text_color'] ) : ?>
.bp-context .gst-page-title .gst-page-title-breadcrumb {
	color: <?php echo esc_attr( gastro_customizer_get_color( $args['breadcrumb_text_color'], $args ) ); ?>;
}
<?php endif; ?>
<?php /*=========================*/?>

<?php /*==Breadcrumb Background Color==*/?>
<?php if ( 'default' !== $args['breadcrumb_background_color'] ) : ?>
.bp-context .gst-page-title > .gst-page-title-breadcrumb {
	background-color: <?php echo gastro_hex_to_rgba( $args['breadcrumb_background_color'], $args['breadcrumb_background_opacity'] / 100 ); ?>;
}
<?php endif; ?>
<?php /*=========================*/?>

<?php /*==Page Title Text Color==*/?>
<?php if ( 'default' !== $args['page_title_text_color'] ) : ?>
.bp-context .gst-page-title .gst-page-title-content {
	color: <?php echo esc_attr( gastro_customizer_get_color( $args['page_title_text_color'], $args ) ); ?>;
}
<?php endif; ?>
<?php /*=========================*/?>

<?php /*==Page Title Background Color==*/?>
<?php if ( 'default' !== $args['page_title_background_color'] ) : ?>
.bp-context .gst-page-title .gst-background-overlay {
	background-color: <?php echo gastro_hex_to_rgba( $args['page_title_background_color'], $args['page_title_background_opacity'] / 100 ); ?>;
}
<?php endif; ?>
<?php /*=========================*/?>
