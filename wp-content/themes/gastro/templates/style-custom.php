<?php $args = gastro_template_args( 'style-custom' ); ?>
<?php $args = gastro_style_custom_options( $args ); ?>
<?php $fonts = $args['fonts']; ?>

<?php if ( !empty( $args['custom_fonts'] ) ) : ?>
	<?php $registered_custom_fonts = get_option( 'bp_custom_font' ); ?>
	<?php if ( !empty( $registered_custom_fonts ) ) : ?>
		<?php foreach ( $args['custom_fonts'] as $custom_font ) : ?>
			<?php $custom_font_family = $custom_font['family']; ?>
			<?php if ( isset( $registered_custom_fonts[$custom_font['family']] ) ) : ?>
				@font-face {
					font-family: <?php echo esc_attr( $custom_font_family ); ?>;
				<?php if ( isset( $custom_font['eot_url'] ) ) : ?>
					src: url('<?php echo esc_url( $registered_custom_fonts[$custom_font['family']]['eot_url'] ); ?>');
				<?php endif; ?>
					src: url('<?php echo esc_url( $registered_custom_fonts[$custom_font['family']]['url'] ); ?>');
				}
			<?php endif; ?>
		<?php endforeach; ?>
	<?php endif; ?>
<?php endif; ?>

<?php /*====Heading typography====*/?>
h1, h2, h3, h4, h5, h6 {
	<?php echo gastro_s( $fonts[$args['heading_typography']], false ); ?>
}
<?php /*==========================*/?>

<?php /*=====Theme typography=====*/?>
<?php foreach ( $fonts as $key => $value ) : ?>
.gst-<?php echo esc_attr( $key ); ?>-font {
	<?php echo gastro_s( $value, false ); ?>
}
<?php endforeach; ?>
<?php /*==========================*/?>

<?php /*=Native Tag & Basic Class==*/?>
body {
	color: <?php echo esc_attr( $args['primary_text'] ); ?>;
	background-color: <?php echo esc_attr( $args['primary_background'] ); ?>;
}

.gst-wrapper--parallax-footer .gst-content {
	background-color: <?php echo esc_attr( $args['primary_background'] ); ?>;
}

::selection {
	color: <?php echo esc_attr( $args['primary_brand_contrast'] ); ?>;
	background-color: <?php echo esc_attr( $args['primary_brand'] ); ?>;
}

::-moz-selection {
	color: <?php echo esc_attr( $args['primary_brand_contrast'] ); ?>;
	background-color: <?php echo esc_attr( $args['primary_brand'] ); ?>;
}

a,
a:hover.gst-s-text-color,
a:hover.gst-p-text-color,
.comment-form-rating p.stars a:before,
.comment-rating > span,
.comment-like-dislike a:visited {
	color: <?php echo esc_attr( $args['primary_brand'] ); ?>;
}

a:hover,
a:focus,
a:active,
a:hover.gst-p-brand-color {
	color: <?php echo esc_attr( $args['secondary_brand'] ); ?>;
}

a:focus.gst-s-text-color {
	color: <?php echo esc_attr( $args['secondary_text'] ); ?>;
}

a:focus.gst-p-text-color {
	color: <?php echo esc_attr( $args['primary_text'] ); ?>;
}

.btnx,
ins, code, kbd, tt {
	color: <?php echo esc_attr( $args['primary_brand'] ); ?>;
}

.btnx:hover,
.btnx:focus {
	color: <?php echo esc_attr( $args['secondary_brand'] ); ?>;
}

<?php
gastro_sch_css( array_merge( $args['base_sch_args'], array(
	'selector' => 'a:hover.gst-p-bg-color',
	'property' => array(
		'color' => 'secondary_background'
	)
) ) );
gastro_sch_css( array_merge( $args['base_sch_args'], array(
	'selector' => 'label',
	'property' => array(
		'color' => 'primary_text'
	)
) ) );
gastro_sch_css( array_merge( $args['base_sch_args'], array(
	'selector' => array(
		'pre',
		'cite',
		'mark',
		'h1','h2','h3','h4','h5','h6',
		'.gst-text-bullet.decimal li:before'
	),
	'property' => array(
		'color' => 'secondary_text'
	)
) ) );
gastro_sch_css( array_merge( $args['base_sch_args'], array(
	'selector' => array(
		'code',
		'kbd',
		'tt',
		'pre'
	),
	'property' => array(
		'background-color' => 'secondary_background'
	)
) ) );
gastro_sch_css( array_merge( $args['base_sch_args'], array(
	'selector' => 'pre',
	'property' => array(
		'border-color' => 'primary_border'
	)
) ) );
?>
<?php /*=========================*/?>

<?php /*===Main Color Selector===*/?>
.gst-light-scheme,
.gst-entry-light-scheme,
.gst-slider-light-scheme {
	color: <?php echo esc_attr( $args['bp_color_3'] ); ?>;
}

.gst-dark-scheme,
.gst-entry-dark-scheme,
.gst-slider-dark-scheme {
	color: <?php echo esc_attr( $args['bp_color_8'] ); ?>;
}

.gst-p-brand-color {
	color: <?php echo esc_attr( $args['primary_brand'] ); ?>;
}

.gst-p-brand-bg {
	background-color: <?php echo esc_attr( $args['primary_brand'] ); ?>;
}

.gst-p-brand-border,
.widget .gst-menu.anchor .menu-item.current-menu-item > a {
	border-color: <?php echo esc_attr( $args['primary_brand'] ); ?>;
}

.gst-s-brand-color {
	color: <?php echo esc_attr( $args['secondary_brand'] ); ?>;
}

.gst-s-brand-bg {
	background-color: <?php echo esc_attr( $args['secondary_brand'] ); ?>;
}

.gst-s-brand-border {
	border-color: <?php echo esc_attr( $args['secondary_brand'] ); ?>;
}

.gst-p-brand-contrast-color {
	color: <?php echo esc_attr( $args['primary_brand_contrast'] ); ?>;
}

.gst-s-brand-contrast-color {
	color: <?php echo esc_attr( $args['secondary_brand_contrast'] ); ?>;
}

<?php
gastro_sch_css( array_merge( $args['base_sch_args'], array(
	'selector' => '.gst-p-text-contrast-color',
	'property' => array(
		'color' => 'primary_text_contrast'
	),
	'parent' => array( 'slider' )
) ) );
gastro_sch_css( array_merge( $args['base_sch_args'], array(
	'selector' => '.gst-s-text-contrast-color',
	'property' => array(
		'color' => 'secondary_text_contrast'
	),
	'parent' => array( 'slider' )
) ) );
gastro_sch_css( array_merge( $args['base_sch_args'], array(
	'selector' => '.gst-p-text-color',
	'property' => array(
		'color' => 'primary_text'
	),
	'parent' => array( 'slider', 'entry' )
) ) );
gastro_sch_css( array_merge( $args['base_sch_args'], array(
	'selector' => '.gst-p-text-bg',
	'property' => array(
		'background-color' => 'primary_text'
	),
	'parent' => array( 'slider', 'entry' )
) ) );
gastro_sch_css( array_merge( $args['base_sch_args'], array(
	'selector' => '.gst-s-text-color',
	'property' => array(
		'color' => 'secondary_text'
	),
	'parent' => array( 'slider', 'entry' )
) ) );
gastro_sch_css( array_merge( $args['base_sch_args'], array(
	'selector' => '.gst-s-text-bg',
	'property' => array(
		'background-color' => 'secondary_text'
	),
	'parent' => array( 'slider', 'entry' )
) ) );
gastro_sch_css( array_merge( $args['base_sch_args'], array(
	'selector' => '.gst-s-text-bg.gst-overlay',
	'property' => array(
		'background-color' => 'secondary_text'
	),
	'opacity' => 0.9,
	'parent' => array( 'slider', 'entry' )
) ) );
gastro_sch_css( array_merge( $args['base_sch_args'], array(
	'selector' => '.gst-s-text-border',
	'property' => array(
		'border-color' => 'secondary_text'
	),
	'parent' => array( 'slider', 'entry' )
) ) );
gastro_sch_css( array_merge( $args['base_sch_args'], array(
	'selector' => '.gst-p-bg-color',
	'property' => array(
		'color' => 'primary_background'
	),
	'parent' => array( 'slider', 'entry' )
) ) );
gastro_sch_css( array_merge( $args['base_sch_args'], array(
	'selector' => '.gst-p-bg-bg',
	'property' => array(
		'background-color' => 'primary_background'
	),
	'parent' => array( 'slider', 'entry' )
) ) );
gastro_sch_css( array_merge( $args['base_sch_args'], array(
	'selector' => '.gst-p-bg-border',
	'property' => array(
		'border-color' => 'primary_background'
	),
	'parent' => array( 'slider', 'entry' )
) ) );
gastro_sch_css( array_merge( $args['base_sch_args'], array(
	'selector' => '.gst-s-bg-color',
	'property' => array(
		'color' => 'secondary_background'
	),
	'parent' => array( 'slider', 'entry' )
) ) );
gastro_sch_css( array_merge( $args['base_sch_args'], array(
	'selector' => '.gst-s-bg-bg',
	'property' => array(
		'background-color' => 'secondary_background'
	),
	'parent' => array( 'slider', 'entry' )
) ) );
gastro_sch_css( array_merge( $args['base_sch_args'], array(
	'selector' => '.gst-s-bg-border',
	'property' => array(
		'border-color' => 'secondary_background'
	),
	'parent' => array( 'slider', 'entry' )
) ) );
gastro_sch_css( array_merge( $args['base_sch_args'], array(
	'selector' => '.gst-p-border-color',
	'property' => array(
		'color' => 'primary_border'
	),
	'parent' => array( 'slider', 'entry' )
) ) );
gastro_sch_css( array_merge( $args['base_sch_args'], array(
	'selector' => '.gst-p-border-bg',
	'property' => array(
		'background-color' => 'primary_border'
	),
	'parent' => array( 'slider', 'entry' )
) ) );
gastro_sch_css( array_merge( $args['base_sch_args'], array(
	'selector' => '.gst-p-border-border',
	'property' => array(
		'border-color' => 'primary_border'
	),
	'parent' => array( 'slider', 'entry', 'navbar-dropdown' ),
	'is_high_level' => true
) ) );
gastro_sch_css( array_merge( $args['base_sch_args'], array(
	'selector' => '.gst-overlay',
	'property' => array(
		'background-color' => 'primary_background'
	),
	'opacity' => 0.9,
	'parent' => array( 'slider', 'entry' )
) ) );
?>
<?php /*=========================*/?>

<?php /*=======Brand Button======*/?>
<?php
gastro_sch_css( array_merge( $args['base_sch_args'], array(
	'selector' => array(
		'.gst-button--border.gst-button-color--brand > .btnx',
		'.gst-button--fill.gst-button-hover--inverse > .btnx:hover'
	),
	'property' => array(
		'color' => 'primary_brand',
		'border-color' => 'primary_brand'
	),
	'parent' => 'slider'
) ) );
gastro_sch_css( array_merge( $args['base_sch_args'], array(
	'selector' => '.gst-button--border.gst-button-hover--brand > .btnx:hover',
	'property' => array(
		'color' => 'secondary_brand',
		'border-color' => 'secondary_brand'
	),
	'parent' => 'slider'
) ) );
gastro_sch_css( array_merge( $args['base_sch_args'], array(
	'selector' => array(
		'.gst-form-group input[type=submit]',
		'.gst-button--layer.gst-button-color--brand > .btnx',
		'.gst-button--fill.gst-button-color--brand > .btnx',
		'.gst-button--border.gst-button-hover--inverse > .btnx:hover'
	),
	'property' => array(
		'color' => 'primary_brand_contrast',
		'border-color' => 'primary_brand',
		'background-color' => 'primary_brand'
	),
	'parent' => 'slider'
) ) );
gastro_sch_css( array_merge( $args['base_sch_args'], array(
	'selector' => array(
		'.gst-form-group input[type=submit]:hover',
		'.gst-button--fill.gst-button-hover--brand > .btnx:hover'
	),
	'property' => array(
		'color' => 'secondary_brand_contrast',
		'border-color' => 'secondary_brand',
		'background-color' => 'secondary_brand'
	),
	'parent' => 'slider'
) ) );
?>
<?php /*=========================*/?>

<?php /*=======Basic Button======*/?>
<?php
gastro_sch_css( array_merge( $args['base_sch_args'], array(
	'selector' => array(
		'.gst-button-color--basic.gst-button--border > .btnx',
		'.gst-button-color--basic.gst-button--fill.gst-button-hover--inverse > .btnx:hover'
	),
	'property' => array(
		'color' => 'secondary_text',
		'border-color' => 'secondary_text'
	),
	'parent' => 'slider'
) ) );
gastro_sch_css( array_merge( $args['base_sch_args'], array(
	'selector' => array(
		'.gst-button-color--basic.gst-button--layer > .btnx',
		'.gst-button-color--basic.gst-button--fill > .btnx',
		'.gst-button-color--basic.gst-button--border.gst-button-hover--inverse > .btnx:hover',
	),
	'property' => array(
		'color' => 'secondary_text_contrast',
		'border-color' => 'secondary_text',
		'background-color' => 'secondary_text'
	),
	'parent' => 'slider'
) ) );
gastro_sch_css( array_merge( $args['base_sch_args'], array(
	'selector' => '.gst-button-color--basic.gst-button--border.gst-button-hover--brand > .btnx:hover',
	'property' => array(
		'color' => 'primary_brand',
		'border-color' => 'primary_brand',
	),
	'parent' => 'slider'
) ) );
gastro_sch_css( array_merge( $args['base_sch_args'], array(
	'selector' => '.gst-button-color--basic.gst-button--fill.gst-button-hover--brand > .btnx:hover',
	'property' => array(
		'color' => 'primary_brand_contrast',
		'border-color' => 'primary_brand',
		'background-color' => 'primary_brand'
	),
	'parent' => 'slider'
) ) );
?>
<?php /*=========================*/?>

<?php /*=====Tab & Accordion=====*/?>
.gst-tab--button .gst-tab-nav .active {
	background-color: <?php echo esc_attr( $args['primary_brand'] ); ?>;
}

.gst-tab--button .gst-tab-nav .active > .gst-tab-nav-title {
	color: <?php echo esc_attr( $args['primary_brand_contrast'] ); ?>;
}
<?php /*=========================*/?>

<?php /*=========Widgets=========*/?>
<?php
gastro_sch_css( array_merge( $args['base_sch_args'], array(
	'selector' => array(
		'.widget .tagcloud a',
		'.widget_calendar tbody',
		'.widget a .gst-widget-meta',
		'.widget a .gst-widget-category'
	),
	'property' => array(
		'color' => 'primary_text'
	)
) ) );
gastro_sch_css( array_merge( $args['base_sch_args'], array(
	'selector' => array(
		'.widget a',
		'.widget.widget_calendar a',
		'.widget_calendar thead'
	),
	'property' => array(
		'color' => 'secondary_text'
	)
) ) );
gastro_sch_css( array_merge( $args['base_sch_args'], array(
	'selector' => array(
		'.widget a:hover',
		'.widget.widget_calendar tfoot a',
		'.widget .gst-widget-viewall',
		'.gst-widget-feature a'
	),
	'property' => array(
		'color' => 'primary_brand'
	)
) ) );
gastro_sch_css( array_merge( $args['base_sch_args'], array(
	'selector' => array(
		'.widget .gst-widget-viewall:hover',
		'.gst-widget-feature a:hover'
	),
	'property' => array(
		'color' => 'secondary_brand'
	)
) ) );
gastro_sch_css( array_merge( $args['base_sch_args'], array(
	'selector' => '.tagcloud a',
	'property' => array(
		'background-color' => 'primary_background'
	)
) ) );
gastro_sch_css( array_merge( $args['base_sch_args'], array(
	'selector' => '.widget_calendar #today',
	'property' => array(
		'color' => 'primary_brand_contrast',
		'background-color' => 'primary_brand'
	)
) ) );
gastro_sch_css( array_merge( $args['base_sch_args'], array(
	'selector' => array( '.tagcloud a', '.widget_calendar caption' ),
	'property' => array(
		'border-color' => 'primary_border'
	)
) ) );
?>
<?php /*=========================*/?>

<?php /*==========Entry==========*/?>
<?php $brand_shadow_color = gastro_hex_to_rgba( $args['primary_brand'], 0.8 ); ?>
.gst-entries--layer .gst-entry-inner {
	box-shadow: 5px 5px 0px 0px <?php echo esc_attr( $brand_shadow_color ); ?>;
}

<?php
gastro_sch_css( array_merge( $args['base_sch_args'], array(
	'selector' => '.gst-entry-flash span.onsale',
	'property' => array(
		'color' => 'secondary_text_contrast'
	)
) ) );
gastro_sch_css( array_merge( $args['base_sch_args'], array(
	'selector' => array(
		'.gst-entry-title a:hover',
		'.gst-entry-meta a:hover',
		'.gst-relatedpost .gst-entry:hover .gst-entry-title'
	),
	'property' => array(
		'color' => 'primary_brand'
	)
) ) );
?>

.gst-entry .more-link {
	color: <?php echo esc_attr( $args['primary_brand'] ); ?>;
}

.gst-entry .more-link:hover {
	color: <?php echo esc_attr( $args['secondary_brand'] ); ?>;
}
<?php /*=========================*/?>

<?php /*========Utilities========*/?>
<?php
gastro_sch_css( array_merge( $args['base_sch_args'], array(
	'selector' => '.slick-dots li.slick-active button:before',
	'property' => array(
		'color' => 'secondary_text'
	),
	'parent' => 'slider'
) ) );
gastro_sch_css( array_merge( $args['base_sch_args'], array(
	'selector' => '.gst-carousel-arrow',
	'property' => array(
		'color' => 'secondary_text_contrast',
		'background-color' => 'secondary_text',
	),
	'opacity' => 0.7,
	'parent' => 'slider'
) ) );
gastro_sch_css( array_merge( $args['base_sch_args'], array(
	'selector' => '.gst-carousel-arrow.transparent',
	'property' => array(
		'color' => 'secondary_text'
	),
	'parent' => 'slider'
) ) );
gastro_sch_css( array_merge( $args['base_sch_args'], array(
	'selector' => array(
		'.mCSB_scrollTools .mCSB_dragger .mCSB_dragger_bar',
		'.mCSB_scrollTools .mCSB_dragger:hover .mCSB_dragger_bar',
		'.mCSB_scrollTools .mCSB_dragger.mCSB_dragger_onDrag .mCSB_dragger_bar'
	),
	'property' => array(
		'background-color' => 'primary_border',
	)
) ) );
gastro_sch_css( array_merge( $args['base_sch_args'], array(
	'selector' => array(
		'.gst-pagination .page-numbers',
		'.gst-filter-list > .active',
		'.gst-filter-list > a:focus'
	),
	'property' => array(
		'color' => 'secondary_text',
	)
) ) );
gastro_sch_css( array_merge( $args['base_sch_args'], array(
	'selector' => array(
		'.gst-filter-list > .active:after',
		'.page-numbers.current:after'
	),
	'property' => array(
		'background-color' => 'secondary_text',
	)
) ) );
gastro_sch_css( array_merge( $args['base_sch_args'], array(
	'selector' => array(
		'.gst-button--fill .btnx .gst-bounce',
		'.gst-button--border.gst-button-hover--inverse .btnx:hover .gst-bounce'
	),
	'property' => array(
		'background-color' => 'primary_brand_contrast',
	)
) ) );
gastro_sch_css( array_merge( $args['base_sch_args'], array(
	'selector' => '.with-border',
	'property' => array(
		'border-color' => 'primary_border',
	)
) ) );
?>
<?php /*=========================*/?>

<?php /*=======Navbar Menu=======*/?>
<?php
gastro_sch_css( array_merge( $args['base_sch_args'], array(
	'selector' => array(
		'.gst-menu a',
		'.gst-extra-menu a',
		'.gst-navbar-brand',
		'.gst-navbar-social .gst-icon-normal',
	),
	'property' => array(
		'color' => 'secondary_text',
	)
) ) );
gastro_sch_css( array_merge( $args['base_sch_args'], array(
	'selector' => array(
		'.gst-navbar-social .gst-icon-normal.gst-icon-fill',
		'.gst-navbar-social .gst-icon-normal.gst-icon-fill-square'
	),
	'property' => array(
		'background-color' => 'secondary_text',
	)
) ) );
gastro_sch_css( array_merge( $args['base_sch_args'], array(
	'selector' => array(
		'.gst-navbar-social .gst-icon-normal.gst-icon-fill',
		'.gst-navbar-social .gst-icon-normal.gst-icon-fill-square'
	),
	'property' => array(
		'color' => 'secondary_text_contrast',
	)
) ) );
gastro_sch_css( array_merge( $args['base_sch_args'], array(
	'selector' => array(
		'.sub-menu a',
		'.gst-cart-box'
	),
	'property' => array(
		'color' => 'primary_text',
	),
	'parent' => 'navbar-dropdown'
) ) );
gastro_sch_css( array_merge( $args['base_sch_args'], array(
	'selector' => array(
		'strong',
		'.gst-mega-menu-title a',
		'.gst-cart-box a',
		'.mini_cart_item > *'
	),
	'property' => array(
		'color' => 'secondary_text',
	),
	'parent' => 'navbar-dropdown'
) ) );
gastro_sch_css( array_merge( $args['base_sch_args'], array(
	'selector' => array(
		'.gst-menu a:hover',
		'.gst-extra-menu a:hover',
		'.gst-navbar-social .gst-icon-hover',
		'.current-menu-item > a',
		'.current-menu-parent > a',
		'.current-menu-ancestor > a'
	),
	'property' => array(
		'color' => 'primary_brand',
	)
) ) );
gastro_sch_css( array_merge( $args['base_sch_args'], array(
	'selector' => '.gst-mega-menu-title a:hover',
	'property' => array(
		'color' => 'secondary_text'
	),
	'parent' => 'navbar-dropdown'
) ) );
gastro_sch_css( array_merge( $args['base_sch_args'], array(
	'selector' => '.gst-topnav-menu .sub-menu a:hover',
	'property' => array(
		'color' => 'secondary_text',
		'background-color' => 'primary_background'
	),
	'parent' => 'navbar-dropdown'
) ) );
gastro_sch_css( array_merge( $args['base_sch_args'], array(
	'selector' => array(
		'.gst-navbar-social .gst-icon-hover.gst-icon-fill',
		'.gst-navbar-social .gst-icon-hover.gst-icon-fill-square'
	),
	'property' => array(
		'background-color' => 'primary_brand',
	)
) ) );
gastro_sch_css( array_merge( $args['base_sch_args'], array(
	'selector' => array(
		'.gst-navbar-social .gst-icon-hover.gst-icon-fill',
		'.gst-navbar-social .gst-icon-hover.gst-icon-fill-square'
	),
	'property' => array(
		'color' => 'primary_brand_contrast',
	)
) ) );
gastro_sch_css( array_merge( $args['base_sch_args'], array(
	'selector' => ".gst-navbar-search .gst-search-form input[type='text']",
	'property' => array(
		'border-color' => 'primary_border',
	)
) ) );
?>

.gst-highlight-border .gst-topnav-menu > li > a:hover:after,
.gst-highlight-border .gst-sidenav-menu a:hover:after {
	background-color: <?php echo esc_attr( $args['primary_brand'] ); ?>;
}

.gst-navbar-search .gst-search-form input[type='text']:focus {
	border-color: <?php echo esc_attr( $args['primary_brand'] ); ?> !important;
}

<?php
gastro_sch_css( array_merge( $args['base_sch_args'], array(
	'selector' => array(
		'.gst-lines',
		'.gst-lines:before',
		'.gst-lines:after'
	),
	'property' => array(
		'background-color' => 'secondary_text',
	)
) ) );
?>
<?php /*=========================*/?>

<?php /*=======Single Post=======*/?>
.gst-post-tag a:hover {
	color: <?php echo esc_attr( $args['secondary_text'] ); ?>;
}

.gst-post-featured--overlap .gst-main {
	background-color: <?php echo esc_attr( $args['primary_background'] ); ?>;
}
<?php /*=========================*/?>

<?php /*======Page Pre-load======*/?>
.gst-loading--fading-circle .gst-circle:before {
	background-color: <?php echo esc_attr( $args['primary_brand'] ); ?>;
}
<?php /*=========================*/?>

<?php /*====Unsolvable Color=====*/?>
<?php if ( 'light' === $args['default_color_scheme'] ) : ?>
select {
	background-image: url("data:image/svg+xml;utf8,<svg fill='black' height='20' viewBox='0 0 24 24' width='20' xmlns='https://www.w3.org/2000/svg'><path d='M7 10l5 5 5-5z'/><path d='M0 0h24v24H0z' fill='none'/></svg>");
}

.gst-dark-scheme select {
	background-image: url("data:image/svg+xml;utf8,<svg fill='white' height='20' viewBox='0 0 24 24' width='20' xmlns='https://www.w3.org/2000/svg'><path d='M7 10l5 5 5-5z'/><path d='M0 0h24v24H0z' fill='none'/></svg>");
}

.gst-light-scheme select {
	background-image: url("data:image/svg+xml;utf8,<svg fill='black' height='20' viewBox='0 0 24 24' width='20' xmlns='https://www.w3.org/2000/svg'><path d='M7 10l5 5 5-5z'/><path d='M0 0h24v24H0z' fill='none'/></svg>");
}

.gst-button--layer > .btnx {
	box-shadow: 4px 4px #e4e4e4;
}

.gst-dark-scheme .gst-button--layer > .btnx,
.gst-slider-dark-scheme .gst-button--layer > .btnx {
	box-shadow: 4px 4px #454545;
}

.gst-light-scheme .gst-button--layer > .btnx,
.gst-slider-light-scheme .gst-button--layer > .btnx {
	box-shadow: 4px 4px #e4e4e4;
}

<?php if ( 'layer' === $args['button_style'] ) : ?>
	button,
	input[type=button],
	input[type=submit],
	.woocommerce #respond input#submit,
	.woocommerce a.button,
	.woocommerce button.button,
	.woocommerce input.button {
		box-shadow: 4px 4px #e4e4e4;
	}

	.gst-dark-scheme button,
	.gst-dark-scheme input[type=button],
	.gst-dark-scheme input[type=submit],
	.gst-dark-scheme #respond input#submit,
	.gst-dark-scheme a.button,
	.gst-dark-scheme button.button,
 	.gst-dark-schemeinput.button {
		box-shadow: 4px 4px #454545;
	}

	.gst-light-scheme button,
	.gst-light-scheme input[type=button],
	.gst-light-scheme input[type=submit],
	.gst-light-scheme #respond input#submit,
	.gst-light-scheme a.button,
	.gst-light-scheme button.button,
	.gst-light-scheme input.button {
		box-shadow: 4px 4px #e4e4e4;
	}
<?php endif; ?>

.slick-dots li button:before {
	color: #ccc;
}

.gst-dark-scheme .slick-dots li button:before,
.gst-slider-dark-scheme .slick-dots li button:before {
	color: #999;
}

.gst-light-scheme .slick-dots li button:before,
.gst-slider-light-scheme .slick-dots li button:before {
	color: #ccc;
}

.gst-pricing-item.highlighted {
	box-shadow: 0 2px 8px rgba(0, 0, 0, 0.15);
}

.gst-dark-scheme .gst-pricing-item.highlighted {
	box-shadow: 0 2px 8px rgba(0, 0, 0, 0.4);
}

.gst-light-scheme .gst-pricing-item.highlighted {
	box-shadow: 0 2px 8px rgba(0, 0, 0, 0.15);
}

.tagcloud a:hover {
	box-shadow: 0 2px 6px rgba(0,0,0, 0.05);
}

.gst-dark-scheme .tagcloud a:hover {
	box-shadow: 0 2px 6px rgba(0,0,0, 0.3);
}

.gst-light-scheme .tagcloud a:hover {
	box-shadow: 0 2px 6px rgba(0,0,0, 0.05);
}

.gst-with-shadow {
	box-shadow: 0 2px 8px rgba(0, 0, 0, 0.15);
}

.gst-dark-scheme .gst-with-shadow {
	box-shadow: 0 2px 8px rgba(0, 0, 0, 0.3);
}

.gst-light-scheme .gst-with-shadow {
	box-shadow: 0 2px 8px rgba(0, 0, 0, 0.15);
}

.gst-with-hover-shadow:hover {
	box-shadow: 0 10px 24px rgba(0, 0, 0, 0.1);
}

.gst-dark-scheme .gst-with-hover-shadow:hover {
	box-shadow: 0 10px 24px rgba(0, 0, 0, 0.2);
}

.gst-light-scheme .gst-with-hover-shadow:hover {
	box-shadow: 0 10px 24px rgba(0, 0, 0, 0.1);
}

.woocommerce input.button:hover,
.woocommerce input.button:focus {
	background-color: #eee;
	border-color: #eee;
}
<?php else : ?>
select {
	background-image: url("data:image/svg+xml;utf8,<svg fill='white' height='20' viewBox='0 0 24 24' width='20' xmlns='https://www.w3.org/2000/svg'><path d='M7 10l5 5 5-5z'/><path d='M0 0h24v24H0z' fill='none'/></svg>");
}

.gst-light-scheme select {
	background-image: url("data:image/svg+xml;utf8,<svg fill='white' height='20' viewBox='0 0 24 24' width='20' xmlns='https://www.w3.org/2000/svg'><path d='M7 10l5 5 5-5z'/><path d='M0 0h24v24H0z' fill='none'/></svg>");
}

.gst-dark-scheme select {
	background-image: url("data:image/svg+xml;utf8,<svg fill='black' height='20' viewBox='0 0 24 24' width='20' xmlns='https://www.w3.org/2000/svg'><path d='M7 10l5 5 5-5z'/><path d='M0 0h24v24H0z' fill='none'/></svg>");
}

.gst-button--layer > .btnx {
	box-shadow: 4px 4px #454545;
}

.gst-light-scheme .gst-button--layer > .btnx,
.gst-slider-light-scheme .gst-button--layer > .btnx {
	box-shadow: 4px 4px #e4e4e4;
}

.gst-dark-scheme .gst-button--layer > .btnx,
.gst-slider-dark-scheme .gst-button--layer > .btnx {
	box-shadow: 4px 4px #454545;
}

<?php if ( 'layer' === $args['button_style'] ) : ?>
	button,
	input[type=button],
	input[type=submit],
	.woocommerce #respond input#submit,
	.woocommerce a.button,
	.woocommerce button.button,
	.woocommerce input.button {
		box-shadow: 4px 4px #454545;
	}

	.gst-light-scheme button,
	.gst-light-scheme input[type=button],
	.gst-light-scheme input[type=submit],
	.gst-light-scheme #respond input#submit,
	.gst-light-scheme a.button,
	.gst-light-scheme button.button,
	.gst-light-scheme input.button {
		box-shadow: 4px 4px #e4e4e4;
	}

	.gst-dark-scheme button,
	.gst-dark-scheme input[type=button],
	.gst-dark-scheme input[type=submit],
	.gst-dark-scheme #respond input#submit,
	.gst-dark-scheme a.button,
	.gst-dark-scheme button.button,
	.gst-dark-scheme input.button {
		box-shadow: 4px 4px #454545;
	}
<?php endif; ?>

.slick-dots li button:before {
	color: #999;
}

.gst-light-scheme .slick-dots li button:before,
.gst-slider-light-scheme .slick-dots li button:before {
	color: #ccc;
}

.gst-dark-scheme .slick-dots li button:before,
.gst-slider-dark-scheme .slick-dots li button:before {
	color: #999;
}

.gst-pricing-item.highlighted {
	box-shadow: 0 2px 8px rgba(0, 0, 0, 0.4);
}

.gst-light-scheme .gst-pricing-item.highlighted {
	box-shadow: 0 2px 8px rgba(0, 0, 0, 0.15);
}

.gst-dark-scheme .gst-pricing-item.highlighted {
	box-shadow: 0 2px 8px rgba(0, 0, 0, 0.4);
}

.tagcloud a:hover {
	box-shadow: 0 2px 6px rgba(0,0,0, 0.3);
}

.gst-light-scheme .tagcloud a:hover {
	box-shadow: 0 2px 6px rgba(0,0,0, 0.05);
}

.gst-dark-scheme .tagcloud a:hover {
	box-shadow: 0 2px 6px rgba(0,0,0, 0.3);
}

.gst-with-shadow {
	box-shadow: 0 2px 8px rgba(0, 0, 0, 0.3);
}

.gst-light-scheme .gst-with-shadow {
	box-shadow: 0 2px 8px rgba(0, 0, 0, 0.15);
}

.gst-dark-scheme .gst-with-shadow {
	box-shadow: 0 2px 8px rgba(0, 0, 0, 0.3);
}

.gst-with-hover-shadow:hover {
	box-shadow: 0 10px 24px rgba(0, 0, 0, 0.2);
}

.gst-light-scheme .gst-with-hover-shadow:hover {
	box-shadow: 0 10px 24px rgba(0, 0, 0, 0.1);
}

.gst-dark-scheme .gst-with-hover-shadow:hover {
	box-shadow: 0 10px 24px rgba(0, 0, 0, 0.2);
}

.woocommerce input.button:hover,
.woocommerce input.button:focus {
	background-color: #111;
	border-color: #111;
}
<?php endif; ?>
<?php /*=========================*/?>

<?php /*===WooCommerce Color=====*/?>
<?php if ( gastro_is_woocommerce_activated() ) : ?>

<?php /*======Shop Dropdown======*/?>
.gst-dropdown-menu li.active a {
	color: <?php echo esc_attr( $args['primary_brand'] ); ?>;
}

.gst-dropdown-menu li:hover a {
	color: <?php echo esc_attr( $args['primary_brand_contrast'] ); ?>;
}

.gst-dropdown-menu li:hover {
	background-color: <?php echo esc_attr( $args['primary_brand'] ); ?>;
}

.gst-dropdown-display:focus,
.gst-dropdown-display:hover {
	color: <?php echo esc_attr( $args['secondary_text'] ); ?>;
}
<?php /*=========================*/?>

.woocommerce .woocommerce-breadcrumb,
.woocommerce .woocommerce-title,
.woocommerce table.shop_attributes th,
.woocommerce [itemprop="author"],
.woocommerce .group_table .label a,
.woocommerce nav.woocommerce-pagination .page-numbers,
.woocommerce nav.woocommerce-pagination .page-numbers.current,
.woocommerce nav.woocommerce-pagination .page-numbers:hover,
.select2-container .select2-choice,
.woocommerce table.shop_table td,
.woocommerce table.cart .product-name a,
.woocommerce table.woocommerce-checkout-review-order-table td.product-name,
.woocommerce table.woocommerce-checkout-review-order-table td.product-total,
.woocommerce table.woocommerce-checkout-review-order-table .order-total th,
.woocommerce table.woocommerce-checkout-review-order-table .order-total td,
.woocommerce .cart-collaterals .cart_totals table .order-total th,
.woocommerce-page .cart-collaterals .cart_totals table .order-total th,
.woocommerce-account .myaccount_user,
.woocommerce-account fieldset {
	color: <?php echo esc_attr( $args['secondary_text'] ); ?>;
}

<?php
gastro_sch_css( array_merge( $args['base_sch_args'], array(
	'selector' => array(
		'.woocommerce-variation-price',
		'.woocommerce div.product .product_title',
		'.woocommerce div.product p.price',
		'.woocommerce div.product span.price',
		'.woocommerce div.product p.price ins',
		'.woocommerce div.product span.price ins',
		'.woocommerce input.button:hover',
		'.woocommerce input.button:focus'
	),
	'property' => array(
		'color' => 'secondary_text',
	)
) ) );
gastro_sch_css( array_merge( $args['base_sch_args'], array(
	'selector' => array(
		'.widget_shopping_cart .total',
		'.woocommerce.widget_shopping_cart .total',
		'.woocommerce .widget_shopping_cart .total',
	),
	'property' => array(
		'color' => 'secondary_text',
	),
	'parent' => 'navbar-dropdown'
) ) );
?>

.woocommerce .woocommerce-breadcrumb a,
.woocommerce .woocommerce-breadcrumb span,
.woocommerce table.shop_table th,
.woocommerce table.woocommerce-checkout-review-order-table td,
.woocommerce table.woocommerce-checkout-review-order-table .shipping td label {
	color: <?php echo esc_attr( $args['primary_text'] ); ?>;
}

.select2-results li:hover {
	color: <?php echo esc_attr( $args['primary_brand_contrast'] ); ?>;
}

<?php
		gastro_sch_css( array_merge( $args['base_sch_args'], array(
				'selector' => '.woocommerce span.onsale',
				'property' => array(
						'color' => 'secondary_text_contrast',
				)
		) ) );
?>

.woocommerce .group_table .label a:hover,
.woocommerce .comment-form-rating p.stars a:before,
.woocommerce table.cart .product-name a:hover,
.woocommerce .addresses .edit {
	color: <?php echo esc_attr( $args['primary_brand'] ); ?>;
}

<?php
gastro_sch_css( array_merge( $args['base_sch_args'], array(
	'selector' => '.woocommerce .star-rating span',
	'property' => array(
		'color' => 'primary_brand',
	)
) ) );
?>

.woocommerce .addresses .edit:hover {
	color: <?php echo esc_attr( $args['secondary_brand'] ); ?>;
}

.woocommerce-cart .wc-proceed-to-checkout a.checkout-button.alt,
.woocommerce #add_payment_method #payment .button,
.woocommerce .woocommerce-checkout #payment .button,
.woocommerce-cart .wc-proceed-to-checkout a.checkout-button.alt:hover,
.woocommerce #add_payment_method #payment .button:hover,
.woocommerce .woocommerce-checkout #payment .button:hover,
.woocommerce-cart .wc-proceed-to-checkout a.checkout-button.alt:focus,
.woocommerce #add_payment_method #payment .button:focus,
.woocommerce .woocommerce-checkout #payment .button:focus {
	color: <?php echo esc_attr( $args['secondary_text_contrast'] ); ?>;
	background-color: <?php echo esc_attr( $args['secondary_text'] ); ?>;
	border-color: <?php echo esc_attr( $args['secondary_text'] ); ?>;
}

.woocommerce .select2-results {
	background-color: <?php echo esc_attr( $args['primary_background'] ); ?>;
}

<?php
gastro_sch_css( array_merge( $args['base_sch_args'], array(
	'selector' => '.woocommerce .widget_price_filter .ui-slider .ui-slider-handle',
	'property' => array(
		'background-color' => 'primary_background',
	)
) ) );
?>

.woocommerce table.shop_attributes th,
.woocommerce table.shop_attributes td,
.woocommerce .cart-collaterals .cart_totals,
.woocommerce-page .cart-collaterals .cart_totals,
.woocommerce-checkout .col-2,
.woocommerce form.login,
.woocommerce form.register {
	background-color: <?php echo esc_attr( $args['secondary_background'] ); ?>;
}

<?php
gastro_sch_css( array_merge( $args['base_sch_args'], array(
	'selector' => array(
		'.woocommerce input.button',
		'.woocommerce .widget_price_filter .price_slider_wrapper .ui-widget-content',
		'.woocommerce .addresses > div'
	),
	'property' => array(
		'background-color' => 'secondary_background',
	)
) ) );
?>

.woocommerce-error,
.woocommerce-info,
.woocommerce-message {
	background-color: transparent;
	border: 1px solid <?php echo esc_attr( $args['primary_border'] ); ?>;
}

.select2-results li:hover {
	background-color: <?php echo esc_attr( $args['primary_brand'] ); ?>;
}

<?php
gastro_sch_css( array_merge( $args['base_sch_args'], array(
	'selector' => '.woocommerce .widget_price_filter .ui-slider .ui-slider-range',
	'property' => array(
		'background-color' => 'primary_brand',
	)
) ) );
?>

.woocommerce #reviews #comments ol.commentlist li,
.select2-container .select2-choice,
.select2-container-active .select2-choice,
.select2-search,
.select2-drop-active,
.woocommerce table.shop_table td,
.woocommerce-cart table.cart td.actions .coupon .input-text,
.woocommerce form.checkout_coupon,
.woocommerce form.login h3,
.woocommerce form.register h3,
.woocommerce .addresses header,
.woocommerce .cart-collaterals .cart_totals tr td,
.woocommerce .cart-collaterals .cart_totals tr th,
.woocommerce-page .cart-collaterals .cart_totals tr td,
.woocommerce-page .cart-collaterals .cart_totals tr th {
	border-color: <?php echo esc_attr( $args['primary_border'] ); ?>;
}

<?php
gastro_sch_css( array_merge( $args['base_sch_args'], array(
	'selector' => array(
		'.woocommerce input.button',
		'.widget_shopping_cart .total',
		'.woocommerce.widget_shopping_cart .total',
		'.woocommerce .widget_shopping_cart .total',
		'.woocommerce .widget_price_filter .ui-slider .ui-slider-handle'
	),
	'property' => array(
		'border-color' => 'primary_border',
	)
) ) );
?>

.variations-radio input[type="radio"]:checked + .variations-radio-option,
.woocommerce-cart table.cart td.actions .coupon .input-text:focus,
.woocommerce-MyAccount-navigation .woocommerce-MyAccount-navigation-link.is-active {
	border-color: <?php echo esc_attr( $args['primary_brand'] ); ?>;
}

<?php /*===WooCommerce Button====*/?>
<?php if ( 'layer' === $args['button_style'] ) : ?>
	.woocommerce #respond input#submit,
	.woocommerce a.button,
	.woocommerce button.button,
	.woocommerce input.button {
		-ms-transform: translate(0, 0);
		-webkit-transform: translate(0, 0);
		transform: translate(0, 0);
	}
	.woocommerce #respond input#submit:hover,
	.woocommerce a.button:hover,
	.woocommerce button.button:hover,
	.woocommerce input.button:hover {
		-ms-transform: translate(4px, 4px);
		-webkit-transform: translate(4px, 4px);
		transform: translate(4px, 4px);
	}
	.woocommerce #respond input#submit:hover,
	.woocommerce a.button:hover,
	.woocommerce button.button:hover,
	.woocommerce input.button:hover {
		box-shadow: 0 0 transparent !important;
	}

	.woocommerce #respond input#submit.alt:hover,
	.woocommerce a.button.alt:hover,
	.woocommerce button.button.alt:hover,
	.woocommerce input.button.alt:hover {
		color: <?php echo esc_attr( $args['button_text_color'] ); ?>;
		background-color: <?php echo esc_attr( $args['button_background_color'] ); ?>;
		border-color: <?php echo esc_attr( $args['button_border_color'] ); ?>;
	}
<?php else : ?>
	.woocommerce #respond input#submit.alt:hover,
	.woocommerce a.button.alt:hover,
	.woocommerce button.button.alt:hover,
	.woocommerce #respond input#submit:hover,
	.woocommerce a.button:hover,
	.woocommerce button.button:hover,
	.woocommerce .added_to_cart:hover,
	.woocommerce-account input.button:hover {
		color: <?php echo esc_attr( $args['button_text_hover_color'] ); ?>;
		background-color: <?php echo esc_attr( $args['button_background_hover_color'] ); ?>;
		border-color: <?php echo esc_attr( $args['button_border_hover_color'] ); ?>;
	}
<?php endif; ?>

.woocommerce #respond input#submit.alt,
.woocommerce a.button.alt,
.woocommerce button.button.alt,
.woocommerce #respond input#submit,
.woocommerce a.button,
.woocommerce button.button,
.woocommerce .added_to_cart,
.woocommerce-account input.button {
	<?php echo gastro_s( $fonts[$args['button_typography']], false ); ?>
	border-radius: <?php echo esc_attr( $args['button_radius'] ); ?>px;
	border-width: <?php echo esc_attr( $args['button_border'] ); ?>px;
	border-color: <?php echo esc_attr( $args['button_border_color'] ); ?>;
	color: <?php echo esc_attr( $args['button_text_color'] ); ?>;
	background-color: <?php echo esc_attr( $args['button_background_color'] ); ?>;
<?php if ( $args['button_uppercase'] ) : ?>
	text-transform: uppercase;
<?php else : ?>
	text-transform: none;
<?php endif; ?>
}

.woocommerce input.button {
	<?php echo gastro_s( $fonts[$args['button_typography']], false ); ?>
	border-radius: <?php echo esc_attr( $args['button_radius'] ); ?>px;
	border-width: <?php echo esc_attr( $args['button_border'] ); ?>px;
<?php if ( $args['button_uppercase'] ) : ?>
	text-transform: uppercase;
<?php else : ?>
	text-transform: none;
<?php endif; ?>
}

.woocommerce #respond input#submit.alt.disabled,
.woocommerce #respond input#submit.alt.disabled:hover,
.woocommerce #respond input#submit.alt:disabled,
.woocommerce #respond input#submit.alt:disabled:hover,
.woocommerce #respond input#submit.alt:disabled[disabled],
.woocommerce #respond input#submit.alt:disabled[disabled]:hover,
.woocommerce a.button.alt.disabled, .woocommerce a.button.alt.disabled:hover,
.woocommerce a.button.alt:disabled, .woocommerce a.button.alt:disabled:hover,
.woocommerce a.button.alt:disabled[disabled],
.woocommerce a.button.alt:disabled[disabled]:hover,
.woocommerce button.button.alt.disabled,
.woocommerce button.button.alt.disabled:hover,
.woocommerce button.button.alt:disabled,
.woocommerce button.button.alt:disabled:hover,
.woocommerce button.button.alt:disabled[disabled],
.woocommerce button.button.alt:disabled[disabled]:hover,
.woocommerce input.button.alt.disabled,
.woocommerce input.button.alt.disabled:hover,
.woocommerce input.button.alt:disabled,
.woocommerce input.button.alt:disabled:hover,
.woocommerce input.button.alt:disabled[disabled],
.woocommerce input.button.alt:disabled[disabled]:hover {
	color: <?php echo esc_attr( $args['button_text_color'] ); ?>;
	background-color: <?php echo esc_attr( $args['button_background_color'] ); ?>;
}
<?php /*=========================*/?>

<?php /*==Product Detail Width==*/?>
.woocommerce #content div.product div.summary,
.woocommerce div.product div.summary,
.woocommerce-page #content div.product div.summary,
.woocommerce-page div.product div.summary {
	width: <?php echo esc_attr( $args['product_detail_width'] - 2 ); ?>%;
}

.woocommerce #content div.product div.images,
.woocommerce div.product div.images,
.woocommerce-page #content div.product div.images,
.woocommerce-page div.product div.images {
	width: <?php echo esc_attr( 96 - $args['product_detail_width'] ); ?>%;
}

@media only screen and (max-width: 768px) {
	.woocommerce #content div.product div.summary,
	.woocommerce div.product div.summary,
	.woocommerce-page #content div.product div.summary,
	.woocommerce-page div.product div.summary,
	.woocommerce #content div.product div.images,
	.woocommerce div.product div.images,
	.woocommerce-page #content div.product div.images,
	.woocommerce-page div.product div.images {
		width: 100%;
	}
}
<?php /*=========================*/?>

<?php /*=== Shop spacing ===*/?>
.gst-main > .gst-shop .gst-entries-content {
	margin-right: <?php echo esc_attr( -$args['shop_spacing']/2 ); ?>px;
	margin-left: <?php echo esc_attr( -$args['shop_spacing']/2 ); ?>px;
}

.gst-main > .gst-shop .gst-entry {
	padding-right: <?php echo esc_attr( $args['shop_spacing']/2 ); ?>px;
	padding-left: <?php echo esc_attr( $args['shop_spacing']/2 ); ?>px;
	margin-bottom: <?php echo esc_attr( $args['shop_spacing']); ?>px;
}
<?php /*=========================*/?>
<?php endif; //end WooCommerce activate condition ?>
<?php /*=========================*/?>

<?php /*=======Navbar Scheme=====*/?>
<?php
	$light_menu_color = $args['bp_color_4'];
	$light_menu_contrast_color = gastro_contrast_color( $args['bp_color_4'] );
	$dark_menu_color = $args['bp_color_9'];
	$dark_menu_contrast_color = gastro_contrast_color( $args['bp_color_9'] );

	if ( 'light' === $args['navbar_color_scheme'] ) {
		$navbar_menu_color = $light_menu_color;
	} elseif ( 'dark' === $args['navbar_color_scheme'] ) {
		$navbar_menu_color = $dark_menu_color;
	}
?>

.gst-navbar-inner.gst-light-scheme,
nav.gst-navbar--alternate > .gst-light-scheme {
	background-color: <?php echo esc_attr( $args['bp_color_5'] ); ?>;
	border-color: <?php echo esc_attr( $args['bp_color_7'] ); ?>;
}

.gst-navbar-inner.gst-dark-scheme,
nav.gst-navbar--alternate > .gst-dark-scheme {
	background-color: <?php echo esc_attr( $args['bp_color_10'] ); ?>;
	border-color: <?php echo esc_attr( $args['bp_color_12'] ); ?>;
}

nav.gst-navbar--alternate .gst-topnav-menu > .menu-item > a,
nav.gst-navbar--alternate .gst-extra-menu a,
nav.gst-navbar--alternate .gst-navbar-social .gst-icon-normal {
	color: <?php echo esc_attr( $navbar_menu_color ); ?>;
}

nav.gst-navbar--alternate .gst-navbar-social .gst-icon-normal.gst-icon-fill,
nav.gst-navbar--alternate .gst-navbar-social .gst-icon-normal.gst-icon-fill-square {
	background-color: <?php echo esc_attr( $navbar_menu_color ); ?>;
	color: <?php echo esc_attr( gastro_contrast_color( $navbar_menu_color ) ); ?>;
}

nav.gst-navbar--alternate .gst-lines,
nav.gst-navbar--alternate .gst-lines:before,
nav.gst-navbar--alternate .gst-lines:after {
	background-color: <?php echo esc_attr( $navbar_menu_color ); ?>;
}
<?php /*=========================*/?>

<?php /*=======Navbar Light Scheme=====*/?>
.gst-navbar--light .gst-navbar-inner,
.gst-navbar--alternate.gst-navbar--light.fixed-transparent .gst-navbar-inner {
	border-color: <?php echo esc_attr( $args['bp_color_7'] ); ?>;
}

.gst-navbar--light:not(.gst-navbar--alternate):not(.gst-side-navbar) .gst-navbar-logo--text,
.gst-navbar--light .gst-topnav-menu > .menu-item > a,
.gst-navbar--light .gst-extra-menu a,
.gst-navbar--light .gst-navbar-social .gst-icon-normal,
.gst-navbar--light .gst-side-navbar-nav .gst-navbar-logo--text,
.gst-navbar--alternate.gst-navbar--light.fixed-transparent .gst-navbar-logo--text,
.gst-navbar--alternate.gst-navbar--light.fixed-transparent .gst-topnav-menu > .menu-item > a,
.gst-navbar--alternate.gst-navbar--light.fixed-transparent .gst-extra-menu a,
.gst-navbar--alternate.gst-navbar--light.fixed-transparent .gst-navbar-social .gst-icon-normal {
	color: <?php echo esc_attr( $light_menu_color ); ?>;
}

.gst-navbar--light .gst-navbar-social .gst-icon-normal.gst-icon-fill,
.gst-navbar--light .gst-navbar-social .gst-icon-normal.gst-icon-fill-square,
.gst-navbar--alternate.gst-navbar--light.fixed-transparent .gst-navbar-social .gst-icon-normal.gst-icon-fill,
.gst-navbar--alternate.gst-navbar--light.fixed-transparent .gst-navbar-social .gst-icon-normal.gst-icon-fill-square {
	background-color: <?php echo esc_attr( $light_menu_color ); ?>;
	color: <?php echo esc_attr( $light_menu_contrast_color ); ?>;
}

.gst-navbar--light .gst-lines,
.gst-navbar--light .gst-lines:before,
.gst-navbar--light .gst-lines:after,
.gst-navbar--alternate.gst-navbar--light.fixed-transparent .gst-lines,
.gst-navbar--alternate.gst-navbar--light.fixed-transparent .gst-lines:before,
.gst-navbar--alternate.gst-navbar--light.fixed-transparent .gst-lines:after {
	background-color: <?php echo esc_attr( $light_menu_color ); ?>;
}
<?php /*=========================*/?>

<?php /*=======Navbar Dark Scheme=====*/?>
.gst-navbar--dark .gst-navbar-inner,
.gst-navbar--alternate.gst-navbar--dark.fixed-transparent .gst-navbar-inner {
	border-color: <?php echo esc_attr( $args['bp_color_12'] ); ?>;
}

.gst-navbar--dark:not(.gst-navbar--alternate):not(.gst-side-navbar) .gst-navbar-logo--text,
.gst-navbar--dark .gst-topnav-menu > .menu-item > a,
.gst-navbar--dark .gst-extra-menu a,
.gst-navbar--dark .gst-navbar-social .gst-icon-normal,
.gst-navbar--dark .gst-side-navbar-nav .gst-navbar-logo--text,
.gst-navbar--alternate.gst-navbar--dark.fixed-transparent .gst-navbar-logo--text,
.gst-navbar--alternate.gst-navbar--dark.fixed-transparent .gst-topnav-menu > .menu-item > a,
.gst-navbar--alternate.gst-navbar--dark.fixed-transparent .gst-extra-menu a,
.gst-navbar--alternate.gst-navbar--dark.fixed-transparent .gst-navbar-social .gst-icon-normal {
	color: <?php echo esc_attr( $dark_menu_color ); ?>;
}

.gst-navbar--dark .gst-navbar-social .gst-icon-normal.gst-icon-fill,
.gst-navbar--dark .gst-navbar-social .gst-icon-normal.gst-icon-fill-square,
.gst-navbar--alternate.gst-navbar--dark.fixed-transparent .gst-navbar-social .gst-icon-normal.gst-icon-fill,
.gst-navbar--alternate.gst-navbar--dark.fixed-transparent .gst-navbar-social .gst-icon-normal.gst-icon-fill-square {
	background-color: <?php echo esc_attr( $dark_menu_color ); ?>;
	color: <?php echo esc_attr( $dark_menu_contrast_color ); ?>;
}

.gst-navbar--dark .gst-lines,
.gst-navbar--dark .gst-lines:before,
.gst-navbar--dark .gst-lines:after,
.gst-navbar--alternate.gst-navbar--dark.fixed-transparent .gst-lines,
.gst-navbar--alternate.gst-navbar--dark.fixed-transparent .gst-lines:before,
.gst-navbar--alternate.gst-navbar--dark.fixed-transparent .gst-lines:after {
	background-color: <?php echo esc_attr( $dark_menu_color ); ?>;
}
<?php /*=========================*/?>

<?php /*=======Navbar Brand Scheme=====*/?>
.gst-navbar--light .gst-topnav-menu > .menu-item > a:hover,
.gst-navbar--dark .gst-topnav-menu > .menu-item > a:hover,
.gst-navbar--light.gst-navbar--alternate.fixed-transparent .gst-topnav-menu > .menu-item > a:hover,
.gst-navbar--dark.gst-navbar--alternate.fixed-transparent .gst-topnav-menu > .menu-item > a:hover {
	color: <?php echo esc_attr( $args['primary_brand'] ); ?>;
}
<?php /*=========================*/?>

<?php /*=====Dropdown Scheme=====*/?>
.gst-navbar-dropdown-light-scheme .sub-menu,
.gst-navbar-dropdown-light-scheme .gst-mega-menu,
.gst-navbar-dropdown-light-scheme .gst-cart-box {
	background-color: <?php echo esc_attr( $args['bp_color_6'] ); ?>;
}

.gst-navbar-dropdown-dark-scheme .sub-menu,
.gst-navbar-dropdown-dark-scheme .gst-mega-menu,
.gst-navbar-dropdown-dark-scheme .gst-cart-box {
	background-color: <?php echo esc_attr( $args['bp_color_11'] ); ?>;
}
<?php /*=========================*/?>

<?php /*=======Topbar Scheme=====*/?>
<?php
	if ( 'light' === $args['topbar_color_scheme'] ) {
		$topbar_secondary_bg = $args['bp_color_6'];
		$topbar_primary_border = $args['bp_color_7'];
	} elseif ( 'dark' === $args['topbar_color_scheme'] ) {
		$topbar_secondary_bg = $args['bp_color_11'];
		$topbar_primary_border = $args['bp_color_12'];
	}
?>
.gst-topbar {
	background-color: <?php echo esc_attr( $topbar_secondary_bg ); ?>;
	border-color: <?php echo esc_attr( $topbar_primary_border ); ?>;
}
<?php /*=========================*/?>

<?php /*==Header Widgets Scheme==*/?>
<?php
	if ( 'light' === $args['header_widget_color_scheme'] ) {
		$headerwidget_primary_bg = $args['bp_color_6'];
	} elseif ( 'dark' === $args['header_widget_color_scheme'] ) {
		$headerwidget_primary_bg = $args['bp_color_11'];
	}
?>
.gst-header-widgets {
	background-color: <?php echo esc_attr( $headerwidget_primary_bg ); ?>;
}
<?php /*=========================*/?>

<?php /*=Offcanvas Cursor Image=*/?>
<?php if ( !empty( $args['navbar_offcanvas_cursor'] ) ) : ?>
.gst-offcanvas-overlay {
	cursor: url('<?php echo esc_url( $args['navbar_offcanvas_cursor'] ); ?>'), crosshair;
}
<?php endif; ?>
<?php /*=========================*/?>

<?php /*=======Logo Width========*/?>
img.gst-navbar-logo--image {
	max-width: <?php echo esc_attr( $args['logo_width'] ); ?>px;
}

.gst-navbar--inline .gst-navbar-body-inner:first-child {
	padding-right: <?php echo esc_attr( $args['logo_width'] / 2 + 40 ); ?>px;
}

.gst-navbar--inline .gst-navbar-body-inner:last-child {
	padding-left: <?php echo esc_attr( $args['logo_width'] / 2 + 40 ); ?>px;
}
<?php /*=========================*/?>

<?php /*=====Logo Typography=====*/?>
.gst-navbar-brand {
	<?php echo gastro_s( $fonts[$args['logo_typography']], false ); ?>
}
<?php /*=========================*/?>

<?php /*=====Logo Font Color=====*/?>
<?php if ( 'default' !== $args['logo_font_color'] ) : ?>
.gst-navbar-brand > .gst-navbar-logo--text {
	color: <?php echo esc_attr( gastro_customizer_get_color( $args['logo_font_color'], $args ) ); ?> !important;
}
<?php endif; ?>
<?php /*=========================*/?>

<?php /*======Logo Font Size=====*/?>
.gst-navbar-brand {
	font-size: <?php echo esc_attr( $args['logo_font_size'] ); ?>px;
}
<?php /*=========================*/?>

<?php /*===Logo Letter Spacing===*/?>
<?php if ( '0' === $args['logo_letter_spacing'] ) : ?>
.gst-navbar-brand {
	letter-spacing: 0;
}
<?php elseif ( !empty( $args['logo_letter_spacing'] ) ) : ?>
.gst-navbar-brand {
	letter-spacing: <?php echo esc_attr( $args['logo_letter_spacing'] ); ?>em;
}
<?php endif; ?>
<?php /*=========================*/?>

<?php /*=Fixed Navbar Logo Width=*/?>
.gst-navbar-brand img.gst-fixed-nav-logo {
	max-width: <?php echo esc_attr( $args['fixed_navbar_logo_width'] ); ?>px;
}
<?php /*=========================*/?>

<?php /*=======Mobile Logo Width========*/?>
.gst-navbar--mobile img.gst-navbar-logo--image,
.gst-navbar--mobile img.gst-fixed-nav-logo {
	max-width: <?php echo esc_attr( $args['mobile_navbar_logo_width'] ); ?>px;
}
<?php /*=========================*/?>

<?php /*=======Frame Color=======*/?>
<?php if ( 'default' !== $args['frame_color'] ) : ?>
.gst-frame {
	background-color: <?php echo gastro_customizer_get_color( $args['frame_color'], $args ); ?>;
}
<?php endif; ?>
<?php /*=========================*/?>

<?php /*=======Frame Width=======*/?>
.gst-layout--frame {
	padding: <?php echo esc_attr( $args['frame_width'] ); ?>px;
}

.gst-layout--frame:not(.header-on-frame) .gst-navbar--fixed {
	top: <?php echo esc_attr( $args['frame_width'] ); ?>px;
}

.gst-frame--top,
.gst-frame--bottom {
	height: <?php echo esc_attr( $args['frame_width'] ); ?>px;
}

.gst-frame--left,
.gst-frame--right {
	width: <?php echo esc_attr( $args['frame_width'] ); ?>px;
}

.gst-layout--frame .gst-side-navbar {
	top: <?php echo esc_attr( (int)$args['frame_width'] ); ?>px;
	bottom: <?php echo esc_attr( (int)$args['frame_width'] ); ?>px;
}

.gst-layout--frame .gst-side-navbar--left {
	left: <?php echo esc_attr( (int)$args['frame_width'] ); ?>px;
}

.gst-layout--frame .gst-side-navbar--right {
	right: <?php echo esc_attr( (int)$args['frame_width'] ); ?>px;
}

.gst-layout--frame .gst-navbar--fixed {
	right: <?php echo esc_attr( (int)$args['frame_width'] ); ?>px;
	left: <?php echo esc_attr( (int)$args['frame_width'] ); ?>px;
}

.gst-layout--frame .gst-collapsed-menu--offcanvas {
	top: <?php echo esc_attr( (int)$args['frame_width'] ); ?>px;
	right: <?php echo esc_attr( (int)$args['frame_width'] ); ?>px;
	bottom: <?php echo esc_attr( (int)$args['frame_width'] ); ?>px;
}

.gst-layout--frame .gst-wrapper--parallax-footer .gst-footer {
	padding-left: <?php echo esc_attr( (int)$args['frame_width'] ); ?>px;
	padding-right: <?php echo esc_attr( (int)$args['frame_width'] ); ?>px;
	margin-bottom: <?php echo esc_attr( (int)$args['frame_width'] ); ?>px;
}

.gst-layout--frame.gst-layout--sidenav-fixed .gst-wrapper--parallax-footer .gst-footer {
	max-width: 70%;
	max-width: calc( 100% - <?php echo esc_attr( 280 + $args['frame_width'] * 2 ); ?>px );
}

.gst-layout--frame.gst-layout--sidenav-full .gst-wrapper--parallax-footer .gst-footer {
	max-width: 87%;
	max-width: calc( 100% - <?php echo esc_attr( 80 + $args['frame_width'] * 2 ); ?>px );
}

.gst-layout--frame.gst-layout--sidenav-minimal .gst-wrapper--parallax-footer .gst-footer {
	max-width: 95%;
	max-width: calc( 100% - <?php echo esc_attr( $args['frame_width'] * 2 ); ?>px );
}
<?php /*=========================*/?>

<?php /*====Content Max Width====*/?>
.gst-layout--wide .gst-container,
.gst-layout--frame .gst-container {
	max-width: <?php echo esc_attr( (int)$args['content_max_width'] + 200 ); ?>px;
	max-width: calc( <?php echo esc_attr( $args['content_max_width'] . "px + " . $args['side_padding'] . "%" ); ?> );
}

.gst-layout--boxed .gst-wrapper {
	max-width: <?php echo esc_attr( (int)$args['content_max_width'] ); ?>px;
}

@media only screen and (min-width: <?php echo esc_attr( (int)$args['content_max_width'] ); ?>px ) {
	.gst-layout--boxed .gst-side-navbar--left {
		left: calc( 50% - <?php echo esc_attr( (int)$args['content_max_width'] / 2 ); ?>px );
	}

	.gst-layout--boxed .gst-side-navbar--right {
		right: calc( 50% - <?php echo esc_attr( (int)$args['content_max_width'] / 2 ); ?>px );
	}

	.gst-layout--boxed .gst-side-navbar--minimal.gst-side-navbar--left .gst-side-navbar-nav {
		left: calc( 50% - <?php echo esc_attr( (int)$args['content_max_width'] / 2 - 30 ); ?>px );
	}

	.gst-layout--boxed .gst-side-navbar--minimal.gst-side-navbar--right .gst-side-navbar-nav {
		right: calc( 50% - <?php echo esc_attr( (int)$args['content_max_width'] / 2 - 30 ); ?>px );
	}

	.gst-layout--boxed .gst-navbar--fixed {
		left: calc( 50% - <?php echo esc_attr( $args['content_max_width'] / 2 ); ?>px );
		max-width: <?php echo esc_attr( (int)$args['content_max_width'] ); ?>px;
	}

	.gst-layout--boxed .gst-wrapper--parallax-footer .gst-footer {
		max-width: <?php echo esc_attr( (int)$args['content_max_width'] ); ?>px;
	}

	.gst-layout--boxed.gst-layout--sidenav-full .gst-wrapper--parallax-footer .gst-footer {
		max-width: <?php echo esc_attr( (int)$args['content_max_width'] - 80 ); ?>px;
	}

	.gst-layout--boxed.gst-layout--sidenav-fixed .gst-wrapper--parallax-footer .gst-footer {
		max-width: <?php echo esc_attr( (int)$args['content_max_width'] - 280 ); ?>px;
	}
}
<?php /*=========================*/?>

<?php /*=======Side Padding======*/?>
.gst-container {
	padding-right: <?php echo esc_attr( (int)$args['side_padding'] / 2 ); ?>%;
	padding-left: <?php echo esc_attr( (int)$args['side_padding'] / 2 ); ?>%;
}
<?php /*=========================*/?>

<?php /*======Sidebar Width======*/?>
.gst-main {
	width: <?php echo esc_attr( (100 - (int)$args['sidebar_width']) ); ?>%;
}

.gst-sidebar {
	width: <?php echo esc_attr( (int)$args['sidebar_width'] ); ?>%;
}
<?php /*=========================*/?>

<?php /*===Sidebar Top Padding===*/?>
.gst-sidebar,
.gst-main.blueprint-inactive {
	padding-top: <?php echo esc_attr( $args['sidebar_top_padding'] ); ?>px;
	padding-bottom: <?php echo esc_attr( $args['sidebar_top_padding'] ); ?>px;
}

<?php if ( $args['product_sidebar'] ) : ?>
.gst-main.gst-single-product {
	padding-top: <?php echo esc_attr( $args['sidebar_top_padding'] ); ?>px;
	padding-bottom: <?php echo esc_attr( $args['sidebar_top_padding'] ); ?>px;
}
<?php endif; ?>
<?php /*=========================*/?>

<?php /*=Sidebar Background Color*/?>
<?php if ( 'default' !== $args['sidebar_background_color'] ) : ?>
.gst-sidebar > .gst-sidebar-background {
	background-color: <?php echo esc_attr( gastro_customizer_get_color( $args['sidebar_background_color'], $args ) ); ?>;
}
<?php endif; ?>
<?php /*=========================*/?>

<?php /*=Content Background Color*/?>
<?php if ( 'default' !== $args['content_background_color'] ) : ?>
.gst-wrapper {
	background-color: <?php echo esc_attr( gastro_customizer_get_color( $args['content_background_color'], $args ) ); ?>;
}

.gst-wrapper--parallax-footer .gst-content {
	background-color: <?php echo esc_attr( gastro_customizer_get_color( $args['content_background_color'], $args ) ); ?>;
}

.gst-post-featured--overlap .gst-main {
	background-color: <?php echo esc_attr( gastro_customizer_get_color( $args['content_background_color'], $args ) ); ?>;
}
<?php endif; ?>
<?php /*=========================*/?>

<?php /*=Content Background Image*/?>
<?php if ( !empty( $args['content_background_image'] ) ) : ?>
.gst-wrapper,
.gst-wrapper--parallax-footer .gst-content {
	<?php echo gastro_get_background_image( array(
		'url' => $args['content_background_image'],
		'size' => $args['content_background_size'],
		'position' => $args['content_background_position'],
		'repeat' => $args['content_background_repeat'],
		'fixed' => $args['content_background_attachment']
	) ); ?>
}
<?php endif; ?>
<?php /*=========================*/?>

<?php /*==Body Background Color==*/?>
<?php if ( 'default' !== $args['body_background_color'] ) : ?>
.gst-layout--boxed {
	background-color: <?php echo esc_attr( gastro_customizer_get_color( $args['body_background_color'], $args ) ); ?>;
}
<?php endif; ?>
<?php /*=========================*/?>

<?php /*==Body Background Image==*/?>
<?php if ( !empty( $args['body_background_image'] ) ) : ?>
.gst-layout--boxed {
	<?php echo gastro_get_background_image( array(
		'url' => $args['body_background_image'],
		'size' => $args['body_background_size'],
		'position' => $args['body_background_position'],
		'repeat' => $args['body_background_repeat'],
		'fixed' => $args['body_background_attachment']
	) ); ?>
}
<?php endif; ?>
<?php /*=========================*/?>

<?php /*=======Boxed Shadow======*/?>
<?php if ( !empty( $args['boxed_shadow'] ) ): ?>
.gst-layout--boxed .gst-wrapper {
	box-shadow: <?php echo esc_attr( $args['boxed_shadow'] ); ?>;
}
<?php endif; ?>
<?php /*=========================*/?>

<?php /*====Design Typography====*/?>
body {
	<?php echo gastro_s( $fonts[$args['style_typography']], false ); ?>
	font-size: <?php echo esc_attr( (int)$args['style_font_size'] ); ?>px;
}

select,
textarea,
input,
.gst-banner-content-inner,
.gst-section-wrapper,
.gst-box-content {
	font-size: <?php echo esc_attr( (int)$args['style_font_size'] ); ?>px;
}
<?php /*=========================*/?>

<?php /*=====Design Heading======*/?>
h1 {
	font-size: <?php echo esc_attr( $args['heading_size_h1'] ); ?>px;
}

h2 {
	font-size: <?php echo esc_attr( $args['heading_size_h2'] ); ?>px;
}

h3 {
	font-size: <?php echo esc_attr( $args['heading_size_h3'] ); ?>px;
}

h4 {
	font-size: <?php echo esc_attr( $args['heading_size_h4'] ); ?>px;
}

h5 {
	font-size: <?php echo esc_attr( $args['heading_size_h5'] ); ?>px;
}

h6 {
	font-size: <?php echo esc_attr( $args['heading_size_h6'] ); ?>px;
}
<?php /*=========================*/?>

<?php /*=======Design Button=====*/?>
button,
input[type=button],
input[type=submit],
.btnx {
	<?php echo gastro_s( $fonts[$args['button_typography']], false ); ?>
<?php if ( $args['button_uppercase'] ) : ?>
	text-transform: uppercase;
<?php else : ?>
	text-transform: none;
<?php endif; ?>
}

button,
input[type=button],
input[type=submit] {
	padding: <?php echo esc_attr( $args['button_padding'] ); ?>;
	border-radius: <?php echo esc_attr( $args['button_radius'] ); ?>px;
	border-width: <?php echo esc_attr( $args['button_border'] ); ?>px;
	color: <?php echo esc_attr( $args['button_text_color'] ); ?>;
	background-color: <?php echo esc_attr( $args['button_background_color'] ); ?>;
	border-color: <?php echo esc_attr( $args['button_border_color'] ); ?>;
}

.gst-button > .btnx {
	border-radius: <?php echo esc_attr( $args['button_radius'] ); ?>px;
	border-width: <?php echo esc_attr( $args['button_border'] ); ?>px;
}

<?php if ( 'layer' === $args['button_style'] ) : ?>
	button,
	input[type=button],
	input[type=submit] {
		-ms-transform: translate(0, 0);
		-webkit-transform: translate(0, 0);
		transform: translate(0, 0);
	}
	button:hover,
	input[type=button]:hover,
	input[type=submit]:hover {
		-ms-transform: translate(4px, 4px);
		-webkit-transform: translate(4px, 4px);
		transform: translate(4px, 4px);
	}
	button:hover,
	input[type=button]:hover,
	input[type=submit]:hover {
		box-shadow: 0 0 transparent !important;
	}
<?php else : ?>
	button:hover,
	input[type=button]:hover,
	input[type=submit]:hover,
	button:focus,
	input[type=button]:focus,
	input[type=submit]:focus {
		color: <?php echo esc_attr( $args['button_text_hover_color'] ); ?>;
		background-color: <?php echo esc_attr( $args['button_background_hover_color'] ); ?>;
		border-color: <?php echo esc_attr( $args['button_border_hover_color'] ); ?>;
	}
<?php endif; ?>
<?php /*=========================*/?>

<?php /*==========Preload========*/?>
<?php if ( 'default' !== $args['preload_background_color'] ) : ?>
.gst-page-load {
	background-color: <?php echo esc_attr( gastro_customizer_get_color( $args['preload_background_color'], $args ) ); ?>;
}
<?php endif; ?>

.gst-loading img {
	max-width: <?php echo esc_attr( $args['preload_logo_width'] ); ?>px;
}
<?php /*=========================*/?>

<?php /*=Back To Top Background==*/?>
<?php if ( 'default' !== $args['back_to_top_background_color'] ) : ?>
.gst-back-to-top-background {
	background-color: <?php echo esc_attr( gastro_customizer_get_color( $args['back_to_top_background_color'], $args ) ); ?>;
}
<?php endif; ?>
<?php /*=========================*/?>

<?php /*====Back To Top Arrow====*/?>
<?php if ( 'default' !== $args['back_to_top_arrow_color'] ) : ?>
.gst-back-to-top {
	color: <?php echo esc_attr( gastro_customizer_get_color( $args['back_to_top_arrow_color'], $args ) ); ?>;
}
<?php endif; ?>
<?php /*=========================*/?>

<?php /*===Form Style===*/?>
<?php
gastro_sch_css( array_merge( $args['base_sch_args'], array(
	'selector' => array(
		'select[disabled]',
		'select[readonly]',
		'select fieldset[disabled]',
		'textarea[disabled]',
		'textarea[readonly]',
		'textarea fieldset[disabled]',
		'input[disabled]',
		'input[readonly]',
		'input fieldset[disabled]'
	),
	'property' => array(
		'color' => 'secondary_text',
		'background-color' => 'secondary_background'
	),
	'opacity' => 0.5
) ) );
?>
<?php if ( 'line' === $args['form_style'] ) : ?>
<?php
gastro_sch_css( array_merge( $args['base_sch_args'], array(
	'selector' => array(
		'select',
		"input:not([type='submit']):not([type='button'])",
		'textarea',
	),
	'property' => array(
		'color' => 'secondary_text',
		'border-bottom-color' => 'secondary_text'
	)
) ) );
gastro_sch_css( array_merge( $args['base_sch_args'], array(
	'selector' => array(
		'select:focus',
		"input:not([type='submit']):not([type='button']):focus",
		'textarea:focus',
		'.picker__input.picker__input--active'
	),
	'property' => array(
		'border-bottom-color' => 'primary_brand'
	)
) ) );
?>
input,
textarea,
select {
	padding-left: 0;
	padding-right: 0;
	border-width: 0;
	border-bottom-width: 2px;
}

.gst-reservation .form-container.gst-reservation-branch select {
	border-color: transparent;
}

input,
textarea,
select,
.gst-reservation input[readonly],
.gst-opentable input[readonly] {
	background-color: transparent;
}

.gst-form-group input[type=submit] {
	width: calc( 30% - 30px );
	margin-left: 30px;
}

.gst-search-form:before {
	left: 0;
}

.gst-search-form input[type="text"] {
	padding-left: 25px;
}

@media only screen and (max-width: 480px) {
	.gst-layout-responsive .gst-form-group input[type=submit] {
		width: calc(50% - 30px);
	}
}
<?php elseif ( 'fill' === $args['form_style'] ) : ?>
<?php
gastro_sch_css( array_merge( $args['base_sch_args'], array(
	'selector' => array(
		'input',
		'textarea',
		'select',
		'.gst-opentable input[readonly]',
		'.gst-reservation .gst-reservation-form input[readonly]',
		'.delivery-field input[readonly]'
	),
	'property' => array(
		'color' => 'secondary_text',
		'background-color' => 'secondary_background'
	)
) ) );
gastro_sch_css( array_merge( $args['base_sch_args'], array(
	'selector' => array(
		'input:focus',
		'textarea:focus',
		'select:focus',
		'.picker__input.picker__input--active'
	),
	'property' => array(
		'border-color' => 'primary_brand'
	)
) ) );
?>
<?php else : ?>
<?php
gastro_sch_css( array_merge( $args['base_sch_args'], array(
	'selector' => array(
		'input',
		'textarea',
		'select'
	),
	'property' => array(
		'color' => 'secondary_text',
		'border-color' => 'primary_border'
	)
) ) );
gastro_sch_css( array_merge( $args['base_sch_args'], array(
	'selector' => array(
		'input:focus',
		'textarea:focus',
		'select:focus',
		'.picker__input.picker__input--active'
	),
	'property' => array(
		'border-color' => 'primary_brand'
	)
) ) );
?>

input,
textarea,
select,
.gst-reservation input[readonly],
.gst-opentable input[readonly] {
	background-color: transparent;
}
<?php endif; ?>
<?php /*=========================*/?>

<?php /*======Navbar Height======*/?>
<?php $content_padding_top = gastro_get_header_height( $args['logo'] ); ?>
<?php $stacked_content_padding_top = gastro_get_header_height( $args['logo'], 'top', 'stacked' ); ?>
<?php $sidenav_padding_top = ( $args['topbar'] ) ? ( $args['topbar_height'] ) : 0; ?>

.gst-navbar--custom {
	height: <?php echo esc_attr( (int)$args['navbar_height'] ); ?>px;
	line-height: <?php echo esc_attr( (int)$args['navbar_height'] ); ?>px;
}

.gst-content-wrapper,
.gst-wrapper--header-transparent .gst-content-wrapper > .gst-page-title {
	padding-top: <?php echo esc_attr( $content_padding_top ); ?>px;
}

.gst-layout--topnav-stacked .gst-content-wrapper,
.gst-layout--topnav-stacked .gst-wrapper--header-transparent .gst-content-wrapper > .gst-page-title {
	padding-top: <?php echo esc_attr( $stacked_content_padding_top ); ?>px;
}

.gst-layout--sidenav .gst-content-wrapper,
.gst-layout--sidenav .gst-wrapper--header-transparent .gst-content-wrapper > .gst-page-title {
	padding-top: <?php echo esc_attr( $sidenav_padding_top ); ?>px;
}

.gst-wrapper--header-transparent .gst-post-featured--fullwidth .gst-page-title,
.gst-wrapper--header-transparent .gst-post-featured--overlap .gst-page-title {
	top: <?php echo esc_attr( $content_padding_top ); ?>px;
}

.gst-layout--topnav-stacked .gst-wrapper--header-transparent .gst-post-featured--fullwidth .gst-page-title,
.gst-layout--topnav-stacked .gst-wrapper--header-transparent .gst-post-featured--overlap .gst-page-title {
	top: <?php echo esc_attr( $stacked_content_padding_top ); ?>px;
}

.gst-layout--sidenav .gst-wrapper--header-transparent .gst-post-featured--fullwidth .gst-page-title,
.gst-layout--sidenav .gst-wrapper--header-transparent .gst-post-featured--overlap .gst-page-title {
	top: <?php echo esc_attr( $sidenav_padding_top ); ?>px;
}
<?php /*=========================*/?>

<?php /*===Navbar Stacked Line Height====*/?>
.gst-navbar--stacked .gst-navbar-content,
.gst-navbar--stacked.gst-navbar--fixed .gst-navbar-header {
	height: <?php echo esc_attr( (int)$args['navbar_stacked_lineheight'] ); ?>px;
	line-height: <?php echo esc_attr( (int)$args['navbar_stacked_lineheight'] ); ?>px;
}
<?php /*=========================*/?>

<?php /*===Navbar Stacked Header Background Color====*/?>
<?php if ( 'default' !== $args['navbar_stacked_background_color'] ) : ?>
.gst-navbar--stacked .gst-navbar-header {
	background-color: <?php echo gastro_hex_to_rgba( $args['navbar_stacked_background_color'], $args['navbar_stacked_opacity'] / 100 ); ?>;
}
<?php endif; ?>
<?php /*=========================*/?>

<?php /*===Navbar Menu Border====*/?>
.gst-navbar .gst-navbar-inner,
.gst-navbar--mobile .gst-navbar-inner {
	border-bottom-width: <?php echo esc_attr( $args['navbar_menu_border_thickness'] ); ?>px;
}

.gst-topnav-menu > li > .gst-mega-menu,
.gst-topnav-menu > li > .sub-menu {
	margin-top: <?php echo esc_attr( $args['navbar_menu_border_thickness'] ); ?>px;
}
<?php /*=========================*/?>

<?php /*=Navbar Menu Border Color*/?>
<?php if ( 'default' !== $args['navbar_menu_border_color'] ) : ?>
.gst-navbar .gst-navbar-inner,
.gst-navbar--mobile .gst-navbar-inner {
	border-bottom-color: <?php echo esc_attr( gastro_customizer_get_color( $args['navbar_menu_border_color'], $args ) ); ?>;
}
<?php endif; ?>
<?php /*=========================*/?>

<?php /*====Navbar Logo Offset===*/?>
<?php if ( is_numeric( $args['navbar_logo_offset_top'] ) ) : ?>
.gst-navbar:not(.gst-navbar--alternate) .gst-navbar-header {
	vertical-align: top;
	line-height: 1;
}

.gst-navbar:not(.gst-navbar--alternate) .gst-navbar-brand,
.gst-side-navbar .gst-navbar-header .gst-navbar-brand {
	margin-top: <?php echo esc_attr( (int)$args['navbar_logo_offset_top'] ); ?>px;
}
<?php endif; ?>
<?php /*=========================*/?>

<?php /*====Navbar Menu Offset===*/?>
<?php if ( is_numeric( $args['navbar_menu_offset_top'] ) ) : ?>
.gst-navbar:not(.gst-navbar--stacked):not(.gst-navbar--alternate) .gst-topnav-menu > li > a,
.gst-navbar:not(.gst-navbar--stacked):not(.gst-navbar--alternate) .gst-extra-menu > li > a,
.gst-navbar:not(.gst-navbar--stacked):not(.gst-navbar--alternate) .gst-collapsed-button,
.gst-navbar:not(.gst-navbar--stacked):not(.gst-navbar--alternate) .gst-navbar-footer {
	padding-top: <?php echo esc_attr( (int)$args['navbar_menu_offset_top'] ); ?>px;
	line-height: 1;
}

.gst-navbar--stacked:not(.gst-navbar--alternate) .gst-navbar-brand,
.gst-side-navbar .gst-navbar-header .gst-navbar-brand {
	margin-bottom: <?php echo esc_attr( (int)$args['navbar_menu_offset_top'] ); ?>px;
}
<?php endif; ?>
<?php /*=========================*/?>

<?php /*===Mega Menu Separator===*/?>
<?php if ( $args['mega_menu_separator'] ) : ?>
.gst-topnav-menu .gst-mega-menu-column {
	border-right-width: 1px;
	border-right-style: solid;
}
<?php endif; ?>
<?php /*=========================*/?>

<?php /*=Navbar Background Color=*/?>
<?php if ( 'default' !== $args['navbar_background_color'] ) : ?>
.gst-navbar .gst-navbar-inner,
.gst-navbar--mobile .gst-navbar-inner {
	background-color: <?php echo gastro_hex_to_rgba( $args['navbar_background_color'], $args['navbar_opacity'] / 100 ); ?>;
}
<?php endif; ?>
<?php /*=========================*/?>

<?php /*====Navbar Menu Color====*/?>
<?php if ( 'default' !== $args['navbar_menu_color'] ) : ?>
.gst-navbar .gst-topnav-menu > .menu-item > a,
.gst-navbar .gst-extra-menu a,
.gst-navbar .gst-navbar-social .gst-icon-normal,
.gst-navbar--mobile .gst-navbar-social .gst-icon-normal {
	color: <?php echo esc_attr( gastro_customizer_get_color( $args['navbar_menu_color'], $args ) ); ?>;
}

.gst-navbar .gst-navbar-social .gst-icon-normal.gst-icon-fill,
.gst-navbar .gst-navbar-social .gst-icon-normal.gst-icon-fill-square,
.gst-navbar--mobile .gst-navbar-social .gst-icon-normal.gst-icon-fill,
.gst-navbar--mobile .gst-navbar-social .gst-icon-normal.gst-icon-fill-square {
	background-color: <?php echo esc_attr( gastro_customizer_get_color( $args['navbar_menu_color'], $args ) ); ?>;
	color: <?php echo esc_attr( gastro_contrast_color( $args['navbar_menu_color'] ) ); ?>;
}

.gst-navbar .gst-lines,
.gst-navbar .gst-lines:before,
.gst-navbar .gst-lines:after,
.gst-navbar--mobile .gst-lines,
.gst-navbar--mobile .gst-lines:before,
.gst-navbar--mobile .gst-lines:after {
	background-color: <?php echo esc_attr( gastro_customizer_get_color( $args['navbar_menu_color'], $args ) ); ?>;
}
<?php endif; ?>
<?php /*=========================*/?>

<?php /*=Navbar Menu Active Color*/?>
.gst-sidenav-menu .current-menu-parent .gst-mega-menu-title a,
.gst-fullnav-menu .current-menu-parent .gst-mega-menu-title a,
.gst-classicnav-menu .current-menu-parent .gst-mega-menu-title a,
.gst-navbar--light .gst-topnav-menu > .current-menu-ancestor > a,
.gst-navbar--light .gst-topnav-menu > .current-menu-parent > a,
.gst-navbar--light .gst-topnav-menu > .current-menu-item > a,
.gst-navbar--dark .gst-topnav-menu > .current-menu-ancestor > a,
.gst-navbar--dark .gst-topnav-menu > .current-menu-parent > a,
.gst-navbar--dark .gst-topnav-menu > .current-menu-item > a {
	color: <?php echo esc_attr( $args['primary_brand'] ); ?>;
}

<?php if ( 'default' !== $args['navbar_menu_active_color'] ) : ?>
.gst-navbar .gst-topnav-menu > .current-menu-item > a,
.gst-navbar .gst-topnav-menu > .current-menu-parent > a,
.gst-navbar .gst-topnav-menu > .current-menu-ancestor > a {
	color: <?php echo esc_attr( gastro_customizer_get_color( $args['navbar_menu_active_color'], $args ) ); ?>;
}
<?php endif; ?>
<?php /*=========================*/?>

<?php /*=Navbar Menu Hover Color*/?>
<?php if ( 'default' !== $args['navbar_menu_hover_color'] ) : ?>
.gst-navbar .gst-topnav-menu > .menu-item > a:hover,
.gst-navbar .gst-extra-menu a:hover,
.gst-navbar .gst-navbar-social .gst-icon-hover,
.gst-navbar--mobile .gst-navbar-social .gst-icon-hover {
	color: <?php echo esc_attr( gastro_customizer_get_color( $args['navbar_menu_hover_color'], $args ) ); ?>;
}

.gst-navbar .gst-navbar-social .gst-icon-hover.gst-icon-fill,
.gst-navbar .gst-navbar-social .gst-icon-hover.gst-icon-fill-square,
.gst-navbar--mobile .gst-navbar-social .gst-icon-hover.gst-icon-fill,
.gst-navbar--mobile .gst-navbar-social .gst-icon-hover.gst-icon-fill-square {
	background-color: <?php echo esc_attr( gastro_customizer_get_color( $args['navbar_menu_hover_color'], $args ) ); ?>;
	color: <?php echo esc_attr( gastro_contrast_color( $args['navbar_menu_hover_color'] ) ); ?>;
}
<?php endif; ?>
<?php /*=========================*/?>

<?php /*=Navbar Menu Typography==*/?>
.gst-topnav-menu .menu-item a {
	<?php echo gastro_s( $fonts[$args['navbar_menu_typography']], false ); ?>
}
<?php /*=========================*/?>

<?php /*=Navbar Menu Uppercase==*/?>
<?php if ( $args['navbar_menu_uppercase'] ) : ?>
.gst-topnav-menu > .menu-item > a {
	text-transform: uppercase;
}
<?php else : ?>
.gst-topnav-menu > .menu-item > a {
	text-transform: none;
}
<?php endif; ?>
<?php /*=========================*/?>

<?php /*==Navbar Menu Font Size==*/?>
.gst-topnav-menu > .menu-item > a {
	font-size: <?php echo esc_attr( (int)$args['navbar_menu_font_size'] ); ?>px;
}
<?php /*=========================*/?>

<?php /*==Navbar Menu Letter Spacing==*/?>
<?php if ( '0' === $args['navbar_menu_letter_spacing'] ) : ?>
.gst-topnav-menu > .menu-item > a {
	letter-spacing: 0;
}
<?php elseif ( !empty( $args['navbar_menu_letter_spacing'] ) ) : ?>
.gst-topnav-menu > .menu-item > a {
	letter-spacing: <?php echo esc_attr( $args['navbar_menu_letter_spacing'] ); ?>em;
}
<?php endif; ?>
<?php /*=========================*/?>

<?php /*==Navbar Menu Separator==*/?>
<?php if ( 'none' !== $args['navbar_menu_separator'] ) : ?>
.gst-topnav-menu > .menu-item > a:before,
.horizontal .gst-fullnav-menu > .menu-item > a:before {
	content: "<?php echo esc_attr( $args['navbar_menu_separator'] ); ?>";
}
<?php endif; ?>
<?php /*=========================*/?>

<?php /*=Navbar Dropdown Menu Uppercase==*/?>
<?php if ( $args['dropdown_menu_uppercase'] ) : ?>
.gst-topnav-menu .sub-menu {
	text-transform: uppercase;
}
<?php else : ?>
.gst-topnav-menu .sub-menu {
	text-transform: none;
}
<?php endif; ?>
<?php /*=========================*/?>

<?php /*==Navbar Dropdown Font Size==*/?>
.gst-topnav-menu .sub-menu a,
.gst-topnav-menu .gst-mega-menu-title a {
	font-size: <?php echo esc_attr( (int)$args['dropdown_menu_font_size'] ); ?>px;
}
<?php /*=========================*/?>

<?php /*==Navbar Dropdown Letter Spacing==*/?>
<?php if ( '0' === $args['dropdown_menu_letter_spacing'] ) : ?>
.gst-topnav-menu .sub-menu a,
.gst-topnav-menu .gst-mega-menu-title a {
	letter-spacing: 0;
}
<?php elseif ( !empty( $args['dropdown_menu_letter_spacing'] ) ) : ?>
.gst-topnav-menu .sub-menu a,
.gst-topnav-menu .gst-mega-menu-title a {
	letter-spacing: <?php echo esc_attr( $args['dropdown_menu_letter_spacing'] ); ?>em;
}
<?php endif; ?>
<?php /*=========================*/?>

<?php /*==Navbar Dropdown Min Width==*/?>
.gst-topnav-menu > li > .sub-menu,
.gst-topnav-menu > li > .sub-menu .sub-menu {
	min-width: <?php echo esc_attr( (int)$args['dropdown_menu_min_width'] ); ?>px;
}
<?php /*=========================*/?>

<?php /*==Navbar Dropdown Background Color==*/?>
<?php if ( 'default' !== $args['dropdown_background_color'] ) : ?>
.gst-topnav-menu .sub-menu,
.gst-topnav-menu .gst-mega-menu,
.gst-topnav-menu .gst-cart-box {
	background-color: <?php echo gastro_hex_to_rgba( $args['dropdown_background_color'], $args['dropdown_opacity'] / 100 ); ?>;
}
<?php endif; ?>
<?php /*=========================*/?>

<?php /*==Navbar Dropdown Menu Color==*/?>
<?php if ( 'default' !== $args['dropdown_menu_color'] ) : ?>
.gst-navbar--top .gst-topnav-menu .sub-menu a,
.gst-navbar--top .gst-topnav-menu .gst-cart-box a,
.gst-navbar--top .gst-topnav-menu .gst-mega-menu-title a {
	color: <?php echo esc_attr( gastro_customizer_get_color( $args['dropdown_menu_color'], $args ) ); ?>;
}
<?php endif; ?>
<?php /*=========================*/?>

<?php /*==Navbar Dropdown Menu Hover Color==*/?>
<?php if ( 'default' !== $args['dropdown_hover_color'] ) : ?>
.gst-navbar--top .gst-topnav-menu .sub-menu a:hover,
.gst-navbar--top .gst-topnav-menu .gst-cart-box a:hover,
.gst-navbar--top .gst-topnav-menu .gst-mega-menu-title a:hover {
	color: <?php echo esc_attr( gastro_customizer_get_color( $args['dropdown_hover_color'], $args ) ); ?>;
}
<?php endif; ?>
<?php /*=========================*/?>

<?php /*=Side Navbar Background Color=*/?>
<?php if ( 'default' !== $args['sidenav_background_color'] ) : ?>
.gst-side-navbar .gst-navbar-inner,
.gst-collapsed-menu--offcanvas .gst-collapsed-menu-inner {
	background-color: <?php echo esc_attr( gastro_customizer_get_color( $args['sidenav_background_color'], $args ) ); ?>;
}
<?php endif; ?>
<?php /*=========================*/?>

<?php /*=Side Navbar Background Image=*/?>
<?php if ( !empty( $args['navbar_background_image'] ) ) : ?>
.gst-side-navbar .gst-navbar-inner,
.gst-collapsed-menu--offcanvas .gst-collapsed-menu-inner {
	<?php echo gastro_get_background_image( array(
		'url' => $args['sidenav_background_image'],
		'size' => $args['sidenav_background_size'],
		'position' => $args['sidenav_background_position'],
		'repeat' => $args['sidenav_background_repeat'],
		'fixed' => $args['sidenav_background_attachment']
	) ); ?>
}
<?php endif; ?>
<?php /*=========================*/?>

<?php /*====Side Navbar Menu Color====*/?>
<?php if ( 'default' !== $args['sidenav_menu_color'] ) : ?>
.gst-sidenav-menu .menu-item a {
	color: <?php echo esc_attr( gastro_customizer_get_color( $args['sidenav_menu_color'], $args ) ); ?>;
}

.gst-side-navbar .gst-lines,
.gst-side-navbar .gst-lines:before,
.gst-side-navbar .gst-lines:after {
	background-color: <?php echo esc_attr( gastro_customizer_get_color( $args['sidenav_menu_color'], $args ) ); ?>;
}
<?php endif; ?>
<?php /*=========================*/?>

<?php /*=Side Navbar Menu Active Color*/?>
<?php if ( 'default' !== $args['sidenav_menu_active_color'] ) : ?>
.gst-sidenav-menu .current-menu-item > a,
.gst-sidenav-menu .current-menu-parent > a,
.gst-sidenav-menu .current-menu-ancestor > a,
.gst-sidenav-menu .current-menu-parent .gst-mega-menu-title a {
	color: <?php echo esc_attr( gastro_customizer_get_color( $args['sidenav_menu_active_color'], $args ) ); ?>;
}
<?php endif; ?>
<?php /*=========================*/?>

<?php /*=Side Navbar Menu Hover Color*/?>
<?php if ( 'default' !== $args['sidenav_menu_hover_color'] ) : ?>
.gst-sidenav-menu .menu-item a:hover {
	color: <?php echo esc_attr( gastro_customizer_get_color( $args['sidenav_menu_hover_color'], $args ) ); ?>;
}
<?php endif; ?>
<?php /*=========================*/?>

<?php /*=Side Navbar Menu Typography==*/?>
.gst-sidenav-menu .menu-item a {
	<?php echo gastro_s( $fonts[$args['sidenav_menu_typography']], false ); ?>
}
<?php /*=========================*/?>

<?php /*=Side Navbar Menu Uppercase==*/?>
<?php if ( $args['sidenav_menu_uppercase'] ) : ?>
.gst-sidenav-menu .menu-item a {
	text-transform: uppercase;
}
<?php else : ?>
.gst-sidenav-menu .menu-item a {
	text-transform: none;
}
<?php endif; ?>
<?php /*=========================*/?>

<?php /*==Side Navbar Menu Font Size==*/?>
.gst-sidenav-menu .menu-item a {
	font-size: <?php echo esc_attr( (int)$args['sidenav_menu_font_size'] ); ?>px;
}
<?php /*=========================*/?>

<?php /*==Side Navbar Menu Letter Spacing==*/?>
<?php if ( '0' === $args['sidenav_menu_letter_spacing'] ) : ?>
.gst-sidenav-menu .menu-item a {
	letter-spacing: 0;
}
<?php elseif ( !empty( $args['sidenav_menu_letter_spacing'] ) ) : ?>
.gst-sidenav-menu .menu-item a {
	letter-spacing: <?php echo esc_attr( $args['sidenav_menu_letter_spacing'] ); ?>em;
}
<?php endif; ?>
<?php /*=========================*/?>

<?php /*=Full Navbar Background Color=*/?>
<?php if ( 'default' !== $args['navbar_full_background_color'] ) : ?>
.gst-collapsed-menu.gst-collapsed-menu--full {
	background-color: <?php echo gastro_hex_to_rgba( $args['navbar_full_background_color'] , $args['navbar_full_opacity'] / 100 ); ?>;
}
.gst-collapsed-button--full.gst-closed .gst-lines:before,
.gst-collapsed-button--full.gst-closed .gst-lines:after {
	background-color: <?php echo gastro_contrast_color( $args['navbar_full_background_color'] ); ?> !important;
}
<?php endif; ?>
<?php
gastro_sch_css( array_merge( $args['base_sch_args'], array(
	'selector' => array(
		'.gst-collapsed-button--full.gst-closed .gst-lines:before',
		'.gst-collapsed-button--full.gst-closed .gst-lines:after'
	),
	'property' => array(
		'background-color' => 'primary_background_contrast'
	)
) ) );
?>
<?php /*=========================*/?>

<?php /*=Full Navbar Background Image=*/?>
<?php if ( !empty( $args['navbar_full_background_image'] ) ) : ?>
.gst-collapsed-menu.gst-collapsed-menu--full {
	<?php echo gastro_get_background_image( array(
		'url' => $args['navbar_full_background_image'],
		'size' => $args['navbar_full_background_size'],
		'position' => $args['navbar_full_background_position'],
		'repeat' => $args['navbar_full_background_repeat']
	) ); ?>
}
<?php endif; ?>
<?php /*=========================*/?>

<?php /*====Full Navbar Menu Color====*/?>
<?php if ( 'default' !== $args['navbar_full_menu_color'] ) : ?>
.gst-fullnav-menu .menu-item a  {
	color: <?php echo esc_attr( gastro_customizer_get_color( $args['navbar_full_menu_color'], $args ) ); ?>;
}
<?php endif; ?>
<?php /*=========================*/?>

<?php /*=Full Navbar Menu Active Color*/?>
<?php if ( 'default' !== $args['navbar_full_menu_active_color'] ) : ?>
.gst-fullnav-menu .current-menu-item > a,
.gst-fullnav-menu .current-menu-parent > a,
.gst-fullnav-menu .current-menu-ancestor > a,
.gst-fullnav-menu .current-menu-parent .gst-mega-menu-title a {
	color: <?php echo esc_attr( gastro_customizer_get_color( $args['navbar_full_menu_active_color'], $args ) ); ?>;
}
<?php endif; ?>
<?php /*=========================*/?>

<?php /*=Full Navbar Menu Hover Color*/?>
<?php if ( 'default' !== $args['navbar_full_menu_hover_color'] ) : ?>
.gst-fullnav-menu .menu-item a:hover {
	color: <?php echo esc_attr( gastro_customizer_get_color( $args['navbar_full_menu_hover_color'], $args ) ); ?>;
}
<?php endif; ?>
<?php /*=========================*/?>

<?php /*=Full Navbar Menu Typography==*/?>
.gst-fullnav-menu .menu-item a {
	<?php echo gastro_s( $fonts[$args['navbar_full_menu_typography']], false ); ?>
}
<?php /*=========================*/?>

<?php /*==Full Navbar Menu Font Size==*/?>
.gst-fullnav-menu .menu-item a {
	font-size: <?php echo esc_attr( (int)$args['navbar_full_menu_font_size'] ); ?>px;
}
<?php /*=========================*/?>

<?php /*==Full Navbar Menu Letter Spacing==*/?>
<?php if ( '0' === $args['navbar_full_menu_letter_spacing'] ) : ?>
.gst-fullnav-menu .menu-item a {
	letter-spacing: 0;
}
<?php elseif ( !empty( $args['navbar_full_menu_letter_spacing'] ) ) : ?>
.gst-fullnav-menu .menu-item a {
	letter-spacing: <?php echo esc_attr( $args['navbar_full_menu_letter_spacing'] ); ?>em;
}
<?php endif; ?>
<?php /*=========================*/?>

<?php /*=Mobile Navbar Menu Typography==*/?>
.gst-navbar--mobile .menu-item a {
	<?php echo gastro_s( $fonts[$args['mobile_navbar_menu_typography']], false ); ?>
}
<?php /*=========================*/?>

<?php /*=Mobile Navbar Menu Uppercase=*/?>
<?php if ( $args['mobile_navbar_menu_uppercase'] ) : ?>
.gst-navbar--mobile .menu-item a {
	text-transform: uppercase;
}
<?php else : ?>
.gst-navbar--mobile .menu-item a {
	text-transform: none;
}
<?php endif; ?>
<?php /*=========================*/?>

<?php /*==Mobile Navbar Menu Font Size==*/?>
.gst-navbar--mobile .menu-item a {
	font-size: <?php echo esc_attr( (int)$args['mobile_navbar_menu_font_size'] ); ?>px;
}
<?php /*=========================*/?>

<?php /*==Mobile Navbar Menu Letter Spacing==*/?>
<?php if ( '0' === $args['mobile_navbar_menu_letter_spacing'] ) : ?>
.gst-navbar--mobile .menu-item a {
	letter-spacing: 0;
}
<?php elseif ( !empty( $args['mobile_navbar_menu_letter_spacing'] ) ) : ?>
.gst-navbar--mobile .menu-item a {
	letter-spacing: <?php echo esc_attr( $args['mobile_navbar_menu_letter_spacing'] ); ?>em;
}
<?php endif; ?>
<?php /*=========================*/?>

<?php /*=Mobile Navbar Background Color=*/?>
<?php if ( 'default' !== $args['mobile_navbar_background_color'] ) : ?>
.gst-collapsed-menu.gst-collapsed-menu--classic {
	background-color: <?php echo gastro_hex_to_rgba( $args['mobile_navbar_background_color'] , $args['mobile_navbar_opacity'] / 100 ); ?>;
}
<?php endif; ?>
<?php /*=========================*/?>

<?php /*=Mobile Navbar Background Image=*/?>
<?php if ( !empty( $args['mobile_navbar_background_image'] ) ) : ?>
.gst-collapsed-menu.gst-collapsed-menu--classic {
	<?php echo gastro_get_background_image( array(
		'url' => $args['mobile_navbar_background_image'],
		'size' => $args['mobile_navbar_background_size'],
		'position' => $args['mobile_navbar_background_position'],
		'repeat' => $args['mobile_navbar_background_repeat']
	) ); ?>
}
<?php endif; ?>
<?php /*=========================*/?>

<?php /*====Mobile Navbar Menu Color====*/?>
<?php if ( 'default' !== $args['mobile_navbar_menu_color'] ) : ?>
.gst-collapsed-menu--classic .menu-item a {
	color: <?php echo esc_attr( gastro_customizer_get_color( $args['mobile_navbar_menu_color'], $args ) ); ?>;
}
<?php endif; ?>
<?php /*=========================*/?>

<?php /*=Mobile Navbar Menu Active Color*/?>
<?php if ( 'default' !== $args['mobile_navbar_menu_active_color'] ) : ?>
.gst-collapsed-menu--classic .current-menu-item > a,
.gst-collapsed-menu--classic .current-menu-parent > a,
.gst-collapsed-menu--classic .current-menu-ancestor > a,
.gst-collapsed-menu--classic .current-menu-parent .gst-mega-menu-title a {
	color: <?php echo esc_attr( gastro_customizer_get_color( $args['mobile_navbar_menu_active_color'], $args ) ); ?>;
}
<?php endif; ?>
<?php /*=========================*/?>

<?php /*=Mobile Navbar Menu Hover Color*/?>
<?php if ( 'default' !== $args['mobile_navbar_menu_hover_color'] ) : ?>
.gst-collapsed-menu--classic .menu-item a:hover {
	color: <?php echo esc_attr( gastro_customizer_get_color( $args['mobile_navbar_menu_hover_color'], $args ) ); ?>;
}
<?php endif; ?>
<?php /*=========================*/?>

<?php /*===Fixed Navbar Height===*/?>
.gst-navbar--alternate {
	height: <?php echo esc_attr( $args['fixed_navbar_height'] ); ?>px;
	line-height: <?php echo esc_attr( $args['fixed_navbar_height'] ); ?>px;
}
<?php /*=========================*/?>

<?php /*===Fixed Navbar Border===*/?>
.gst-navbar--alternate .gst-navbar-inner {
	border-bottom-width: <?php echo esc_attr( $args['fixed_navbar_bottom_border_thickness'] ); ?>px;
}

.gst-navbar--alternate .gst-topnav-menu > li > .gst-mega-menu {
	margin-top: <?php echo esc_attr( $args['fixed_navbar_bottom_border_thickness'] ); ?>px;
}
<?php /*=========================*/?>

<?php /*Fixed Navbar Border Color*/?>
<?php if ( 'default' !== $args['fixed_navbar_bottom_border_color'] ) : ?>
.gst-navbar--top.gst-navbar--fixed.gst-navbar--alternate .gst-navbar-inner {
	border-bottom-color: <?php echo esc_attr( gastro_customizer_get_color( $args['fixed_navbar_bottom_border_color'], $args ) ); ?>;
}
<?php endif; ?>
<?php /*=========================*/?>

<?php /*==Fixed Navbar Background Color==*/?>
<?php if ( 'default' !== $args['fixed_navbar_background_color'] ) : ?>
.gst-navbar--fixed.gst-navbar--alternate .gst-navbar-inner {
	background-color: <?php echo gastro_hex_to_rgba( $args['fixed_navbar_background_color'] , $args['fixed_navbar_opacity'] / 100 ); ?>;
}
<?php endif; ?>
<?php /*=========================*/?>

<?php /*==Fixed Navbar Menu Font Size==*/?>
.gst-navbar--alternate .gst-topnav-menu > .menu-item > a {
	font-size: <?php echo esc_attr( $args['fixed_navbar_menu_font_size'] ); ?>px;
}
<?php /*=========================*/?>

<?php /*=Fixed Navbar Menu Color=*/?>
<?php if ( 'default' !== $args['fixed_navbar_menu_color'] ) : ?>
.gst-navbar--top.gst-navbar--fixed.gst-navbar--alternate .gst-topnav-menu > .menu-item > a,
.gst-navbar--top.gst-navbar--fixed.gst-navbar--alternate .gst-extra-menu a,
.gst-navbar--top.gst-navbar--fixed.gst-navbar--alternate .gst-navbar-social .gst-icon-normal {
	color: <?php echo esc_attr( gastro_customizer_get_color( $args['fixed_navbar_menu_color'], $args ) ); ?>;
}

.gst-navbar--top.gst-navbar--fixed.gst-navbar--alternate .gst-navbar-social .gst-icon-normal.gst-icon-fill,
.gst-navbar--top.gst-navbar--fixed.gst-navbar--alternate .gst-navbar-social .gst-icon-normal.gst-icon-fill-square {
	background-color: <?php echo esc_attr( gastro_customizer_get_color( $args['fixed_navbar_menu_color'], $args ) ); ?>;
	color: <?php echo esc_attr( gastro_contrast_color( $args['fixed_navbar_menu_color'] ) ); ?>;
}

.gst-navbar--top.gst-navbar--fixed.gst-navbar--alternate .gst-lines,
.gst-navbar--top.gst-navbar--fixed.gst-navbar--alternate .gst-lines:before,
.gst-navbar--top.gst-navbar--fixed.gst-navbar--alternate .gst-lines:after {
	background-color: <?php echo esc_attr( gastro_customizer_get_color( $args['fixed_navbar_menu_color'], $args ) ); ?>;
}
<?php endif; ?>
<?php /*=========================*/?>

<?php /*=Fixed Navbar Menu Active Color=*/?>
<?php if ( 'default' !== $args['fixed_navbar_menu_active_color'] ) : ?>
.gst-navbar--top.gst-navbar--fixed.gst-navbar--alternate .gst-topnav-menu > .current-menu-item > a,
.gst-navbar--top.gst-navbar--fixed.gst-navbar--alternate .gst-topnav-menu > .current-menu-parent > a,
.gst-navbar--top.gst-navbar--fixed.gst-navbar--alternate .gst-topnav-menu > .current-menu-ancestor > a {
	color: <?php echo esc_attr( gastro_customizer_get_color( $args['fixed_navbar_menu_active_color'], $args ) ); ?>;
}
<?php else: ?>
.gst-navbar--top.gst-navbar--fixed.gst-navbar--alternate .gst-topnav-menu > .current-menu-item > a,
.gst-navbar--top.gst-navbar--fixed.gst-navbar--alternate .gst-topnav-menu > .current-menu-parent > a,
.gst-navbar--top.gst-navbar--fixed.gst-navbar--alternate .gst-topnav-menu > .current-menu-ancestor > a {
	color: <?php echo esc_attr( $args['primary_brand'] ); ?>;
}
<?php endif; ?>
<?php /*=========================*/?>

<?php /*=Fixed Navbar Menu Hover Color=*/?>
<?php if ( 'default' !== $args['fixed_navbar_menu_hover_color'] ) : ?>
.gst-navbar--top.gst-navbar--fixed.gst-navbar--alternate .gst-topnav-menu > .menu-item > a:hover,
.gst-navbar--top.gst-navbar--fixed.gst-navbar--alternate .gst-extra-menu a:hover,
.gst-navbar--top.gst-navbar--fixed.gst-navbar--alternate .gst-navbar-social .gst-icon-hover {
	color: <?php echo esc_attr( gastro_customizer_get_color( $args['fixed_navbar_menu_hover_color'], $args ) ); ?>;
}

.gst-navbar--top.gst-navbar--fixed.gst-navbar--alternate .gst-navbar-social .gst-icon-hover.gst-icon-fill,
.gst-navbar--top.gst-navbar--fixed.gst-navbar--alternate .gst-navbar-social .gst-icon-hover.gst-icon-fill-square {
	background-color: <?php echo esc_attr( gastro_customizer_get_color( $args['fixed_navbar_menu_hover_color'], $args ) ); ?>;
	color: <?php echo esc_attr( gastro_contrast_color( $args['fixed_navbar_menu_hover_color'] ) ); ?>;
}
<?php else: ?>
.gst-navbar--top.gst-navbar--fixed.gst-navbar--alternate .gst-topnav-menu > .menu-item > a:hover,
.gst-navbar--top.gst-navbar--fixed.gst-navbar--alternate .gst-extra-menu a:hover,
.gst-navbar--top.gst-navbar--fixed.gst-navbar--alternate .gst-navbar-social .gst-icon-hover {
	color: <?php echo esc_attr( $args['primary_brand'] ); ?>;
}

.gst-navbar--top.gst-navbar--fixed.gst-navbar--alternate .gst-navbar-social .gst-icon-hover.gst-icon-fill,
.gst-navbar--top.gst-navbar--fixed.gst-navbar--alternate .gst-navbar-social .gst-icon-hover.gst-icon-fill-square {
	background-color: <?php echo esc_attr( $args['primary_brand'] ); ?>;
	color: <?php echo esc_attr( $args['primary_brand_contrast'] ); ?>;
}
<?php endif; ?>
<?php /*=========================*/?>

<?php /*======Topbar Height======*/?>
<?php
	$mobile_navbar_height = 60;
	$tablet_padding_top = (int)$args['topbar_height'] + $mobile_navbar_height;
	$mobile_topbar_height = (int)$args['topbar_height'] * (int)$args['topbar_column'];
	$mobile_padding_top = $mobile_topbar_height + $mobile_navbar_height;
?>
.gst-topbar {
	height: <?php echo esc_attr( (int)$args['topbar_height'] ); ?>px;
	line-height: <?php echo esc_attr( (int)$args['topbar_height'] ); ?>px;
}

@media only screen and (min-width: 768px) and (max-width: 960px) {
	.gst-layout-responsive .mobile-topbar-enable .gst-content-wrapper,
	.gst-layout-responsive.gst-layout--topnav-stacked .mobile-topbar-enable .gst-content-wrapper,
	.gst-layout-responsive.gst-layout--sidenav .mobile-topbar-enable .gst-content-wrapper,
	.gst-layout-responsive .gst-wrapper--header-transparent.mobile-topbar-enable .gst-content--no-header .gst-content-wrapper,
	.gst-layout-responsive .gst-wrapper--header-transparent.mobile-topbar-enable .gst-content-wrapper > .gst-page-title,
	.gst-layout-responsive.gst-layout--topnav-stacked .gst-wrapper--header-transparent.mobile-topbar-enable .gst-content-wrapper > .gst-page-title,
	.gst-layout-responsive.gst-layout--sidenav .gst-wrapper--header-transparent.mobile-topbar-enable .gst-content-wrapper > .gst-page-title {
		padding-top: <?php echo esc_attr( $tablet_padding_top ); ?>px;
	}

	.gst-layout-responsive .gst-wrapper--header-transparent.mobile-topbar-enable .gst-post-featured--fullwidth .gst-page-title,
	.gst-layout-responsive .gst-wrapper--header-transparent.mobile-topbar-enable .gst-post-featured--overlap .gst-page-title,
	.gst-layout-responsive.gst-layout--topnav-stacked .gst-wrapper--header-transparent.mobile-topbar-enable .gst-post-featured--fullwidth .gst-page-title,
	.gst-layout-responsive.gst-layout--topnav-stacked .gst-wrapper--header-transparent.mobile-topbar-enable .gst-post-featured--overlap .gst-page-title,
	.gst-layout-responsive.gst-layout--sidenav .gst-wrapper--header-transparent.mobile-topbar-enable .gst-post-featured--fullwidth .gst-page-title,
	.gst-layout-responsive.gst-layout--sidenav .gst-wrapper--header-transparent.mobile-topbar-enable .gst-post-featured--overlap .gst-page-title {
		top: <?php echo esc_attr( $tablet_padding_top ); ?>px;
	}
}

@media only screen and (max-width: 767px) {
	.mobile-topbar-enable .gst-topbar {
		height: <?php echo esc_attr( $mobile_topbar_height ); ?>px;
	}

	.gst-layout-responsive .mobile-topbar-enable .gst-content-wrapper,
	.gst-layout-responsive.gst-layout--topnav-stacked .mobile-topbar-enable .gst-content-wrapper,
	.gst-layout-responsive.gst-layout--sidenav .mobile-topbar-enable .gst-content-wrapper,
	.gst-layout-responsive .gst-wrapper--header-transparent.mobile-topbar-enable .gst-content--no-header .gst-content-wrapper,
	.gst-layout-responsive .gst-wrapper--header-transparent.mobile-topbar-enable .gst-content-wrapper > .gst-page-title,
	.gst-layout-responsive.gst-layout--topnav-stacked .gst-wrapper--header-transparent.mobile-topbar-enable .gst-content-wrapper > .gst-page-title,
	.gst-layout-responsive.gst-layout--sidenav .gst-wrapper--header-transparent.mobile-topbar-enable .gst-content-wrapper > .gst-page-title {
		padding-top: <?php echo esc_attr( $mobile_padding_top ); ?>px;
	}

	.gst-layout-responsive .gst-wrapper--header-transparent.mobile-topbar-enable .gst-post-featured--fullwidth .gst-page-title,
	.gst-layout-responsive .gst-wrapper--header-transparent.mobile-topbar-enable .gst-post-featured--overlap .gst-page-title,
	.gst-layout-responsive.gst-layout--topnav-stacked .gst-wrapper--header-transparent.mobile-topbar-enable .gst-post-featured--fullwidth .gst-page-title,
	.gst-layout-responsive.gst-layout--topnav-stacked .gst-wrapper--header-transparent.mobile-topbar-enable .gst-post-featured--overlap .gst-page-title,
	.gst-layout-responsive.gst-layout--sidenav .gst-wrapper--header-transparent.mobile-topbar-enable .gst-post-featured--fullwidth .gst-page-title,
	.gst-layout-responsive.gst-layout--sidenav .gst-wrapper--header-transparent.mobile-topbar-enable .gst-post-featured--overlap .gst-page-title {
		top: <?php echo esc_attr( $mobile_padding_top ); ?>px;
	}
}
<?php /*=========================*/?>

<?php /*=====Topbar Separator====*/?>
<?php if ( $args['topbar_separator'] ) : ?>
.gst-topbar .gst-topbar-column {
	border-right-width: 1px;
	border-right-style: solid;
}
<?php endif; ?>
<?php /*=========================*/?>

<?php /*======Topbar Border======*/?>
.gst-topbar {
	border-bottom-width: <?php echo esc_attr( $args['topbar_bottom_border_thickness'] ); ?>px;
}
<?php /*=========================*/?>

<?php /*===Topbar Border Color===*/?>
<?php if ( 'default' !== $args['topbar_bottom_border_color'] ) : ?>
.gst-topbar {
	border-bottom-color: <?php echo esc_attr( gastro_customizer_get_color( $args['topbar_bottom_border_color'], $args ) ); ?>;
}
<?php endif; ?>
<?php /*=========================*/?>

<?php /*=Topbar Background Color=*/?>
<?php if ( 'default' !== $args['topbar_background_color'] ) : ?>
.gst-topbar {
	background-color: <?php echo gastro_hex_to_rgba( $args['topbar_background_color'], $args['topbar_background_opacity'] / 100 ); ?>;
}
<?php endif; ?>
<?php /*=========================*/?>

<?php /*====Topbar Text Color====*/?>
<?php if ( 'default' !== $args['topbar_text_color'] ) : ?>
.gst-topbar {
	color: <?php echo esc_attr( gastro_customizer_get_color( $args['topbar_text_color'], $args ) ); ?>;
}
<?php endif; ?>
<?php /*=========================*/?>

<?php /*====Topbar Link Color====*/?>
<?php if ( 'default' !== $args['topbar_link_color'] ) : ?>
.gst-topbar .gst-widgets a {
	color: <?php echo esc_attr( gastro_customizer_get_color( $args['topbar_link_color'], $args ) ); ?>;
}
<?php endif; ?>
<?php /*=========================*/?>

<?php /*=Topbar Link Hover Color=*/?>
<?php if ( 'default' !== $args['topbar_link_hover_color'] ) : ?>
.gst-topbar .gst-widgets a:hover {
	color: <?php echo esc_attr( gastro_customizer_get_color( $args['topbar_link_hover_color'], $args ) ); ?>;
}
<?php endif; ?>
<?php /*=========================*/?>

<?php /*==Action Button on Fixed Nav==*/?>
<?php if ( !$args['fixed_nav_action_button'] ) : ?>
.gst-navbar--fixed .gst-navbar-widget {
	display: none;
}
<?php endif; ?>
<?php /*=========================*/?>

<?php /*Header Widgets Max Height*/?>
<?php if ( !empty( $args['header_widget_max_height'] ) && is_numeric( $args['header_widget_max_height'] ) ) : ?>
.gst-header-widgets {
	height: <?php echo esc_attr( $args['header_widget_max_height'] ); ?>px;
}
<?php endif; ?>
<?php /*=========================*/?>

<?php /*Header Widgets Separator=*/?>
<?php if ( $args['header_widget_separator'] ) : ?>
.gst-header-widgets .gst-header-widgets-column {
	border-right-width: 1px;
	border-right-style: solid;
}
<?php endif; ?>
<?php /*=========================*/?>

<?php /*==Action Button Border===*/?>
.gst-navbar-widget.gst-button > .btnx {
	border-radius: <?php echo esc_attr( $args['action_button_radius'] ); ?>px;
	border-width: <?php echo esc_attr( $args['action_button_border'] ); ?>px;
}
<?php /*=========================*/?>

<?php /*=Header Widgets Background Color=*/?>
<?php if ( 'default' !== $args['header_widget_background_color'] ) : ?>
.gst-header-widgets {
	background-color: <?php echo gastro_hex_to_rgba( $args['header_widget_background_color'], $args['header_widget_background_opacity'] / 100 ); ?>;
}
<?php endif; ?>
<?php /*=========================*/?>

<?php /*=Header Widgets Background Image=*/?>
<?php if ( !empty( $args['header_widget_background_image'] ) ) : ?>
.gst-header-widgets {
	<?php echo gastro_get_background_image( array(
		'url' => $args['header_widget_background_image'],
		'size' => $args['header_widget_background_size'],
		'position' => $args['header_widget_background_position'],
		'repeat' => $args['header_widget_background_repeat']
	) ); ?>
}
<?php endif; ?>
<?php /*=========================*/?>

<?php /*=Header Widgets Text Color=*/?>
<?php if ( 'default' !== $args['header_widget_text_color'] ) : ?>
.gst-header-widgets {
	color: <?php echo esc_attr( gastro_customizer_get_color( $args['header_widget_text_color'], $args ) ); ?>;
}
<?php endif; ?>
<?php /*=========================*/?>

<?php /*=Header Widgets Link Color=*/?>
<?php if ( 'default' !== $args['header_widget_link_color'] ) : ?>
.gst-header-widgets .gst-widgets a {
	color: <?php echo esc_attr( gastro_customizer_get_color( $args['header_widget_link_color'], $args ) ); ?>;
}
<?php endif; ?>
<?php /*=========================*/?>

<?php /*=Header Widgets Link Hover Color=*/?>
<?php if ( 'default' !== $args['header_widget_link_hover_color'] ) : ?>
.gst-header-widgets .gst-widgets a:hover {
	color: <?php echo esc_attr( gastro_customizer_get_color( $args['header_widget_link_hover_color'], $args ) ); ?>;
}
<?php endif; ?>
<?php /*=========================*/?>

<?php /*==Breadcrumb Text Color==*/?>
<?php if ( 'default' !== $args['breadcrumb_text_color'] ) : ?>
.gst-page-title .gst-page-title-breadcrumb {
	color: <?php echo esc_attr( gastro_customizer_get_color( $args['breadcrumb_text_color'], $args ) ); ?>;
}
<?php endif; ?>
<?php /*=========================*/?>

<?php /*==Breadcrumb Background Color==*/?>
<?php if ( 'default' !== $args['breadcrumb_background_color'] ) : ?>
.gst-page-title > .gst-page-title-breadcrumb {
	background-color: <?php echo gastro_hex_to_rgba( $args['breadcrumb_background_color'], $args['breadcrumb_background_opacity'] / 100 ); ?>;
}
<?php endif; ?>
<?php /*=========================*/?>

<?php /*==Page Title Text Color==*/?>
<?php if ( 'default' !== $args['page_title_text_color'] ) : ?>
.gst-page-title .gst-page-title-content {
	color: <?php echo esc_attr( gastro_customizer_get_color( $args['page_title_text_color'], $args ) ); ?>;
}
<?php endif; ?>
<?php /*=========================*/?>

<?php /*==Page Title Background Color==*/?>
<?php if ( 'default' !== $args['page_title_background_color'] ) : ?>
.gst-page-title .gst-background-overlay {
	background-color: <?php echo gastro_hex_to_rgba( $args['page_title_background_color'], $args['page_title_background_opacity'] / 100 ); ?>;
}
<?php endif; ?>
<?php /*=========================*/?>

<?php /*==Page Title Background Image==*/?>
<?php if ( !empty( $args['page_title_background_image'] ) ) : ?>
.gst-page-title--default .gst-background-inner {
	<?php echo gastro_get_background_image( array(
		'url' => $args['page_title_background_image'],
		'size' => $args['page_title_background_size'],
		'position' => $args['page_title_background_position'],
		'repeat' => $args['page_title_background_repeat']
	) ); ?>
}
<?php endif; ?>
<?php /*=========================*/?>
