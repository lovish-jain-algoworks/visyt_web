<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product, $woocommerce_loop;

// Ensure visibility
if ( ! $product || ! $product->is_visible() ) {
	return;
}

$columns = ( isset( $woocommerce_loop['columns'] ) && !empty( $woocommerce_loop['columns'] ) && is_numeric( $woocommerce_loop['columns'] ) ) ? (int)$woocommerce_loop['columns'] : gastro_mod( 'shop_column' );
$entry_components = gastro_mod( 'shop_component' );
$entry_components = !empty( $entry_components ) ? $entry_components : array();
$entry_args = array(
	'style' => esc_attr( 'grid' ),
	'enable_link' => true,
	'no_of_columns' => $columns,
	'image_on' => in_array( 'image', $entry_components ),
	'subtitle_on' => in_array( 'subtitle', $entry_components ),
	'badge_on' => in_array( 'badge', $entry_components ),
	'image_size' => gastro_mod( 'shop_image_size' ),
	'image_ratio' => gastro_mod( 'shop_image_ratio' ),
	'title_size' => gastro_mod( 'shop_title_font_size' ),
	'title_uppercase' => gastro_mod( 'shop_title_uppercase' ),
	'title_letter_spacing' => gastro_mod( 'shop_title_letter_spacing' ) . 'em'
);

gastro_template( 'entry-product', $entry_args );
?>
