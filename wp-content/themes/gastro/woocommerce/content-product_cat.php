<?php
/**
 * The template for displaying product category thumbnails within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product_cat.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woothemes.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.6.1
 */


if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

global $woocommerce_loop;

$columns = ( isset( $woocommerce_loop['columns'] ) && !empty( $woocommerce_loop['columns'] ) && is_numeric( $woocommerce_loop['columns'] ) ) ? (int)$woocommerce_loop['columns'] : gastro_mod( 'shop_column' );
$entry_args = array(
	'style' => apply_filters( 'gst_productcat_style', 'overlay' ),
	'category_obj' => $category,
	'no_of_columns' => $columns,
	'image_size' => gastro_mod( 'shop_image_size' ),
	'image_ratio' => gastro_mod( 'shop_image_ratio' ),
	'hover' => apply_filters( 'gst_productcat_hover', 'none' ),
	'title_size' => gastro_mod( 'shop_title_font_size' ),
	'title_uppercase' => gastro_mod( 'shop_title_uppercase' ),
	'title_letter_spacing' => gastro_mod( 'shop_title_letter_spacing' ) . 'em'
);

gastro_template( 'entry-productcat', $entry_args );
?>
