<?php
/**
 * Single Product Meta
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/meta.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

global $product;
?>
<div class="product_meta gst-center-align gst-p-border-border">
	<div class="gst-container">
		<?php do_action( 'woocommerce_product_meta_start' ); ?>

		<?php if ( wc_product_sku_enabled() && ( $product->get_sku() || $product->is_type( 'variable' ) ) ) : ?>

			<div class="sku_wrapper">
				<div class="gst-product-meta-label"><?php esc_html_e( 'SKU :', 'gastro' ); ?></div>
				<span class="sku gst-s-text-color" itemprop="sku">
					<?php echo ( $sku = $product->get_sku() ) ? $sku : esc_html__( 'N/A', 'gastro' ); ?>
				</span>
			</div>

		<?php endif; ?>

		<?php echo wc_get_product_category_list( $product->get_id(), '', '<div class="posted_in gst-s-text-color"><div class="gst-product-meta-label gst-p-text-color">' . _n( 'Category:', 'Categories:', count( $product->get_category_ids() ), 'gastro' ) . '</div> ', '</div>' ); ?>

		<?php do_action( 'woocommerce_product_meta_end' ); ?>
	</div><?php //close container ?>
</div>
