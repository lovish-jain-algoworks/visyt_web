<?php
/**
 * Single Product Image
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/product-image.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.5.1
 */

defined( 'ABSPATH' ) || exit;

// Note: `wc_get_gallery_image_html` was added in WC 3.3.2 and did not exist prior. This check protects against theme overrides being used on older versions of WC.
if ( ! function_exists( 'wc_get_gallery_image_html' ) ) {
	return;
}

global $product;

$tn_position = gastro_mod( 'product_thumbnail_position' );
$column = gastro_mod( 'product_thumbnail_item' );
$column_class = '';
if ( 'left' !== $tn_position && 'right' !== $tn_position ) {
	$column_class .= ( 5 == $column ) ? ' gst-col-1-5' : ' gst-col-' . 12 / $column;
}

$image_action = gastro_mod( 'product_image_action' );
if ( 'both' === $image_action ) {
	$action = 'zoom';
} else {
	$action = $image_action;
}

$thumbnail_ids = $product->get_gallery_image_ids();
$thumbnail_ids = !empty( $thumbnail_ids ) ? $thumbnail_ids : array( get_post_thumbnail_id() );
$image_number = count( $thumbnail_ids );
$content_class = ( $image_number > 1 ) ? 'gst-gallery-content with-thumbnail' : 'gst-gallery-content';
$thumbnail_set_ratio = gastro_mod( 'product_thumbnail_ratio' );
?>

<div class="images gst-product-gallery gst-product-gallery--<?php echo esc_attr( $action ); ?> gst-product-gallery--<?php echo esc_attr( $tn_position ); ?> gst-gallery--carousel" data-style="carousel" data-action="<?php echo esc_attr( $image_action ); ?>">
	<div class="<?php echo esc_attr( $content_class ); ?>" data-adaptive_height="true">
		<?php if ( $image_number > 0 ): ?>
			<?php foreach ( $thumbnail_ids as $index => $thumbnail_id ) : ?>
				<div class="gst-gallery-item" data-id="<?php echo esc_attr( $thumbnail_id ); ?>">
					<div class="gst-gallery-body">
						<?php if ( 'zoom' === $image_action ) : ?>
							<div class="gst-gallery-media">
								<?php
									$image = wp_get_attachment_image( $thumbnail_id, apply_filters( 'woocommerce_product_thumbnails_large_size', 'shop_single' ) );
									echo gastro_escape_content( $image );
									echo gastro_escape_content( $image );
								?>
							</div>
						<?php elseif ( 'both' === $image_action ) : ?>
							<?php $full_image_src = wp_get_attachment_image_src( $thumbnail_id, 'full' ); ?>
							<a class="gst-gallery-media" data-index="<?php echo esc_attr( $index ); ?>" href="<?php echo esc_url( $full_image_src[0] ); ?>">
								<?php
									$image = wp_get_attachment_image( $thumbnail_id, apply_filters( 'single_product_large_thumbnail_size', 'shop_single' ) );
									echo gastro_escape_content( $image );
									echo gastro_escape_content( $image );
								?>
							</a>
						<?php else : ?>
							<?php $full_image_src = wp_get_attachment_image_src( $thumbnail_id, 'full' ); ?>
							<a class="gst-gallery-media" data-index="<?php echo esc_attr( $index ); ?>" href="<?php echo esc_url( $full_image_src[0] ); ?>">
								<?php echo wp_get_attachment_image( $thumbnail_id, apply_filters( 'single_product_large_thumbnail_size', 'shop_single' ) ); ?>
							</a>
						<?php endif; ?>
					</div>
				</div>
			<?php endforeach; ?>
		<?php else: ?>
			<?php echo apply_filters( 'woocommerce_single_product_image_html', sprintf( '<img src="%s" alt="%s" />', wc_placeholder_img_src(), esc_html__( 'Placeholder', 'gastro' ) ), $product->get_id() ); ?>
		<?php endif; ?>

	</div>
	<?php if ( $image_number > 1 ) : ?>
		<div class="thumbnails columns-<?php echo esc_attr( $column ); ?> gst-gallery-thumbnail gst-gallery-thumbnail--<?php echo esc_attr( $tn_position ); ?>" data-thumbnail="<?php echo esc_attr( $column ); ?>">
			<?php foreach ( $thumbnail_ids as $thumbnail_id ) : ?>
				<div class="gst-gallery-thumbnail-item<?php echo esc_attr( $column_class ); ?>">
					<?php
						$thumbnail_image = wp_get_attachment_image_src( $thumbnail_id, apply_filters( 'single_product_small_thumbnail_size', 'shop_single' ) );
						$thumbnail_style_attr = '';
						if ( 'auto' !== $thumbnail_set_ratio && $thumbnail_image && 0 != $thumbnail_image[1] ) {
							$thumbnail_image_ratio = ($thumbnail_image[2]/$thumbnail_image[1])*100;
							if ( $thumbnail_image_ratio < (int)$thumbnail_set_ratio ) {
								$thumbnail_style_attr .= 'height:100%;';
								$thumbnail_style_attr .= 'width:auto;';
							} else {
								$thumbnail_style_attr .= 'width:100%;';
								$thumbnail_style_attr .= 'height:auto;';
							}
							?><div class="gst-gallery-thumbnail-item-inner" style="padding-bottom:<?php echo esc_attr( $thumbnail_set_ratio ); ?>%;">
								<?php echo wp_get_attachment_image( $thumbnail_id, apply_filters( 'single_product_small_thumbnail_size', 'shop_single' ), false, array( 'style' => $thumbnail_style_attr ) ); ?>
							</div><?php
						} else {
							echo wp_get_attachment_image( $thumbnail_id, apply_filters( 'single_product_small_thumbnail_size', 'shop_single' ) );
						}
					?>
				</div>
			<?php endforeach;?>
		</div>
	<?php endif; ?>
</div>
